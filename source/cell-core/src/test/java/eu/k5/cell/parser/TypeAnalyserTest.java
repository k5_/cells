package eu.k5.cell.parser;

import org.junit.Assert;
import org.junit.Test;

import eu.k5.cell.annotation.In;

public class TypeAnalyserTest {

	public static class Empty {

	}

	@Test
	public void empty() throws NoSuchMethodException, SecurityException {
		TypeAnalyser<Empty> analyser = new TypeAnalyser<>(Empty.class).init();

		Assert.assertEquals(Empty.class.getConstructor(), analyser.getConstructor());
		Assert.assertTrue(analyser.getConstructorParameters().isEmpty());

	}

	public static class WithConstructor {

		final String param;

		public WithConstructor(@In("param") String param) {
			this.param = param;
		}
	}

	@Test
	public void test() throws NoSuchMethodException, SecurityException {
		TypeAnalyser<WithConstructor> analyser = new TypeAnalyser<>(WithConstructor.class).init();

		Assert.assertEquals(WithConstructor.class.getConstructor(String.class), analyser.getConstructor());
		Assert.assertEquals(1, analyser.getConstructorParameters().size());
		Input input = analyser.getConstructorParameters().get(0);
		Assert.assertEquals(new NamedReference(String.class, "param"), input.getSource());

	}
}
