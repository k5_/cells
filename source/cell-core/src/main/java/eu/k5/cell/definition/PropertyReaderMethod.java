package eu.k5.cell.definition;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import eu.k5.cell.extension.Binding;

public class PropertyReaderMethod implements PropertyReader {
	private final Binding binding;
	private final Method method;

	public PropertyReaderMethod(Binding binding, Method method) {
		this.binding = binding;
		this.method = method;
	}

	@Override
	public Object read(Object instance) {
		try {
			return method.invoke(instance);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public Binding getBinding() {
		return binding;
	}

	@Override
	public boolean valid(Object object) {
		// Methods can be inherited/overriden so use instanceof instead of
		// equals
		return method.getDeclaringClass().isInstance(object);
	}
}
