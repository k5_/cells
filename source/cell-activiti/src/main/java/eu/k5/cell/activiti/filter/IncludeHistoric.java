package eu.k5.cell.activiti.filter;

import eu.k5.cell.activiti.ProcessState;

public class IncludeHistoric implements AspectFilter {

	@Override
	public AspectQuery filter(AspectQuery query) {
		return query.includeHistoric(true);
	}

	@Override
	public boolean accept(ProcessState state) {
		return true;
	}

}
