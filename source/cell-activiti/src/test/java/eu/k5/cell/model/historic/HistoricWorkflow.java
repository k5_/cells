package eu.k5.cell.model.historic;

import eu.k5.cell.annotation.Aspect;
import eu.k5.cell.annotation.Bind;
import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Task;
import eu.k5.cell.annotation.workflow.Workflow;

@Workflow(processKey = "historicProcess",
		activity = HistoricWorkflow.Activity.class,
		binder = HistoricWorkflow.Binder.class)
public interface HistoricWorkflow {

	interface Binder {

		@Bind(businessKey = true)
		String getKey();

		String getProperty();

		String getProperty2();

		String getProperty3();

	}

	interface Activity {
		@Bind
		@Start
		HistoricStart getStart();

		@Task(definition = "Task")
		HistoricTask getData();

		@Bind
		@Aspect
		HistoricAspect getAspect();

	}

}
