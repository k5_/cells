package prototypes;

import eu.k5.cell.generator.Struct;

public final class StructParser implements eu.k5.cell.parser.Parser<eu.k5.cell.generator.Struct> {
	static final class Board {
		String _0;
	}

	@Override
	public Struct parse(eu.k5.cell.parser.Context context) {
		Board board = new Board();
		board._0 = (String) context.getVariable("param");
		return newInstance(board);
	}
	private static Struct newInstance(Board board){
		Struct instance = new Struct(board._0);
		
		return instance;
	}
}
