package eu.k5.cell.definition;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PropertyAssignerMethod implements PropertyAssigner {

	private final Method method;

	public PropertyAssignerMethod(Method method) {

		this.method = method;
	}

	@Override
	public void assign(Object instance, Object value) {
		try {
			method.invoke(instance, value);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public boolean isAnnotationPresent(Class<? extends Annotation> annotation) {
		return method.isAnnotationPresent(annotation);
	}

	@Override
	public <A extends Annotation> A getAnnotation(Class<A> annotation) {
		return method.getAnnotation(annotation);
	}

	@Override
	public Class<?> getType() {
		return method.getReturnType();
	}
}
