package eu.k5.cell.model.start;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.activiti.engine.runtime.ProcessInstance;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import eu.k5.cell.activiti.CellManager;
import eu.k5.cell.activiti.TestEngine;
import eu.k5.cell.annotation.CellContext;
import eu.k5.cell.extension.CellExtension;
import eu.k5.cell.model.TestProducer;

@RunWith(CdiRunner.class)
@AdditionalClasses({ TestProducer.class, CellExtension.class, StartWorkflow.class })
public class StartTest {

	private static TestEngine refEngine = new TestEngine("test1", "start");

	@CellContext(StartWorkflow.class)
	@Inject
	CellManager cell;

	@Produces
	TestEngine engine = refEngine;

	@AfterClass
	public static void destoy() {
		refEngine.destroy();
	}

	@Test
	public void start() {
		StartStart start = new StartStart("key", "property");
		cell.start(start);

		ProcessInstance instance = engine.find("key");
		Assert.assertNotNull(instance);
		Assert.assertEquals("property", instance.getProcessVariables().get("property"));

	}
}
