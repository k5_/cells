package eu.k5.cell.strategem;

import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Workflow;

@Workflow(processKey = "Strategem",
binder = StrategemWorkflow.Binder.class, 
activity = StrategemWorkflow.Activity.class

)
public interface StrategemWorkflow {

	public interface Binder {

	}
	interface Activity {
		@Start
		StrategemStart getStart();
	}
}
