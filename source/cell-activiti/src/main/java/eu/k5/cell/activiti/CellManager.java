package eu.k5.cell.activiti;

import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.k5.cell.activiti.filter.AspectFilter;
import eu.k5.cell.activiti.filter.ProcessFilter;
import eu.k5.cell.activiti.filter.TaskFilter;

public interface CellManager {

	void start(Object start);

	void complete(Object task);

	<T> T findTask(Class<T> task, String query, Map<String, Object> parameters);

	<T> T findTask(Class<T> task, TaskFilter... filter);

	<T> T findTask(Class<T> task, List<TaskFilter> filter);

	<T> List<T> listTask(Class<T> task, TaskFilter... filter);

	<T> List<T> listTask(Class<T> task, List<TaskFilter> filter);

	<A> A findAspect(Class<A> aspect, AspectFilter... filter);

	<A> A findAspect(Class<A> aspect, List<AspectFilter> filter);

	<A> List<A> listAspect(Class<A> aspect, AspectFilter... filter);

	<A> List<A> listAspect(Class<A> aspect, List<AspectFilter> filter);

	<T> T readData(Class<T> data, ProcessFilter... filter);

	<T> T readData(Class<T> data, List<ProcessFilter> filter);

	<T> List<T> listData(Class<T> data, ProcessFilter... filter);

	<T> List<T> listData(Class<T> data, List<ProcessFilter> filter);

	boolean instanceExists(String businessKey, boolean searchHistoric);

	Set<String> listBusinessKeys(boolean includeHistoric);

	<U> U unwrap(Class<U> type);

}
