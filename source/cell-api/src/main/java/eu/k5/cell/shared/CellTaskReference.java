package eu.k5.cell.shared;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class CellTaskReference {
	private final String processInstance;
	private final String taskId;
	private final String taskDefinition;
	private final String token;

	public CellTaskReference(@MapsTo("processInstance") String processInstance, @MapsTo("taskId") String taskId,
			@MapsTo("taskDefinition") String taskDefinition, @MapsTo("token") String token) {
		this.processInstance = processInstance;
		this.taskId = taskId;
		this.taskDefinition = taskDefinition;
		this.token = token;

	}

	public String getProcessInstance() {
		return processInstance;
	}

	public String getTaskId() {
		return taskId;
	}

	public String getTaskDefinition() {
		return taskDefinition;
	}

	public String getToken() {
		return token;
	}

	@Override
	public String toString() {
		return "CellTaskReference [processInstance=" + processInstance + ", taskId=" + taskId + ", taskDefinition="
				+ taskDefinition + ", token=" + token + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (processInstance == null ? 0 : processInstance.hashCode());
		result = prime * result + (taskDefinition == null ? 0 : taskDefinition.hashCode());
		result = prime * result + (taskId == null ? 0 : taskId.hashCode());
		result = prime * result + (token == null ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CellTaskReference other = (CellTaskReference) obj;
		if (processInstance == null) {
			if (other.processInstance != null) {
				return false;
			}
		} else if (!processInstance.equals(other.processInstance)) {
			return false;
		}
		if (taskDefinition == null) {
			if (other.taskDefinition != null) {
				return false;
			}
		} else if (!taskDefinition.equals(other.taskDefinition)) {
			return false;
		}
		if (taskId == null) {
			if (other.taskId != null) {
				return false;
			}
		} else if (!taskId.equals(other.taskId)) {
			return false;
		}
		if (token == null) {
			if (other.token != null) {
				return false;
			}
		} else if (!token.equals(other.token)) {
			return false;
		}
		return true;
	}

}
