package eu.k5.cell;

public class NonUniqueResultException extends CellException {
	private static final long serialVersionUID = 1L;

	public NonUniqueResultException(String message) {
		super(message);
	}

}
