package eu.k5.cell.extension;

import eu.k5.cell.definition.PropertyReader;
import eu.k5.cell.definition.StartDefinition;

public class StartBuilder extends Builder<StartDefinition, StartBuilder> {
	private final WriterBuilder writer = new WriterBuilder();
	private PropertyReader businessKeyReader;

	public StartBuilder() {
		super(StartDefinition.class);
		setBuilder(this);
	}

	@Override
	protected void doInit() {
		writer.allowFinals(true).withBinder(getBinder()).forEntity(getType()).withValidator(getValidator())
				.useAll(true).init();
		plusViolations(writer.getViolations());
		initBusinessKey();
	}

	private void initBusinessKey() {
		businessKeyReader = writer.getBusinessKeyReader();
	}

	@Override
	public StartDefinition doBuild() {
		return new StartDefinition(getWorkflow(), getType(), writer.build(), businessKeyReader);
	}
}
