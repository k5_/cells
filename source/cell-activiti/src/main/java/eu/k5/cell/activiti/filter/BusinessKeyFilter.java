package eu.k5.cell.activiti.filter;

import org.activiti.engine.runtime.ProcessInstanceQuery;
import org.activiti.engine.task.TaskQuery;

import eu.k5.cell.activiti.ProcessState;

public class BusinessKeyFilter implements TaskFilter, AspectFilter, ProcessFilter {

	private final String businessKey;

	public BusinessKeyFilter(String businessKey) {
		this.businessKey = businessKey;
	}

	@Override
	public TaskQuery filter(TaskQuery query) {
		return query.processInstanceBusinessKey(businessKey);
	}

	@Override
	public AspectQuery filter(AspectQuery query) {
		return query.processInstanceBusinessKey(businessKey);
	}

	@Override
	public ProcessInstanceQuery filter(ProcessInstanceQuery query) {
		return query.processInstanceBusinessKey(businessKey);
	}

	@Override
	public boolean accept(ProcessState state) {
		return true;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (businessKey == null ? 0 : businessKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BusinessKeyFilter other = (BusinessKeyFilter) obj;
		if (businessKey == null) {
			if (other.businessKey != null) {
				return false;
			}
		} else if (!businessKey.equals(other.businessKey)) {
			return false;
		}
		return true;
	}

}
