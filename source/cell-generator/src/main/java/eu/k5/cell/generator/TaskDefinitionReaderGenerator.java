package eu.k5.cell.generator;

import static org.jboss.jdeparser.JMod.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.jboss.jdeparser.FormatPreferences;
import org.jboss.jdeparser.JClassDef;
import org.jboss.jdeparser.JDeparser;
import org.jboss.jdeparser.JFiler;
import org.jboss.jdeparser.JMethodDef;
import org.jboss.jdeparser.JSourceFile;
import org.jboss.jdeparser.JSources;

public class TaskDefinitionReaderGenerator {

	public void generator() {

		JSources sources = JDeparser.createSources(getFiler(), new FormatPreferences(new Properties()));

		JSourceFile source = sources.createSourceFile("eu.k5.cell", "TestDefinition");

		JClassDef reader = source._class(PUBLIC | FINAL, "TestDefinition");

		reader._implements(TaskReader.class);

		JMethodDef method = reader.method(PUBLIC, Object.class, "read");

		method.body()._new(Type.class);

	}

	private final JFiler filer = new JFiler() {
		@Override
		public OutputStream openStream(final String packageName, final String fileName) throws IOException {
			final Key key = new Key(packageName, fileName + ".java");
			if (!sourceFiles.containsKey(key)) {
				final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				if (sourceFiles.putIfAbsent(key, stream) == null) {
					return stream;
				}
			}
			throw new IOException("Already exists");
		}
	};

	public JFiler getFiler() {
		return filer;
	}

	private final ConcurrentMap<Key, ByteArrayOutputStream> sourceFiles = new ConcurrentHashMap<>();

	static final class Key {
		private final String packageName;
		private final String fileName;

		Key(final String packageName, final String fileName) {
			this.packageName = packageName;
			this.fileName = fileName;
		}

		@Override
		public boolean equals(final Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			final Key key = (Key) o;
			return fileName.equals(key.fileName) && packageName.equals(key.packageName);
		}

		@Override
		public int hashCode() {
			int result = packageName.hashCode();
			result = 31 * result + fileName.hashCode();
			return result;
		}
	}
}
