package eu.k5.cell.activiti.delegate;

import org.activiti.engine.ManagementService;

import eu.k5.cell.WorkflowManager;
import eu.k5.cell.definition.AspectDefinition;
import eu.k5.cell.wao.DelegateDefinition;

public class AspectDelegateDefinition implements DelegateDefinition {
	private final ListTaskDefinition tasks;
	private final ListInstancesDefinition instances;
	private final AspectDefinition definition;
	private final boolean isList;

	public AspectDelegateDefinition(ListTaskDefinition tasks, ListInstancesDefinition instances,
			AspectDefinition definition, boolean isList) {
		this.tasks = tasks;
		this.instances = instances;
		this.definition = definition;
		this.isList = isList;
	}

	@Override
	public AspectDelegate bind(WorkflowManager cellManager) {
		ManagementService managementService = cellManager.unwrap(ManagementService.class);

		ListTasksDelegate tasksDelegate = tasks.bind(managementService);

		ListInstancesDelegate instancesDelegate = instances.bind(managementService);

		return new AspectDelegate(instancesDelegate, tasksDelegate, definition, isList);
	}

}
