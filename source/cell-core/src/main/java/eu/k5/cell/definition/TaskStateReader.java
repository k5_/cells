package eu.k5.cell.definition;

import eu.k5.cell.CellTask;
import eu.k5.cell.annotation.states.TaskStateResolver;
import eu.k5.cell.annotation.states.TaskStateType;

public class TaskStateReader {

	private final TaskStateType type;
	private final TaskStateResolver reader;
	private final String name;

	public TaskStateReader(String name, TaskStateType type, TaskStateResolver reader) {
		this.name = name;
		this.type = type;
		this.reader = reader;
	}

	public Object read(CellTask task) {
		return reader.read(task);
	}

	public TaskStateType getType() {
		return type;
	}

	public TaskStateResolver getReader() {
		return reader;
	}

	public String getName() {
		return name;
	}

}
