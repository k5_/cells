package eu.k5.cell.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import eu.k5.cell.CellProcessState;
import eu.k5.cell.CellTask;
import eu.k5.cell.definition.ProcessStateReader;
import eu.k5.cell.extension.StateDefinition;

public class AspectBoard {

	private int size;
	private Data[] datas;

	private Inst[] dataInst;

	private List<StateDefinition<ProcessStateReader>> processStates;

	private TaskSelector[] tasks;

	private Instantiator aspect;

	public Object createBoard(CellProcessState state) {
		Object[] board = new Object[size];
		for (Data data : datas) {
			board[data.getIndex()] = data.getSource().read(null);
		}
		for (Inst d : dataInst) {
			board[d.getIndex()] = d.getInstantiator().instantiate(board);
		}
		for (TaskSelector task : tasks) {
			board[task.getIndex()] = task.create(state.getTasks(), board);
		}

		for (StateDefinition<ProcessStateReader> states : processStates) {

			Object read = states.getReader().read(state);

		}

		return aspect.instantiate(board);
	}

	public static class TaskKey {
		private final int index;

		public TaskKey(int index) {
			this.index = index;
		}

	}

	public static class TaskSelector {
		private int index;
		private int taskKeyIndex;
		private boolean isList;

		private Map<String, Instantiator<?>> inst;

		public Object create(Collection<CellTask> tasks, Object[] board) {
			List<Object> read = new ArrayList<>();
			for (CellTask task : tasks) {
				Instantiator<?> i = inst.get(task.getTaskDefinitionKey());
				if (i != null) {
					board[taskKeyIndex] = null;
					read.add(i.instantiate(board));
				}
			}
			if (isList) {
				return read;
			} else if (read.size() == 1) {
				return read.get(0);
			} else if (read.size() == 0) {
				return null;
			} else {
				throw new IllegalStateException("Multiple results");
			}

		}

		public int getIndex() {
			return index;
		}
	}

	public static class Inst {
		private int index;
		private Instantiator instantiator;

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		public Instantiator getInstantiator() {
			return instantiator;
		}

		public void setInstantiator(Instantiator instantiator) {
			this.instantiator = instantiator;
		}

	}
}
