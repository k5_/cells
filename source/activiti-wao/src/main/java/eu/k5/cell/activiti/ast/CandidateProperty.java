package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

public class CandidateProperty extends Property {

	@Override
	public void accept(ValueVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public TypeHint getHint(Context context) {
		return TypeHint.STRING;
	}

}
