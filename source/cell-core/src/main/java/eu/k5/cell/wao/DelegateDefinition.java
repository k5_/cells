package eu.k5.cell.wao;

import eu.k5.cell.WorkflowManager;

public interface DelegateDefinition {
	Delegate bind(WorkflowManager cellManager);
}
