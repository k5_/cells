package eu.k5.cell.parser;

import java.lang.reflect.Type;

public class NamedReference implements SourceReference {

	private final String name;
	private final Type type;

	public NamedReference(Type type, String name) {
		this.type = type;
		this.name = name;
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public Source getSource() {
		return new VariableSource(type, name);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (name == null ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NamedReference other = (NamedReference) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

}
