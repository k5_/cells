package eu.k5.cell.extension;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class ReaderBuilderTest {
	ReaderBuilder builder;

	Binder binder;

	@Before
	public void init() {

		binder = Mockito.mock(Binder.class);

		builder = new ReaderBuilder();
		builder.withBinder(binder);
		builder.withValidator(new Validator());
	}

	public static class Data {
		private String x;

		public String getX() {
			return x;
		}
	}

	@Test
	public void test() {
		addBinding("x", String.class);
		builder.useAll(true).forEntity(Data.class).init();

		Data data = (Data) builder.build().read(variables("x", "value"));
		Assert.assertEquals("value", data.getX());

	}

	@Test
	public void unboundField() {
		builder.useAll(true).forEntity(Data.class).init();
		Assert.assertFalse(builder.isInit());
	}

	@Test
	public void mismatchedType() {
		addBinding("x", int.class);
		builder.useAll(true).forEntity(Data.class).init();
		Assert.assertFalse(builder.isInit());
	}

	private void addBinding(String name, Type type) {
		Mockito.when(binder.getBinding(name)).thenReturn(new Binding(false, false, name, type));

		// bindings.put(name, new Binding(false, false, name, type));
	}

	private Map<String, Object> variables(Object... pairs) {

		Map<String, Object> variables = new HashMap<>();

		for (int i = 0; i < pairs.length; i += 2) {
			variables.put(pairs[i].toString(), pairs[i + 1]);
		}
		return variables;
	}
}
