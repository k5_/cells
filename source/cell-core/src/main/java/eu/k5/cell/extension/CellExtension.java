package eu.k5.cell.extension;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterBeanDiscovery;
import javax.enterprise.inject.spi.AfterDeploymentValidation;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.InjectionTarget;
import javax.enterprise.inject.spi.ProcessAnnotatedType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.k5.cell.annotation.workflow.Workflow;
import eu.k5.cell.annotation.workflow.Workflows;
import eu.k5.cell.extension.Validator.Violation;

public class CellExtension implements Extension {
	private static final Logger LOGGER = LoggerFactory.getLogger(CellExtension.class);

	private final List<CellBuilder> builders = new ArrayList<>();
	private final List<CompositeCellBuilder> compositeBuilders = new ArrayList<>();
	private final List<Type> requiredBeans = new ArrayList<>();

	public <X> void processAnnotatedType(@Observes ProcessAnnotatedType<X> pat) {
		if (pat.getAnnotatedType().isAnnotationPresent(Workflow.class)) {
			builders.add(initCellBuilder(pat.getAnnotatedType().getJavaClass()));
		} else if (pat.getAnnotatedType().isAnnotationPresent(Workflows.class)) {

			compositeBuilders.add(initCompositeBuilder(pat.getAnnotatedType().getJavaClass()));

		}
	}

	public void registerContext(@Observes final AfterBeanDiscovery abd, BeanManager bm) {

		AnnotatedType<Cells> at = bm.createAnnotatedType(Cells.class);

		// use this to instantiate the class and inject dependencies

		int uninitialized = 0;
		for (CellBuilder builder : builders) {
			if (!builder.isInit()) {
				uninitialized++;
				for (Violation violation : builder.getValidator().getViolations()) {
					LOGGER.warn(violation.toString());
				}
			} else {
				requiredBeans.addAll(builder.getRequiredBeans());
			}
		}

		for (CompositeCellBuilder builder : compositeBuilders) {
			builder.withCells(builders).init();
			if (!builder.isInit()) {
				uninitialized++;

				for (Violation violation : builder.getValidator().getViolations()) {
					LOGGER.warn(violation.toString());
				}
			}
		}

		if (uninitialized != 0) {
			LOGGER.error(
					"There are {} uninitizalized cells; No cells will be available for injection; see prior log messages for details",
					uninitialized);
			builders.clear();
			return;
		}

		List<Cell> cells = new ArrayList<>();
		for (CellBuilder builder : builders) {
			cells.add(builder.build());
		}
		for (CompositeCellBuilder builder : compositeBuilders) {
			cells.add(builder.build());
		}

		builders.clear();

		InjectionTarget<Cells> it = bm.createInjectionTarget(at);
		abd.addBean(new CellsBean(it, new Cells(cells)));

	}

	public void validateServants(@Observes AfterDeploymentValidation adv, BeanManager bm) {

		List<Type> missing = new ArrayList<>();
		for (Type beanType : requiredBeans) {
			if (!bm.getBeans(beanType).iterator().hasNext()) {
				LOGGER.error("Missing bean implementing {}", beanType);
				missing.add(beanType);
			}
		}
		if (!missing.isEmpty()) {
			LOGGER.error(
					"There are {} unsatisfied beans; No cells will be availble for injection; see prior log messages for details",
					missing);

			adv.addDeploymentProblem(new RuntimeException("There are " + missing
					+ " missing beans; No cells will be availble for injection; see prior log messages for details"));

		}

	}

	private CellBuilder initCellBuilder(Class<?> type) {
		CellBuilder builder = new CellBuilder();
		builder.with(type).init();
		return builder;
	}

	private CompositeCellBuilder initCompositeBuilder(Class<?> type) {
		CompositeCellBuilder builder = new CompositeCellBuilder();
		builder.withType(type);
		return builder;
	}
}
