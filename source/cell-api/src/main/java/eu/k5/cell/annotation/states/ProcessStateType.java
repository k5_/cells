package eu.k5.cell.annotation.states;

import java.util.Map;

public enum ProcessStateType {
	IS_ENDED(Boolean.class), INSTANCE_ID(String.class), DERIVED(Object.class), PROCESS_DEFINITION_KEY(String.class), PROCESS_DEFINITION_ID(
			String.class), VARIABLES(Map.class);

	private final Class<?> type;

	private ProcessStateType(Class<?> type) {
		this.type = type;
	}

	public Class<?> getType() {
		return type;
	}
}
