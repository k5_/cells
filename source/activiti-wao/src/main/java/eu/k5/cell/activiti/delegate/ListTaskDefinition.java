package eu.k5.cell.activiti.delegate;

import java.util.List;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.persistence.entity.TaskEntity;

import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.ast.Query;
import eu.k5.cell.activiti.builder.QueryParameter;

public final class ListTaskDefinition extends TaskQueryDefinition<ListTasksDelegate> {
	ListTaskDefinition(Query query, QueryParameter[] parameters, Context... context) {
		super(query, parameters, context);
	}

	@Override
	public ListTasksDelegate bind(ManagementService service) {
		return DelegateFactory.listTaskDelegate(service, render(service), getParameters());
	}

	public List<TaskEntity> invoke(ManagementService service, Object... arguments) {
		return bind(service).invoke(arguments);
	}
}