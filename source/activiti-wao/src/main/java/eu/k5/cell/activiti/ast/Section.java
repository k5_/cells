package eu.k5.cell.activiti.ast;

import java.util.Set;

import eu.k5.cell.activiti.TypeHint;

public class Section {
	private final Expression whereExpression;
	private final Set<String> variables;

	public Section(Expression whereExpression, Set<String> variables) {
		this.whereExpression = whereExpression;
		this.variables = variables;
	}

	public Expression getWhereExpression() {
		return whereExpression;
	}

	public TypeHint getWhereTypeHint(Context context) {
		return whereExpression.getHint(context);
	}

	public void accept(QueryVisitor visitor) {
		visitor.visit(this);
	}

	public Set<String> getVariables() {
		return variables;
	}
}
