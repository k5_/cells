package eu.k5.cell.model.historic;

import eu.k5.cell.annotation.Out;

public class HistoricStart {
	@Out
	private final String key;
	@Out
	private final String property;

	public HistoricStart(String key, String property) {
		this.key = key;
		this.property = property;
	}

	public String getKey() {
		return key;
	}

	public String getProperty() {
		return property;
	}
}
