package eu.k5.cell.definition;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class PropertyAssignerField implements PropertyAssigner {
	private final Field field;

	public PropertyAssignerField(Field field) {

		this.field = field;
	}

	@Override
	public void assign(Object instance, Object value) {
		try {
			field.set(instance, value);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public boolean isAnnotationPresent(Class<? extends Annotation> annotations) {
		return field.isAnnotationPresent(annotations);
	}

	@Override
	public <A extends Annotation> A getAnnotation(Class<A> annotation) {
		return field.getAnnotation(annotation);
	}

	@Override
	public Class<?> getType() {
		return field.getType();
	}

	public String getFieldName() {
		return field.getName();
	}

}
