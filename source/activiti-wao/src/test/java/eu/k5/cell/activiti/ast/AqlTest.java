package eu.k5.cell.activiti.ast;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class AqlTest {
	private AqlQueries aql;

	@Before
	public void init() {
		aql = new AqlQueries();
	}

	@Test
	public void assignee() {
		String raw = "assignee = 'test'";
		Query query = aql.parseTaskQuery(raw);
		Expression expression = query.getSections().get(0).getWhereExpression();

		// System.out.println(expression.getHint());

		Assert.assertTrue(((UnaryExpression) expression).getExpression() instanceof Condition);
		Condition condition = (Condition) ((UnaryExpression) expression).getExpression();
		Assert.assertEquals(Operator.EQ, condition.getOperator());
		BasicProperty left = (BasicProperty) condition.getLeft();
		Assert.assertEquals(BasicProperty.PropertyType.ASSIGNEE, left.getType());
	}

	@Test
	@Ignore("Grammer error doesnt support parameters named liked keywords")
	public void propertyAsNamedParameter() {
		String raw = "assignee =:assignee";
		Query query = aql.parseTaskQuery(raw);
		Expression expression = query.getSections().get(0).getWhereExpression();

		Assert.assertTrue(((UnaryExpression) expression).getExpression() instanceof Condition);
		Condition condition = (Condition) ((UnaryExpression) expression).getExpression();
		Assert.assertEquals(Operator.EQ, condition.getOperator());
		BasicProperty left = (BasicProperty) condition.getLeft();
		Assert.assertEquals(BasicProperty.PropertyType.ASSIGNEE, left.getType());
	}

	@Test
	public void namedParameter() {
		String raw = ":a = :b";

		Query query = aql.parseTaskQuery(raw);
		Assert.assertThat(query.getNamedParameters(), Matchers.containsInAnyOrder("b", "a"));

		// System.out.println(query.getWhereExpression().getHint());

	}

	@Test
	public void variable() {
		String raw = "var.test = :b";
		Query query = aql.parseTaskQuery(raw);
		Expression expression = query.getSections().get(0).getWhereExpression();
		Assert.assertTrue(((UnaryExpression) expression).getExpression() instanceof Condition);
		Condition condition = (Condition) ((UnaryExpression) expression).getExpression();
		Assert.assertEquals(Operator.EQ, condition.getOperator());
		Variable left = (Variable) condition.getLeft();
		Assert.assertEquals("test", left.getVariable());
	}

}
