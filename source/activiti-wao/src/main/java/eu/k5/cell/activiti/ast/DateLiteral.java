package eu.k5.cell.activiti.ast;

import java.util.Date;

import eu.k5.cell.activiti.TypeHint;

public class DateLiteral extends Literal<Date> {

	protected DateLiteral(Date value, TypeHint hint) {
		super(value, hint);
	}

	@Override
	public void accept(ValueVisitor visitor) {
		visitor.visit(this);
	}

}
