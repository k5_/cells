package eu.k5.cell.extension;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.k5.cell.annotation.Complete;
import eu.k5.cell.annotation.Query;
import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.definition.AspectDefinition;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.wao.CompleteDelegateDefinition;
import eu.k5.cell.wao.DelegateDefinition;
import eu.k5.cell.wao.StartDelegateDefinition;
import eu.k5.cell.wao.WaoDefinition;

public class DelegateBuilder extends Builder<WaoDefinition, DelegateBuilder> {
	private final Map<Method, DelegateDefinition> delegates = new HashMap<>();

	private List<TaskDefinition> tasks;

	private List<AspectDefinition> aspects;

	public DelegateBuilder() {
		super(WaoDefinition.class);
		setBuilder(this);
	}

	@Override
	public void doInit() {
		Method[] methods = getType().getMethods();
		for (Method method : methods) {

			int count = countRelevantAnnotations(method);

			if (isStart(method, count)) {
				initStartDelegate(method);
			} else if (isComplete(method, count)) {
				initCompleteDelegate(method);
			} else if (isQuery(method, count)) {
				initQuery(method);
			}

		}
	}

	private boolean isStart(Method method, int count) {
		if (method.isAnnotationPresent(Start.class) && count == 1) {
			return true;
		}
		if (count == 0 && method.getName().equals("start")) {
			return true;
		}
		return false;
	}

	private boolean isComplete(Method method, int count) {
		if (method.isAnnotationPresent(Complete.class) && count == 1) {
			return true;
		}
		if (count == 0 && method.getName().equals("complete")) {
			return true;
		}
		return false;
	}

	private boolean isQuery(Method method, int count) {
		return method.isAnnotationPresent(Query.class) && count == 1;
	}

	private void initStartDelegate(Method method) {
		if (method.getParameterTypes().length != 1) {
			addViolation("Invalid start method with {} parameters", method.getParameterTypes().length);
		} else if (!method.getReturnType().equals(void.class)) {
			addViolation("Invalid start method with return type", method.getParameterTypes().length);
		} else {
			delegates.put(method, new StartDelegateDefinition());
		}
	}

	private void initCompleteDelegate(Method method) {
		if (method.getParameterTypes().length != 1) {
			addViolation("Invalid complete method with {} parameters", method.getParameterTypes().length);
		} else if (!method.getReturnType().equals(void.class)) {
			addViolation("Invalid complete method with return type", method.getParameterTypes().length);
		} else {
			delegates.put(method, new CompleteDelegateDefinition());
		}
	}

	private void initQuery(Method method) {
		List<AspectDefinition> aspects = filterPotentialAspects(method);
		List<TaskDefinition> tasks = filterPotentialTasks(method);
		if (aspects.isEmpty() && tasks.isEmpty()) {
			addViolation("No task or aspect available for {}", method);
			return;
		}
		if (!aspects.isEmpty() && !tasks.isEmpty()) {
			addViolation("Aspect and tasks relevant for same query");
			return;
		}

		if (!aspects.isEmpty()) {
			if (aspects.size() == 1) {
				initAspectDelgate(method, aspects.get(0));
			} else {
				addViolation("Multiple aspects relevant {}", aspects);
			}
		}
		if (!tasks.isEmpty()) {
			initTaskDelegate(method, tasks);
		}
	}

	private void initAspectDelgate(Method method, AspectDefinition aspect) {

		// Query query = method.getAnnotation(Query.class);
		//
		// AspectDelegateBuilder builder = new AspectDelegateBuilder();
		// builder.withMethod(method).withAspect(aspect).withVariableBounds(getBinder().getVariables())
		// .withQueryString(query.value());
		//
		// builder.asList(List.class.equals(method.getReturnType()));
		//
		// delegates.put(method, builder.build());

	}

	private void initTaskDelegate(Method method, List<TaskDefinition> tasks) {

		// Query query = method.getAnnotation(Query.class);
		//
		// TaskDelegateBuilder builder = new TaskDelegateBuilder();
		// builder.withMethod(method).withTasks(tasks).withQueryString(query.value()).withTasks(tasks);
		//
		// builder.asList(List.class.equals(method.getReturnType()));
		// delegates.put(method, builder.build());
	}

	private List<AspectDefinition> filterPotentialAspects(Method method) {
		List<AspectDefinition> definitions = new ArrayList<>();
		Type relevant = getRelevantType(method.getGenericReturnType());
		for (AspectDefinition aspect : aspects) {
			if (aspect.getType().equals(relevant)) {
				definitions.add(aspect);
			}
		}
		return definitions;
	}

	private List<TaskDefinition> filterPotentialTasks(Method method) {
		List<TaskDefinition> definitions = new ArrayList<>();
		Type relevant = getRelevantType(method.getGenericReturnType());
		for (TaskDefinition task : tasks) {
			if (task.getType().equals(relevant)) {
				definitions.add(task);
			}
		}
		return definitions;
	}

	private Type getRelevantType(Type type) {
		if (type instanceof ParameterizedType && List.class.equals(((ParameterizedType) type).getRawType())) {
			Type actualType = ((ParameterizedType) type).getActualTypeArguments()[0];
			return actualType;
		}
		return type;
	}

	@Override
	protected WaoDefinition doBuild() {
		return new WaoDefinition(getType(), delegates);
	}

	private int countRelevantAnnotations(Method method) {
		int count = 0;

		if (method.isAnnotationPresent(Start.class)) {
			count++;
		}
		if (method.isAnnotationPresent(Query.class)) {
			count++;
		}
		if (method.isAnnotationPresent(Complete.class)) {
			count++;
		}
		return count;
	}

	public DelegateBuilder withTasks(List<TaskDefinition> tasks) {
		this.tasks = tasks;
		return this;
	}

	public DelegateBuilder withAspects(List<AspectDefinition> aspects) {
		this.aspects = aspects;
		return this;
	}

}
