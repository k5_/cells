package eu.k5.cell.model;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;

import eu.k5.cell.activiti.CellManagerProducer;
import eu.k5.cell.activiti.TestEngine;

@ApplicationScoped
public class TestProducer {
	@Inject
	private TestEngine engine;

	@Inject
	CellManagerProducer cmp;

	@ApplicationScoped
	@Produces
	public RuntimeService produceRuntimeService() {
		return engine.getEngine().getRuntimeService();
	}

	@ApplicationScoped
	@Produces
	public TaskService produceTaskService() {
		return engine.getEngine().getTaskService();
	}

	@ApplicationScoped
	@Produces
	public HistoryService produceHistoryService() {
		return engine.getEngine().getHistoryService();
	}

	@ApplicationScoped
	@Produces
	public ManagementService produceManagementService() {
		return engine.getEngine().getManagementService();
	}
}
