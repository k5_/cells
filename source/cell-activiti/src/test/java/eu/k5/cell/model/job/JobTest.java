package eu.k5.cell.model.job;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.activiti.engine.runtime.ProcessInstance;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import eu.k5.cell.activiti.CellManager;
import eu.k5.cell.activiti.TestEngine;
import eu.k5.cell.annotation.CellContext;
import eu.k5.cell.extension.CellExtension;
import eu.k5.cell.model.TestProducer;

@RunWith(CdiRunner.class)
@AdditionalClasses({ TestProducer.class, CellExtension.class, JobWorkflow.class })
public class JobTest {
	private static TestEngine refEngine = new TestEngine("job", "job");

	@CellContext(JobWorkflow.class)
	@Inject
	CellManager cell;

	@Produces
	TestEngine engine = refEngine;

	@AfterClass
	public static void destoy() {
		refEngine.destroy();
	}

	@Produces
	public JobWorkflow.Servant produce() {
		return new JobWorkflow.Servant() {

			@Override
			public void execute(JobJob job) {
				System.out.println(job);
				job.setProperty("abc");
				job.setProperty3("result");
				Assert.assertEquals("derived", job.getDerived());
				Assert.assertEquals("inValue", job.getInOut());
				Assert.assertNotNull(job.getInstance());
				job.setInOut("outValue");
			}
		};
	}

	@Test
	public void start() {
		JobStart start = new JobStart("key", "property", "inValue");
		cell.start(start);

		ProcessInstance instance = engine.find("key");
		Assert.assertNotNull(instance);
		Assert.assertEquals("property", instance.getProcessVariables().get("property"));
		Assert.assertEquals("result", instance.getProcessVariables().get("property3"));
		Assert.assertEquals("defaultValue", instance.getProcessVariables().get("defaultOut"));
		Assert.assertEquals("outValue", instance.getProcessVariables().get("inOut"));
	}
}
