package eu.k5.cell.model.composite;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.In;
import eu.k5.cell.shared.CellTaskReference;

public abstract class AbstractCompositeTask implements Comparable<AbstractCompositeTask> {

	@CellReference
	private CellTaskReference reference;

	@In
	private String property;

	public String getProperty() {
		return property;
	}

	@Override
	public int compareTo(AbstractCompositeTask o) {
		return property.compareTo(o.property);
	}
}
