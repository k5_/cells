package eu.k5.cell.activiti.builder;

import java.util.ArrayList;
import java.util.List;

public class Violations {

	public static enum Type {
		INVALID_PARAMETER, UNKOWN_VARIABLE;
	}

	static class Violation {
		private final Type type;
		private final String message;

		public Violation(Type type, String message) {
			this.type = type;
			this.message = message;
		}

		public Type getType() {
			return type;
		}

		public String getMessage() {
			return message;
		}

	}

	private final Violations parent;
	private final List<Violation> violations = new ArrayList<>();

	public Violations() {
		this(null);
	}

	public Violations(Violations parent) {
		this.parent = parent;
	}

	public boolean isEmpty() {
		return violations.isEmpty();
	}

	public boolean contains(Type type) {
		for (Violation v : violations) {
			if (v.getType().equals(type)) {
				return true;
			}
		}
		return false;
	}

	public void add(Type type, String message) {
		violations.add(new Violation(type, message));
		if (parent != null) {
			parent.add(type, message);
		}
	}

	public Violations sub() {
		return new Violations(this);
	}
}
