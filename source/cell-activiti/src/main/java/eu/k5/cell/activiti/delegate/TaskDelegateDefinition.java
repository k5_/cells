package eu.k5.cell.activiti.delegate;

import java.util.List;

import org.activiti.engine.ManagementService;

import eu.k5.cell.WorkflowManager;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.wao.DelegateDefinition;

public class TaskDelegateDefinition implements DelegateDefinition {

	private final TaskQueryDefinition<ListTasksDelegate> query;

	private final List<TaskDefinition> definitions;

	private final boolean isList;

	public TaskDelegateDefinition(TaskQueryDefinition<ListTasksDelegate> query, List<TaskDefinition> definitions,
			boolean isList) {
		this.query = query;
		this.definitions = definitions;
		this.isList = isList;
	}

	@Override
	public TaskDelegate bind(WorkflowManager cellManager) {

		ManagementService managementService = cellManager.unwrap(ManagementService.class);

		return new TaskDelegate(query.bind(managementService), definitions, isList);
	}

}
