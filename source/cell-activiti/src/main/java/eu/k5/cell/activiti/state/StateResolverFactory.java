package eu.k5.cell.activiti.state;

import java.util.Date;
import java.util.Map;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.task.Task;

import eu.k5.cell.TaskStateResolverFactory;
import eu.k5.cell.annotation.states.JobStateResolver;
import eu.k5.cell.annotation.states.JobStateType;
import eu.k5.cell.annotation.states.ProcessStateResolver;
import eu.k5.cell.annotation.states.ProcessStateType;
import eu.k5.cell.annotation.states.TaskStateResolver;
import eu.k5.cell.annotation.states.TaskStateType;

public class StateResolverFactory implements TaskStateResolverFactory {

	@Override
	public TaskStateResolver taskReader(TaskStateType type) {
		switch (type) {
		case ASSIGNEE:
			return new AssigneeState();
		case CREATE_TIME:
			return new CreateTimeState();
		case PROCESS_INSTANCE_ID:
			return new ProcessInstanceIdTaskState();
		case PROCESS_DEFINITION_ID:
			return new ProcessDefinitionIdState();
		case VARIABLES:
			return new ProcessVariablesTaskState();
		case LOCAL_VARIABLES:
			return new TaskLocalVariablesTaskState();
		default:
			break;
		}
		addViolation("Unkown TaskStateType {}", type);
		return null;
	}

	private void addViolation(String string, Object... type) {

	}

	@Override
	public ProcessStateResolver stateReader(ProcessStateType type) {
		switch (type) {
		case INSTANCE_ID:
			return new ProcessInstanceId();
		case PROCESS_DEFINITION_KEY:
			return new ProcessDefintionKeyProcessState();
		case PROCESS_DEFINITION_ID:
			return new ProcessDefintionIdProcessState();
		case IS_ENDED:
			return new ProcessIsEnded();
		case VARIABLES:
			return new ProcessVariablesProcessState();
		default:
			break;
		}

		addViolation("Unkown processstatetype");
		return null;
	}

	private static final class TaskLocalVariablesTaskState implements TaskStateResolver {
		@Override
		public Map<String, Object> read(Object state) {
			return ((Task) state).getTaskLocalVariables();
		}

		@Override
		public Class<?> getReturnType() {
			return Map.class;
		}
	}

	private static final class ProcessVariablesTaskState implements TaskStateResolver {
		@Override
		public Map<String, Object> read(Object state) {
			return ((Task) state).getProcessVariables();
		}

		@Override
		public Class<?> getReturnType() {
			return Map.class;
		}
	}

	private static final class AssigneeState implements TaskStateResolver {
		@Override
		public String read(Object state) {
			return ((Task) state).getAssignee();
		}

		@Override
		public Class<?> getReturnType() {
			return String.class;
		}
	}

	private static final class CreateTimeState implements TaskStateResolver {
		@Override
		public Date read(Object state) {
			return ((Task) state).getCreateTime();
		}

		@Override
		public Class<?> getReturnType() {
			return Date.class;
		}
	}

	private static final class ProcessInstanceIdTaskState implements TaskStateResolver {
		@Override
		public String read(Object state) {
			return ((Task) state).getProcessInstanceId();
		}

		@Override
		public Class<?> getReturnType() {
			return String.class;
		}
	}

	private static final class ProcessDefinitionIdState implements TaskStateResolver {
		@Override
		public String read(Object state) {
			return ((Task) state).getProcessDefinitionId();
		}

		@Override
		public Class<?> getReturnType() {
			return String.class;
		}
	}

	private static final class ProcessIsEnded implements ProcessStateResolver {
		@Override
		public Object read(Object state) {
			return ((eu.k5.cell.activiti.ProcessState) state).getProcessInstance().isEnded();
		}

		@Override
		public Class<?> getReturnType() {
			return boolean.class;
		}
	}

	private static final class ProcessInstanceId implements ProcessStateResolver {
		@Override
		public Object read(Object state) {
			return ((eu.k5.cell.activiti.ProcessState) state).getProcessInstance().getProcessInstanceId();
		}

		@Override
		public Class<?> getReturnType() {
			return String.class;
		}
	}

	private static final class ProcessDefintionKeyProcessState implements ProcessStateResolver {
		@Override
		public Object read(Object state) {
			return ((eu.k5.cell.activiti.ProcessState) state).getProcessInstance().getProcessDefinitionKey();
		}

		@Override
		public Class<?> getReturnType() {
			return String.class;
		}
	}

	private static final class ProcessDefintionIdProcessState implements ProcessStateResolver {
		@Override
		public Object read(Object state) {
			return ((eu.k5.cell.activiti.ProcessState) state).getProcessInstance().getProcessDefinitionId();
		}

		@Override
		public Class<?> getReturnType() {
			return String.class;
		}
	}

	private static final class ProcessVariablesProcessState implements ProcessStateResolver {
		@Override
		public Object read(Object state) {
			return ((eu.k5.cell.activiti.ProcessState) state).getProcessInstance().getProcessVariables();
		}

		@Override
		public Class<?> getReturnType() {
			return Map.class;
		}
	}

	@Override
	public JobStateResolver jobReader(JobStateType type) {

		switch (type) {
		case INSTANCE_ID:
			return new JobStateResolver() {

				@Override
				public Object read(Object state) {
					return ((DelegateExecution) state).getProcessInstanceId();
				}

				@Override
				public Class<?> getReturnType() {
					return String.class;
				}
			};
		case PROCESS_DEFINITION_ID:
			return new JobStateResolver() {

				@Override
				public Object read(Object state) {
					return ((DelegateExecution) state).getProcessDefinitionId();
				}

				@Override
				public Class<?> getReturnType() {
					return String.class;
				}
			};
		case VARIABLES:
			return new JobStateResolver() {

				@Override
				public Object read(Object state) {
					return ((DelegateExecution) state).getVariables();
				}

				@Override
				public Class<?> getReturnType() {
					return Map.class;
				}
			};
		default:
			break;
		}

		addViolation("Unknown JobStateTpye {}", type);
		return null;
	}
}
