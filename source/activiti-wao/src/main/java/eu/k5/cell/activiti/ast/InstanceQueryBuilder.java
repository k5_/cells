package eu.k5.cell.activiti.ast;

public class InstanceQueryBuilder extends BaseQueryRenderer {

	private final String executionTable;
	private Context context;

	public InstanceQueryBuilder(DatabaseType type, String executionTable, String processDefinitionTable,
			String variableTable, String identityTable) {
		super(type, variableTable, identityTable, processDefinitionTable);
		this.executionTable = executionTable;
	}

	@Override
	public void visit(Section section) {

		StringBuilder form = currentContext().getFrom();

		form.append("FROM ").append(executionTable).append(" ").append(getResourcePrefix());
		form.append(" inner join ").append(getProcessDefinitionTable()).append(" ")
				.append(getProcessDefinitionPrefix()).append(" on ").append(getResourcePrefix())
				.append(".PROC_DEF_ID_ = ").append(getProcessDefinitionPrefix()).append(".ID_");
		form.append(" left outer join ").append(getVariableTable()).append(" ").append(getVariablePrefix());
		form.append(" ON ").append(getResourcePrefix()).append(".PROC_INST_ID_ = ").append(getVariablePrefix())
				.append(".EXECUTION_ID_ and ").append(getVariablePrefix()).append(".TASK_ID_ is ").append(" null ");

		currentContext().getWhere().append("WHERE RES.PARENT_ID_ is null AND ");
		section.getWhereExpression().accept(this);

		appendSelectProcessWithVariables();
		// selectProcessDefinition();
		// selectVariables();
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	protected String addVariable(boolean local) {
		String alias = "A" + currentContext().getVariableIndex().incrementAndGet();

		currentContext().getFrom().append("inner join ").append(getVariableTable()).append(" ").append(alias)
				.append(" on res.PROC_INST_ID_ = ").append(alias).append(".PROC_INST_ID_ ");
		return alias;
	}

	public void appendSelectProcessWithVariables() {
		StringBuilder select = currentContext().getSelect();

		currentContext().getPrefix().append(getType().limitBefore());
		currentContext().getSuffix().append(getType().limitAfter());
		if (DatabaseType.MSSQ.equals(getType())) {
			select.append("select distinct ");
			select.append("TEMPRES_ID_ as ID_, ");
			select.append("TEMPP_KEY_ as ProcessDefinitionKey, ");
			select.append("TEMPP_ID_ as ProcessDefinitionId, ");
			select.append("TEMPP_NAME_ as ProcessDefinitionName, ");
			select.append("TEMPP_VERSION_ as ProcessDefinitionVersion, ");
			select.append("TEMPP_DEPLOYMENT_ID_ as DeploymentId, ");
			select.append("TEMPRES_REV_ as REV_, ");
			select.append("TEMPRES_ACT_ID_ as ACT_ID_, ");
			select.append("TEMPRES_BUSINESS_KEY_ as BUSINESS_KEY_, ");
			select.append("TEMPRES_IS_ACTIVE_ as IS_ACTIVE_, ");
			select.append("TEMPRES_IS_CONCURRENT_ as IS_CONCURRENT_, ");
			select.append("TEMPRES_IS_SCOPE_ as IS_SCOPE_, ");
			select.append("TEMPRES_IS_EVENT_SCOPE_ as IS_EVENT_SCOPE_, ");
			select.append("TEMPRES_PARENT_ID_ as PARENT_ID_, ");
			select.append("TEMPRES_PROC_INST_ID_ as PROC_INST_ID_, ");
			select.append("TEMPRES_SUPER_EXEC_ as SUPER_EXEC_, ");
			select.append("TEMPRES_SUSPENSION_STATE_ as SUSPENSION_STATE_, ");
			select.append("TEMPRES_CACHED_ENT_STATE_ as CACHED_ENT_STATE_, ");
			select.append("TEMPVAR_ID_ as VAR_ID_, ");
			select.append("TEMPVAR_NAME_ as VAR_NAME_, ");
			select.append("TEMPVAR_TYPE_ as VAR_TYPE_, ");
			select.append("TEMPVAR_REV_ as VAR_REV_, ");
			select.append("TEMPVAR_PROC_INST_ID_ as VAR_PROC_INST_ID_, ");
			select.append("TEMPVAR_EXECUTION_ID_ as VAR_EXECUTION_ID_, ");
			select.append("TEMPVAR_TASK_ID_ as VAR_TASK_ID_, ");
			select.append("TEMPVAR_BYTEARRAY_ID_ as VAR_BYTEARRAY_ID_, ");
			select.append("TEMPVAR_DOUBLE_ as VAR_DOUBLE_, ");
			select.append("TEMPVAR_TEXT_ as VAR_TEXT_, ");
			select.append("TEMPVAR_TEXT2_ as VAR_TEXT2_, ");
			select.append("TEMPVAR_LONG_ as VAR_LONG_ ");

			select.append(getType().limitOuterJoinBetween());

			select.append("RES.ID_ as TEMPRES_ID_, ");
			select.append("RES.REV_ as TEMPRES_REV_, ");
			select.append("P.KEY_ as TEMPP_KEY_, ");
			select.append("P.ID_ as TEMPP_ID_, ");
			select.append("P.NAME_ as TEMPP_NAME_, ");
			select.append("P.VERSION_ as TEMPP_VERSION_, ");
			select.append("P.DEPLOYMENT_ID_ as TEMPP_DEPLOYMENT_ID_, ");
			select.append("RES.ACT_ID_ as TEMPRES_ACT_ID_, ");
			select.append("RES.PROC_INST_ID_ as TEMPRES_PROC_INST_ID_, ");
			select.append("RES.BUSINESS_KEY_ as TEMPRES_BUSINESS_KEY_, ");
			select.append("RES.IS_ACTIVE_ as TEMPRES_IS_ACTIVE_, ");
			select.append("RES.IS_CONCURRENT_ as TEMPRES_IS_CONCURRENT_, ");
			select.append("RES.IS_SCOPE_ as TEMPRES_IS_SCOPE_, ");
			select.append("RES.IS_EVENT_SCOPE_ as TEMPRES_IS_EVENT_SCOPE_, ");
			select.append("RES.PARENT_ID_ as TEMPRES_PARENT_ID_, ");
			select.append("RES.SUPER_EXEC_ as TEMPRES_SUPER_EXEC_, ");
			select.append("RES.SUSPENSION_STATE_ as TEMPRES_SUSPENSION_STATE_, ");
			select.append("RES.CACHED_ENT_STATE_ as TEMPRES_CACHED_ENT_STATE_, ");
			select.append("VAR.ID_ as TEMPVAR_ID_, ");
			select.append("VAR.NAME_ as TEMPVAR_NAME_, ");
			select.append("VAR.TYPE_ as TEMPVAR_TYPE_, ");
			select.append("VAR.REV_ as TEMPVAR_REV_, ");
			select.append("VAR.PROC_INST_ID_ as TEMPVAR_PROC_INST_ID_, ");
			select.append("VAR.EXECUTION_ID_ as TEMPVAR_EXECUTION_ID_, ");
			select.append("VAR.TASK_ID_ as TEMPVAR_TASK_ID_, ");
			select.append("VAR.BYTEARRAY_ID_ as TEMPVAR_BYTEARRAY_ID_, ");
			select.append("VAR.DOUBLE_ as TEMPVAR_DOUBLE_, ");
			select.append("VAR.TEXT_ as TEMPVAR_TEXT_, ");
			select.append("VAR.TEXT2_ as TEMPVAR_TEXT2_, ");
			select.append("VAR.LONG_ as TEMPVAR_LONG_ ");
		} else {
			select.append("SELECT ").append(getResourcePrefix()).append(".*, ");
			selectProcessDefinition();
			selectVariables();

		}

	}

	@Override
	protected String getResourcePrefix() {
		return "res";
	}

	private void selectProcessDefinition() {
		String processPrefix = getProcessDefinitionPrefix();
		StringBuilder select = currentContext().getSelect();
		select.append(processPrefix).append(".KEY_ as ProcessDefinitionKey, ");
		select.append(processPrefix).append(".ID_ as ProcessDefinitionId, ");
		select.append(processPrefix).append(".NAME_ as ProcessDefinitionName, ");
		select.append(processPrefix).append(".VERSION_ as ProcessDefinitionVersion, ");
		select.append(processPrefix).append(".DEPLOYMENT_ID_ as DeploymentId, ");
	}

	@Override
	protected String getProcessDefinitionPrefix() {
		return "P";
	}

	class InstanceRenderContext extends RenderContext {

		public InstanceRenderContext(Context context) {
			super(context);
		}

		@Override
		public void includeProcessDefinition() {

		}

	}

	@Override
	protected RenderContext createContext() {
		return new InstanceRenderContext(context);
	}

}
