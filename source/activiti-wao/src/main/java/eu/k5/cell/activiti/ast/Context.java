package eu.k5.cell.activiti.ast;

import java.util.Map;

import eu.k5.cell.activiti.ParameterBound;
import eu.k5.cell.activiti.VariableBound;

public class Context {

	private final Map<String, VariableBound> variables;
	private final Map<String, ParameterBound> parameters;

	public Context(Map<String, VariableBound> variables, Map<String, ParameterBound> parameters) {
		this.variables = variables;
		this.parameters = parameters;
	}

	public VariableBound getVariable(String variable) {
		return variables.get(variable);
	}

	public ParameterBound getParameterBound(String parameter) {
		return parameters.get(parameter);
	}

	public Object getVariables() {
		// TODO Auto-generated method stub
		return null;
	}
}
