package eu.k5.cell.parser;

import java.lang.reflect.Type;

public abstract class Input {
	private final Type type;

	public Input(Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	abstract public SourceReference getSource();
}
