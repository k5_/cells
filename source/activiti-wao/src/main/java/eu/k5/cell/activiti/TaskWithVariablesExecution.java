package eu.k5.cell.activiti;

import java.util.Map;

import org.activiti.engine.impl.cmd.AbstractCustomSqlExecution;
import org.activiti.engine.impl.persistence.entity.TaskEntity;

public final class TaskWithVariablesExecution extends AbstractCustomSqlExecution<AqlMapper, TaskEntity> {
	private final Map<String, Object> parameters;

	public TaskWithVariablesExecution(Map<String, Object> parameters) {
		super(AqlMapper.class);
		this.parameters = parameters;
	}

	@Override
	public TaskEntity execute(AqlMapper mapper) {
		return mapper.selectTaskWithVariables(parameters);
	}
}