package eu.k5.cell.activiti.delegate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.persistence.entity.TaskEntity;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import eu.k5.cell.CellException;
import eu.k5.cell.activiti.ActivitiCellTask;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.wao.Delegate;

public class TaskDelegate implements Delegate {

	private final Map<TaskKey, TaskDefinition> definitions;

	private final ListTasksDelegate delegate;

	private final boolean isList;

	public TaskDelegate(ListTasksDelegate delegate, List<TaskDefinition> definitions, boolean isList) {
		this.delegate = delegate;
		this.isList = isList;
		Builder<TaskKey, TaskDefinition> builder = new ImmutableMap.Builder<TaskKey, TaskDefinition>();
		for (TaskDefinition task : definitions) {
			builder.put(TaskKey.fromDefintion(task), task);
		}
		this.definitions = builder.build();
	}

	@Override
	public Object invoke(Object... args) {
		List<TaskEntity> tasks = delegate.invoke(args);
		List<Object> result = new ArrayList<Object>();
		for (TaskEntity t : tasks) {

			TaskDefinition definition = definitions.get(TaskKey.fromTask(t));

			result.add(definition.read(new ActivitiCellTask(t)));
		}

		if (isList) {
			return result;
		}
		if (result.isEmpty()) {
			return null;
		}
		if (result.size() > 1) {
			throw new CellException("None unique result");
		}
		return result.get(0);
	}
}