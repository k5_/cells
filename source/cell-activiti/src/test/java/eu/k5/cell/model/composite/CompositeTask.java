package eu.k5.cell.model.composite;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.In;
import eu.k5.cell.shared.CellTaskReference;

public class CompositeTask implements Comparable<CompositeTask> {
	@CellReference
	private CellTaskReference reference;

	@In
	private String property;

	public String getProperty() {
		return property;
	}

	@Override
	public int compareTo(CompositeTask o) {
		return property.compareTo(o.getProperty());
	}

}
