package eu.k5.cell.extension;

import java.lang.reflect.Type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.k5.cell.TaskStateResolverFactory;
import eu.k5.cell.annotation.states.JobState;
import eu.k5.cell.annotation.states.JobStateResolver;
import eu.k5.cell.annotation.states.JobStateType;
import eu.k5.cell.annotation.states.ProcessState;
import eu.k5.cell.annotation.states.ProcessStateResolver;
import eu.k5.cell.annotation.states.ProcessStateType;
import eu.k5.cell.annotation.states.TaskState;
import eu.k5.cell.annotation.states.TaskStateResolver;
import eu.k5.cell.annotation.states.TaskStateType;
import eu.k5.cell.definition.JobStateReader;
import eu.k5.cell.definition.ProcessStateReader;
import eu.k5.cell.definition.TaskStateReader;

public class StateBuilder {

	private static final Logger LOGGER = LoggerFactory.getLogger(StateBuilder.class);

	private final Validator validator;

	private final Class<?> location;

	private TaskStateResolverFactory resolverFacctory;

	public StateBuilder(Validator validator, Class<?> location) {
		this.validator = validator;
		this.location = location;
	}

	private void addViolation(String message, Object... arguments) {
		validator.add(location, message, arguments);
	}

	public ProcessStateReader readProcessStates(ProcessState state, String name, Type type) {

		if (ProcessStateType.DERIVED.equals(state.value())) {
			Class<? extends ProcessStateResolver> derived = state.resolver();

			if (derived == null) {
				addViolation("Invalid DerivedProcessState bound to {}", name);
			} else {
				try {

					ProcessStateResolver processStateReader = derived.newInstance();

					if (!type.equals(processStateReader.getReturnType())) {
						addViolation("Missmatching type ProcesssState bound to {}", name);
					} else {
						return new ProcessStateReader(name, ProcessStateType.DERIVED, processStateReader);
					}
				} catch (InstantiationException | IllegalAccessException e) {
					LOGGER.info("Unable to instantiate derivedProcessState", e);
					addViolation("Unable to instantiate ProcesssState bound to {}", name);
				}
			}

		} else if (!state.value().getType().equals(type)) {
			addViolation("Invalid ProcesssState bound to {}", name);
		} else {
			return new ProcessStateReader(name, state.value(), resolverFacctory.stateReader(state.value()));
		}
		return null;
	}

	public JobStateReader readJobStates(JobState state, String name, Type type) {
		if (JobStateType.DERIVED.equals(state.value())) {
			Class<? extends JobStateResolver> derived = state.resolver();
			if (derived == null) {
				addViolation("Invalid DerivedProcessState bound to " + name);
			} else {
				try {
					JobStateResolver jobStateReader = derived.newInstance();

					if (!type.equals(jobStateReader.getReturnType())) {
						addViolation("Missmatching type JobState bound to " + name);
					} else {
						return new JobStateReader(name, JobStateType.DERIVED, jobStateReader);

					}
				} catch (InstantiationException | IllegalAccessException e) {
					LOGGER.info("Unable to instantiate derivedProcessState", e);
					addViolation("Unable to instantiate ProcesssState bound to " + name);
				}
			}

		} else if (!state.value().getType().equals(type)) {
			addViolation("Invalid ProcesssState bound to " + name);
		} else {
			return new JobStateReader(name, state.value(), resolverFacctory.jobReader(state.value()));
		}
		return null;
	}

	public TaskStateReader readTaskStates(TaskState state, String name, Type type) {

		if (TaskStateType.DERIVED.equals(state.value())) {
			Class<? extends TaskStateResolver> derived = state.resolver();
			if (derived == null) {
				addViolation("Invalid DerivedTaskState bound to " + name);
			} else {
				try {
					TaskStateResolver taskStateReader = derived.newInstance();

					if (!type.equals(taskStateReader.getReturnType())) {
						addViolation("Missmatching type ProcesssState bound to ", name);
					} else {
						return new TaskStateReader(name, TaskStateType.DERIVED, taskStateReader);
					}
				} catch (InstantiationException | IllegalAccessException e) {
					LOGGER.info("Unable to instantiate derivedProcessState", e);
					addViolation("Unable to instantiate ProcesssState bound to " + name);
				}
			}
		} else if (!state.value().getType().equals(type)) {
			addViolation("Invalid TaskState bound to {}", name);
		} else {
			return new TaskStateReader(name, state.value(), resolverFacctory.taskReader(state.value()));
		}
		return null;
	}

}
