package eu.k5.cell.generator;

import java.io.IOException;

import eu.k5.cell.parser.StructParser;
import eu.k5.cell.parser.StructParserBuilder;

public class StructGen {

	public static void main(String[] args) throws IOException {
		StructParserBuilder<Struct> builder = new StructParserBuilder<>(Struct.class);

		StructParser<Struct> struct = builder.build();

		GeneratorContext context = new GeneratorContext();
		StructParserGenerator gen = new StructParserGenerator(context);

		gen.generate(struct);
		context.getSources().writeSources();
		context.dump();
	}
}
