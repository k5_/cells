package eu.k5.cell.parser;

public class TaskParser<T> implements Parser<T> {
	private final Board board;

	private final Instantiator<T> task;

	public TaskParser(Board board, Instantiator<T> task) {
		this.board = board;
		this.task = task;
	}

	@Override
	public T parse(Context context) {

		Object[] board = new Object[size];
		for (Data data : datas) {
			board[data.getIndex()] = data.getSource().read(context);
		}
		return task.instantiate(board);
	}

}
