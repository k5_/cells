package eu.k5.cell.activiti.builder;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.persistence.entity.VariableInstanceEntity;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class QueryDelegateBuilderTest {

	private QueryDelegateBuilder builder;

	private ManagementService service;

	@Before
	public void init() {
		builder = QueryDelegateBuilder.listTasks();
		ManagementService service = Mockito.mock(ManagementService.class);
		Mockito.when(service.getTableName(TaskEntity.class)).thenReturn("TASK");
		Mockito.when(service.getTableName(VariableInstanceEntity.class)).thenReturn("VAR");
		builder.withViolations(new Violations());
	}

	@Test
	public void test() {
		builder.withQueryString("");

		builder.build();
	}
}
