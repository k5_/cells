package eu.k5.cell.parser;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

import eu.k5.cell.annotation.In;
import eu.k5.cell.annotation.Unbound;

public class TypeAnalyser<T> {

	private final Class<T> type;
	private Constructor<T> constructor;

	private List<Input> fields;

	private List<Input> constructorParameters;

	private List<Input> properties;

	public TypeAnalyser(Class<T> type) {
		this.type = type;
	}

	public Constructor<T> getConstructor() {
		return constructor;
	}

	public TypeAnalyser<T> init() {

		try {
			if (!analyseConstructors()) {
				return this;
			}

		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	@SuppressWarnings("unchecked")
	private boolean analyseConstructors() throws NoSuchMethodException, SecurityException {
		List<Constructor<?>> constructors = selectConstructors();
		if (constructors.size() == 1) {
			setConstructor((Constructor<T>) constructors.get(0));

			return true;
		} else if (constructors.isEmpty()) {
			setConstructor(type.getConstructor());
			return true;
		}

		return false;

	}

	@SuppressWarnings("unused")
	private boolean analyseFields() {

		Class<?> current = type;
		while (current != null) {
			analyseFields(current);
			current = current.getSuperclass();
		}

		return false;
	}

	private boolean analyseFields(Class<?> current) {
		Field[] fields = current.getDeclaredFields();
		for (Field field : fields) {
			if (Modifier.isStatic(field.getModifiers())) {
				continue;
			}
			if (field.isAnnotationPresent(Unbound.class)) {
				continue;
			}

			In in = field.getAnnotation(In.class);
			String bind = field.getName();
			if (in != null && !in.value().equals(In.DEFAULT)) {
				bind = in.value();
			}

		}
		return false;
	}

	public static class InputParameter {
		private String name;

	}

	public static class ConstructorInputParameter extends InputParameter {
		private int index;
	}

	public static class FieldInputParameter extends InputParameter {
		private Field field;
	}

	private boolean analyseProperties() {

		return false;
	}

	private void setConstructor(Constructor<T> constructor) {
		this.constructor = constructor;
		this.constructorParameters = new ArrayList<>();
		Annotation[][] annotations = constructor.getParameterAnnotations();
		Class<?>[] parameter = constructor.getParameterTypes();

		for (int index = 0; index < parameter.length; index++) {

			In in = getAnnotation(annotations[0], In.class);
			if (in != null) {
				NamedReference reference = new NamedReference(parameter[index], in.value());
				constructorParameters.add(new InputConstructor(type, reference));
			}

			addViolation();

		}
	}

	public List<Input> getConstructorParameters() {
		return constructorParameters;
	}

	private void addViolation() {

	}

	private <A> A getAnnotation(Annotation[] annotations, Class<A> type) {
		for (Annotation annotation : annotations) {
			if (type.equals(annotation.annotationType())) {
				return type.cast(annotation);
			}
		}
		return null;
	}

	private List<Constructor<?>> selectConstructors() {
		List<Constructor<?>> constructors = new ArrayList<>();
		for (Constructor<?> constructor : type.getConstructors()) {

			if (constructorParametersHasAnnotation(constructor, In.class)) {
				constructors.add(constructor);
			}
		}
		return constructors;
	}

	@SafeVarargs
	private final boolean constructorParametersHasAnnotation(Constructor<?> constructor,
			Class<? extends Annotation>... annotations) {
		Set<Class<? extends Annotation>> types = Sets.newHashSet(annotations);
		for (Annotation[] byParam : constructor.getParameterAnnotations()) {
			for (Annotation p : byParam) {

				if (types.contains(p.annotationType())) {
					return true;
				}
			}
		}
		return false;
	}

	public void getIns() {

	}

}
