package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

abstract class Literal<T> extends Value {

	private final T value;
	private final TypeHint hint;

	protected Literal(T value, TypeHint hint) {
		this.value = value;
		this.hint = hint;
	}

	public T getValue() {
		return value;
	}

	@Override
	public TypeHint getHint(Context context) {
		return hint;
	}
}
