package eu.k5.cell.activiti.builder;

import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.ast.Query;
import eu.k5.cell.activiti.ast.QueryTransformer;
import eu.k5.cell.activiti.delegate.FindTaskDefinition;
import eu.k5.cell.activiti.delegate.InstanceQueryDefinition;
import eu.k5.cell.activiti.delegate.ListInstancesDefinition;
import eu.k5.cell.activiti.delegate.ListTaskDefinition;
import eu.k5.cell.activiti.delegate.TaskQueryDefinition;
import eu.k5.cell.activiti.query.AqlLexer;
import eu.k5.cell.activiti.query.AqlParser;
import eu.k5.cell.activiti.query.AqlParser.SectionContext;

public abstract class QueryDelegateBuilder<T> {
	private static final Logger LOGGER = LoggerFactory.getLogger(QueryDelegateBuilder.class);
	private String queryString;
	private String sql;
	private boolean init = false;
	private boolean parsed = false;

	private Violations violations;
	private QueryParameter[] parameters;
	private Query query;
	private Context[] context;

	public QueryDelegateBuilder<T> withQueryString(String query) {
		System.out.println(query);
		queryString = query;
		return this;
	}

	public QueryDelegateBuilder<T> withViolations(Violations violations) {
		this.violations = violations;
		return this;
	}

	public QueryDelegateBuilder<T> withContext(List<? extends Context> context) {

		this.context = context.toArray(new Context[context.size()]);
		return this;
	}

	public void init() {
		if (init) {
			return;
		}

		if ("".equals(queryString)) {
			queryString = "true";
		}
		ensureParsed();
		System.out.println(sql);
		init = true;
	}

	private void ensureParsed() {
		if (parsed) {
			return;
		}
		QueryTransformer parse = parse();
		query = parse.getQuery();

		parsed = true;
	}

	private QueryTransformer parse() {
		LOGGER.debug("Parsing {}", queryString);
		CharStream stream = new ANTLRInputStream(queryString);
		AqlLexer lexer = new AqlLexer(stream);

		CommonTokenStream tokens = new CommonTokenStream(lexer);

		AqlParser parser = new AqlParser(tokens);

		SectionContext section = parser.section();

		ParseTreeWalker walker = new ParseTreeWalker();
		QueryTransformer transformer = new QueryTransformer();
		walker.walk(transformer, section);
		return transformer;
	}

	private void ensureInit() {
		init();
		if (!violations.isEmpty()) {
			throw new IllegalStateException("Not initialized");
		}
	}

	public Query getQuery() {
		ensureInit();
		return query;
	}

	public QueryDelegateBuilder<T> withParameters(QueryParameter[] parameters) {
		this.parameters = parameters;
		return this;
	}

	public T build() {
		ensureInit();

		return create();
	}

	public QueryParameter[] getParameters() {
		return parameters;
	}

	protected abstract T create();

	public static QueryDelegateBuilder<ListTaskDefinition> listTasks() {
		return new QueryDelegateBuilder<ListTaskDefinition>() {

			@Override
			protected ListTaskDefinition create() {
				return TaskQueryDefinition.list(getQuery(), getParameters(), getContext());
			}

		};
	}

	public static QueryDelegateBuilder<FindTaskDefinition> findTasks() {
		return new QueryDelegateBuilder<FindTaskDefinition>() {

			@Override
			public FindTaskDefinition create() {
				return TaskQueryDefinition.find(getQuery(), getParameters(), getContext());
			}

		};
	}

	public static QueryDelegateBuilder<ListInstancesDefinition> listInstances() {
		return new QueryDelegateBuilder<ListInstancesDefinition>() {

			@Override
			protected ListInstancesDefinition create() {
				return InstanceQueryDefinition.list(getQuery(), getParameters(), getContext());
			}

		};
	}

	public static QueryDelegateBuilder<ListInstancesDefinition> findInstance() {
		return new QueryDelegateBuilder<ListInstancesDefinition>() {

			@Override
			protected ListInstancesDefinition create() {
				return InstanceQueryDefinition.list(getQuery(), getParameters(), getContext());
			}
		};
	}

	public Context[] getContext() {
		return context;
	}

	public Set<String> getRequiredVariables() {
		ensureParsed();
		return query.getVariables();
	}

	public List<String> getRequiredParameters() {
		ensureParsed();
		return query.getNamedParameters();
	}

	public QueryDelegateBuilder<T> withContext(Context... context) {
		this.context = context;
		return this;
	}

}
