package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

abstract class Expression {

	public abstract TypeHint getHint(Context context);

	public abstract void accept(QueryVisitor visitor);
}
