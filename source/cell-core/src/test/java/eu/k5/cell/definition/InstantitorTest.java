package eu.k5.cell.definition;

import org.junit.Assert;
import org.junit.Test;

import eu.k5.cell.parser.Instantiator;
import eu.k5.cell.parser.InstantiatorBuilder;


public class InstantitorTest {

	public static class Instant1 {

		public Double field;

		public Float property;

		public String name;

		public Integer value1;

		public Instant1(String name, Integer value1) {
			this.name = name;
			this.value1 = value1;

		}

		public void setProperty(Float property) {
			this.property = property;
		}
	}

	@Test
	public void test() throws NoSuchFieldException, SecurityException, NoSuchMethodException {
		Object[] board = new Object[4];
		board[0] = "name";
		board[1] = 1;
		board[2] = 1.0d;
		board[3] = 2.0f;

		InstantiatorBuilder<Instant1> builder = InstantiatorBuilder.create(Instant1.class)
				.addField(2, Instant1.class.getDeclaredField("field"))
				.addProperty(3, Instant1.class.getMethod("setProperty", Float.class));
		Instantiator<Instant1> instance = builder.withConstructorIndex(new int[] { 0, 1 }, Instant1.class.getConstructor(String.class, Integer.class))
				.build();
		
		Instant1 i = instance.instantiate(board);
		
		Assert.assertEquals("name", i.name);
		Assert.assertEquals(Integer.valueOf(1), i.value1);
		Assert.assertEquals(Double.valueOf(1.0d), i.field);
		Assert.assertEquals(Float.valueOf(2.0f), i.property);

	}
}
