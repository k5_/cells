package eu.k5.cell.definition;

import eu.k5.cell.extension.Binding;

public interface PropertyReader {

	Object read(Object object);

	boolean valid(Object object);

	Binding getBinding();

}
