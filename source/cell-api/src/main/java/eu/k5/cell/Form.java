package eu.k5.cell;

import org.slf4j.helpers.MessageFormatter;

class Form {
	static String at(String format, Object... variables) {
		return MessageFormatter.arrayFormat(format, variables).getMessage();
	}

}
