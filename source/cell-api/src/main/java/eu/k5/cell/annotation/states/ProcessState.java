package eu.k5.cell.annotation.states;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
public @interface ProcessState {

	ProcessStateType value() default ProcessStateType.DERIVED;

	Class<? extends ProcessStateResolver> resolver() default ProcessStateResolver.class;

}
