package eu.k5.cell.activiti.ast;

import java.util.List;

class OrExpression extends NaryExpression {

	public OrExpression(List<Expression> parts) {
		super(parts);
	}

	@Override
	public void accept(QueryVisitor visitor) {
		visitor.visit(this);
	}
	

}
