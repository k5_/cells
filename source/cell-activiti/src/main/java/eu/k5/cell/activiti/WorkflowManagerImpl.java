package eu.k5.cell.activiti;

import java.lang.reflect.Proxy;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.k5.cell.CellException;
import eu.k5.cell.InvalidReferenceException;
import eu.k5.cell.NoDefinitionException;
import eu.k5.cell.NonUniqueResultException;
import eu.k5.cell.WorkflowManager;
import eu.k5.cell.activiti.delegate.TaskKey;
import eu.k5.cell.activiti.filter.CellTaskReferenceFilter;
import eu.k5.cell.definition.StartDefinition;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.definition.WorkflowDefinition;
import eu.k5.cell.extension.Cell;
import eu.k5.cell.shared.CellTaskReference;
import eu.k5.cell.wao.DelegateInvocation;
import eu.k5.cell.wao.Wao;
import eu.k5.cell.wao.WaoDefinition;

public class WorkflowManagerImpl implements WorkflowManager {
	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowManagerImpl.class);
	private final Cell cell;

	private final RuntimeService runtimeService;
	private final HistoryService historyService;
	private final TaskService taskService;

	private final ManagementService managementService;

	public WorkflowManagerImpl(Cell cell, RuntimeService runtimeService, HistoryService historyService,
			TaskService taskService, ManagementService managementService) {
		this.cell = cell;
		this.runtimeService = runtimeService;
		this.historyService = historyService;
		this.taskService = taskService;
		this.managementService = managementService;
	}

	@Override
	public void start(Object start) {
		if (start == null) {
			throw new NullPointerException(Form.at("Cell {}: Unable to start with null", cell.getName()));
		}

		StartDefinition definition = cell.getStart(start.getClass());
		if (definition == null) {
			throw new NoDefinitionException(
					Form.at("Cell {}: No StartDefinition found for {}", cell.getName(), start.getClass()));
		}

		WorkflowDefinition workflow = definition.getWorkflow();

		Map<String, Object> variables = definition.write(start);
		LOGGER.info("Cell {}: Starting process {}  with variables {}", cell.getName(),
				definition.getWorkflow().getProcessDefinition(), variables);

		if (definition.useBusinessKey()) {
			runtimeService.startProcessInstanceByKey(workflow.getProcessDefinition(), definition.getBusinessKey(start),
					variables);
		} else {
			runtimeService.startProcessInstanceByKey(workflow.getProcessDefinition(), variables);
		}
	}

	@Override
	public void complete(Object task) {
		if (task == null) {
			throw new NullPointerException(Form.at("Cell {}: Unable to complete null as task", cell.getName()));
		}

		LOGGER.debug("Cell {}: Attempting to complete task with type {}", cell.getName(), task.getClass());
		List<TaskDefinition> definitions = cell.resolveTaskDefinition(task);

		if (definitions.isEmpty()) {
			throw new NoDefinitionException(
					Form.at("Cell {}: No TaskDefinition found for task {}", cell.getName(), task.getClass()));
		} else if (definitions.size() > 1) {
			throw new NonUniqueResultException(
					Form.at("Cell {}: Multiple TaskDefinitions can complete task {}", cell.getName(), task.getClass()));
		}

		TaskDefinition definition = definitions.get(0);

		CellTaskReference reference = definition.getTaskReference(task);
		Task aTask;
		try {
			aTask = new CellTaskReferenceFilter(reference).filter(taskService.createTaskQuery()).singleResult();
			if (aTask == null) {
				throw new InvalidReferenceException(
						Form.at("Cell {}: task for CellReference not found {}", cell.getName(), reference));
			}
		} catch (ActivitiException e) {
			throw new InvalidReferenceException(
					Form.at("Cell {}: task for CellReference not found {}", cell.getName(), reference), e);
		}

		Map<String, Object> variables = definition.write(task);
		LOGGER.debug("Cell {}: Completing task {} with {}", cell.getName(), aTask, variables);
		taskService.complete(aTask.getId(), variables);
	}

	@Override
	public <U> U unwrap(Class<U> type) {
		if (RuntimeService.class.equals(type)) {
			return type.cast(runtimeService);
		} else if (HistoryService.class.equals(type)) {
			return type.cast(historyService);
		} else if (TaskService.class.equals(type)) {
			return type.cast(taskService);
		} else if (Cell.class.equals(type)) {
			return type.cast(cell);
		} else if (ManagementService.class.equals(type)) {
			return type.cast(managementService);
		}
		throw new CellException(Form.at("Unable to unwrap {}", type));
	}

	@Override
	public <D> D wao(Class<D> type) {
		WaoDefinition definition = cell.findDelegate(type);
		if (definition == null) {
			throw new NoDefinitionException(
					Form.at("Cell {}: No WaoDefinition found for type {}", cell.getName(), type));
		}
		Wao pao = definition.bind(this);

		return type.cast(Proxy.newProxyInstance(CellManagerImpl.class.getClassLoader(), new Class<?>[] { type },
				new DelegateInvocation(pao)));
	}

	@Override
	public <T> T find(Class<T> cls, String query, Object... parameters) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> list(Class<T> cls, String query, Object... parameters) {
		Collection<TaskDefinition> taskDefinition = cell.getTaskDefinition(cls);
		if (taskDefinition.isEmpty()) {
			return Collections.emptyList();
		}
		return null;

		// query = "(taskDefinitionKey=:taskDefinitionKey AND
		// processDefinitionKey=:processDefinitionKey) AND " + query;
		//
		// Violations violations = new Violations();
		// ParameterBuilder parameterBuilder = new
		// ParameterBuilder().withViolations(violations).forVarargs(parameters);
		// QueryDelegateBuilder<ListTaskDefinition> builder =
		// QueryDelegateBuilder.listTasks().withViolations(violations)
		// .withParameters(parameterBuilder.create()).withQueryString(query);
		//
		// List<Context> context = createTaskQueryContext(taskDefinition,
		// parameterBuilder.getBounds(),
		// builder.getRequiredVariables());
		// builder.withContext(context);
		//
		// List<T> result = new ArrayList<>();
		// Map<TaskKey, TaskDefinition> definitionsByKey =
		// taskDefinitions(taskDefinition);
		// for (TaskEntity task : builder.build().invoke(managementService,
		// parameters)) {
		// TaskDefinition definition =
		// definitionsByKey.get(TaskKey.fromTask(task));
		// result.add(cls.cast(definition.read(task)));
		// }
		// return result;
		// }
		//
		// private List<Context>
		// createTaskQueryContext(Collection<TaskDefinition> definitions,
		// Map<String, ParameterBound> parameters, Set<String> variables) {
		// List<Context> context = new ArrayList<>();
		//
		// for (TaskDefinition definition : definitions) {
		// Map<String, ParameterBound> local = new HashMap<>(parameters);
		// local.put("taskDefinitionKey",
		// new ParameterLiteralBound(TypeHint.STRING,
		// definition.getTaskDefinitionKey()));
		// local.put("processDefinitionKey",
		// new ParameterLiteralBound(TypeHint.STRING,
		// definition.getWorfklow().getProcessDefinition()));
		//
		// Map<String, VariableBound> bounds =
		// definition.getWorfklow().getBinder().getVariables(variables);
		// context.add(new Context(bounds, local));
		// }
		// return context;
	}

	private Map<TaskKey, TaskDefinition> taskDefinitions(Collection<TaskDefinition> definitions) {
		Map<TaskKey, TaskDefinition> result = new HashMap<>();
		for (TaskDefinition definition : definitions) {
			result.put(TaskKey.fromDefintion(definition), definition);
		}
		return result;
	}

}
