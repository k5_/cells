package eu.k5.cell;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.inject.spi.Annotated;
import javax.enterprise.inject.spi.InjectionPoint;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import eu.k5.cell.activiti.CellManagerProducer;
import eu.k5.cell.annotation.CellContext;
import eu.k5.cell.extension.Cells;

public class CellManagerProducerTest {
	@InjectMocks
	private CellManagerProducer producer;

	@Mock
	private InjectionPoint ip;
	@Mock
	private Annotated annotated;

	@Mock
	private CellContext context;

	@Mock
	private Cells cells;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		Mockito.when(ip.getAnnotated()).thenReturn(annotated);

		Mockito.when(annotated.getAnnotation(CellContext.class)).thenReturn(context);
	}

	@Test(expected = CellException.class)
	public void produceWorkflow_invalidContext() {
		Mockito.doReturn(Object.class).when(context).value();
		Mockito.when(cells.get(Object.class)).thenReturn(null);
		producer.produceWorkflow(ip);
	}

}
