package eu.k5.cell.activiti.delegate;

import org.activiti.engine.ManagementService;

import eu.k5.cell.activiti.builder.QueryParameter;

public class DelegateFactory {

	public static FindTaskDelegate findTaskDelegate(ManagementService service, String sql, QueryParameter[] parameters) {
		return new FindTaskDelegate(sql, service, parameters);
	}

	public static Delegate variableHint(String name) {
		return new VariableHintDelegate(name);
	}

	public static ListTasksDelegate listTaskDelegate(ManagementService service, String sql, QueryParameter[] parameters) {
		System.out.println(sql);
		return new ListTasksDelegate(sql, service, parameters);
	}

	public static FindInstanceDelegate findInstanceDelegate(ManagementService service, String sql,
			QueryParameter[] parameters) {
		return new FindInstanceDelegate(sql, service, parameters);
	}

	public static ListInstancesDelegate listInstancesDelegate(ManagementService service, String sql,
			QueryParameter[] parameters) {
		return new ListInstancesDelegate(sql, service, parameters);
	}

}
