package eu.k5.cell.parser;

public interface Parser<T> {

	T parse(Context context);
}
