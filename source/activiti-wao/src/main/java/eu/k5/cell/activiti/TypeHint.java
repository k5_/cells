package eu.k5.cell.activiti;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

public enum TypeHint {

	STRING(false), STRING_LIST(true) {
		@Override
		public Object transform(Object list) {
			StringBuilder builder = new StringBuilder();
			builder.append(" '");
			for (Object object : (Iterable<?>) list) {
				builder.append(object.toString().replaceAll("'", "''"));
				builder.append("','");
			}

			return builder.substring(0, builder.length() - 2);

		}
	},
	LONG(false), DOUBLE(false), DATE(false), BOOLEAN(false), INVALID(false);

	private final boolean inline;

	private TypeHint(boolean inline) {
		this.inline = inline;
	}

	public boolean isInline() {
		return inline;
	}

	public Object transform(Object object) {
		return object;
	}

	public static TypeHint forType(Type type) {
		if (String.class.equals(type)) {
			return STRING;
		} else if (Long.class.equals(type)) {
			return LONG;
		} else if (Double.class.equals(type)) {
			return DOUBLE;
		} else if (type instanceof ParameterizedType) {
			ParameterizedType parameterizedType = (ParameterizedType) type;
			if (List.class.equals(parameterizedType.getRawType())
					&& String.class.equals(parameterizedType.getActualTypeArguments()[0])) {
				return STRING_LIST;
			}
		}
		return null;
	}
}
