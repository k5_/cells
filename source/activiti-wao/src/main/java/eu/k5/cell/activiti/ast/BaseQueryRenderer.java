package eu.k5.cell.activiti.ast;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public abstract class BaseQueryRenderer implements QueryVisitor {
	private static final String VARIABLE_PREFIX = "var";

	private final String variableTable;

	private final String identityTable;

	private final DatabaseType type;

	private final String processDefinitionTable;

	private final Deque<RenderContext> context = new ArrayDeque<>();

	private final List<RenderContext> rendered = new ArrayList<>();

	public BaseQueryRenderer(DatabaseType type, String variableTable, String identityTable,
			String processDefinitionTable) {
		this.type = type;
		this.variableTable = variableTable;
		this.identityTable = identityTable;
		this.processDefinitionTable = processDefinitionTable;

	}

	public abstract void setContext(Context context);

	@Override
	public final void visit(Query query) {
		for (Section section : query.getSections()) {

			RenderContext ctx = createContext();
			context.push(ctx);
			section.accept(this);

			rendered.add(context.pop());
		}
	}

	public String getQuery() {
		return rendered.stream().map(c -> c.getQuery()).collect(Collectors.joining(" UNION "));
	}

	protected abstract RenderContext createContext();

	public RenderContext currentContext() {
		return context.peek();
	}

	public String getVariablePrefix() {
		return VARIABLE_PREFIX;
	}

	public RenderContext pushContext(RenderContext context) {
		this.context.push(context);
		return context;
	}

	public RenderContext popContext() {
		return context.pop();
	}

	protected abstract String getResourcePrefix();

	protected abstract String getProcessDefinitionPrefix();

	@Override
	public void visit(UnaryExpression unary) {
		StringBuilder where = currentContext().getWhere();
		where.append(" ( ");
		unary.getExpression().accept(this);
		where.append(" ) ");
	}

	@Override
	public void visit(AndExpression andExpression) {
		boolean first = true;
		for (Expression e : andExpression.getParts()) {
			if (!first) {
				currentContext().getWhere().append(" AND ");
			} else {
				first = false;
			}
			e.accept(this);
		}
	}

	@Override
	public void visit(OrExpression orExpression) {
		boolean first = true;
		for (Expression e : orExpression.getParts()) {
			if (!first) {
				currentContext().getWhere().append(" OR ");
			} else {
				first = false;
			}
			e.accept(this);
		}
	}

	@Override
	public void visit(Condition condition) {
		ConditionRenderer conditionRenderer = new ConditionRenderer(this, context.peek().getContext());

		conditionRenderer.render(condition.getOperator(), condition.getLeft(), condition.getRight());
	}

	protected abstract String addVariable(boolean local);

	protected void selectVariables() {
		StringBuilder select = currentContext().getSelect();

		select.append(VARIABLE_PREFIX).append(".ID_ as VAR_ID_, ");
		select.append(VARIABLE_PREFIX).append(".NAME_ as VAR_NAME_, ");
		select.append(VARIABLE_PREFIX).append(".TYPE_ as VAR_TYPE_, ");
		select.append(VARIABLE_PREFIX).append(".REV_ as VAR_REV_, ");
		select.append(VARIABLE_PREFIX).append(".PROC_INST_ID_ as VAR_PROC_INST_ID_, ");
		select.append(VARIABLE_PREFIX).append(".EXECUTION_ID_ as VAR_EXECUTION_ID_, ");
		select.append(VARIABLE_PREFIX).append(".TASK_ID_ as VAR_TASK_ID_, ");
		select.append(VARIABLE_PREFIX).append(".BYTEARRAY_ID_ as VAR_BYTEARRAY_ID_, ");
		select.append(VARIABLE_PREFIX).append(".DOUBLE_ as VAR_DOUBLE_, ");
		select.append(VARIABLE_PREFIX).append(".TEXT_ as VAR_TEXT_, ");
		select.append(VARIABLE_PREFIX).append(".TEXT2_ as VAR_TEXT2_, ");
		select.append(VARIABLE_PREFIX).append(".LONG_ as VAR_LONG_ ");
	}

	@Override
	public void visit(BooleanExpression booleanExpression) {
		boolean value = booleanExpression.getValue();
		if (value) {
			currentContext().getWhere().append(" ").append(getType().trueLiteral()).append(" ");
		} else {
			currentContext().getWhere().append(" ").append(getType().falseLiteral()).append(" ");

		}
	}

	@Override
	public void visit(Limit limit) {
		// limit.getLength().accept(this);
		// limit.getOffset().accept(this);
	}

	public String getVariableTable() {
		return variableTable;
	}

	public String getIdentityTable() {
		return identityTable;
	}

	public String getIdentityPrefix() {
		return "I";
	}

	public DatabaseType getType() {
		return type;
	}

	public String getProcessDefinitionTable() {
		return processDefinitionTable;
	}

	public abstract class RenderContext {
		private final StringBuilder prefix = new StringBuilder();

		private final StringBuilder select = new StringBuilder();

		private final StringBuilder from = new StringBuilder();

		private final StringBuilder where = new StringBuilder();

		private final AtomicInteger variableIndex = new AtomicInteger();

		private final StringBuilder suffix = new StringBuilder();

		private boolean identityAdded = false;

		private final Context context;

		public RenderContext(Context context) {
			this.context = context;
		}

		public StringBuilder getFrom() {
			return from;
		}

		public StringBuilder getSelect() {
			return select;
		}

		public StringBuilder getWhere() {
			return where;
		}

		public StringBuilder getPrefix() {
			return prefix;
		}

		public StringBuilder getSuffix() {
			return suffix;
		}

		public Context getContext() {
			return context;
		}

		public String getQuery() {
			return prefix.toString() + select.toString() + from.toString() + where.toString() + suffix.toString();
		}

		public String getWherePart() {
			return where.toString();
		}

		protected void includeIdentity() {
			if (identityAdded) {
				return;
			}

			from.append("join ").append(getIdentityTable()).append(" ").append(getIdentityPrefix()).append(" on ")
					.append(getIdentityPrefix()).append(".TASK_ID_ = ").append(getResourcePrefix()).append(".ID_ ");

			identityAdded = true;
		}

		public AtomicInteger getVariableIndex() {
			return variableIndex;
		}

		public abstract void includeProcessDefinition();
	}

}
