package eu.k5.cell.extension;

import eu.k5.cell.annotation.states.ProcessStateResolver;

public class BindDerivedProcessState implements ProcessStateResolver {

	@Override
	public String read(Object state) {
		return "bindDerived";
	}

	@Override
	public Class<?> getReturnType() {
		return String.class;
	}

}
