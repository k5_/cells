package eu.k5.cell;

import eu.k5.cell.annotation.states.JobStateResolver;
import eu.k5.cell.annotation.states.JobStateType;
import eu.k5.cell.annotation.states.ProcessStateResolver;
import eu.k5.cell.annotation.states.ProcessStateType;
import eu.k5.cell.annotation.states.TaskStateResolver;
import eu.k5.cell.annotation.states.TaskStateType;

public interface TaskStateResolverFactory {

	TaskStateResolver taskReader(TaskStateType type);

	ProcessStateResolver stateReader(ProcessStateType type);

	JobStateResolver jobReader(JobStateType value);

}
