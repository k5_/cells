package eu.k5.cell.model.job;

import eu.k5.cell.annotation.In;
import eu.k5.cell.annotation.Out;
import eu.k5.cell.annotation.states.JobState;
import eu.k5.cell.annotation.states.JobStateType;

public class JobJob {
	static final String IGNORE = "ignore";
	@In
	private String key;

	@In
	private String property;

	@JobState(resolver = JobDerivedJobState.class)
	private String derived;

	@JobState(JobStateType.INSTANCE_ID)
	private String instance;

	@In
	@Out
	private String inOut;

	@Out
	private String defaultOut = "defaultValue";

	@Out
	private String property3;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getProperty3() {
		return property3;
	}

	public void setProperty3(String property3) {
		this.property3 = property3;
	}

	public String getDefaultOut() {
		return defaultOut;
	}

	public void setDefaultOut(String defaultOut) {
		this.defaultOut = defaultOut;
	}

	public String getInOut() {
		return inOut;
	}

	public void setInOut(String inOut) {
		this.inOut = inOut;
	}

	public String getDerived() {
		return derived;
	}

	public void setDerived(String derived) {
		this.derived = derived;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

}
