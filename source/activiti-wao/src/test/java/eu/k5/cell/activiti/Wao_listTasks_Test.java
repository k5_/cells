package eu.k5.cell.activiti;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import eu.k5.cell.activiti.annotation.Param;
import eu.k5.cell.activiti.annotation.Query;

public class Wao_listTasks_Test extends AbstractExample {

	public interface Variables {
		@VariableHint
		String name();

		@VariableHint
		String stringVar2();

		@VariableHint
		Long longVar();

	}

	public interface WaoList extends Variables {

		@Query("")
		List<Task> list();

	}

	public <U, T extends U> List<U> call(T t) {
		return null;
	}

	public interface Wao extends Variables {

		@Query("taskDefinitionKey = :key AND var.name=:name")
		List<Task> listByDefinition(@Param("key") String key, @Param("name") String name);

		@Query("var.name=:name")
		List<Task> listByStringVariable(@Param("name") String name);

		@Query("var.longVar=:longVar")
		List<Task> listByLongVariable(@Param("longVar") Long longVar);

	}

	public interface VarToVar extends Variables {

		@Query("var.name=var.stringVar2")
		List<Task> varToVar();

		@Query("var.name!=var.stringVar2")
		List<Task> varNotToVar();
	}

	@Test
	public void list_oneavailable() {
		start(2);
		Assert.assertEquals(1, getWao(WaoList.class).list().size());
	}

	@Test
	public void filterOnStringVariable() {
		start(1, "name", "test");
		start(1, "name", "test2");

		Assert.assertEquals(1, getWao(Wao.class).listByStringVariable("test").size());
	}

	@Test
	public void filterOnLongVariable() {
		start(1, "longVar", 1l);
		start(1, "longVar", 2l);

		Assert.assertEquals(1, getWao(Wao.class).listByLongVariable(1l).size());
	}

	@Test
	public void filterVarToVar() {
		start(1, "name", "value", "stringVar2", "value");
		start(1, "name", "value", "stringVar2", "value2");

		Assert.assertEquals(1, getWao(VarToVar.class).varToVar().size());
	}

	@Test
	public void filterVarNotToVar() {
		start(1, "name", "value", "stringVar2", "value");
		start(1, "name", "value", "stringVar2", "value2");

		Assert.assertEquals(1, getWao(VarToVar.class).varNotToVar().size());
	}

	public interface ListWithLimit {

		@Query("true LIMIT :offset, :length")
		List<Task> withOffsetAndLimit(@Param("offset") int offset, @Param("length") int length);

		@Query("true LIMIT :offset, 1")
		List<Task> withOffset(@Param("offset") int offset);

		@Query("true LIMIT 1, :limit")
		List<Task> withLimit(@Param("limit") int limit);

	}

	@Test
	@Ignore
	public void listWithLimit() {
		start(1);
		start(1);
		start(1);
		Assert.assertEquals(1, getWao(ListWithLimit.class).withOffsetAndLimit(0, 1).size());
	}

	public interface Candidate extends Variables {

		@Query("assignee=:assign")
		List<Task> listAssignee(@Param("assign") String assign);

		@Query("candidate IN :candidates")
		List<Task> list(@Param("candidates") List<String> candidates);

	}

	@Test
	public void listCandidates() {
		start(3, "candidates", new ArrayList<String>(Arrays.asList("testCandidate")));

		Assert.assertEquals(1, getWao(Candidate.class).list(Arrays.asList("testCandidate")).size());

	}

	@Test
	public void listDifferentCandidates() {
		start(3, "candidates", new ArrayList<String>(Arrays.asList("testCandidate", "testCandidate1")));
		start(3, "candidates", new ArrayList<String>(Arrays.asList("testCandidate", "testCandidate2")));
		start(3, "candidates", new ArrayList<String>(Arrays.asList("testCandidate", "testCandidate3")));

		Assert.assertEquals(3, getWao(Candidate.class).list(Arrays.asList("testCandidate")).size());
		Assert.assertEquals(2, getWao(Candidate.class).list(Arrays.asList("testCandidate1", "testCandidate2")).size());
		Assert.assertEquals(
				3,
				getWao(Candidate.class).list(
						Arrays.asList("testCandidate1", "testCandidate2", "testCandidate3", "testCandidate")).size());

	}

	@Test
	public void listAssignee() {
		start(1);
		Assert.assertEquals(1, getWao(Candidate.class).listAssignee("testAssignee").size());
		Assert.assertEquals(0, getWao(Candidate.class).listAssignee("notAssignned").size());

	}

	public interface ByInstance extends Variables {
		@Query("processInstanceId = :instance")
		List<Task> listByInstance(@Param("instance") String instance);

		@Query("")
		List<ProcessInstance> listAll();
	}

	@Test
	public void listTaskByProcessInstance() {
		start(1).start(1);
		ByInstance wao = getWao(ByInstance.class);
		List<ProcessInstance> instances = wao.listAll();

		Assert.assertEquals(2, instances.size());
		Assert.assertEquals(1, wao.listByInstance(instances.get(0).getId()).size());
	}
}
