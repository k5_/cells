package eu.k5.cell.model.task;

import eu.k5.cell.annotation.Bind;
import eu.k5.cell.annotation.workflow.Data;
import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Task;
import eu.k5.cell.annotation.workflow.Workflow;

@Workflow(processKey = "taskProcess",binder = TaskWorkflow.Binder.class,activity = TaskWorkflow.Activity.class)
public interface TaskWorkflow {

	interface Binder {

		void setKey(String key);

		@Bind(businessKey = true)
		String getKey();

		void setProperty(String key);

		@Bind
		String getProperty();

		void setProperty2(int property2);

		@Bind
		String getProperty2();

		void setProperty3(int property3);

		@Bind
		String getProperty3();

		String getDefaultOut();

		String getInOut();

	}

	interface Activity {
		@Start
		TaskStart getStart1();

		@Task(definition = "TestTask")
		TaskTask getTask();

		@Task(definition = "TestTask")
		TaskUnbound unbound();

		@Data
		TaskData getTaskData();
	}

}
