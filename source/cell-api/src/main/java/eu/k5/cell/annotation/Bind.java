package eu.k5.cell.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, })
public @interface Bind {

	public static final String DEFAULT = "###default";

	String name() default DEFAULT;

	boolean businessKey() default false;

	boolean isFinal() default false;

}
