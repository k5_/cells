package eu.k5.cell.activiti.delegate;

import java.util.Map;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.persistence.entity.TaskEntity;

import eu.k5.cell.activiti.TaskWithVariablesExecution;
import eu.k5.cell.activiti.builder.QueryParameter;

class FindTaskDelegate extends ParameterMappingDelegate {

	public FindTaskDelegate(String sql, ManagementService service, QueryParameter[] parameters) {
		super(sql, service, parameters);
	}

	@Override
	public TaskEntity invoke(Object... arguments) {
		Map<String, Object> parameters = getParameters(arguments);
		return getService().executeCustomSql(new TaskWithVariablesExecution(parameters));
	}

}
