package eu.k5.cell.activiti.delegate;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.ManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.k5.cell.activiti.AqlMapper;
import eu.k5.cell.activiti.builder.QueryParameter;

abstract class ParameterMappingDelegate implements Delegate {
	private static final Logger LOGGER = LoggerFactory.getLogger(ParameterMappingDelegate.class);

	private final QueryParameter[] parameters;
	private final String sql;
	private final ManagementService service;

	public ParameterMappingDelegate(String sql, ManagementService service, QueryParameter[] parameters) {
		this.sql = sql;
		this.service = service;
		this.parameters = parameters;
	}

	public Map<String, Object> getParameters(Object[] arguments) {
		Map<String, Object> parameterMap = new HashMap<String, Object>();

		String sql = this.sql;
		for (int index = 0; index < parameters.length; index++) {
			QueryParameter parameter = parameters[index];
			if (parameter.isInline()) {
				sql = sql.replaceAll("#\\{" + parameter.getName() + "}", parameter.transform(arguments).toString());
			} else {
				parameterMap.put(parameter.getName(), parameter.transform(arguments));
			}
		}
		LOGGER.debug("Sql: {}", sql);
		parameterMap.put(AqlMapper.SQL, sql);
		parameterMap.put("limitFrom", getLimitOffset(arguments));
		parameterMap.put("limitTo", getLimitLength(arguments));
		return parameterMap;
	}

	protected int getLimitOffset(Object[] arguments) {
		return 0;
	}

	protected int getLimitLength(Object[] arguments) {
		return Integer.MAX_VALUE;
	}

	public ManagementService getService() {
		return service;
	}

}