package eu.k5.cell.model.start;

import eu.k5.cell.annotation.Bind;
import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Workflow;

@Workflow(processKey = "startProcess", binder = StartWorkflow.Binder.class, activity = StartWorkflow.Activity.class)
public interface StartWorkflow {

	interface Binder {

		void setKey(String key);

		@Bind(businessKey = true, name = "ey")
		String getKey();

		void setProperty(String key);

		@Bind
		String getProperty();

		void setProperty2(int property2);

		@Bind
		String getProperty2();

		void setProperty3(int property3);

		@Bind
		String getProperty3();

	}

	interface Activity {
		@Start
		StartStart getStart1();

	}

}
