package eu.k5.cell.extension;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import eu.k5.cell.definition.AssignerBound;

public class Reader {

	private final Constructor<?> constructor;
	private final List<Binding> constructorBindings;
	private final List<AssignerBound> assigners;

	public Reader(Constructor<?> constructor, List<Binding> constructorBindings, List<AssignerBound> assigners) {
		this.constructor = constructor;
		this.constructorBindings = constructorBindings;
		this.assigners = assigners;
	}

	public Object read(Map<String, Object> variables) {

		Object[] parameters = new Object[constructorBindings.size()];
		int index = 0;
		for (Binding binding : constructorBindings) {
			parameters[index++] = variables.get(binding.getBind());
		}
		try {

			Object instance = constructor.newInstance(parameters);
			for (AssignerBound assigner : assigners) {
				assigner.assign(instance, variables);
			}
			return instance;
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			throw new IllegalArgumentException("unable to create instance", e);
		}
	}

	public List<AssignerBound> getAssigners() {
		return assigners;
	}
}
