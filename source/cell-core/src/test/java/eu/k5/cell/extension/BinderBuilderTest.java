package eu.k5.cell.extension;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.k5.cell.annotation.Bind;
import eu.k5.cell.annotation.states.JobState;
import eu.k5.cell.annotation.states.JobStateType;
import eu.k5.cell.annotation.states.ProcessState;
import eu.k5.cell.annotation.states.ProcessStateType;
import eu.k5.cell.annotation.states.TaskState;
import eu.k5.cell.annotation.states.TaskStateType;
import eu.k5.cell.definition.JobStateReader;
import eu.k5.cell.definition.ProcessStateReader;
import eu.k5.cell.definition.TaskStateReader;

public class BinderBuilderTest {

	private Validator validator;

	@Before
	public void init() {
		validator = new Validator();
	}

	public interface Binder1 {
		String getBind();

		@Bind(businessKey = true)
		String getBusinessKey();

		@Bind(isFinal = true)
		String getFinal();
	}

	@Test
	public void bind_simple() {
		Binder binder = forClass(Binder1.class).build();
		Binding binding = binder.getBinding("bind");
		Assert.assertNotNull(binding);
		Assert.assertEquals(String.class, binding.getType());
		Assert.assertFalse(binding.isBusinessKey());
		Assert.assertFalse(binding.isFinal());
	}

	@Test
	public void bind_businessKey() {
		Binder binder = forClass(Binder1.class).build();
		Binding binding = binder.getBinding("businessKey");

		Assert.assertNotNull(binding);
		Assert.assertEquals(String.class, binding.getType());
		Assert.assertTrue(binding.isBusinessKey());
		Assert.assertTrue(binding.isFinal());
	}

	@Test
	public void bind_final() {
		Binder binder = forClass(Binder1.class).build();
		Binding binding = binder.getBinding("final");

		Assert.assertNotNull(binding);
		Assert.assertEquals(String.class, binding.getType());
		Assert.assertFalse(binding.isBusinessKey());
		Assert.assertTrue(binding.isFinal());
	}

	public interface BindProcessState {

		@ProcessState(value = ProcessStateType.INSTANCE_ID)
		String getState();

		@ProcessState(value = ProcessStateType.DERIVED, resolver = BindDerivedProcessState.class)
		String getDerivedState();
	}

	@Test
	public void bind_processState() {
		Binder binder = forClass(BindProcessState.class).build();

		Assert.assertNull(binder.getBinding("state"));
		ProcessStateReader state = binder.getProcessState("state");
		Assert.assertEquals(ProcessStateType.INSTANCE_ID, state.getType());
	}

	@Test
	public void bind_processState_derived() {
		Binder binder = forClass(BindProcessState.class).build();

		Assert.assertNull(binder.getBinding("derivedState"));
		ProcessStateReader state = binder.getProcessState("derivedState");
		Assert.assertEquals(ProcessStateType.DERIVED, state.getType());
		Assert.assertTrue(state.getReader() instanceof BindDerivedProcessState);
	}

	public interface BindJobState {
		@JobState(value = JobStateType.INSTANCE_ID)
		String getState();

		@JobState(resolver = BindDerivedJobState.class)
		String getDerivedState();

	}

	@Test
	public void bind_jobState() {
		Binder binder = forClass(BindJobState.class).build();

		Assert.assertNull(binder.getBinding("state"));
		JobStateReader state = binder.getJobState("state");
		Assert.assertNotNull(state);
		Assert.assertEquals(JobStateType.INSTANCE_ID, state.getType());
	}

	@Test
	public void bind_jobDerivedState() {
		Binder binder = forClass(BindJobState.class).build();

		Assert.assertNull(binder.getBinding("derivedState"));
		JobStateReader state = binder.getJobState("derivedState");
		Assert.assertNotNull(state);
		Assert.assertEquals(JobStateType.DERIVED, state.getType());
		Assert.assertTrue(state.getReader() instanceof BindDerivedJobState);
	}

	public interface BindTaskState {
		@TaskState(TaskStateType.ASSIGNEE)
		String getState();

		@TaskState(resolver = BindDerivedTaskState.class)
		String getDerivedState();
	}

	@Test
	public void bind_taskState() {
		Binder binder = forClass(BindTaskState.class).build();

		Assert.assertNull(binder.getBinding("state"));
		TaskStateReader state = binder.getTaskState("state");
		Assert.assertNotNull(state);
		Assert.assertEquals(TaskStateType.ASSIGNEE, state.getType());
	}

	@Test
	public void bind_taskDerivedState() {
		Binder binder = forClass(BindTaskState.class).build();

		Assert.assertNull(binder.getBinding("derivedState"));
		TaskStateReader state = binder.getTaskState("derivedState");
		Assert.assertNotNull(state);
		Assert.assertEquals(TaskStateType.DERIVED, state.getType());
		Assert.assertTrue(state.getReader() instanceof BindDerivedTaskState);
	}

	private BinderBuilder forClass(Class<?> cls) {
		return new BinderBuilder().withValidator(validator).addClass(cls).init();
	}

}
