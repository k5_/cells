package eu.k5.cell.activiti.ast;

public enum DatabaseType {
	MSSQ("mssql") {
		@Override
		public String limitOuterJoinBetween() {
			return " ,row_number() over (ORDER BY TEMPRES_ID_ asc) rnk FROM ( select distinct ";
		}

		@Override
		public String limitBefore() {
			return "SELECT SUB.* FROM ( ";
		}

		@Override
		public String limitAfter() {
			return " )RES ) SUB WHERE SUB.rnk >= #{limitFrom} AND SUB.rnk < #{limitTo} ";
		}

		@Override
		public String trueLiteral() {
			return "1=1";
		};

		@Override
		public String falseLiteral() {
			return "1=2";
		};
	},
	H2("h2") {
		@Override
		public String limitOuterJoinBetween() {
			return "";
		}

		@Override
		public String limitBefore() {
			return "";
		}

		@Override
		public String limitAfter() {
			return "";
		}

	};

	private final String token;

	private DatabaseType(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public static DatabaseType resolve(String token) {
		for (DatabaseType type : values()) {
			if (type.getToken().equals(token)) {
				return type;
			}
		}
		return null;
	}

	public abstract String limitOuterJoinBetween();

	public abstract String limitBefore();

	public abstract String limitAfter();

	public String trueLiteral() {
		return "TRUE";
	};

	public String falseLiteral() {
		return "FALSE";
	}
}
