package eu.k5.cell.activiti.delegate;


class VariableHintDelegate implements Delegate {
	private final String name;

	public VariableHintDelegate(String name) {
		super();
		this.name = name;
	}

	@Override
	public Object invoke(Object... args) {
		throw new UnsupportedOperationException("Variable Hint method for '" + name + "' should not be used");
	}
}