package eu.k5.cell.generator;

import java.io.IOException;

import eu.k5.cell.parser.InstantiatorBuilder;

public class InstantiatorExp {
	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IOException {
		InstantiatorBuilder<Example> example = new InstantiatorBuilder<>(Example.class);

		example.withConstructorIndex(new int[] { 0, 1 }, Example.class.getConstructor(String.class, String.class));

		example.addProperty(2, Example.class.getMethod("setParam3", String.class));
		GeneratorContext context = new GeneratorContext();
		InstantiatorGenerator gen = new InstantiatorGenerator(context);

		gen.generate(example.build());

		context.getSources().writeSources();
		context.dump();
	}

}
