package eu.k5.cell.activiti;

import java.util.Map;

import org.activiti.engine.runtime.ProcessInstance;

import eu.k5.cell.CellProcessInstance;

public class ActivitiCellProcessInstance implements CellProcessInstance {
	private final ProcessInstance processInstance;

	public ActivitiCellProcessInstance(ProcessInstance processInstance) {
		this.processInstance = processInstance;
	}

	@Override
	public Map<String, Object> getProcessVariables() {
		return processInstance.getProcessVariables();
	}

}
