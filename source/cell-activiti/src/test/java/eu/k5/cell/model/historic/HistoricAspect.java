package eu.k5.cell.model.historic;

import eu.k5.cell.annotation.workflow.Task;

public class HistoricAspect {

	static final String IGNORE = "ignore";

	@Task
	private HistoricTask task;

	public HistoricTask getTask() {
		return task;
	}

	public void setTask(HistoricTask task) {
		this.task = task;
	}

}
