package eu.k5.cell.model.composite;

import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Task;
import eu.k5.cell.annotation.workflow.Workflow;

@Workflow(processKey = "composite1",binder = CompositeWorkflows.Binder.class,activity = CompositeWorkflow1.Activity.class)
public interface CompositeWorkflow1 {
	interface Activity extends CompositeWorkflows.Activity {

		@Start
		Workflow1Start start();

		@Task(definition = "TestTask")
		CompositeTask1Task task2();

	}

	public class CompositeTask1Task extends AbstractCompositeTask {

	}
}
