package eu.k5.cell.activiti.filter;

import eu.k5.cell.activiti.ProcessState;

public interface AspectFilter {
	AspectQuery filter(AspectQuery query);

	boolean accept(ProcessState state);
}
