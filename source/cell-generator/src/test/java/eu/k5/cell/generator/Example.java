package eu.k5.cell.generator;

public class Example {

	private final String param;
	private final String param2;
	private String param3;

	public Example(String param, String param2) {
		this.param = param;
		this.param2 = param2;
	}

	public void setParam3(String param3) {
		this.param3 = param3;
	}

	public String getParam() {
		return param;
	}

	public String getParam2() {
		return param2;
	}

	public String getParam3() {
		return param3;
	}

}
