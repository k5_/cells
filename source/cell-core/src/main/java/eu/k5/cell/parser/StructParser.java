package eu.k5.cell.parser;

public class StructParser<T> implements Parser<T> {
	private final Board board;

	private final Instantiator<T> struct;

	public StructParser(Board board, Instantiator<T> data) {
		this.board = board;

		this.struct = data;
	}

	public Instantiator<T> getStruct() {
		return struct;
	}

	@Override
	public T parse(Context context) {
		Object[] bound = new Object[board.getSize()];
		for (Data data : board.getEntries()) {
			bound[data.getIndex()] = data.getSource().read(context);
		}
		return struct.instantiate(bound);
	}

	public Board getBoard() {
		return board;
	}

}
