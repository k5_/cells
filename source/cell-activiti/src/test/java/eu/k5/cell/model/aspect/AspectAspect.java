package eu.k5.cell.model.aspect;

import eu.k5.cell.annotation.states.ProcessState;
import eu.k5.cell.annotation.states.ProcessStateType;
import eu.k5.cell.annotation.workflow.Data;
import eu.k5.cell.annotation.workflow.Task;

public class AspectAspect {

	static final String IGNORE = "ignore";

	@Task
	private AspectTask task;

	@Data
	private AspectData data;

	private Boolean isEnded;

	private String instance;

	private String derivedTest;

	@ProcessState(resolver = AspectDerivedProcessState.class)
	private String derivedDirectTest;

	@ProcessState(value = ProcessStateType.INSTANCE_ID)
	private String instanceId;

	public AspectTask getTask() {
		return task;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setTask(AspectTask task) {
		this.task = task;
	}

	public AspectData getData() {
		return data;
	}

	public void setData(AspectData data) {
		this.data = data;
	}

	public Boolean getIsEnded() {
		return isEnded;
	}

	public void setIsEnded(Boolean isEnded) {
		this.isEnded = isEnded;
	}

	public String getDerivedTest() {
		return derivedTest;
	}

	public void setDerivedTest(String derivedTest) {
		this.derivedTest = derivedTest;
	}

	public String getDerivedDirectTest() {
		return derivedDirectTest;
	}

	public void setDerivedDirectTest(String derivedDirectTest) {
		this.derivedDirectTest = derivedDirectTest;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

}
