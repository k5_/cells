package eu.k5.cell.activiti;

import java.util.Map;

public interface Command<T> {

	T get(Map<String, Object> parameters);

}
