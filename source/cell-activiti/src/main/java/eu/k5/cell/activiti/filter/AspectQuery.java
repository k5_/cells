package eu.k5.cell.activiti.filter;

import java.util.List;

public interface AspectQuery {

	AspectQuery processInstanceBusinessKey(String businessKey);

	AspectQuery processVariableValueEquals(String variableName, Object variableValue);

	AspectQuery taskAssignee(String assignee);

	AspectQuery includeHistoric(boolean state);

	AspectQuery processVariableValueLessThanOrEqual(String variableName, Object variableValue);

	AspectQuery processVariableValueGreaterThanOrEqual(String variableName, Object variableValue);

	AspectQuery processVariableValueGreaterThan(String variableName, Object variableValue);

	AspectQuery processVariableValueLessThan(String variableName, Object variableValue);

	AspectQuery taskCandidateGroupIn(List<String> candidateGroups);

	AspectQuery superProcessInstanceId(String superProcessInstanceId);

	AspectQuery subProcessInstanceId(String superProcessInstanceId);
}
