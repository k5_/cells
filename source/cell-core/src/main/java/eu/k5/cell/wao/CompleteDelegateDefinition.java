package eu.k5.cell.wao;

import eu.k5.cell.WorkflowManager;

public class CompleteDelegateDefinition implements DelegateDefinition {

	@Override
	public Delegate bind(final WorkflowManager cellManager) {
		return new Delegate() {
			@Override
			public Object invoke(Object... args) {
				cellManager.complete(args[0]);
				return null;
			}
		};
	}
}
