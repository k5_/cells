package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

class NamedParameter extends Value {
	private final String name;

	public NamedParameter(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public TypeHint getHint(Context context) {
		return context.getParameterBound(name).getType();
	}

	@Override
	public String toString() {
		return "NamedParameter [name=" + name + "]";
	}

	@Override
	public void accept(ValueVisitor visitor) {
		visitor.visit(this);

	}

}
