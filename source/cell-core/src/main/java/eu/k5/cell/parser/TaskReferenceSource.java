package eu.k5.cell.parser;

public class TaskReferenceSource implements Source {

	@Override
	public Object read(Context context) {
		return context.getTaskReference();
	}

	@Override
	public <R, P> R accept(SourceVisitor<R, P> visitor, P param) {
		return visitor.visit(this, param);
	}

}
