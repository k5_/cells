package eu.k5.cell.model.call;

import eu.k5.cell.annotation.Bind;
import eu.k5.cell.annotation.workflow.Task;

public class AbstractWorkflow {

	interface AbstractBinder {
		@Bind(businessKey = true)
		String getKey();
	}

	interface AbstractActivity {

		@Task
		CallTask getTask();

	}

}
