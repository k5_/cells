package eu.k5.cell.activiti.builder;

import eu.k5.cell.activiti.TypeHint;

public class QueryParameter {
	private final String name;
	private final TypeHint hint;
	private final int index;

	public QueryParameter(String name, TypeHint hint, int index) {
		this.name = name;

		this.hint = hint;
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public int getIndex() {
		return index;
	}

	public TypeHint getHint() {
		return hint;
	}

	public boolean isInline() {
		TypeHint hint = getHint();
		if (hint == null) {
			return false;
		}
		return hint.isInline();

	}

	public Object transform(Object[] arguments) {
		return hint.transform(arguments[index]);
	}
}