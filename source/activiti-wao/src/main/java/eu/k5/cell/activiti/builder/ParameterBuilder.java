package eu.k5.cell.activiti.builder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import eu.k5.cell.activiti.ParameterBound;
import eu.k5.cell.activiti.ParameterNameBound;
import eu.k5.cell.activiti.TypeHint;
import eu.k5.cell.activiti.annotation.Param;

public class ParameterBuilder {
	private static final String NAME_PATTERN = "[A-Za-z][_A-Za-z0-9]*";
	private final Map<String, QueryParameter> parameters = new HashMap<>();
	private Violations violations;

	private final Pattern namePattern = Pattern.compile(NAME_PATTERN);

	public ParameterBuilder withViolations(Violations violations) {
		this.violations = violations;
		return this;
	}

	public ParameterBuilder forMethod(Method method) {
		Annotation[][] annotations = method.getParameterAnnotations();

		Type[] types = method.getGenericParameterTypes();
		// String[] parameters = new String[types.length];
		for (int i = 0; i < types.length; i++) {
			String param = getParameterAnnotation(annotations[i]);
			TypeHint hint = TypeHint.forType(types[i]);
			if (param == null) {
				violations.add(Violations.Type.INVALID_PARAMETER, "Parameter number " + i
						+ " is missing @Param annotation");
			} else if (!validName(param)) {
				violations.add(Violations.Type.INVALID_PARAMETER, "Parameter name '" + param
						+ "' does not match name pattern " + namePattern);
			} else if (parameters.containsKey(param)) {
				violations.add(Violations.Type.INVALID_PARAMETER, "Parameter name  '" + param + "' already used");
			} else if (hint == null) {
				violations.add(Violations.Type.INVALID_PARAMETER, "Parameter  with name  '" + param
						+ "' doesnt have a mapable type found " + types[i]);
			} else {

				parameters.put(param, new QueryParameter(param, hint, i));
			}

		}
		return this;
	}

	public ParameterBuilder forVarargs(Object[] parameters) {
		for (int index = 0; index < parameters.length; index++) {
			TypeHint hint = TypeHint.forType(parameters[index].getClass());
			if (hint == null) {
				violations.add(Violations.Type.INVALID_PARAMETER, "Parameter with index '" + index
						+ "' doesnt have a mapable type found " + parameters[index].getClass());
			}
			String name = "" + (index + 1);
			this.parameters.put(name, new QueryParameter(name, hint, index));
		}
		return this;
	}

	private boolean validName(String name) {
		return namePattern.matcher(name).matches();
	}

	public Map<String, ParameterBound> getBounds() {
		Map<String, ParameterBound> bounds = new HashMap<>();
		for (Entry<String, QueryParameter> queryParameter : parameters.entrySet()) {
			bounds.put(queryParameter.getKey(), new ParameterNameBound(queryParameter.getValue().getHint(),
					queryParameter.getKey()));
		}
		return bounds;

	}

	public Map<String, TypeHint> getHints() {
		Map<String, TypeHint> parameterHints = new HashMap<>();
		for (Entry<String, QueryParameter> queryParameter : parameters.entrySet()) {
			parameterHints.put(queryParameter.getKey(), queryParameter.getValue().getHint());
		}
		return parameterHints;
	}

	protected String getParameterAnnotation(Annotation[] annotations) {
		for (Annotation a : annotations) {
			if (a instanceof Param) {
				return ((Param) a).value();
			}
		}
		return null;
	}

	public String[] getNames() {
		String[] names = new String[parameters.size()];
		for (Entry<String, QueryParameter> entry : parameters.entrySet()) {
			names[entry.getValue().getIndex()] = entry.getValue().getName();
		}
		return names;
	}

	public boolean isValid() {
		return violations.isEmpty();
	}

	public QueryParameter[] create() {
		return parameters.values().toArray(new QueryParameter[parameters.size()]);
	}

	public ParameterBuilder addParameter(String name, TypeHint type) {
		parameters.put(name, new QueryParameter(name, type, parameters.size()));
		return this;
	}
}
