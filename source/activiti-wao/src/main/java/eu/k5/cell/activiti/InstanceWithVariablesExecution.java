package eu.k5.cell.activiti;

import java.util.Map;

import org.activiti.engine.impl.cmd.AbstractCustomSqlExecution;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;

public class InstanceWithVariablesExecution extends AbstractCustomSqlExecution<AqlMapper, ExecutionEntity> {
	private final Map<String, Object> parameters;

	public InstanceWithVariablesExecution(Map<String, Object> parameters) {
		super(AqlMapper.class);
		this.parameters = parameters;
	}

	@Override
	public ExecutionEntity execute(AqlMapper mapper) {
		return mapper.selectInstanceWithVariables(parameters);
	}
}