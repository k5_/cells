package eu.k5.cell.activiti;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.k5.cell.CellException;
import eu.k5.cell.WorkflowManager;
import eu.k5.cell.annotation.CellContext;
import eu.k5.cell.extension.Cell;
import eu.k5.cell.extension.Cells;

@ApplicationScoped
public class CellManagerProducer {

	private static final Logger LOGGER = LoggerFactory.getLogger(CellManagerProducer.class);

	@Inject
	private Cells cells;

	@Inject
	private RuntimeService runtimeService;

	@Inject
	private TaskService taskService;

	@Inject
	private HistoryService historyService;

	@Inject
	private ManagementService managementService;

	@CellContext
	@Produces
	public CellManager produceCell(InjectionPoint ip) {
		CellContext context = ip.getAnnotated().getAnnotation(CellContext.class);
		Cell binder = cells.get(context.value());
		return new CellManagerImpl(binder, runtimeService, historyService, taskService, managementService);
	}

	@CellContext
	@Produces
	public WorkflowManager produceWorkflow(InjectionPoint ip) {
		CellContext context = ip.getAnnotated().getAnnotation(CellContext.class);
		Cell cell = cells.get(context.value());
		if (cell == null) {
			throw new CellException(Form.at("No cell found for context {}", context.value()));
		}
		return new WorkflowManagerImpl(cell, runtimeService, historyService, taskService, managementService);
	}
}
