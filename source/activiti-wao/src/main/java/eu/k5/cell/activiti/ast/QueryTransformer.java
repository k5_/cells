package eu.k5.cell.activiti.ast;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.misc.NotNull;

import eu.k5.cell.activiti.query.AqlBaseListener;
import eu.k5.cell.activiti.query.AqlParser;
import eu.k5.cell.activiti.query.AqlParser.AndExpressionContext;
import eu.k5.cell.activiti.query.AqlParser.ConditionalContext;
import eu.k5.cell.activiti.query.AqlParser.ExpressionContext;
import eu.k5.cell.activiti.query.AqlParser.LiteralContext;
import eu.k5.cell.activiti.query.AqlParser.NamedParameterContext;
import eu.k5.cell.activiti.query.AqlParser.OperatorContext;
import eu.k5.cell.activiti.query.AqlParser.OrExpressionContext;
import eu.k5.cell.activiti.query.AqlParser.PrimaryExpressionContext;
import eu.k5.cell.activiti.query.AqlParser.SectionContext;
import eu.k5.cell.activiti.query.AqlParser.UnaryExpressionContext;

public class QueryTransformer extends AqlBaseListener {
	private Query query;
	private final List<Section> sections = new ArrayList<>();
	private final Deque<Value> values = new ArrayDeque<Value>();
	private final Deque<List<Expression>> parts = new ArrayDeque<>();
	private final Map<String, NamedParameter> namedParameter = new HashMap<>();

	private final Set<String> variables = new HashSet<String>();
	private final List<String> parameters = new ArrayList<>();

	private Operator operator;

	@Override
	public void exitSection(SectionContext ctx) {
		query = new Query(sections, parameters);
	}

	@Override
	public void enterSection(SectionContext ctx) {
		System.out.println("section");
	}

	@Override
	public void enterExpression(ExpressionContext ctx) {
		parts.push(new ArrayList<Expression>());
	}

	@Override
	public void exitExpression(ExpressionContext ctx) {
		sections.add(new Section(parts.pop().get(0), variables));
	}

	@Override
	public void enterUnaryExpression(UnaryExpressionContext ctx) {
		parts.push(new ArrayList<Expression>());
	}

	@Override
	public void enterAndExpression(AndExpressionContext ctx) {
		parts.push(new ArrayList<Expression>());
	}

	@Override
	public void enterOrExpression(OrExpressionContext ctx) {
		parts.push(new ArrayList<Expression>());
	}

	@Override
	public void exitUnaryExpression(UnaryExpressionContext ctx) {
		List<Expression> currentParts = parts.pop();
		if (currentParts.size() == 1) {
			parts.peek().add(new UnaryExpression(currentParts.get(0)));
			return;
		}
		throw new IllegalStateException();
	}

	@Override
	public void exitOrExpression(OrExpressionContext ctx) {
		List<Expression> currentParts = parts.pop();
		if (currentParts.size() == 1) {
			parts.peek().add(currentParts.get(0));
			return;
		}
		parts.peek().add(new OrExpression(currentParts));
	}

	@Override
	public void exitAndExpression(AndExpressionContext ctx) {
		List<Expression> currentParts = parts.pop();
		if (currentParts.size() == 1) {
			parts.peek().add(currentParts.get(0));
			return;
		}
		parts.peek().add(new AndExpression(currentParts));
	}

	@Override
	public void exitConditional(ConditionalContext ctx) {
		Value right = values.pop();
		Value left = values.pop();
		parts.peek().add(new Condition(operator, left, right));
	}

	@Override
	public void enterOperator(OperatorContext ctx) {
		switch (ctx.getStart().getType()) {
		case AqlParser.EQUALS:
			operator = Operator.EQ;
			break;
		case AqlParser.NOTEQUALS:
			operator = Operator.NEQ;
			break;
		case AqlParser.GT:
			operator = Operator.GT;
			break;
		case AqlParser.LT:
			operator = Operator.LT;
			break;
		case AqlParser.GTEQ:
			operator = Operator.GE;
			break;
		case AqlParser.LTEQ:
			operator = Operator.LE;
			break;
		case AqlParser.IN:
			operator = Operator.IN;
		}
	}

	@Override
	public void enterPrimaryExpression(PrimaryExpressionContext ctx) {
		if (ctx.getStart().getType() == AqlParser.BOOLEAN) {
			if ("true".equals(ctx.getChild(0).toString().toLowerCase())) {
				parts.peek().add(new BooleanExpression(true));
			} else {
				parts.peek().add(new BooleanExpression(false));
			}
		}
	}

	@Override
	public void enterLiteral(LiteralContext ctx) {
		switch (ctx.getStart().getType()) {
		case AqlParser.STRING:
			values.push(new StringLiteral(unescapeSingleQuote(ctx.getChild(0).getText())));
			break;
		}

	}

	@Override
	public void enterProperty(@NotNull AqlParser.PropertyContext ctx) {
		switch (ctx.getStart().getType()) {
		case AqlParser.ASSIGNEE:
			values.push(new BasicProperty(BasicProperty.PropertyType.ASSIGNEE));
			break;
		case AqlParser.TASK_DEFINITION_KEY:
			values.push(new BasicProperty(BasicProperty.PropertyType.TASK_DEFINITION_KEY));
			break;
		case AqlParser.PROCESS_DEFINITION_KEY:
			values.push(new BasicProperty(BasicProperty.PropertyType.PROCESS_DEFINITION_KEY));
			break;
		case AqlParser.PROCESS_INSTANCE_ID:
			values.push(new BasicProperty(BasicProperty.PropertyType.PROCESS_INSTANCE_ID));
			break;
		case AqlParser.CANDIDATE:
			values.push(new CandidateProperty());
			break;

		case AqlParser.VARIABLE_PREFIX:
			String variable = ctx.getChild(0).getChild(1).getText();
			variables.add(variable);
			values.push(new Variable(variable));
			break;
		}
		System.out.println(ctx.getStart().getType());

		System.out.println(ctx.getText());
	}

	@Override
	public void exitLimit(eu.k5.cell.activiti.query.AqlParser.LimitContext ctx) {

		Value length = values.pop();
		Value offset = values.pop();

	};

	@Override
	public void enterNamedParameter(NamedParameterContext ctx) {
		String name = ctx.getChild(0).toString().substring(1);

		if (!namedParameter.containsKey(name)) {
			parameters.add(name);
			namedParameter.put(name, new NamedParameter(name));
		}

		values.push(namedParameter.get(name));
	}

	public Query getQuery() {
		return query;
	}

	static String unescapeSingleQuote(String raw) {
		if (raw.length() > 1) {
			return raw.substring(1, raw.length() - 1);
		}
		throw new IllegalStateException("Too short to be escaped");
	}
}