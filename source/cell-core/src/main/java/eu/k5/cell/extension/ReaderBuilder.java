package eu.k5.cell.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import eu.k5.cell.annotation.In;
import eu.k5.cell.annotation.Unbound;
import eu.k5.cell.definition.AssignerBound;
import eu.k5.cell.definition.Initializer;
import eu.k5.cell.definition.PropertyAssigner;
import eu.k5.cell.definition.PropertyAssignerField;

public class ReaderBuilder extends Builder<Reader, ReaderBuilder> {

	public ReaderBuilder() {
		super(Reader.class);
		setBuilder(this);
	}

	private Constructor<?> constructor;
	private final List<PropertyBound> bounds = new ArrayList<>();
	private final List<AssignerBound> assigners = new ArrayList<>();

	private boolean loaded = false;

	static class PropertyBound {
		private final String name;
		private final Binding binding;
		private Initializer initializer;
		private final PropertyAssigner assigner;
		private boolean ignored;

		public PropertyBound(String name, Binding binding, Initializer initializer, PropertyAssigner assigner) {
			this.name = name;
			this.binding = binding;
			this.initializer = initializer;
			this.assigner = assigner;
		}

		public String getName() {
			return name;
		}

		public Initializer getInitializer() {
			return initializer;
		}

		public void setInitializer(Initializer initializer) {
			this.initializer = initializer;
		}

		public PropertyAssigner getAssigner() {
			return assigner;
		}

		boolean isIgnored() {
			return ignored;
		}

		public Binding getBinding() {
			return binding;
		}

		public void setIgnored(boolean state) {
			ignored = state;
		}

		public <A extends Annotation> A getAnnotation(Class<A> annotation) {
			return assigner.getAnnotation(annotation);
		}

		public <A extends Annotation> boolean isAnnotationPresent(Class<A> annotation) {
			return assigner.isAnnotationPresent(annotation);
		}

		public Class<?> getType() {
			return assigner.getType();
		}

	}

	public static class BindingInitializer implements Initializer {

		private final String binding;

		public BindingInitializer(String binding) {
			this.binding = binding;
		}

		@Override
		public Object init(Map<String, Object> variables) {
			return variables.get(binding);
		}

		public String getBinding() {
			return binding;
		}

	}

	@Override
	protected void doInit() {
		if (!loaded) {
			load();
		}
		initConstructor();
		initAssigners();
	}

	private void initConstructor() {
		try {
			//
			// List<Constructor<?>> options = new ArrayList<>();
			//
			// // TODO: resolve non zero parameter constructor
			// Constructor<?>[] constructors = getType().getConstructors();
			// for (Constructor<?> c : constructors) {
			// if (c.isAnnotationPresent(In.class)) {
			//
			// c.getParameterAnnotations()
			//
			//
			// }
			// }

			constructor = getType().getConstructor();

		} catch (SecurityException | NoSuchMethodException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private void initAssigners() {

		for (PropertyBound bound : bounds) {
			if (bound.ignored) {
				// Do nothing
			} else if (bound.getInitializer() != null) {
				assigners.add(new AssignerBound(bound.getInitializer(), bound.getAssigner()));
			} else if (bound.getBinding() != null) {
				if (!bound.getBinding().getType().equals(bound.getType())) {
					addViolation("Mismatched types for field:" + bound.getName() + " " + bound.getType() + " "
							+ bound.getBinding().getType());
				} else {
					assigners.add(new AssignerBound(new BindingInitializer(bound.getBinding().getBind()), bound
							.getAssigner()));
				}

			} else {
				addViolation("Missing binding for field: " + bound.name);
			}

		}

	}

	public ReaderBuilder load() {
		Class<?> current = getType();
		while (current != null) {
			Field[] fields = current.getDeclaredFields();
			for (Field field : fields) {
				if (Modifier.isStatic(field.getModifiers())) {
					continue;
				}
				if (field.isAnnotationPresent(Unbound.class)) {
					continue;
				}

				In in = field.getAnnotation(In.class);
				String bind = field.getName();
				if (in != null && !in.value().equals(In.DEFAULT)) {
					bind = in.value();
				}

				Binding binding = getBinder().getBinding(bind);
				field.setAccessible(true);
				bounds.add(new PropertyBound(field.getName(), binding, null, new PropertyAssignerField(field)));
			}
			current = current.getSuperclass();
		}

		loaded = true;
		return this;
	}

	public List<PropertyBound> selectReaders(Class<? extends Annotation> annotations) {
		List<PropertyBound> selected = new ArrayList<>();
		for (PropertyBound bound : bounds) {
			if (bound.getAssigner().isAnnotationPresent(annotations)) {
				selected.add(bound);
			}
		}
		return selected;
	}

	public List<PropertyBound> getBounds() {
		return bounds;
	}

	@Override
	public Reader doBuild() {
		return new Reader(constructor, new ArrayList<Binding>(), assigners);
	}

}
