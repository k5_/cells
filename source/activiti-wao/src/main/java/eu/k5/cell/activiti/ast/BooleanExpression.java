package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

public class BooleanExpression extends Expression {

	private final boolean value;

	public BooleanExpression(boolean value) {
		this.value = value;
	}

	@Override
	public TypeHint getHint(Context context) {
		return TypeHint.BOOLEAN;
	}

	public boolean getValue() {
		return value;
	}

	@Override
	public void accept(QueryVisitor visitor) {
		visitor.visit(this);
	}

}
