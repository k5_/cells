package eu.k5.cell.activiti;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Assert;
import org.junit.Test;

public class AqlInstanceExamples extends AbstractExample {

	@Test
	public void getAll() {
		start(1);

		List<ProcessInstance> list = getEngine().getEngine().getRuntimeService().createProcessInstanceQuery()
				.includeProcessVariables().processDefinitionKey("").list();

		String query = getAql().parseInstanceQuery(getService(), "",
				Collections.singletonMap("assign", TypeHint.STRING), Collections.singletonMap("test", TypeHint.STRING));

		System.out.println(query);
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("assign", "testAssignee");

		List<TaskEntity> tasks = Aql.getTasksWithVariables(getService(), query, parameters);
		Assert.assertEquals(1, tasks.size());
	}

	@Test
	public void example() {
		start(1, "property", "value");

		String query = getAql().parseInstanceQuery(getService(), "var.property = :parameter",
				Collections.singletonMap("parameter", TypeHint.STRING),
				Collections.singletonMap("property", TypeHint.STRING));

		System.out.println(query);
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("parameter", "value");

		List<TaskEntity> tasks = Aql.getTasksWithVariables(getService(), query, parameters);
		Assert.assertEquals(1, tasks.size());
	}

}
