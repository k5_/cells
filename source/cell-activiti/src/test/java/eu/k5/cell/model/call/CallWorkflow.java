package eu.k5.cell.model.call;

import eu.k5.cell.annotation.Bind;
import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Task;
import eu.k5.cell.annotation.workflow.Workflow;

@Workflow(processKey = "callerProcess", activity = CallWorkflow.Activity.class, binder = CallWorkflow.Binder.class)
public interface CallWorkflow {
	interface Binder {
		@Bind(businessKey = true)
		String getKey();

		String getProperty();
	}

	interface Activity {
		@Start
		CallStart getStart();

		@Task
		CallTask getTask();

		@Call
		CalledWorkflow getCalled();
	}
}
