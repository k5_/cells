package eu.k5.cell.definition;

import java.lang.reflect.Field;

import eu.k5.cell.extension.Binding;

public class PropertyReaderField implements PropertyReader {
	private final Field field;
	private final Binding binding;

	public PropertyReaderField(Binding binding, Field field) {
		this.binding = binding;
		this.field = field;
	}

	@Override
	public Object read(Object object) {
		try {
			return field.get(object);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public Binding getBinding() {
		return binding;
	}

	@Override
	public boolean valid(Object object) {
		return field.getDeclaringClass().isInstance(object);
	}
}
