package eu.k5.cell.model.call;

import eu.k5.cell.annotation.In;

public class CallStart {
	@In
	private String key;
	@In
	private String property;

	public CallStart(String key, String property) {
		this.key = key;
		this.property = property;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

}
