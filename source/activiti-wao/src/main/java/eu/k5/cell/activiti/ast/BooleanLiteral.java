package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

class BooleanLiteral extends Literal<Boolean> {

	protected BooleanLiteral(Boolean value) {
		super(value, TypeHint.BOOLEAN);
	}

	@Override
	public void accept(ValueVisitor visitor) {
		visitor.visit(this);
	}

}
