package eu.k5.cell.activiti;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.k5.cell.activiti.ast.AqlQueries;

public class AqlTaskExamples {
	private Awi aql;

	private TestEngine engine;

	private ManagementService service;

	private AqlQueries queries;

	@Before
	public void init() {
		engine = TestEngine.mssql("test", "Test");
		engine.reset();
		service = engine.getEngine().getManagementService();
		aql = new Awi(service);
		queries = new AqlQueries();
	}

	@After
	public void destroy() {
		engine.destroy();
	}

	@Test
	public void example() {
		start(1);

		TaskQuery taskQuery = engine.getEngine().getTaskService().createTaskQuery();
		List<Task> ts = taskQuery.includeProcessVariables().includeTaskLocalVariables().taskAssignee("testAssignee")
				.list();

		String query = queries.parseTaskQuery(service, "assignee = :assign",
				Collections.singletonMap("assign", TypeHint.STRING), Collections.<String, TypeHint> emptyMap());

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("assign", "testAssignee");

		List<TaskEntity> tasks = aql.getTasksWithVariables(service, query, parameters);
		Assert.assertEquals(1, tasks.size());
	}

	@Test
	public void exampleInteger() {
		start(1, "intVariable", 1);

		String query = aql.parseTaskQuery(service, "var.intVariable = :variable",
				Collections.singletonMap("variable", TypeHint.LONG),
				Collections.singletonMap("intVariable", TypeHint.LONG));
		System.out.println(query);

		List<TaskEntity> tasks = Aql.getTasksWithVariables(service, query,
				Collections.<String, Object> singletonMap("variable", 1));
		Assert.assertEquals(1, tasks.size());
	}

	@Test
	public void taskDefinitionKey() {
		start(1);
		start(2);

		String query = aql.parseTaskQuery(service, "taskDefinitionKey = :def OR taskDefinitionKey = 'Task1'",
				Collections.singletonMap("def", TypeHint.STRING), Collections.<String, TypeHint> emptyMap());

		{
			List<TaskEntity> tasks = Aql.getTasksWithVariables(service, query,
					Collections.<String, Object> singletonMap("def", "Task1"));
			Assert.assertEquals(1, tasks.size());
		}
		{
			List<TaskEntity> tasks = Aql.getTasksWithVariables(service, query,
					Collections.<String, Object> singletonMap("def", "Task2"));
			Assert.assertEquals(2, tasks.size());
		}
	}

	private void start(int process, Object... parameters) {
		Map<String, Object> variables = new HashMap<String, Object>();
		for (int index = 0; index < parameters.length; index += 2) {
			variables.put(parameters[index].toString(), parameters[index + 1]);
		}
		engine.getEngine().getRuntimeService().startProcessInstanceByKey("Process" + process, variables);
	}

}
