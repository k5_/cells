package eu.k5.cell.strategem;

import java.util.Date;

import eu.k5.cell.annotation.Out;

public class StrategemStart {
	@Out
	private String game;
	@Out
	private String owner;
	@Out
	private String current;

	@Out
	private String ownerName;
	@Out
	private String gameName;
//	@Out
//	private Board board;
	@Out
	private boolean open;

	@Out
	private int rows;

	@Out
	private int columns;

	@Out
	private Date started;

	public String getGame() {
		return game;
	}

	public void setGame(String game) {
		this.game = game;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		current = owner;
		this.owner = owner;
	}

//	public Board getBoard() {
//		return board;
//	}
//
//	public void setBoard(Board board) {
//		this.board = board;
//	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public void setGameName(String gameName) {

	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public Date getStarted() {
		return started;
	}

	public void setStarted(Date started) {
		this.started = started;
	}

	public String getGameName() {
		return gameName;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

}
