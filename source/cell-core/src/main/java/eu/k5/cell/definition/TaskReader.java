package eu.k5.cell.definition;

import java.util.List;
import java.util.Map;

import eu.k5.cell.CellTask;
import eu.k5.cell.extension.Reader;
import eu.k5.cell.extension.StateDefinition;
import eu.k5.cell.shared.CellTaskReference;

public class TaskReader {
	private final Reader reader;
	private final PropertyAssigner referenceAssigner;

	private final List<StateDefinition<TaskStateReader>> taskStates;
	private final List<NestedDataDefinition> nestedDatas;

	public TaskReader(Reader reader, PropertyAssigner referenceAssigner,
			List<StateDefinition<TaskStateReader>> taskStates, List<NestedDataDefinition> nestedDatas) {
		this.reader = reader;
		this.referenceAssigner = referenceAssigner;
		this.taskStates = taskStates;
		this.nestedDatas = nestedDatas;
	}

	public Object read(CellTask task, Map<String, Object> variables) {
		Object instance = reader.read(variables);

		referenceAssigner.assign(instance, createTaskReference(task));

		for (StateDefinition<TaskStateReader> reader : taskStates) {
			reader.getAssigner().assign(instance, reader.getReader().read(task));
		}
		for (NestedDataDefinition nested : nestedDatas) {
			nested.getAssigner().assign(instance, nested.getDefinition().read(variables));
		}

		return instance;
	}

	public Reader getReader() {
		return reader;
	}

	private CellTaskReference createTaskReference(CellTask task) {
		return new CellTaskReference(task.getProcessInstanceId(), task.getId(), task.getTaskDefinitionKey(),
				task.getToken());
		// (String) task.getTaskLocalVariables().get(TaskTokenCreator.TOKEN));
	}
}
