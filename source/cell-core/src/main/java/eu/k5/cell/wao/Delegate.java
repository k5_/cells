package eu.k5.cell.wao;

public interface Delegate {

	Object invoke(Object... args);

}
