package eu.k5.cell.activiti.builder;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ManagementService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import eu.k5.cell.activiti.TypeHint;
import eu.k5.cell.activiti.VariableBound;
import eu.k5.cell.activiti.VariableHint;
import eu.k5.cell.activiti.annotation.Query;
import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.delegate.Delegate;
import eu.k5.cell.activiti.delegate.DelegateFactory;

public class WaoBuilder {
	private Class<?> type;
	private final Map<Method, Delegate> delegates = new HashMap<>();
	private Map<String, VariableBound> variableHints;
	private ManagementService managementService;
	private boolean init = false;

	private final Violations violations = new Violations();

	public WaoBuilder withType(Class<?> type) {
		this.type = type;
		return this;
	}

	public WaoBuilder withManagementService(ManagementService managementService) {

		this.managementService = managementService;
		return this;
	}

	public void init() {
		if (init) {
			return;
		}
		List<Method> queries = new ArrayList<>();
		List<Method> variables = new ArrayList<>();
		for (Method method : type.getMethods()) {

			if (method.isAnnotationPresent(Query.class)) {
				queries.add(method);
			} else if (method.isAnnotationPresent(VariableHint.class)) {
				variables.add(method);
			}
		}

		initVariables(variables);
		initQueries(queries);
		init = true;
	}

	private void initVariables(List<Method> variables) {
		variableHints = new HashMap<>();

		for (Method method : variables) {
			TypeHint hint = TypeHint.forType(method.getReturnType());
			if (hint == null) {
				throw new IllegalArgumentException("No Valid typeHint for " + method.getReturnType() + " available");
			}
			variableHints.put(method.getName(), new VariableBound(method.getName(), hint));
			delegates.put(method, DelegateFactory.variableHint(method.getName()));

		}
	}

	private void initQueries(List<Method> queries) {
		for (Method queryMethod : queries) {
			Query query = queryMethod.getAnnotation(Query.class);

			ParameterBuilder parameters = new ParameterBuilder().withViolations(violations.sub())
					.forMethod(queryMethod);

			Context context = new Context(variableHints, parameters.getBounds());

			QueryDelegateBuilder<? extends QueryDefinition<? extends Delegate>> builder = queryBuilder(
					queryMethod.getGenericReturnType()).withViolations(violations.sub());

			builder.withParameters(parameters.create()).withContext(context);

			builder.withQueryString(query.value());

			delegates.put(queryMethod, builder.build().bind(managementService));
		}
	}

	private QueryDelegateBuilder<? extends QueryDefinition<? extends Delegate>> queryBuilder(Type type) {
		if (type instanceof ParameterizedType && List.class.equals(((ParameterizedType) type).getRawType())) {
			Type actualType = ((ParameterizedType) type).getActualTypeArguments()[0];
			if (Task.class.equals(actualType)) {
				return QueryDelegateBuilder.listTasks();
				// initTaskListQuery(query);
			} else if (actualType.equals(ProcessInstance.class)) {
				return QueryDelegateBuilder.listInstances();
			}
		} else if (Task.class.equals(type)) {
			return QueryDelegateBuilder.findTasks();
		} else if (ProcessInstance.class.equals(type)) {
			return QueryDelegateBuilder.findInstance();
		}
		return null;
	}

	private static class InvocationHandler implements java.lang.reflect.InvocationHandler {
		private final Map<Method, Delegate> delegates;

		InvocationHandler(Map<Method, Delegate> delegates) {
			this.delegates = delegates;
		}

		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			Delegate delegate = delegates.get(method);
			return delegate.invoke(args);
		}

	}

	public Object build() {
		init();

		InvocationHandler handler = new InvocationHandler(delegates);
		return Proxy.newProxyInstance(type.getClassLoader(), new Class<?>[] { type }, handler);
	}

}
