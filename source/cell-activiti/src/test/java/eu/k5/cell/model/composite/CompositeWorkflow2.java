package eu.k5.cell.model.composite;

import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Task;
import eu.k5.cell.annotation.workflow.Workflow;

@Workflow(processKey = "composite2",binder = CompositeWorkflows.Binder.class,activity = CompositeWorkflow2.Activity.class)
public interface CompositeWorkflow2 {
	interface Activity extends CompositeWorkflows.Activity {

		@Start
		Workflow2Start start();

		@Task(definition = "TestTask")
		CompositeTask2Task task2();
	}

	public class CompositeTask2Task extends AbstractCompositeTask {

	}
}
