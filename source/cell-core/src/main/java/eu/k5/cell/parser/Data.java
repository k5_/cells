package eu.k5.cell.parser;

import java.lang.reflect.Type;

public class Data {
	private final int index;
	private final Type type;
	private final Source source;

	public Data(int index, Type type, Source source) {
		this.index = index;
		this.type = type;
		this.source = source;
	}

	public int getIndex() {
		return index;
	}

	public Source getSource() {
		return source;
	}

	public Type getType() {
		return type;
	}

}