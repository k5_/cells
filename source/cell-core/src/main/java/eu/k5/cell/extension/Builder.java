package eu.k5.cell.extension;

import eu.k5.cell.definition.WorkflowDefinition;

abstract class Builder<T, B> {
	private Class<?> entityType;
	private WorkflowDefinition workflow;

	private boolean useAll;

	private Validator validator;
	private int violations;
	private boolean init;

	private B builder;
	private Binder binder;

	public Builder(Class<T> type) {

	}

	protected final void setBuilder(B builder) {
		this.builder = builder;
	}

	public Binder getBinder() {
		return binder;
	}

	public B forEntity(Class<?> type) {
		this.entityType = type;
		return builder;
	}

	public B forWorkflow(WorkflowDefinition workflow) {
		this.workflow = workflow;
		return builder;
	}

	public WorkflowDefinition getWorkflow() {
		return workflow;
	}

	public B withBinder(Binder binder) {
		this.binder = binder;

		return builder;
	}

	public B withValidator(Validator validator) {
		this.validator = validator;
		return builder;
	}

	public B useAll(boolean state) {
		useAll = state;
		return builder;
	}

	public int getViolations() {
		return violations;
	}

	protected void addViolation(String message, Object... arguments) {
		validator.add(entityType, message, arguments);
		violations++;
	}

	public boolean isInit() {
		return init;
	}

	protected Class<?> getType() {
		return entityType;
	}

	public boolean isUseAll() {
		return useAll;
	}

	public void setInit(boolean state) {
		init = state;
	}

	public Validator getValidator() {
		return validator;
	}

	protected void plusViolations(int violations) {
		this.violations += violations;
	}

	public final B init() {
		if (isInit() || getViolations() > 0) {
			return builder;
		}

		doInit();

		if (getViolations() == 0) {
			setInit(true);
		}

		return builder;
	}

	protected abstract void doInit();

	public final T build() {
		if (!isInit()) {
			throw new IllegalArgumentException("invalid");
		}

		return doBuild();
	}

	protected abstract T doBuild();
}
