package eu.k5.cell.activiti.delegate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import eu.k5.cell.activiti.ParameterBound;
import eu.k5.cell.activiti.ParameterNameBound;
import eu.k5.cell.activiti.TypeHint;
import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.builder.QueryDelegateBuilder;
import eu.k5.cell.activiti.builder.QueryParameter;
import eu.k5.cell.definition.AspectDefinition;
import eu.k5.cell.extension.ParameterBuilder;

public class AspectDelegateBuilder extends AbstractDelegateBuilder<AspectDelegateBuilder, AspectDelegateDefinition> {

	private AspectDefinition aspect;

	public AspectDelegateBuilder() {
		initBuilder(this);
	}

	@Override
	public AspectDelegateDefinition build() {

		eu.k5.cell.activiti.builder.ParameterBuilder parameters = new ParameterBuilder().withViolations(
				getViolations().sub()).forMethod(method);

		eu.k5.cell.activiti.ast.Context context = new eu.k5.cell.activiti.ast.Context(variableHints,
				parameters.getBounds());

		QueryDelegateBuilder<ListInstancesDefinition> builder = QueryDelegateBuilder.listInstances();
		builder.withViolations(getViolations().sub()).withParameters(parameters.create()).withContext(context)
				.withQueryString(queryString);

		return new AspectDelegateDefinition(listTaskDefinition(), builder.build(), aspect, isList);

	}

	private ListTaskDefinition listTaskDefinition() {

		Map<String, ParameterBound> parametersHints = new HashMap<>();
		parametersHints.put("processInstanceIds", new ParameterNameBound(TypeHint.STRING_LIST, "processInstanceIds"));

		QueryParameter[] parameters = new QueryParameter[] { new QueryParameter("processInstanceIds",
				TypeHint.STRING_LIST, 0) };

		QueryDelegateBuilder<ListTaskDefinition> builder = QueryDelegateBuilder.listTasks();

		builder.withViolations(getViolations().sub()).withParameters(parameters)
				.withContext(new Context(Collections.emptyMap(), parametersHints))
				.withQueryString("processInstanceId IN :processInstanceIds");
		return builder.build();
	}

	public AspectDelegateBuilder withAspect(AspectDefinition aspect) {
		this.aspect = aspect;
		return this;
	}

}
