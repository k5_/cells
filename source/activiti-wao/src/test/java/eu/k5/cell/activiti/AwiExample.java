package eu.k5.cell.activiti;

import java.util.List;

import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.junit.Assert;
import org.junit.Test;

public class AwiExample extends AbstractExample {

	@Test
	public void listTasks() {
		start(1, "stringVar", "var1");
		start(1, "stringVar", "var2");

		Awi awi = new Awi(getService());

		List<TaskEntity> tasks = awi.listTasks(Variables.class, "var.stringVar = :1", "var1");
		Assert.assertEquals(1, tasks.size());
		Assert.assertEquals("var1", tasks.get(0).getProcessVariables().get("stringVar"));
	}

	@Test
	public void listInstances() {
		start(1, "stringVar", "var1");
		start(1, "stringVar", "var2");

		Awi awi = new Awi(getService());

		List<ExecutionEntity> instances = awi.listInstances(Variables.class, "var.stringVar = :1", "var1");
		Assert.assertEquals(1, instances.size());
		Assert.assertEquals("var1", instances.get(0).getProcessVariables().get("stringVar"));

	}

	public interface Variables {
		String stringVar();
	}

}
