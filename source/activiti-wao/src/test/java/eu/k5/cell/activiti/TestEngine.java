package eu.k5.cell.activiti;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PreDestroy;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestEngine {
	private static final Logger LOGGER = LoggerFactory.getLogger(TestEngine.class);
	private final ProcessEngine engine;
	private final String module;
	private final String[] files;

	public TestEngine(ProcessEngineConfigurationImpl configuration, String module, String... files) {
		this.module = module;
		this.files = files;
		Set<Class<?>> mappers = new HashSet<>();
		mappers.add(AqlMapper.class);
		configuration.setCustomMybatisMappers(mappers);

		engine = configuration.buildProcessEngine();
		System.out.println("deploy");
		deploy(engine, module, files);

	}

	public static TestEngine inMemory(String module, String... files) {
		ProcessEngineConfigurationImpl configuration = (ProcessEngineConfigurationImpl) ProcessEngineConfiguration
				.createStandaloneInMemProcessEngineConfiguration();
		return new TestEngine(configuration, module, files);
	}

	public static TestEngine mssql(String module, String... files) {
		if (true) {
			return inMemory(module, files);
		}
		ProcessEngineConfigurationImpl configuration = (ProcessEngineConfigurationImpl) ProcessEngineConfiguration
				.createProcessEngineConfigurationFromResource("/deploy.cfg.xml");
		return new TestEngine(configuration, module, files);
	}

	public ProcessEngine getEngine() {
		return engine;
	}

	@PreDestroy
	public void destroy() {
		if (engine != null) {
			engine.close();
		}
	}

	public void cleanUp() {
		RepositoryService repositoryService = engine.getRepositoryService();
		for (Deployment deployment : repositoryService.createDeploymentQuery().list()) {
			repositoryService.deleteDeployment(deployment.getId(), true);
			LOGGER.info("Deleting deployment with id {}", deployment.getId());
		}

	}

	public void redeploy() {
		deploy(engine, module, files);
	}

	public void reset() {
		cleanUp();
		redeploy();
	}

	private static void deploy(ProcessEngine engine, String module, String... files) {
		RepositoryService repository = engine.getRepositoryService();

		DeploymentBuilder builder = repository.createDeployment();

		for (String file : files) {

			InputStream content = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("processes/" + module + "/" + file + ".bpmn20.xml");
			if (content == null) {
				throw new IllegalArgumentException("Unable to locate on the classpath processes/" + module + "/" + file
						+ ".bpmn20.xml");
			} else {
				builder.addInputStream(file + ".bpmn20.xml", content);
			}
		}
		builder.deploy();
	}

	public ProcessInstance find(String key) {
		ProcessInstance result = engine.getRuntimeService().createProcessInstanceQuery()
				.processInstanceBusinessKey(key).includeProcessVariables().singleResult();
		return result;
	}
}
