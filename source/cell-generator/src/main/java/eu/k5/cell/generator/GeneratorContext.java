package eu.k5.cell.generator;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.jboss.jdeparser.FormatPreferences;
import org.jboss.jdeparser.JDeparser;
import org.jboss.jdeparser.JFiler;
import org.jboss.jdeparser.JSources;

public class GeneratorContext {

	public GeneratorContext() {
		sources = JDeparser.createSources(getFiler(), new FormatPreferences(new Properties()));
	}

	private final JFiler filer = new JFiler() {
		@Override
		public OutputStream openStream(final String packageName, final String fileName) throws IOException {
			final Key key = new Key(packageName, fileName + ".java");
			if (!sourceFiles.containsKey(key)) {
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				if (sourceFiles.putIfAbsent(key, stream) == null) {
					return stream;
				}
			}
			throw new IOException("Already exists");
		}
	};

	public JFiler getFiler() {
		return filer;
	}

	private final ConcurrentMap<Key, ByteArrayOutputStream> sourceFiles = new ConcurrentHashMap<>();
	private final JSources sources;

	public JSources getSources() {
		return sources;
	}

	static final class Key {
		private final String packageName;
		private final String fileName;

		Key(final String packageName, final String fileName) {
			this.packageName = packageName;
			this.fileName = fileName;
		}

		@Override
		public boolean equals(final Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			final Key key = (Key) o;
			return fileName.equals(key.fileName) && packageName.equals(key.packageName);
		}

		@Override
		public int hashCode() {
			int result = packageName.hashCode();
			result = 31 * result + fileName.hashCode();
			return result;
		}
	}

	public ByteArrayInputStream openFile(String packageName, String fileName) throws FileNotFoundException {
		final ByteArrayOutputStream out = sourceFiles.get(new Key(packageName, fileName));
		if (out == null) {
			throw new FileNotFoundException("No file found for package " + packageName + " file " + fileName);
		}
		return new ByteArrayInputStream(out.toByteArray());
	}

	public void dump() throws IOException {
		for (Key key : sourceFiles.keySet()) {
			ByteArrayInputStream inputStream = openFile(key.packageName, key.fileName);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		}
	}
}
