package eu.k5.cell.parser;

public class TaskParserBuilder<T> extends ParserBuilder<T, TaskParserBuilder<T>> {

	public TaskParserBuilder(Class<T> type) {
		super(type);
		setBuilder(this);
	}

	private void doInit() {

		new TaskInstantiatorBuilder<>(getType()).withBoard();

	}

	@Override
	public TaskParser<T> build() {
		if(!isInit()){
			doInit();
		}

		return new TaskParser<>(size, datas, task)
	}

}
