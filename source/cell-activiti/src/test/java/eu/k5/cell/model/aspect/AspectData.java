package eu.k5.cell.model.aspect;

import eu.k5.cell.annotation.In;

public class AspectData {
	@In
	private String key;

	@In
	private String property;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

}
