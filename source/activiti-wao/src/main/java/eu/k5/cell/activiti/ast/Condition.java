package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

class Condition extends Expression {

	private final Operator operator;

	private final Value left;
	private final Value right;

	public Condition(Operator operator, Value left, Value right) {
		this.left = left;
		this.right = right;
		this.operator = operator;
	}

	@Override
	public void accept(QueryVisitor visitor) {
		visitor.visit(this);
	}

	public Operator getOperator() {
		return operator;
	}

	public Value getLeft() {
		return left;
	}

	public Value getRight() {
		return right;
	}

	@Override
	public TypeHint getHint(Context context) {
		TypeHint leftHint = getLeft().getHint(context);
		TypeHint rightHint = getRight().getHint(context);
		if (Operator.IN.equals(operator) && TypeHint.STRING.equals(leftHint) && TypeHint.STRING_LIST.equals(rightHint)) {
			return TypeHint.BOOLEAN;
		}

		if (!TypeHint.INVALID.equals(leftHint) && leftHint.equals(rightHint)) {
			return TypeHint.BOOLEAN;
		}
		return TypeHint.INVALID;
	}
}
