package eu.k5.cell.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.enterprise.inject.spi.InjectionTarget;
import javax.enterprise.util.AnnotationLiteral;

final class CellsBean implements Bean<Cells> {
	private final InjectionTarget<Cells> it;
	private final Cells cells;

	CellsBean(InjectionTarget<Cells> it, Cells cells) {
		this.it = it;
		this.cells = cells;
	}

	@Override
	public Class<?> getBeanClass() {
		return Cells.class;
	}

	@Override
	public Set<InjectionPoint> getInjectionPoints() {
		return it.getInjectionPoints();

	}

	@Override
	public String getName() {
		return "cells";
	}

	@Override
	public Set<Annotation> getQualifiers() {
		Set<Annotation> qualifiers = new HashSet<Annotation>();
		qualifiers.add(new AnnotationLiteral<Default>() {
			private static final long serialVersionUID = 1L;
		});
		qualifiers.add(new AnnotationLiteral<Any>() {
			private static final long serialVersionUID = 1L;
		});
		return qualifiers;
	}

	@Override
	public Class<? extends Annotation> getScope() {
		return ApplicationScoped.class;
	}

	@Override
	public Set<Class<? extends Annotation>> getStereotypes() {
		return Collections.emptySet();
	}

	@Override
	public Set<Type> getTypes() {
		Set<Type> types = new HashSet<Type>();
		types.add(Cells.class);
		types.add(Object.class);
		return types;
	}

	@Override
	public boolean isAlternative() {
		return false;
	}

	@Override
	public boolean isNullable() {
		return false;
	}

	@Override
	public Cells create(CreationalContext<Cells> ctx) {
		return cells;
	}

	@Override
	public void destroy(Cells instance, CreationalContext<Cells> ctx) {
		it.preDestroy(instance);
		it.dispose(instance);
		ctx.release();
	}
}