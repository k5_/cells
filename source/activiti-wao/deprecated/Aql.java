package eu.k5.cell.activiti;

import java.util.Map;

import org.activiti.engine.ManagementService;

import eu.k5.cell.activiti.ast.InstanceQueryBuilder;
import eu.k5.cell.activiti.ast.TaskQueryRenderer;
import eu.k5.cell.activiti.builder.WaoBuilder;

public class Aql {

	private static final AqlQueries QUERIES = new AqlQueries();

	// private TaskQueryRenderer createTaskQueryBuilder(ManagementService
	// service) {
	// String taskTable = service.getTableName(TaskEntity.class);
	// String variableTable =
	// service.getTableName(VariableInstanceEntity.class);
	// String identityTable = service.getTableName(IdentityLinkEntity.class);
	// CommandExecutor commandExecutor = ((ServiceImpl)
	// service).getCommandExecutor();
	//
	// DatabaseType databaseType = commandExecutor.execute(new
	// Command<DatabaseType>() {
	//
	// @Override
	// public DatabaseType execute(CommandContext commandContext) {
	// DbSqlSessionFactory dbSqlSessionFactory = (DbSqlSessionFactory)
	// commandContext.getSessionFactories()
	// .get(DbSqlSession.class);
	//
	// return DatabaseType.resolve(dbSqlSessionFactory.getDatabaseType());
	// }
	// });
	//
	// System.out.println(databaseType);
	// return new TaskQueryRenderer(databaseType, taskTable, variableTable,
	// identityTable);
	//
	// }
	//
	// private InstanceQueryBuilder createInstanceQueryBuilder(ManagementService
	// service) {
	//
	// String executionTable = service.getTableName(ExecutionEntity.class);
	// String processDefinitionTable =
	// service.getTableName(ProcessDefinitionEntity.class);
	// String variableTable =
	// service.getTableName(VariableInstanceEntity.class);
	// String identityTable = service.getTableName(IdentityLinkEntity.class);
	// CommandExecutor commandExecutor = ((ServiceImpl)
	// service).getCommandExecutor();
	//
	// DatabaseType databaseType = commandExecutor.execute(new
	// Command<DatabaseType>() {
	//
	// @Override
	// public DatabaseType execute(CommandContext commandContext) {
	// DbSqlSessionFactory dbSqlSessionFactory = (DbSqlSessionFactory)
	// commandContext.getSessionFactories()
	// .get(DbSqlSession.class);
	//
	// return DatabaseType.resolve(dbSqlSessionFactory.getDatabaseType());
	// }
	// });
	//
	// return new InstanceQueryBuilder(databaseType, executionTable,
	// processDefinitionTable, variableTable,
	// identityTable);
	//
	// }

	public String parseTaskQuery(ManagementService service, String query, Map<String, TypeHint> parameters,
			Map<String, TypeHint> variables) {
		TaskQueryRenderer builder = createTaskQueryBuilder(service);
		QUERIES.parseTaskQuery(query, parameters, variables).accept(builder);

		Map<String, String> m;

		return builder.getQuery();

	}

	// static List<TaskEntity> getTasksWithVariables(ManagementService service,
	// String query,
	// Map<String, Object> parameters) {
	// if (parameters.containsKey(AqlMapper.SQL)) {
	// throw new
	// IllegalArgumentException("Parameters can not contain the key 'sql'");
	// }
	//
	// Map<String, Object> par = new HashMap<String, Object>(parameters);
	// par.put(AqlMapper.SQL, query);
	// if (!parameters.containsKey("limitForm")) {
	// par.put("limitFrom", 0);
	// }
	// if (!parameters.containsKey("limitTo")) {
	// par.put("limitTo", Integer.MAX_VALUE);
	// }
	// return service.executeCustomSql(new TasksWithVariablesExecution(par));
	// }
	//
	// static List<ExecutionEntity> getInstancesWithVariables(ManagementService
	// service, String query,
	// Map<String, Object> parameters) {
	// if (parameters.containsKey(AqlMapper.SQL)) {
	// throw new
	// IllegalArgumentException("Parameters can not contain the key 'sql'");
	// }
	//
	// Map<String, Object> par = new HashMap<String, Object>(parameters);
	// par.put(AqlMapper.SQL, query);
	// return service.executeCustomSql(new
	// InstancesWithVariablesExecution(par));
	// }
	//
	// static TaskEntity getTaskWithVariables(ManagementService service, String
	// query, Map<String, Object> parameters) {
	//
	// if (parameters.containsKey(AqlMapper.SQL)) {
	// throw new
	// IllegalArgumentException("Parameters can not contain the key 'sql'");
	// }
	//
	// Map<String, Object> par = new HashMap<String, Object>(parameters);
	// par.put(AqlMapper.SQL, query);
	// return service.executeCustomSql(new TaskWithVariablesExecution(par));
	// }

	public String parseInstanceQuery(ManagementService service, String query, Map<String, TypeHint> parameters,
			Map<String, TypeHint> variables) {
		InstanceQueryBuilder builder = createInstanceQueryBuilder(service);
		QUERIES.parseTaskQuery(query, parameters, variables).accept(builder);
		return builder.getQuery();
	}

	public static <W> W bind(Class<W> waoType, ManagementService service) {
		WaoBuilder builder = new WaoBuilder().withType(waoType);
		builder.withManagementService(service).init();

		return waoType.cast(builder.build());
	}

}
