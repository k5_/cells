package eu.k5.cell.activiti.delegate;

import org.activiti.engine.impl.persistence.entity.TaskEntity;

import eu.k5.cell.definition.TaskDefinition;

public class TaskKey {
	private final String processDefinition;
	private final String taskDefinition;

	public TaskKey(String processDefinition, String taskDefinition) {
		this.processDefinition = processDefinition;
		this.taskDefinition = taskDefinition;
	}

	public static TaskKey fromTask(TaskEntity task) {
		return new TaskKey(getProcessDefinitionKey(task), task.getTaskDefinitionKey());
	}

	public static TaskKey fromDefintion(TaskDefinition definition) {
		return new TaskKey(definition.getWorfklow().getProcessDefinition(), definition.getTaskDefinitionKey());
	}

	private static String getProcessDefinitionKey(TaskEntity task) {
		String id = task.getProcessDefinitionId();
		return id.substring(0, id.indexOf(':'));
	}

	public String getProcessDefinition() {
		return processDefinition;
	}

	public String getTaskDefinition() {
		return taskDefinition;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (processDefinition == null ? 0 : processDefinition.hashCode());
		result = prime * result + (taskDefinition == null ? 0 : taskDefinition.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TaskKey other = (TaskKey) obj;
		if (processDefinition == null) {
			if (other.processDefinition != null) {
				return false;
			}
		} else if (!processDefinition.equals(other.processDefinition)) {
			return false;
		}
		if (taskDefinition == null) {
			if (other.taskDefinition != null) {
				return false;
			}
		} else if (!taskDefinition.equals(other.taskDefinition)) {
			return false;
		}
		return true;
	}

}