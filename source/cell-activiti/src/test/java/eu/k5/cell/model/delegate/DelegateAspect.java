package eu.k5.cell.model.delegate;

import eu.k5.cell.annotation.workflow.Task;

public class DelegateAspect {
	private String property;

	@Task
	private DelegateTask task;

	public String getProperty() {
		return property;
	}

	public DelegateTask getTask() {
		return task;
	}

}
