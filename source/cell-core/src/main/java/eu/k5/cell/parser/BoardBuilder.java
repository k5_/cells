package eu.k5.cell.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.k5.cell.extension.Binding;

public class BoardBuilder {

	private final List<SourceReference> requirements = new ArrayList<>();

	private final Map<SourceReference, Integer> index = new HashMap<>();

	public void addBindings(Binding binding) {

	}

	public List<?> getUnsatisfied() {

		return null;
	}

	public void addRequirements(List<Input> requirements) {
		for (Input input : requirements) {
			SourceReference source = input.getSource();
			if (!index.containsKey(source)) {
				index.put(source, this.requirements.size());
				this.requirements.add(source);

			}
		}
	}

	public List<Bound> bind(List<Input> inputs) {
		List<Bound> bounds = new ArrayList<>();
		for (Input input : inputs) {

			Integer boardIndex = index.get(input.getSource());

			bounds.add(new Bound(boardIndex, input.getType()));

		}
		return bounds;
	}

	public int getSize() {
		return requirements.size();
	}

	private Source getSource(SourceReference reference) {
		return reference.getSource();
	}

	public Board build() {
		int index = 0;
		Data[] data = new Data[requirements.size()];
		for (SourceReference reference : requirements) {

			data[index] = new Data(index, reference.getType(), getSource(reference));
			index++;
		}
		return new Board(data);

	}

}
