package eu.k5.cell.activiti;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;

import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.builder.ParameterBuilder;
import eu.k5.cell.activiti.builder.QueryDelegateBuilder;
import eu.k5.cell.activiti.builder.Violations;
import eu.k5.cell.activiti.builder.WaoBuilder;
import eu.k5.cell.activiti.delegate.ListInstancesDefinition;
import eu.k5.cell.activiti.delegate.ListTaskDefinition;

public class Awi {
	private final ManagementService service;

	public Awi(ManagementService service) {
		this.service = service;
	}

	private Map<String, VariableBound> hintsFromClass(Class<?> type) {
		Map<String, VariableBound> hints = new HashMap<>();
		for (Method method : type.getMethods()) {
			TypeHint hint = TypeHint.forType(method.getReturnType());
			if (hint == null) {
				throw new IllegalArgumentException("No Valid typeHint for " + method.getReturnType() + " available");
			}
			hints.put(method.getName(), new VariableBound(method.getName(), hint));

		}
		return hints;
	}

	public List<ExecutionEntity> listInstances(Class<?> hintProvider, String query, Object... parameters) {
		return listInstances(hintsFromClass(hintProvider), query, parameters);
	}

	public List<ExecutionEntity> listInstances(Map<String, VariableBound> variables, String query, Object... parameters) {
		Violations violations = new Violations();
		ParameterBuilder builder = new ParameterBuilder().withViolations(violations).forVarargs(parameters);

		QueryDelegateBuilder<ListInstancesDefinition> queryBuilder = QueryDelegateBuilder.listInstances()
				.withViolations(violations).withContext(new Context(variables, builder.getBounds()))
				.withParameters(builder.create()).withQueryString(query);

		return queryBuilder.build().invoke(service, parameters);
	}

	public List<TaskEntity> listTasks(Class<?> hintProvider, String query, Object... parameters) {
		return listTasks(hintsFromClass(hintProvider), query, parameters);
	}

	public List<TaskEntity> listTasks(Map<String, VariableBound> variables, String query, Object... parameters) {
		Violations violations = new Violations();
		ParameterBuilder builder = new ParameterBuilder().withViolations(violations).forVarargs(parameters);

		QueryDelegateBuilder<ListTaskDefinition> queryBuilder = QueryDelegateBuilder.listTasks()
				.withViolations(violations).withContext(new Context(variables, builder.getBounds()))
				.withParameters(builder.create()).withQueryString(query);

		return queryBuilder.build().invoke(service, parameters);
	}

	public static <W> W bind(Class<W> waoType, ManagementService service) {
		WaoBuilder builder = new WaoBuilder().withType(waoType);
		builder.withManagementService(service).init();

		return waoType.cast(builder.build());
	}
}
