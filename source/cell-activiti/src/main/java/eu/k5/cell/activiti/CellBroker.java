package eu.k5.cell.activiti;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.repository.ProcessDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.k5.cell.CellDelegateExecution;
import eu.k5.cell.CellException;
import eu.k5.cell.definition.JobDefinition;
import eu.k5.cell.extension.Cells;

public class CellBroker implements JavaDelegate {
	private static final Logger LOGGER = LoggerFactory.getLogger(CellBroker.class);

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		ProcessDefinition def = execution.getEngineServices().getRepositoryService().createProcessDefinitionQuery()
				.processDefinitionId(execution.getProcessDefinitionId()).singleResult();

		BeanManager beanManager = CDI.current().getBeanManager();

		Cells cells = resolve(beanManager, Cells.class);

		JobDefinition definition = cells.getJob(def.getKey(), execution.getCurrentActivityId());
		if (definition == null) {
			LOGGER.info("No job definition found for execution {}", execution.getCurrentActivityId());
			throw new CellException(
					Form.at("No job definition found for execution {}", execution.getCurrentActivityId()));
		}

		Object servant = resolve(beanManager, definition.getServantType());

		Object job = definition.read(new ActivitiCellDelegateExecution(execution));

		try {
			definition.getServantMethod().invoke(servant, job);
		} catch (InvocationTargetException ite) {
			Throwable cause = ite.getCause();
			if (cause instanceof Exception) {
				throw (Exception) cause;
			} else {
				LOGGER.error("Invokation of ServantMethod caused throwable of type {} not assignable to Exception",
						cause.getClass().getCanonicalName());
				throw new Exception("Invokation of ServantMethod caused throwable of type "
						+ cause.getClass().getCanonicalName() + " not assignable to Exception", cause);
			}
		}

		Map<String, Object> write = definition.write(job);

		execution.setVariables(write);
		LOGGER.info("CellServant {} {}: Completing job with {}", def.getKey(), execution.getCurrentActivityId(), write);
	}

	static class ActivitiCellDelegateExecution implements CellDelegateExecution {
		private final DelegateExecution execution;

		public ActivitiCellDelegateExecution(DelegateExecution execution) {
			super();
			this.execution = execution;
		}

		@Override
		public Map<String, Object> getVariables() {
			return execution.getVariables();
		}

	}

	private <T> T resolve(BeanManager beanManager, Class<T> type) {
		Set<Bean<?>> beans = beanManager.getBeans(type);
		if (beans.isEmpty()) {
			throw new IllegalArgumentException("Unable to resolve type:" + type);
		}

		@SuppressWarnings("unchecked")
		Bean<Cells> bean = (Bean<Cells>) beans.iterator().next();
		CreationalContext<Cells> ctx = beanManager.createCreationalContext(bean);
		return type.cast(bean.create(ctx));
	}

}
