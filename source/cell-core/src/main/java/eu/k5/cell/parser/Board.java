package eu.k5.cell.parser;

public class Board {
	private final Data[] entries;

	public Board(Data[] entries) {
		this.entries = entries;
	}

	public Data[] getEntries() {
		return entries;
	}

	public int getSize() {
		return entries.length;
	}
}
