package eu.k5.cell.parser;

import eu.k5.cell.annotation.In;

public class TaskParserBuilderExp {

	public static class Struct {

		private final String param;

		public Struct(@In("param") String param) {
			this.param = param;

		}

		public String getParam() {
			return param;
		}
	}

	public static void main(String[] args) {

	}
}
