package eu.k5.cell;

import java.util.Collection;
import java.util.Map;

public interface CellProcessState {

	Collection<CellTask> getTasks();

	Map<String, Object> getVariables();
}
