package eu.k5.cell.definition;

import java.util.Map;

import eu.k5.cell.extension.Writer;

public class StartDefinition {
	private final WorkflowDefinition workflow;
	private final Class<?> type;
	private final Writer writer;
	private final PropertyReader businessKeyReader;

	public StartDefinition(WorkflowDefinition workflow, Class<?> type, Writer writer, PropertyReader businessKeyReader) {
		this.workflow = workflow;
		this.type = type;
		this.writer = writer;
		this.businessKeyReader = businessKeyReader;
	}

	public Map<String, Object> write(Object object) {
		return writer.write(object);
	}

	public String getBusinessKey(Object object) {
		return (String) businessKeyReader.read(object);
	}

	public boolean useBusinessKey() {
		return businessKeyReader != null;
	}

	public WorkflowDefinition getWorkflow() {
		return workflow;
	}

	public Class<?> getType() {
		return type;
	}
}
