package eu.k5.cell.annotation.workflow;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import eu.k5.cell.annotation.AccessObject;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface Workflows {

	Class<?>[] workflow();

	AccessObject[] delegates() default {};

}
