package eu.k5.cell.activiti;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.lang3.RandomStringUtils;

public class TaskTokenCreator implements TaskListener {
	private static final long serialVersionUID = 1L;

	public static final String TOKEN = "token";

	@Override
	public void notify(DelegateTask delegateTask) {

		String rand = RandomStringUtils.randomAscii(32);
		delegateTask.setVariableLocal("token", rand);
	}
}
