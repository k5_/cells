package eu.k5.cell.activiti;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import eu.k5.cell.CellException;
import eu.k5.cell.CellTask;
import eu.k5.cell.InvalidReferenceException;
import eu.k5.cell.NoDefinitionException;
import eu.k5.cell.NonUniqueResultException;
import eu.k5.cell.activiti.filter.AspectFilter;
import eu.k5.cell.activiti.filter.CellTaskReferenceFilter;
import eu.k5.cell.activiti.filter.ChainFilter;
import eu.k5.cell.activiti.filter.ProcessFilter;
import eu.k5.cell.activiti.filter.TaskFilter;
import eu.k5.cell.definition.AspectDefinition;
import eu.k5.cell.definition.DataDefinition;
import eu.k5.cell.definition.StartDefinition;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.extension.Cell;
import eu.k5.cell.shared.CellTaskReference;

public class CellManagerImpl implements CellManager {
	private static final Logger LOGGER = LoggerFactory.getLogger(CellManagerImpl.class);
	private final Cell cell;

	private final RuntimeService runtimeService;
	private final HistoryService historyService;
	private final TaskService taskService;

	private final ManagementService managementService;

	public CellManagerImpl(Cell cell, RuntimeService runtimeService, HistoryService historyService,
			TaskService taskService, ManagementService managementService) {
		this.cell = cell;
		this.runtimeService = runtimeService;
		this.historyService = historyService;
		this.taskService = taskService;
		this.managementService = managementService;
	}

	@Override
	public void start(Object start) {
		StartDefinition definition = cell.getStart(start.getClass());
		if (definition == null) {
			throw new NoDefinitionException(
					Form.at("Cell {}: No StartDefinition found for {}", cell.getName(), start.getClass()));
		}

		Map<String, Object> variables = definition.write(start);
		LOGGER.info("Cell {}: Starting process {}  with variables {}", cell.getName(),
				definition.getWorkflow().getProcessDefinition(), variables);

		if (definition.useBusinessKey()) {
			runtimeService.startProcessInstanceByKey(definition.getWorkflow().getProcessDefinition(),
					definition.getBusinessKey(start), variables);
		} else {
			runtimeService.startProcessInstanceByKey(definition.getWorkflow().getProcessDefinition(), variables);
		}
	}

	private static class TaskReader {
		private final org.activiti.engine.task.Task task;
		private final TaskDefinition definition;

		public TaskReader(Task task, TaskDefinition definition) {
			this.task = task;
			this.definition = definition;
		}

		public Object read() {
			return definition.read(new ActivitiCellTask(task));
		}
	}

	@Override
	public <T> T findTask(Class<T> task, TaskFilter... filter) {
		return findTask(task, Arrays.asList(filter));
	}

	@Override
	public <T> T findTask(Class<T> task, List<TaskFilter> filter) {
		Collection<TaskDefinition> definitions = cell.getTaskDefinition(task);
		List<TaskReader> readers = new ArrayList<>();
		for (TaskDefinition definition : definitions) {

			String key = definition.getTaskDefinitionKey();
			org.activiti.engine.task.Task aTask = ChainFilter.chainTasks(filter).filter(taskService.createTaskQuery())
					.taskDefinitionKey(key).processDefinitionKey(definition.getWorfklow().getProcessDefinition())
					.includeProcessVariables().includeTaskLocalVariables().singleResult();
			if (aTask != null) {
				readers.add(new TaskReader(aTask, definition));
			}
		}
		if (readers.isEmpty()) {
			return null;
		} else if (readers.size() > 1) {
			throw new NonUniqueResultException(Form.at("Cell {}: findTask for class {} resulted in {} results",
					cell.getName(), task, readers.size()));

		}
		return task.cast(readers.get(0).read());
	}

	@Override
	public <T> List<T> listTask(Class<T> task, TaskFilter... filter) {
		return listTask(task, Arrays.asList(filter));
	}

	@Override
	public <T> List<T> listTask(Class<T> task, List<TaskFilter> filter) {
		Collection<TaskDefinition> definitions = cell.getTaskDefinition(task);
		List<TaskReader> readers = new ArrayList<>();

		taskService.createTaskQuery().or().taskAssignee("").taskAssignee("").endOr();

		for (TaskDefinition definition : definitions) {

			String key = definition.getTaskDefinitionKey();
			for (org.activiti.engine.task.Task aTask : ChainFilter.chainTasks(filter)
					.filter(taskService.createTaskQuery()).taskDefinitionKey(key)
					.processDefinitionKey(definition.getWorfklow().getProcessDefinition()).includeProcessVariables()
					.includeTaskLocalVariables().list()) {

				readers.add(new TaskReader(aTask, definition));
			}
		}
		List<T> result = new ArrayList<>();
		for (TaskReader reader : readers) {
			result.add(task.cast(reader.read()));
		}
		return result;
	}

	@Override
	public void complete(Object task) {
		if (task == null) {
			throw new NullPointerException(Form.at("Cell {}: Unable to complete null as task", cell.getName()));
		}

		LOGGER.debug("Cell {}: Attempting to complete task with type {}", cell.getName(), task.getClass());
		Collection<TaskDefinition> definitions = cell.getTaskDefinition(task.getClass());

		TaskDefinition definition = null;
		for (TaskDefinition testDefinition : definitions) {
			if (testDefinition.handles(task)) {
				if (definition == null) {
					definition = testDefinition;
				} else {
					throw new NonUniqueResultException(Form.at("Cell {}: Multiple TaskDefinitions can complete task {}",
							cell.getName(), task.getClass()));
				}
			}
		}
		if (definition == null) {
			throw new NoDefinitionException(
					Form.at("Cell {}: No TaskDefinition found for task {}", cell.getName(), task.getClass()));
		}

		CellTaskReference reference = definition.getTaskReference(task);

		Task aTask;
		try {
			aTask = new CellTaskReferenceFilter(reference).filter(taskService.createTaskQuery()).singleResult();
			if (aTask == null) {
				throw new InvalidReferenceException(
						Form.at("Cell {}: task for CellReference not found {}", cell.getName(), reference));
			}
		} catch (ActivitiException e) {
			throw new InvalidReferenceException(
					Form.at("Cell {}: task for CellReference not found {}", cell.getName(), reference), e);
		}

		Map<String, Object> variables = definition.write(task);
		LOGGER.info("Cell {}: Completing task with {}", cell.getName(), variables);
		taskService.complete(aTask.getId(), variables);

	}

	@Override
	public <T> T readData(Class<T> data, ProcessFilter... filter) {
		return readData(data, Arrays.asList(filter));
	}

	@Override
	public <T> T readData(Class<T> data, List<ProcessFilter> filter) {
		DataDefinition definition = cell.getData(data);
		ProcessInstance processInstance = ChainFilter.chainProcess(filter)
				.filter(runtimeService.createProcessInstanceQuery()).includeProcessVariables().singleResult();
		if (processInstance == null) {
			return null;
		}
		return data.cast(definition.read(new ActivitiCellProcessInstance(processInstance)));
	}

	@Override
	public <A> A findAspect(Class<A> aspect, AspectFilter... filter) {
		return findAspect(aspect, Arrays.asList(filter));
	}

	@Override
	public <A> A findAspect(Class<A> aspect, List<AspectFilter> filter) {

		AspectDefinition definition = cell.getAspect(aspect);

		List<ProcessState> states = readStates(filter);
		if (states.size() == 1) {
			return aspect.cast(definition.read(states.get(0)));
		} else if (states.isEmpty()) {
			return null;
		}
		throw new NonUniqueResultException(Form.at("Cell {}: findAspect for class {} resulted in {} results",
				cell.getName(), aspect, states.size()));
	}

	@Override
	public <A> List<A> listAspect(Class<A> aspect, AspectFilter... filter) {
		return listAspect(aspect, Arrays.asList(filter));
	}

	@Override
	public <A> List<A> listAspect(Class<A> aspect, List<AspectFilter> filter) {

		AspectDefinition definition = cell.getAspect(aspect);

		List<ProcessState> states = readStates(filter);
		List<A> result = new ArrayList<>();
		for (ProcessState state : states) {
			result.add(aspect.cast(definition.read(state)));
		}
		return result;
	}

	private List<ProcessState> readStates(List<AspectFilter> filter) {
		AspectQueryImpl query = createViewQuery();
		AspectFilter chained = ChainFilter.chainAspects(filter);
		chained.filter(query);

		Multimap<String, Task> tasksByInstance = HashMultimap.create();
		for (Task task : query.getTasks().includeTaskLocalVariables().list()) {
			tasksByInstance.put(task.getProcessInstanceId(), task);
		}

		List<ProcessState> states = new ArrayList<>();
		for (ProcessInstance instance : query.getInstances().includeProcessVariables().list()) {
			ProcessState state = new ProcessState(instance, instance.getProcessVariables(),
					ActivitiCellTask.fromCollections(tasksByInstance.get(instance.getId())));
			if (chained.accept(state)) {
				states.add(state);
			}
		}
		if (query.isIncludeHistoric()) {
			for (HistoricProcessInstance instance : query.getHistoricInstances().includeProcessVariables().finished()
					.list()) {
				ProcessState state = new ProcessState(new HistoricState(instance), instance.getProcessVariables(),
						Collections.<CellTask> emptyList());
				if (chained.accept(state)) {
					states.add(state);
				}
			}
		}
		return states;
	}

	private static class HistoricState implements ProcessInstance {

		private final HistoricProcessInstance instance;

		public HistoricState(HistoricProcessInstance instance) {
			this.instance = instance;
		}

		@Override
		public String getId() {
			return instance.getId();
		}

		@Override
		public String getBusinessKey() {
			return instance.getBusinessKey();
		}

		@Override
		public String getProcessDefinitionId() {
			return instance.getProcessDefinitionId();
		}

		@Override
		public String getTenantId() {
			return instance.getTenantId();
		}

		@Override
		public Map<String, Object> getProcessVariables() {
			return instance.getProcessVariables();
		}

		@Override
		public boolean isEnded() {
			return true;
		}

		@Override
		public String getActivityId() {
			return null;
		}

		@Override
		public String getProcessInstanceId() {
			return instance.getId();
		}

		@Override
		public String getParentId() {
			return null;
		}

		@Override
		public boolean isSuspended() {
			return false;
		}

		@Override
		public String getProcessDefinitionName() {
			return null;
		}

		@Override
		public String getProcessDefinitionKey() {
			return null;
		}

		@Override
		public Integer getProcessDefinitionVersion() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDeploymentId() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	private AspectQueryImpl createViewQuery() {
		return new AspectQueryImpl(runtimeService.createProcessInstanceQuery(),
				historyService.createHistoricProcessInstanceQuery(), taskService.createTaskQuery());
	}

	@Override
	public <T> List<T> listData(Class<T> data, ProcessFilter... filter) {
		return listData(data, Arrays.asList(filter));
	}

	@Override
	public <T> List<T> listData(Class<T> data, List<ProcessFilter> filter) {

		List<T> result = new ArrayList<>();
		DataDefinition definition = cell.getData(data);

		for (ProcessInstance processInstance : ChainFilter.chainProcess(filter)
				.filter(runtimeService.createProcessInstanceQuery()).includeProcessVariables().list()) {

			result.add(data.cast(definition.read(new ActivitiCellProcessInstance(processInstance))));
		}

		return result;
	}

	@Override
	public boolean instanceExists(String businessKey, boolean searchHistoric) {
		if (runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(businessKey)
				.processDefinitionKey(cell.getWorkflow().getProcessDefinition()).count() > 0) {
			return true;
		}
		if (searchHistoric
				&& historyService.createHistoricProcessInstanceQuery().processInstanceBusinessKey(businessKey)
						.processDefinitionKey(cell.getWorkflow().getProcessDefinition()).count() > 0) {
			return true;

		}
		return false;
	}

	@Override
	public Set<String> listBusinessKeys(boolean includeHistoric) {
		Set<String> result = new HashSet<>();
		for (ProcessInstance instance : runtimeService.createProcessInstanceQuery()
				.processDefinitionKey(cell.getWorkflow().getProcessDefinition()).list()) {
			result.add(instance.getBusinessKey());
		}
		if (includeHistoric) {
			for (HistoricProcessInstance instance : historyService.createHistoricProcessInstanceQuery()
					.processDefinitionKey(cell.getWorkflow().getProcessDefinition()).finished().list()) {
				result.add(instance.getBusinessKey());
			}

		}
		return result;
	}

	@Override
	public <U> U unwrap(Class<U> type) {
		if (RuntimeService.class.equals(type)) {
			return type.cast(runtimeService);
		} else if (HistoryService.class.equals(type)) {
			return type.cast(historyService);
		} else if (TaskService.class.equals(type)) {
			return type.cast(taskService);
		} else if (Cell.class.equals(type)) {
			return type.cast(cell);
		} else if (ManagementService.class.equals(type)) {
			return type.cast(managementService);
		}
		throw new CellException(Form.at("Unable to unwrap {}", type));
	}

	@Override
	public <T> T findTask(Class<T> task, String query, Map<String, Object> parameters) {

		// new Aql().parseTaskQuery(managementService, query, parameters,
		// variables);
		//
		// Aql.parseTaskQuery(managementService, query, parameters, variables);
		//
		// System.out.println(query);

		// query = query.replace("#{taskTableName}",
		// managementService.getTableName(TaskEntity.class));
		// query = query.replace("#{variableTableName}",
		// managementService.getTableName(VariableInstanceEntity.class));

		{
			final Map<String, Object> param = new HashMap<String, Object>();
			param.putAll(parameters);
			param.put("sql", query);

			// CustomSqlExecution<TaskCellMapper, List<TaskEntity>>
			// customSqlExecution = new
			// AbstractCustomSqlExecution<TaskCellMapper, List<TaskEntity>>(
			// TaskCellMapper.class) {
			//
			// @Override
			// public List<TaskEntity> execute(TaskCellMapper customMapper) {
			// return customMapper.selectx(param);
			// }
			//
			// };

			// List<TaskEntity> res =
			// managementService.executeCustomSql(customSqlExecution);

			// List<Task> res = managementService.executeCommand(new
			// CommandX(param));

			// for (Task t : res) {
			// Map<String, Object> local = t.getTaskLocalVariables();
			// Map<String, Object> variables = t.getProcessVariables();
			// System.out.println(t);
			// System.out.println(variables);
			// System.out.println(local);
			// System.out.println();
			// }

		}

		// {
		// NativeTaskQuery nativeQuery =
		// taskService.createNativeTaskQuery().sql(query);
		// for (Entry<String, Object> parameter : parameters.entrySet()) {
		// nativeQuery.parameter(parameter.getKey(), parameter.getValue());
		// }
		//
		// List<Task> result = nativeQuery.list();
		// Map<String, Object> local = result.get(0).getTaskLocalVariables();
		//
		// Map<String, Object> variables = result.get(0).getProcessVariables();
		// System.out.println(result);
		// System.out.println(variables);
		// System.out.println(local);
		//
		// }
		return null;
	}
}
