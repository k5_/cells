package eu.k5.cell.activiti;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.ManagementService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.task.TaskQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import eu.k5.cell.InvalidReferenceException;
import eu.k5.cell.LiteralReader;
import eu.k5.cell.NoDefinitionException;
import eu.k5.cell.NonUniqueResultException;
import eu.k5.cell.activiti.TaskTokenCreator;
import eu.k5.cell.definition.PropertyReader;
import eu.k5.cell.definition.StartDefinition;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.definition.TaskReader;
import eu.k5.cell.definition.WorkflowDefinition;
import eu.k5.cell.extension.Cell;
import eu.k5.cell.extension.Writer;
import eu.k5.cell.shared.CellTaskReference;
import eu.k5.cell.wao.DelegateDefinition;
import eu.k5.cell.wao.WaoDefinition;

public class WorkflowManagerImplTest {

	@InjectMocks
	private WorkflowManagerImpl manager;

	@Mock
	private ManagementService managementService;

	@Mock
	private RuntimeService runtimeService;

	@Mock
	private TaskService taskService;

	@Mock
	private TaskQuery taskQuery;

	@Mock
	private Cell cell;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		Mockito.when(taskService.createTaskQuery()).thenReturn(taskQuery);
		Mockito.when(taskQuery.taskDefinitionKey(Matchers.anyString())).thenReturn(taskQuery);
		Mockito.when(taskQuery.processInstanceId(Matchers.anyString())).thenReturn(taskQuery);
		Mockito.when(taskQuery.taskId(Matchers.anyString())).thenReturn(taskQuery);
		Mockito.when(taskQuery.taskVariableValueEquals(Matchers.anyString(), Matchers.anyObject())).thenReturn(
				taskQuery);
	}

	static class Entity {

	}

	@Test(expected = NullPointerException.class)
	public void start_null() {
		manager.start(null);
	}

	@Test(expected = NoDefinitionException.class)
	public void start_noDefinition() {
		manager.start(new Entity());
	}

	@Test
	public void start_noBusinessKey() {
		Entity start = new Entity();
		StartDefinition definition = mockStart(start);
		Mockito.when(cell.getStart(Entity.class)).thenReturn(definition);
		manager.start(start);
		Mockito.verify(runtimeService).startProcessInstanceByKey(processDefinition, variables);
	}

	@Test
	public void start_withBusinessKey() {
		Entity start = new Entity();
		StartDefinition definition = mockStart(start);
		Mockito.when(cell.getStart(Entity.class)).thenReturn(definition);
		Mockito.when(definition.useBusinessKey()).thenReturn(true);
		Mockito.when(definition.getBusinessKey(start)).thenReturn("key");
		manager.start(start);
		Mockito.verify(runtimeService).startProcessInstanceByKey(processDefinition, "key", variables);
	}

	@Test(expected = NoDefinitionException.class)
	public void task_noDefinition() {
		Entity task = new Entity();
		Mockito.when(cell.resolveTaskDefinition(task)).thenReturn(Collections.<TaskDefinition> emptyList());
		manager.complete(task);
	}

	@Test(expected = NonUniqueResultException.class)
	public void task_multipleDefinitions() {
		Entity task = new Entity();
		TaskDefinition def1 = mockTaskDefinition(task, "key", "taskId1");
		TaskDefinition def2 = mockTaskDefinition(task, "key", "taskId2");
		Mockito.when(cell.resolveTaskDefinition(task)).thenReturn(Arrays.asList(def1, def2));
		manager.complete(task);
	}

	@Test(expected = NullPointerException.class)
	public void task_null() {
		manager.complete(null);
	}

	@Test
	public void task_oneTaskDefinitionValidTask() {
		Entity task = new Entity();

		TaskDefinition taskDefinition = mockTaskDefinition(task, "key", "taskId");
		Mockito.when(cell.resolveTaskDefinition(task)).thenReturn(Arrays.asList(taskDefinition));
		TaskEntity taskEntity = new TaskEntity();
		taskEntity.setId("taskId");
		Mockito.when(taskQuery.singleResult()).thenReturn(taskEntity);
		manager.complete(task);

		Mockito.verify(taskService).complete("taskId", variables);
		Mockito.verify(taskQuery).processInstanceId("instance");
		Mockito.verify(taskQuery).taskVariableValueEquals(TaskTokenCreator.TOKEN, "token");
		Mockito.verify(taskQuery).taskDefinitionKey("taskDefinition");
	}

	@Test(expected = InvalidReferenceException.class)
	public void task_oneTaskDefinition_activitiTaskMissing() {
		Entity task = new Entity();

		TaskDefinition taskDefinition = mockTaskDefinition(task, "key", "taskId");
		Mockito.when(cell.resolveTaskDefinition(task)).thenReturn(Arrays.asList(taskDefinition));
		TaskEntity taskEntity = new TaskEntity();
		taskEntity.setId("taskId");
		Mockito.when(taskQuery.singleResult()).thenReturn(null);
		manager.complete(task);
	}

	@Test(expected = InvalidReferenceException.class)
	public void task_oneTaskDefinition_activitiThrows() {
		Entity task = new Entity();
		TaskDefinition taskDefinition = mockTaskDefinition(task, "key", "taskId");
		Mockito.when(cell.resolveTaskDefinition(task)).thenReturn(Arrays.asList(taskDefinition));
		TaskEntity taskEntity = new TaskEntity();
		taskEntity.setId("taskId");
		Mockito.when(taskQuery.singleResult()).thenThrow(new ActivitiException("Mock exception"));
		manager.complete(task);
	}

	@Test(expected = NoDefinitionException.class)
	public void wao_unkown() {
		manager.wao(Object.class);
	}

	interface Wao {

	}

	@Test
	public void wao() {
		WaoDefinition definition = new WaoDefinition(Wao.class, Collections.<Method, DelegateDefinition> emptyMap());
		Mockito.when(cell.findDelegate(Wao.class)).thenReturn(definition);
		manager.wao(Wao.class);
	}

	String processDefinition = "process";

	@Mock
	private Map<String, Object> variables;

	private TaskDefinition mockTaskDefinition(Entity instance, String key, String taskId) {
		CellTaskReference reference = new CellTaskReference("instance", taskId, "taskDefinition", "token");
		Writer writer = Mockito.mock(Writer.class);
		TaskReader reader = Mockito.mock(TaskReader.class);
		Mockito.when(writer.write(instance)).thenReturn(variables);
		PropertyReader referenceReader = new LiteralReader(reference);

		return new TaskDefinition(new WorkflowDefinition(null, processDefinition, null), instance.getClass(), key,
				writer, referenceReader, reader);
	}

	private StartDefinition mockStart(Entity instance) {
		StartDefinition definition = Mockito.mock(StartDefinition.class);
		Mockito.when(definition.getWorkflow()).thenReturn(new WorkflowDefinition(null, processDefinition, null));
		Mockito.when(definition.write(instance)).thenReturn(variables);
		return definition;
	}
}
