package eu.k5.cell.generator;

import static org.jboss.jdeparser.JMod.FINAL;
import static org.jboss.jdeparser.JMod.PUBLIC;

import org.jboss.jdeparser.JBlock;
import org.jboss.jdeparser.JCall;
import org.jboss.jdeparser.JClassDef;
import org.jboss.jdeparser.JExprs;
import org.jboss.jdeparser.JMethodDef;
import org.jboss.jdeparser.JParamDeclaration;
import org.jboss.jdeparser.JSourceFile;
import org.jboss.jdeparser.JTypes;
import org.jboss.jdeparser.JVarDeclaration;

import eu.k5.cell.parser.Instantiator;
import eu.k5.cell.parser.Instantiator.PropertyAssigner;

public class InstantiatorGenerator {
	private final GeneratorContext context;

	public InstantiatorGenerator(GeneratorContext context) {
		this.context = context;
	}

	public void generate(Instantiator<?> instantiator) {

		JSourceFile source = context.getSources().createSourceFile("eu.k5.cell", "TestDefinition");

		JClassDef reader = source._class(PUBLIC | FINAL, "TestInstantiator");

		reader._implements(Inst.class);

		JMethodDef read = reader.method(PUBLIC, Object.class, "read");
		JParamDeclaration board = read.param(JTypes.$t("Object[]"), "board");

		JVarDeclaration instance = init(instantiator, read.body(), board);
		for (PropertyAssigner property : instantiator.getProperties()) {
			read.body().add(JExprs.$v(instance).call(property.getMethod().getName())
					.arg(JExprs.$v(board).idx(property.getIndex())));
		}
		read.body()._return(JExprs.$v(instance));
	}

	private JVarDeclaration init(Instantiator<?> instantiator, JBlock block, JParamDeclaration board) {
		JCall constructor = JTypes.$t(instantiator.getType())._new();

		for (int index : instantiator.getConstructorIndex()) {
			constructor.arg(JExprs.$v(board).idx(index));
		}

		return block.var(0, instantiator.getType(), "instance", constructor);

	}
}
