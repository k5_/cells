package eu.k5.cell.activiti.ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.ServiceImpl;
import org.activiti.engine.impl.db.DbSqlSession;
import org.activiti.engine.impl.db.DbSqlSessionFactory;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.persistence.entity.IdentityLinkEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.persistence.entity.VariableInstanceEntity;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import eu.k5.cell.activiti.TypeHint;
import eu.k5.cell.activiti.query.AqlLexer;
import eu.k5.cell.activiti.query.AqlParser;
import eu.k5.cell.activiti.query.AqlParser.SectionContext;

public class AqlQueries {

	public Query parseTaskQuery(String query) {
		return parseTaskQuery(query, Collections.<String, TypeHint> emptyMap(),
				Collections.<String, TypeHint> emptyMap());
	}

	public String parseTaskQuery(ManagementService service, String query, Map<String, TypeHint> parameters,
			Map<String, TypeHint> variables) {
		TaskQueryRenderer builder = createTaskQueryBuilder(service);
		parseTaskQuery(query, parameters, variables).accept(builder);

		Map<String, String> m;

		return builder.getQuery();

	}

	private TaskQueryRenderer createTaskQueryBuilder(ManagementService service) {
		String taskTable = service.getTableName(TaskEntity.class);
		String variableTable = service.getTableName(VariableInstanceEntity.class);
		String identityTable = service.getTableName(IdentityLinkEntity.class);
		String processDefinitionTable = service.getTableName(ProcessDefinitionEntity.class);
		CommandExecutor commandExecutor = ((ServiceImpl) service).getCommandExecutor();

		DatabaseType databaseType = commandExecutor.execute(new Command<DatabaseType>() {

			@Override
			public DatabaseType execute(CommandContext commandContext) {
				DbSqlSessionFactory dbSqlSessionFactory = (DbSqlSessionFactory) commandContext.getSessionFactories()
						.get(DbSqlSession.class);

				return DatabaseType.resolve(dbSqlSessionFactory.getDatabaseType());
			}
		});
		return new TaskQueryRenderer(databaseType, taskTable, variableTable, identityTable, processDefinitionTable);

	}

	public Query parseTaskQuery(String query, Map<String, TypeHint> parameters, Map<String, TypeHint> variables) {
		if ("".equals(query)) {
			Section section = new Section(new BooleanExpression(true), new HashSet<String>());
			return new Query(Arrays.asList(section), new ArrayList<String>());
		}

		CharStream stream = new ANTLRInputStream(query);

		AqlLexer lexer = new AqlLexer(stream);

		CommonTokenStream tokens = new CommonTokenStream(lexer);

		AqlParser parser = new AqlParser(tokens);

		SectionContext context = parser.section();

		ParseTreeWalker walker = new ParseTreeWalker();
		QueryTransformer listener = new QueryTransformer();
		walker.walk(listener, context);

		return listener.getQuery();
	}

	public Query parseInstanceQuery(String query, Map<String, TypeHint> parameterHints,
			Map<String, TypeHint> variableHints) {
		if ("".equals(query)) {
			Section section = new Section(new BooleanExpression(true), new HashSet<String>());
			return new Query(Arrays.asList(section), new ArrayList<String>());
		}

		CharStream stream = new ANTLRInputStream(query);

		AqlLexer lexer = new AqlLexer(stream);

		CommonTokenStream tokens = new CommonTokenStream(lexer);

		AqlParser parser = new AqlParser(tokens);

		SectionContext section = parser.section();

		ParseTreeWalker walker = new ParseTreeWalker();
		QueryTransformer listener = new QueryTransformer();
		walker.walk(listener, section);

		return listener.getQuery();
	}
}
