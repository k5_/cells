package eu.k5.cell.activiti.ast;

public interface ValueVisitor {

	void visit(BooleanLiteral booleanLiteral);

	void visit(DateLiteral dateLiteral);

	void visit(DoubleLiteral doubleLiteral);

	void visit(LongLiteral longLiteral);

	void visit(NamedParameter namedParameter);

	void visit(BasicProperty property);

	void visit(StringLiteral stringLiteral);

	void visit(Variable variable);

	void visit(CandidateProperty candidateProperty);

}
