package eu.k5.cell.activiti.filter;

import org.activiti.engine.task.TaskQuery;

public interface TaskFilter {
	TaskQuery filter(TaskQuery query);
}
