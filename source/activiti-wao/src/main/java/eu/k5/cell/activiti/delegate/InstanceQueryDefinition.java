package eu.k5.cell.activiti.delegate;

import org.activiti.engine.ManagementService;

import eu.k5.cell.activiti.ast.BaseQueryRenderer;
import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.ast.InstanceQueryBuilder;
import eu.k5.cell.activiti.ast.Query;
import eu.k5.cell.activiti.builder.QueryDefinition;
import eu.k5.cell.activiti.builder.QueryParameter;

public abstract class InstanceQueryDefinition<D extends Delegate> extends QueryDefinition<D> {

	public InstanceQueryDefinition(Query query, QueryParameter[] parameters, Context... context) {
		super(query, parameters, context);

	}

	@Override
	protected BaseQueryRenderer queryRenderer(ManagementService service) {
		return new InstanceQueryBuilder(getDatabaseType(service), getExecutionTable(service),
				getProcessDefinitionTable(service), getVariableTable(service), getIdentityTable(service));
	}

	public static ListInstancesDefinition list(Query query, QueryParameter[] parameters, Context... context) {
		return new ListInstancesDefinition(query, parameters, context);
	}

	public static InstanceQueryDefinition<FindInstanceDelegate> find(Query query, QueryParameter[] parameters,
			Context context) {
		return new FindInstanceDefinition(query, parameters, context);

	}
}
