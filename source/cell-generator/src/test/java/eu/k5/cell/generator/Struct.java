package eu.k5.cell.generator;

import eu.k5.cell.annotation.In;

public class Struct {

	private final String param;

	public Struct(@In("param") String param) {
		this.param = param;

	}

	public String getParam() {
		return param;
	}
}