package eu.k5.cell.model.start;

import eu.k5.cell.annotation.Out;
import eu.k5.cell.annotation.workflow.Start;

@Start
public class StartStart {

	private static final String IGNORE = "ignore";

	@Out("ey")
	private final String key;

	private final String property;

	public StartStart(String key, String property) {
		this.key = key;
		this.property = property;
	}

	public String getKey() {
		return key;
	}

	public String getProperty() {
		return property;
	}

}
