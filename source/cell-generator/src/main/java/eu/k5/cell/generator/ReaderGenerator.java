package eu.k5.cell.generator;

import static org.jboss.jdeparser.JExprs.*;

import java.util.IdentityHashMap;
import java.util.Map;

import org.jboss.jdeparser.JBlock;
import org.jboss.jdeparser.JExprs;
import org.jboss.jdeparser.JParamDeclaration;
import org.jboss.jdeparser.JVarDeclaration;

import eu.k5.cell.definition.AssignerBound;
import eu.k5.cell.definition.Initializer;
import eu.k5.cell.definition.PropertyAssignerField;
import eu.k5.cell.extension.Reader;
import eu.k5.cell.extension.ReaderBuilder.BindingInitializer;

public class ReaderGenerator {

	private final Reader reader;

	private final Map<Initializer, JVarDeclaration> initialized = new IdentityHashMap<>();

	public ReaderGenerator(Reader reader) {
		this.reader = reader;
	}

	public void generate() {
		for (AssignerBound bound : reader.getAssigners()) {

		}

	}

	private void genInitializer(JBlock block, JParamDeclaration variables, BindingInitializer init, Class<?> type) {
		JVarDeclaration var = block.var(0, type, "var1");
		block.add($v(var).assign($v(variables).call("get").arg(JExprs.str(init.getBinding()).cast(type))));
		initialized.put(init, var);
	}

	private void genAssigner(JBlock block, JVarDeclaration result, Initializer initializer, PropertyAssignerField field) {
		JVarDeclaration var = initialized.get(initializer);
		block.add($v(result.name()).field(field.getFieldName()).assign($v(var.name())));
	}

}
