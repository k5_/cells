package eu.k5.cell.model.job;

import eu.k5.cell.annotation.Bind;
import eu.k5.cell.annotation.workflow.Job;
import eu.k5.cell.annotation.workflow.Servant;
import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Workflow;

@Workflow(processKey = "jobProcess",
		binder = JobWorkflow.Binder.class,
		activity = JobWorkflow.Activity.class,
		servants = { @Servant(JobWorkflow.Servant.class) })
public interface JobWorkflow {

	interface Binder {

		void setKey(String key);

		@Bind(businessKey = true)
		String getKey();

		void setProperty(String key);

		@Bind
		String getProperty();

		void setProperty2(int property2);

		String getInOut();

		@Bind
		String getProperty2();

		void setProperty3(int property3);

		@Bind
		String getProperty3();

		String getDefaultOut();

	}

	interface Activity {
		@Start
		JobStart getStart1();

	}

	interface Servant {

		@Job(definition = "TestJob")
		void execute(JobJob job);
	}

}
