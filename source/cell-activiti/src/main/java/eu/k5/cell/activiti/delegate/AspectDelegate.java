package eu.k5.cell.activiti.delegate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import eu.k5.cell.CellException;
import eu.k5.cell.activiti.ActivitiCellTask;
import eu.k5.cell.activiti.ProcessState;
import eu.k5.cell.definition.AspectDefinition;
import eu.k5.cell.wao.Delegate;

public class AspectDelegate implements Delegate {

	private final AspectDefinition definition;

	private final ListInstancesDelegate listInstances;

	private final ListTasksDelegate listTasks;

	private final boolean isList;

	public AspectDelegate(ListInstancesDelegate listInstances, ListTasksDelegate listTasks, AspectDefinition definition,
			boolean isList) {
		this.listInstances = listInstances;
		this.listTasks = listTasks;
		this.definition = definition;
		this.isList = isList;
	}

	@Override
	public Object invoke(Object... args) {

		List<ExecutionEntity> instances = listInstances.invoke(args);

		if (instances.isEmpty()) {
			if (isList) {
				return Collections.emptyList();
			} else {
				return null;
			}
		}

		if (!isList && instances.size() != 1) {
			throw new CellException("Not unique result");
		}

		List<String> processInstanceIds = new ArrayList<String>(instances.size());
		for (ExecutionEntity instance : instances) {
			processInstanceIds.add(instance.getId());
		}

		Multimap<String, Task> tasksByInstance = HashMultimap.create();
		for (Task task : listTasks.invoke(processInstanceIds)) {
			tasksByInstance.put(task.getProcessInstanceId(), task);
		}

		List<Object> aspects = new ArrayList<Object>();
		for (ProcessInstance instance : instances) {
			aspects.add(definition.read(new ProcessState(instance, instance.getProcessVariables(),
					ActivitiCellTask.fromCollections(tasksByInstance.get(instance.getId())))));
		}

		if (isList) {
			return aspects;
		} else {
			return aspects.get(0);
		}
	}
}
