package eu.k5.cell.model.task;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.In;
import eu.k5.cell.annotation.Out;
import eu.k5.cell.annotation.states.TaskState;
import eu.k5.cell.annotation.states.TaskStateType;
import eu.k5.cell.annotation.workflow.Data;
import eu.k5.cell.annotation.workflow.Task;
import eu.k5.cell.shared.CellTaskReference;

@Task
public class TaskTask {

	@CellReference
	private CellTaskReference reference;

	@TaskState(TaskStateType.ASSIGNEE)
	private String assignee;

	@Data
	private TaskData data;

	@In
	private String key;

	@In
	private String property;

	@Out
	private String property3;

	@Out
	private String defaultOut = "defaultValue";

	@In
	@Out
	private String inOut;

	@TaskState(resolver = TaskDerivedTaskState.class)
	private String derived;

	public String getDefaultOut() {
		return defaultOut;
	}

	public void setDefaultOut(String defaultOut) {
		this.defaultOut = defaultOut;
	}

	public CellTaskReference getReference() {
		return reference;
	}

	public void setReference(CellTaskReference reference) {
		this.reference = reference;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getProperty3() {
		return property3;
	}

	public void setProperty3(String property3) {
		this.property3 = property3;
	}

	public String getInOut() {
		return inOut;
	}

	public void setInOut(String inOut) {
		this.inOut = inOut;
	}

	@Override
	public String toString() {
		return "ATestTask [key=" + key + ", property=" + property + ", property3=" + property3 + ", property2=" + inOut
				+ "]";
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getDerived() {
		return derived;
	}

	public void setDerived(String derived) {
		this.derived = derived;
	}

	public TaskData getData() {
		return data;
	}

	public void setData(TaskData data) {
		this.data = data;
	}

}
