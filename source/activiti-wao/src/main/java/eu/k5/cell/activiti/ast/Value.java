package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

abstract class Value {

	public abstract void accept(ValueVisitor visitor);

	public abstract TypeHint getHint(Context context);
}
