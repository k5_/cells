package eu.k5.cell.model.aspect;

import eu.k5.cell.annotation.states.ProcessStateResolver;

public class AspectDerivedProcessState implements ProcessStateResolver {

	@Override
	public String read(Object state) {
		return "aspect";
	}

	@Override
	public Class<?> getReturnType() {
		return String.class;
	}

}
