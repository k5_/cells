package eu.k5.cell.activiti;

import org.junit.Test;

import eu.k5.cell.activiti.builder.WaoBuilder;

public class WaoBuilderTest {

	public void init() {

	}

	interface EmptyType {

	}

	@Test
	public void buildEmpty() {
		WaoBuilder builder = new WaoBuilder();
		builder.withType(EmptyType.class);
		builder.build();
	}
}
