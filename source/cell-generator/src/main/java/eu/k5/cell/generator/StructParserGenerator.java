package eu.k5.cell.generator;

import static org.jboss.jdeparser.JMod.FINAL;
import static org.jboss.jdeparser.JMod.PUBLIC;
import static org.jboss.jdeparser.JMod.STATIC;

import org.jboss.jdeparser.JBlock;
import org.jboss.jdeparser.JCall;
import org.jboss.jdeparser.JClassDef;
import org.jboss.jdeparser.JExpr;
import org.jboss.jdeparser.JExprs;
import org.jboss.jdeparser.JMethodDef;
import org.jboss.jdeparser.JParamDeclaration;
import org.jboss.jdeparser.JSourceFile;
import org.jboss.jdeparser.JTypes;
import org.jboss.jdeparser.JVarDeclaration;

import eu.k5.cell.parser.Context;
import eu.k5.cell.parser.Data;
import eu.k5.cell.parser.Instantiator;
import eu.k5.cell.parser.Instantiator.PropertyAssigner;
import eu.k5.cell.parser.Parser;
import eu.k5.cell.parser.SourceVisitor;
import eu.k5.cell.parser.StructParser;
import eu.k5.cell.parser.TaskReferenceSource;
import eu.k5.cell.parser.VariableSource;

public class StructParserGenerator {

	private final GeneratorContext context;

	public StructParserGenerator(GeneratorContext context) {
		this.context = context;
	}

	public void generate(StructParser<?> parser) {

		Class<?> type = parser.getStruct().getType();

		JSourceFile source = context.getSources().createSourceFile(type.getPackage().getName(),
				type.getName() + "Parser");

		JClassDef cls = source._class(PUBLIC | FINAL, type.getSimpleName() + "Parser");

		JClassDef boardCls = createBoardClass(cls, parser);

		cls._implements(JTypes.$t(Parser.class.getCanonicalName() + "<" + type.getCanonicalName() + ">"));

		JMethodDef parse = cls.method(PUBLIC, type, "parse");
		JParamDeclaration context = parse.param(JTypes.$t(Context.class), "context");

		SourceRenderer renderer = new SourceRenderer(context);

		JVarDeclaration board = parse.body().var(0, JTypes.typeOf(boardCls), "board", JTypes.typeOf(boardCls)._new());
		for (Data data : parser.getBoard().getEntries()) {
			parse.body().assign(JExprs.$v(board).field(fieldName(data.getIndex())),
					data.getSource().accept(renderer, null));
		}

		JVarDeclaration instance = createInstance(parser.getStruct(), parse.body(), board);
		parse.body()._return(JExprs.$v(instance));
	}

	private JVarDeclaration createInstance(Instantiator<?> instantiator, JBlock block, JVarDeclaration board) {
		JVarDeclaration instance = init(instantiator, block, board);
		for (PropertyAssigner property : instantiator.getProperties()) {
			block.add(JExprs.$v(instance).call(property.getMethod().getName())
					.arg(JExprs.$v(board).field(fieldName(property.getIndex()))));
		}
		return instance;
	}

	private String fieldName(int index) {
		return "_" + index;
	}

	private JVarDeclaration init(Instantiator<?> instantiator, JBlock block, JVarDeclaration board) {
		JCall constructor = JTypes.$t(instantiator.getType())._new();

		for (int index : instantiator.getConstructorIndex()) {
			constructor.arg(JExprs.$v(board).field(fieldName(index)));
		}

		return block.var(0, instantiator.getType(), "instance", constructor);

	}

	private static class SourceRenderer implements SourceVisitor<JExpr, Void> {
		private final JParamDeclaration context;

		public SourceRenderer(JParamDeclaration context) {
			this.context = context;
		}

		@Override
		public JExpr visit(VariableSource variable, Void param) {

			return JExprs.$v(context).call("getVariable").arg(JExprs.str(variable.getName()))
					.cast(JTypes.$t((Class<?>) variable.getType()));
		}

		@Override
		public JExpr visit(TaskReferenceSource task, Void param) {
			return JExprs.$v(context).call("getTaskReference");
		}

	}

	private JClassDef createBoardClass(JClassDef outer, StructParser<?> parser) {
		JClassDef board = outer._class(FINAL | STATIC, "Board");
		for (Data data : parser.getBoard().getEntries()) {
			board.field(0, JTypes.$t((Class<?>) data.getType()), fieldName(data.getIndex()));
		}
		return board;
	}

}
