package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

class StringLiteral extends Literal<String> {

	public StringLiteral(String literal) {
		super(literal, TypeHint.STRING);
	}

	@Override
	public void accept(ValueVisitor visitor) {
		visitor.visit(this);
	}

}
