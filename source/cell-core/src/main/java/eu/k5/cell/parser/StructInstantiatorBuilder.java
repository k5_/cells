package eu.k5.cell.parser;

import java.util.List;

public class StructInstantiatorBuilder<T> {

	private final Class<T> type;

	private final TypeAnalyser<T> analyser;

	private BoardBuilder boardBuilder;

	public StructInstantiatorBuilder(Class<T> type) {
		this.type = type;
		analyser = new TypeAnalyser<T>(type);
	}

	public StructInstantiatorBuilder<T> withBoard(BoardBuilder boardBuilder) {
		this.boardBuilder = boardBuilder;
		return this;
	}

	public void doInit() {
		analyser.init();
		boardBuilder.addRequirements(analyser.getConstructorParameters());
	}

	public Instantiator<T> create() {
		List<Bound> bind = boardBuilder.bind(analyser.getConstructorParameters());

		int index = 0;
		int[] constructorIndex = new int[bind.size()];
		for (Bound bound : bind) {
			constructorIndex[index++] = bound.getIndex();
		}

		InstantiatorBuilder<T> instantiator = new InstantiatorBuilder<>(type);
		instantiator.withConstructorIndex(constructorIndex, analyser.getConstructor());

		return instantiator.build();
	}

}
