package eu.k5.cell.extension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.CheckForNull;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import eu.k5.cell.definition.AspectDefinition;
import eu.k5.cell.definition.DataDefinition;
import eu.k5.cell.definition.JobDefinition;
import eu.k5.cell.definition.StartDefinition;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.definition.WorkflowDefinition;
import eu.k5.cell.wao.WaoDefinition;

public class Cell {
	private final String name;
	private final WorkflowDefinition workflow;

	private final Map<Class<?>, StartDefinition> starts;
	private final Map<Class<?>, DataDefinition> data;
	private final Map<Class<?>, AspectDefinition> aspects;

	private final Multimap<Class<?>, TaskDefinition> tasks;

	private final Map<String, JobDefinition> jobs;

	private final Map<Class<?>, WaoDefinition> delegates;

	public Cell(WorkflowDefinition workflow, List<StartDefinition> starts, List<DataDefinition> datas,
			List<TaskDefinition> tasks, List<AspectDefinition> aspects, List<JobDefinition> jobs,
			Map<Class<?>, WaoDefinition> delegates) {
		this.workflow = workflow;
		name = workflow.getType().getSimpleName();
		{
			ImmutableMap.Builder<Class<?>, StartDefinition> startBuilder = ImmutableMap
					.<Class<?>, StartDefinition> builder();
			for (StartDefinition definition : starts) {
				startBuilder.put(definition.getType(), definition);
			}
			this.starts = startBuilder.build();

		}
		{
			ImmutableMap.Builder<Class<?>, DataDefinition> dataBuilder = ImmutableMap
					.<Class<?>, DataDefinition> builder();
			for (DataDefinition defintion : datas) {
				dataBuilder.put(defintion.getType(), defintion);
			}
			data = dataBuilder.build();
		}
		{
			ImmutableMultimap.Builder<Class<?>, TaskDefinition> taskBuilder = ImmutableMultimap
					.<Class<?>, TaskDefinition> builder();
			for (TaskDefinition definition : tasks) {
				taskBuilder.put(definition.getType(), definition);
			}
			this.tasks = taskBuilder.build();
		}
		{
			ImmutableMap.Builder<Class<?>, AspectDefinition> aspectBuilder = ImmutableMap
					.<Class<?>, AspectDefinition> builder();
			for (AspectDefinition defintion : aspects) {
				aspectBuilder.put(defintion.getType(), defintion);
			}
			this.aspects = aspectBuilder.build();

		}
		{
			ImmutableMap.Builder<String, JobDefinition> jobBuilder = ImmutableMap.<String, JobDefinition> builder();
			for (JobDefinition defintion : jobs) {
				jobBuilder.put(defintion.getDefinitionKey(), defintion);
			}
			this.jobs = jobBuilder.build();
		}
		{
			ImmutableMap.Builder<Class<?>, WaoDefinition> delegateBuilder = ImmutableMap
					.<Class<?>, WaoDefinition> builder();
			for (Entry<Class<?>, WaoDefinition> delegate : delegates.entrySet()) {
				delegateBuilder.put(delegate.getKey(), delegate.getValue());
			}
			this.delegates = delegateBuilder.build();
		}
	}

	public Collection<TaskDefinition> getTaskDefinition(Class<?> clazz) {
		return tasks.get(clazz);
	}

	public String getName() {
		return name;
	}

	public Class<?> getType() {
		return workflow.getType();
	}

	public AspectDefinition getAspect(Class<?> aspect) {
		return aspects.get(aspect);
	}

	public StartDefinition getStart(Class<? extends Object> type) {
		return starts.get(type);
	}

	public DataDefinition getData(Class<?> type) {
		return data.get(type);
	}

	public JobDefinition getJob(String job) {
		return jobs.get(job);
	}

	@CheckForNull
	public WaoDefinition findDelegate(Class<?> type) {
		return delegates.get(type);
	}

	Map<Class<?>, StartDefinition> getStarts() {
		return starts;
	}

	public Multimap<Class<?>, TaskDefinition> getTasks() {
		return tasks;
	}

	public List<TaskDefinition> resolveTaskDefinition(Object task) {
		List<TaskDefinition> result = new ArrayList<TaskDefinition>(2);
		for (TaskDefinition testDefinition : getTaskDefinition(task.getClass())) {
			if (testDefinition.handles(task)) {
				result.add(testDefinition);
			}
		}
		return result;
	}

	public boolean isComposite() {
		return false;
	}

	public WorkflowDefinition getWorkflow() {
		return workflow;
	}
}
