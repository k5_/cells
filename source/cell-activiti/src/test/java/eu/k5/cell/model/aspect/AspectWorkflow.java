package eu.k5.cell.model.aspect;

import eu.k5.cell.annotation.Aspect;
import eu.k5.cell.annotation.Bind;
import eu.k5.cell.annotation.states.ProcessState;
import eu.k5.cell.annotation.states.ProcessStateType;
import eu.k5.cell.annotation.workflow.Data;
import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Task;
import eu.k5.cell.annotation.workflow.Workflow;

@Workflow(processKey = "aspectProcess", activity = AspectWorkflow.Activity.class, binder = AspectWorkflow.Binder.class)
public interface AspectWorkflow {

	interface Binder {

		@Bind(businessKey = true)
		String getKey();

		String getProperty();

		String getProperty2();

		String getProperty3();

		@ProcessState(ProcessStateType.IS_ENDED)
		Boolean getIsEnded();

		@ProcessState(ProcessStateType.INSTANCE_ID)
		String getInstance();

		@ProcessState(resolver = AspectDerivedProcessState.class)
		String getDerivedTest();

	}

	interface Activity {
		@Bind
		@Start
		AspectStart getStart();

		@Bind
		@Task(definition = "TestTask")
		AspectTask getTask();

		@Data
		AspectData getData();

		@Bind
		@Aspect
		AspectAspect getAspect();

	}

}
