package eu.k5.cell.generator;

import java.util.Map;

public interface TaskReader<T> {

	T read(Map<String, Object> variables);

}
