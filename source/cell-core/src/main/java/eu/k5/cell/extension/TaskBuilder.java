package eu.k5.cell.extension;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.In;
import eu.k5.cell.annotation.Out;
import eu.k5.cell.annotation.states.TaskState;
import eu.k5.cell.annotation.workflow.Data;
import eu.k5.cell.definition.DataDefinition;
import eu.k5.cell.definition.PropertyAssigner;
import eu.k5.cell.definition.PropertyReader;
import eu.k5.cell.definition.PropertyReaderField;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.definition.TaskReader;
import eu.k5.cell.definition.TaskStateReader;
import eu.k5.cell.extension.ReaderBuilder.PropertyBound;

public class TaskBuilder extends Builder<TaskDefinition, TaskBuilder> {
	private final ReaderBuilder reader = new ReaderBuilder();
	private final WriterBuilder writer = new WriterBuilder();
	private String definition;
	private PropertyReader referenceReader;
	private PropertyAssigner referenceAssigner;
	private final List<StateDefinition<TaskStateReader>> taskStates = new ArrayList<>();
	private NestedDataBuilder nestedData;

	public TaskBuilder() {
		super(TaskDefinition.class);
		setBuilder(this);
	}

	public String getTaskDefinitionKey() {
		return definition;
	}

	@Override
	public void doInit() {
		initTask();

		reader.withBinder(getBinder()).forEntity(getType()).withValidator(getValidator()).load();

		List<PropertyBound> referenceBounds = reader.selectReaders(CellReference.class);
		if (referenceBounds.size() != 1) {
			addViolation("TaskDefinition without cellReference");
		} else {
			referenceBounds.get(0).setIgnored(true);
			referenceAssigner = referenceBounds.get(0).getAssigner();
		}
		initTaskStates();

		writer.withBinder(getBinder()).forEntity(getType()).withValidator(getValidator()).init();

		for (PropertyBound bound : reader.selectReaders(Out.class)) {
			if (!bound.isAnnotationPresent(In.class)) {
				bound.setIgnored(true);
			}
		}

		nestedData.init(reader.selectReaders(Data.class));

		reader.init();
		plusViolations(reader.getViolations());
		plusViolations(writer.getViolations());
	}

	private void initTaskStates() {
		StateBuilder stateBuilder = new StateBuilder(getValidator(), getType());
		for (PropertyBound bound : reader.selectReaders(TaskState.class)) {
			TaskStateReader state = stateBuilder.readTaskStates(bound.getAnnotation(TaskState.class), bound.getName(),
					bound.getType());
			if (state != null) {
				taskStates.add(new StateDefinition<TaskStateReader>(bound.getAssigner(), state));
				bound.setIgnored(true);
			}
		}
	}

	private void initTask() {
		Class<?> current = getType();

		while (current != null) {
			Field[] fields = current.getDeclaredFields();
			for (Field field : fields) {
				if (field.isAnnotationPresent(CellReference.class)) {
					field.setAccessible(true);
					referenceReader = new PropertyReaderField(null, field);
				}
			}
			current = current.getSuperclass();
		}
	}

	public TaskBuilder withDefinitionKey(String definition) {
		this.definition = definition;
		return this;
	}

	public TaskBuilder withDatas(List<DataDefinition> datas) {
		nestedData = new NestedDataBuilder(getType(), getValidator(), datas);
		return this;
	}

	@Override
	protected TaskDefinition doBuild() {
		TaskReader taskReader = new TaskReader(reader.build(), referenceAssigner, taskStates,
				nestedData.getNestedDatas());

		return new TaskDefinition(getWorkflow(), getType(), definition, writer.build(), referenceReader, taskReader);
	}
}
