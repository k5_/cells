package eu.k5.cell.activiti.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import eu.k5.cell.CellException;
import eu.k5.cell.activiti.ProcessState;
import eu.k5.cell.definition.AspectDefinition;

public class AspectDelegateTest {

	AspectDelegate delegate;

	@Mock
	private ListInstancesDelegate listInstances;
	@Mock
	private ListTasksDelegate listTasks;

	@Mock
	private AspectDefinition definition;

	@Captor
	private ArgumentCaptor<List<String>> listTasksCaptor;

	@Captor
	private ArgumentCaptor<ProcessState> processStateCaptor;

	private List<ExecutionEntity> instances;
	private List<TaskEntity> tasks;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		instances = new ArrayList<>();
		tasks = new ArrayList<>();
		Mockito.when(listInstances.invoke(Matchers.anyVararg())).thenReturn(instances);
		Mockito.when(listTasks.invoke(Matchers.anyVararg())).thenReturn(tasks);
	}

	@Test
	public void list_noInstances() {
		delegate = new AspectDelegate(listInstances, listTasks, definition, true);
		Object result = delegate.invoke();
		Assert.assertTrue(result instanceof List);
		Assert.assertTrue(((List<?>) result).isEmpty());
	}

	@Test
	public void noList_noInstances() {
		delegate = new AspectDelegate(listInstances, listTasks, definition, false);
		Object result = delegate.invoke();
		Assert.assertNull(result);
	}

	@Test(expected = CellException.class)
	public void noList_multipleInstances() {
		addExecution("1").addExecution("2");
		delegate = new AspectDelegate(listInstances, listTasks, definition, false);
		delegate.invoke();
	}

	@Test
	public void list_oneInstance() {
		delegate = new AspectDelegate(listInstances, listTasks, definition, true);

		addExecution("1");
		Object result = delegate.invoke();

		Assert.assertEquals(Arrays.asList("1"), captureProcessInstanceIds());
		Assert.assertTrue(result instanceof List);
		Assert.assertEquals(1, ((List<?>) result).size());
		Assert.assertTrue(captureProcessState(0).getTasks().isEmpty());
	}

	private List<String> captureProcessInstanceIds() {
		Mockito.verify(listTasks).invoke(listTasksCaptor.capture());
		return listTasksCaptor.getValue();
	}

	private ProcessState captureProcessState(int index) {
		Mockito.verify(definition).read(processStateCaptor.capture());
		return processStateCaptor.getAllValues().get(index);

	}

	private AspectDelegateTest addExecution(String id) {
		ExecutionEntity execution = Mockito.mock(ExecutionEntity.class);
		Mockito.when(execution.getId()).thenReturn(id);
		instances.add(execution);
		return this;
	}
}
