package eu.k5.cell.activiti.ast;

public class TaskQueryRenderer extends BaseQueryRenderer {
	private static final String TASK_PREFIX = "res";
	private final String taskTable;
	private Context context;

	public TaskQueryRenderer(DatabaseType type, String taskTable, String variableTable, String identityTable,
			String processDefintionTable) {
		super(type, variableTable, identityTable, processDefintionTable);
		this.taskTable = taskTable;

	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public String getResourcePrefix() {
		return TASK_PREFIX;
	}

	@Override
	public void visit(Section section) {

		StringBuilder from = currentContext().getFrom();

		from.append("FROM ").append(taskTable).append(" ").append(getResourcePrefix());
		from.append(" left outer join ").append(getVariableTable()).append(" ").append(getVariablePrefix());
		from.append(" ON ").append(getResourcePrefix()).append(".ID_ = ").append(getVariablePrefix())
				.append(".TASK_ID_ or ").append(getResourcePrefix()).append(".PROC_INST_ID_ = ")
				.append(getVariablePrefix()).append(".EXECUTION_ID_ ");

		currentContext().getWhere().append("WHERE ");
		section.getWhereExpression().accept(this);

		currentContext().getSelect().append("SELECT ");
		appendSelectTaskWithVariables();

	}

	public void appendSelectTaskWithVariables() {
		StringBuilder select = currentContext().getSelect();
		currentContext().getPrefix().append(getType().limitBefore());
		currentContext().getSuffix().append(getType().limitAfter());
		if (DatabaseType.MSSQ.equals(getType())) {

			select.append("DISTINCT ");
			select.append("TEMPRES_ID_ as ID_,");
			select.append("TEMPRES_REV_ as REV_,");
			select.append("TEMPRES_NAME_ as NAME_,");
			select.append("TEMPRES_PARENT_TASK_ID_ as PARENT_TASK_ID_,");
			select.append("TEMPRES_DESCRIPTION_ as DESCRIPTION_,");
			select.append("TEMPRES_PRIORITY_ as PRIORITY_,");
			select.append("TEMPRES_CREATE_TIME_ as CREATE_TIME_,");
			select.append("TEMPRES_OWNER_ as OWNER_,");
			select.append("TEMPRES_ASSIGNEE_ as ASSIGNEE_,");
			select.append("TEMPRES_DELEGATION_ as DELEGATION_,");
			select.append("TEMPRES_EXECUTION_ID_ as EXECUTION_ID_,");
			select.append("TEMPRES_PROC_INST_ID_ as PROC_INST_ID_,");
			select.append("TEMPRES_PROC_DEF_ID_ as PROC_DEF_ID_,");
			select.append("TEMPRES_TASK_DEF_KEY_ as TASK_DEF_KEY_,");
			select.append("TEMPRES_DUE_DATE_ as DUE_DATE_,");
			select.append("TEMPRES_SUSPENSION_STATE_ as SUSPENSION_STATE_,");
			select.append("TEMPVAR_ID_ as VAR_ID_,");
			select.append("TEMPVAR_NAME_ as VAR_NAME_, ");
			select.append("TEMPVAR_TYPE_ as VAR_TYPE_, ");
			select.append("TEMPVAR_REV_ as VAR_REV_, ");
			select.append("TEMPVAR_PROC_INST_ID_ as VAR_PROC_INST_ID_,");
			select.append("TEMPVAR_EXECUTION_ID_ as VAR_EXECUTION_ID_,");
			select.append("TEMPVAR_TASK_ID_ as VAR_TASK_ID_, ");
			select.append("TEMPVAR_BYTEARRAY_ID_ as VAR_BYTEARRAY_ID_, ");
			select.append("TEMPVAR_DOUBLE_ as VAR_DOUBLE_,");
			select.append("TEMPVAR_TEXT_ as VAR_TEXT_, ");
			select.append("TEMPVAR_TEXT2_ as VAR_TEXT2_, ");
			select.append("TEMPVAR_LONG_ as VAR_LONG_ ");

			select.append(getType().limitOuterJoinBetween());

			select.append("");
			select.append("RES.ID_ as TEMPRES_ID_,");
			select.append("RES.REV_ as TEMPRES_REV_,");
			select.append("RES.NAME_ as TEMPRES_NAME_,");
			select.append("RES.PARENT_TASK_ID_ as TEMPRES_PARENT_TASK_ID_,");
			select.append("RES.DESCRIPTION_ as TEMPRES_DESCRIPTION_, ");
			select.append("RES.PRIORITY_ as TEMPRES_PRIORITY_,");
			select.append("RES.CREATE_TIME_ as TEMPRES_CREATE_TIME_,");
			select.append("RES.OWNER_ as TEMPRES_OWNER_, ");
			select.append("RES.ASSIGNEE_ as TEMPRES_ASSIGNEE_,");
			select.append("RES.DELEGATION_ as TEMPRES_DELEGATION_,");
			select.append("RES.EXECUTION_ID_ as TEMPRES_EXECUTION_ID_,");
			select.append("RES.PROC_INST_ID_ as TEMPRES_PROC_INST_ID_, ");
			select.append("RES.PROC_DEF_ID_ as TEMPRES_PROC_DEF_ID_, ");
			select.append("RES.TASK_DEF_KEY_ as TEMPRES_TASK_DEF_KEY_, ");
			select.append("RES.DUE_DATE_ as TEMPRES_DUE_DATE_, ");
			select.append("RES.SUSPENSION_STATE_ as TEMPRES_SUSPENSION_STATE_, ");
			select.append("VAR.ID_ as TEMPVAR_ID_,");
			select.append("VAR.NAME_ as TEMPVAR_NAME_,");
			select.append("VAR.TYPE_ as TEMPVAR_TYPE_,");
			select.append("VAR.REV_ as TEMPVAR_REV_, ");
			select.append("VAR.PROC_INST_ID_ as TEMPVAR_PROC_INST_ID_,");
			select.append("VAR.EXECUTION_ID_ as TEMPVAR_EXECUTION_ID_,");
			select.append("VAR.TASK_ID_ as TEMPVAR_TASK_ID_,");
			select.append("VAR.BYTEARRAY_ID_ as TEMPVAR_BYTEARRAY_ID_, ");
			select.append("VAR.DOUBLE_ as TEMPVAR_DOUBLE_,");
			select.append("VAR.TEXT_ as TEMPVAR_TEXT_,");
			select.append("VAR.TEXT2_ as TEMPVAR_TEXT2_, ");
			select.append("VAR.LONG_ as TEMPVAR_LONG_ ");
		} else {

			select.append(TASK_PREFIX).append(".*, ");
			selectVariables();
		}

	}

	@Override
	protected String addVariable(boolean local) {
		String alias = "A" + currentContext().getVariableIndex().incrementAndGet();

		currentContext().getFrom().append("inner join ").append(getVariableTable()).append(" ").append(alias)
				.append(" on res.PROC_INST_ID_ = ").append(alias).append(".PROC_INST_ID_ ");
		return alias;
	}

	@Override
	protected String getProcessDefinitionPrefix() {
		return "P";
	}

	class TaskRenderContext extends RenderContext {
		private boolean processDefinitionIncluded = false;

		public TaskRenderContext(Context context) {
			super(context);
		}

		@Override
		public void includeProcessDefinition() {
			if (processDefinitionIncluded) {
				return;
			}
			String prefix = getProcessDefinitionPrefix();
			currentContext().getFrom().append(" inner join ").append(getProcessDefinitionTable()).append(" ")
					.append(prefix).append(" ON ").append(getResourcePrefix()).append(".PROC_DEF_ID_ = ")
					.append(prefix).append(".ID_ ");
			processDefinitionIncluded = true;
		}
	}

	@Override
	protected RenderContext createContext() {
		return new TaskRenderContext(context);
	}
}
