package eu.k5.cell.activiti;

public class ParameterNameBound extends ParameterBound {

	private final String name;

	public ParameterNameBound(TypeHint type, String name) {
		super(type);
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
