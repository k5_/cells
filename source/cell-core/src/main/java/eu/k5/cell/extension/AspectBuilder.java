package eu.k5.cell.extension;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.k5.cell.annotation.states.ProcessState;
import eu.k5.cell.annotation.workflow.Data;
import eu.k5.cell.annotation.workflow.Task;
import eu.k5.cell.definition.AspectDefinition;
import eu.k5.cell.definition.DataDefinition;
import eu.k5.cell.definition.NestedTaskDefinition;
import eu.k5.cell.definition.ProcessStateReader;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.extension.ReaderBuilder.PropertyBound;

public class AspectBuilder extends Builder<AspectDefinition, AspectBuilder> {
	private static final Logger LOGGER = LoggerFactory.getLogger(AspectBuilder.class);
	private final ReaderBuilder reader = new ReaderBuilder();

	private final List<NestedTaskDefinition> aspectTasks = new ArrayList<>();
	private List<TaskDefinition> tasks;

	private final List<StateDefinition<ProcessStateReader>> processStates = new ArrayList<>();

	private NestedDataBuilder nestedData;
	private List<DataDefinition> datas;

	public AspectBuilder() {
		super(AspectDefinition.class);
		setBuilder(this);
	}

	@Override
	protected void doInit() {
		LOGGER.info("Initializing {}", getType());

		reader.forEntity(getType()).useAll(true).withBinder(getBinder()).withValidator(getValidator()).load();

		List<PropertyBound> taskBounds = reader.selectReaders(Task.class);
		for (PropertyBound taskBound : taskBounds) {
			useTask(taskBound);
		}

		initProcessStates();
		for (PropertyBound bound : reader.getBounds()) {
			ProcessStateReader reader = getBinder().getProcessState(bound.getName());
			if (reader != null && !bound.isAnnotationPresent(ProcessState.class)) {
				processStates.add(new StateDefinition<ProcessStateReader>(bound.getAssigner(), reader));
				bound.setIgnored(true);
			}
		}

		nestedData = new NestedDataBuilder(getType(), getValidator(), datas);
		nestedData.init(reader.selectReaders(Data.class));

		reader.init();

		plusViolations(reader.getViolations());
	}

	private void initProcessStates() {
		StateBuilder stateBuilder = new StateBuilder(getValidator(), getType());
		for (PropertyBound bound : reader.selectReaders(ProcessState.class)) {
			ProcessStateReader processState = stateBuilder.readProcessStates(bound.getAnnotation(ProcessState.class),
					bound.getName(), bound.getType());
			if (processState != null) {
				processStates.add(new StateDefinition<ProcessStateReader>(bound.getAssigner(), processState));
				bound.setIgnored(true);
			}
		}
	}

	private List<TaskDefinition> getTasks(Task task, Class<?> type) {
		List<TaskDefinition> filtered = new ArrayList<>();
		for (TaskDefinition definition : tasks) {

			if (definition != null && definition.getType().equals(type) && definition.matches(task)) {
				filtered.add(definition);
			}
		}
		return filtered;
	}

	private void useTask(PropertyBound bound) {
		Class<?> type = bound.getType();

		Task task = bound.getAnnotation(Task.class);

		List<TaskDefinition> builders = getTasks(task, type);
		if (builders.isEmpty()) {
			addViolation("No TaskDefinition matching type {} at bound {}", type, bound.getName());
		} else {
			bound.setIgnored(true);
		}

		aspectTasks.add(new NestedTaskDefinition(bound.getAssigner(), builders, false));
	}

	@Override
	protected AspectDefinition doBuild() {
		return new AspectDefinition(getWorkflow(), getType(), reader.build(), aspectTasks, nestedData.getNestedDatas(),
				processStates);
	}

	public AspectBuilder withTasks(List<TaskDefinition> tasks) {
		this.tasks = tasks;
		return this;
	}

	public AspectBuilder withDatas(List<DataDefinition> datas) {
		this.datas = datas;
		return this;
	}
}
