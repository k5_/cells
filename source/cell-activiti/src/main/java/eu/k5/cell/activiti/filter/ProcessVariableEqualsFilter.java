package eu.k5.cell.activiti.filter;

import org.activiti.engine.runtime.ProcessInstanceQuery;
import org.activiti.engine.task.TaskQuery;

import eu.k5.cell.activiti.ProcessState;

public class ProcessVariableEqualsFilter implements AspectFilter, TaskFilter, ProcessFilter {

	private final String variableName;
	private final Object variableValue;

	public ProcessVariableEqualsFilter(String variableName, Object variableValue) {
		this.variableName = variableName;
		this.variableValue = variableValue;
	}

	@Override
	public TaskQuery filter(TaskQuery query) {
		return query.processVariableValueEquals(variableName, variableValue);
	}

	@Override
	public AspectQuery filter(AspectQuery query) {
		return query.processVariableValueEquals(variableName, variableValue);
	}

	@Override
	public ProcessInstanceQuery filter(ProcessInstanceQuery query) {
		return query.variableValueEquals(variableName, variableValue);
	}

	@Override
	public boolean accept(ProcessState state) {
		return true;
	}

}
