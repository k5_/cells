package eu.k5.cell.strategem;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import eu.k5.cell.TestEngine;
import eu.k5.cell.WorkflowManager;
import eu.k5.cell.extension.CellExtension;
import eu.k5.strategem.cell.shared.Decide;
import eu.k5.strategem.cell.shared.MoveTask;
import eu.k5.strategem.cell.shared.SelectFlag;
import eu.k5.strategem.cell.shared.StrategemAspect;
import eu.k5.strategem.shared.Direction;
import eu.k5.strategem.shared.GameReference;
import eu.k5.strategem.shared.Layout;
import eu.k5.strategem.shared.Location;
import eu.k5.strategem.shared.TokenType;

@RunWith(CdiRunner.class)
@AdditionalClasses({  CellExtension.class, StrategemWorkflow.class })
public class StrategemWorkflowTest {
	private static TestEngine engine = new TestEngine("strategem", "Game");


private WorkflowManager<StrategemWorkflow> manager;


	

	@Test
	public void start() {

		GameReference ownerRef = strategems.start("test", Layout._7x7, false);

		{

			StrategemAspect view = strategems.getView(ownerRef);
			Assert.assertNotNull(view);

			SelectFlag selectFlag = view.getSelectFlag();
			selectFlag.setLocation(new Location(0, 0));

			strategems.selectFlag(ownerRef, selectFlag);
		}
		{
			StrategemAspect view = strategems.getView(ownerRef);
			Assert.assertNull(view.getSelectFlag());
		}

		GameReference oppRef = strategems.join(ownerRef.getGame());
		{

			StrategemAspect view = strategems.getView(oppRef);
			Assert.assertNotNull(view);

			SelectFlag selectFlag = view.getSelectFlag();
			selectFlag.setLocation(new Location(6, 6));
			strategems.selectFlag(oppRef, selectFlag);
		}
		{
			StrategemAspect view = strategems.getView(oppRef);
			Assert.assertNull(view.getSelectFlag());

		}

		{
			StrategemAspect view = strategems.getView(ownerRef);
			Assert.assertNotNull(view);
			MoveTask move = view.getMove();
			move.setSource(new Location(1, 6));
			move.setMovement(Direction.DOWN);

			strategems.move(move);

			Assert.assertNull(strategems.getView(ownerRef).getMove());
		}

		move(oppRef, 5, 0, Direction.UP);
		move(ownerRef, 2, 6, Direction.DOWN);
		move(oppRef, 4, 0, Direction.UP);
		move(ownerRef, 3, 6, Direction.DOWN);
		move(oppRef, 3, 0, Direction.UP);
		move(ownerRef, 4, 6, Direction.DOWN);
		// move(oppRef, 2, 0, Movement.UP);
		// move(ownerRef, 5, 6, Movement.DOWN);

		{
			StrategemAspect owner = strategems.getView(ownerRef);
			Decide decide = owner.getDecide();
			Assert.assertNotNull(decide);
			decide.setDecision(TokenType.PAPER);
			strategems.decide(decide);
		}
		{
			StrategemAspect opp = strategems.getView(oppRef);
			Decide other = opp.getDecide();
			other.setDecision(TokenType.ROCK);
			strategems.decide(other);
		}
		move(oppRef, 5, 5, Direction.RIGHT);
		{
			StrategemAspect opp = strategems.getView(oppRef);
			Decide current = opp.getDecide();
			current.setDecision(TokenType.SCISSORS);
			strategems.decide(current);
		}

		move(ownerRef, 1, 1, Direction.DOWN);
		move(oppRef, 2, 0, Direction.UP);

		{
			StrategemAspect opp = strategems.getView(oppRef);
			Decide decide = opp.getDecide();
			Assert.assertNotNull(decide);
			decide.setDecision(TokenType.PAPER);
			strategems.decide(decide);
		}
		{
			StrategemAspect owner = strategems.getView(ownerRef);
			Decide other = owner.getDecide();
			other.setDecision(TokenType.ROCK);
			strategems.decide(other);
		}
		move(ownerRef, 2, 1, Direction.RIGHT);
		move(oppRef, 1, 0, Direction.UP);

	}

	private void move(GameReference reference, int sourceRow, int sourceColumn, Direction movement) {

		StrategemAspect view = strategems.getView(reference);
		MoveTask move = view.getMove();
		Assert.assertNotNull("Opponent is missing move task", move);
		move.setSource(new Location(sourceRow, sourceColumn));
		move.setMovement(movement);
		strategems.move(move);

	}

	@AfterClass
	public static void destroy() {
		engine.destroy();
	}

	@ApplicationScoped
	@Produces
	public TaskService produceTaskService() {
		return engine.getEngine().getTaskService();
	}

	@ApplicationScoped
	@Produces
	public RuntimeService produceRuntimeService() {
		return engine.getEngine().getRuntimeService();
	}

	@ApplicationScoped
	@Produces
	public HistoryService produceHistoryService() {
		return engine.getEngine().getHistoryService();
	}
}
