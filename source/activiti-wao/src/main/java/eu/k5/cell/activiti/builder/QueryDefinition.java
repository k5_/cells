package eu.k5.cell.activiti.builder;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.db.DbSqlSession;
import org.activiti.engine.impl.db.DbSqlSessionFactory;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.IdentityLinkEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.persistence.entity.VariableInstanceEntity;

import eu.k5.cell.activiti.ast.BaseQueryRenderer;
import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.ast.DatabaseType;
import eu.k5.cell.activiti.ast.Query;
import eu.k5.cell.activiti.delegate.Delegate;

public abstract class QueryDefinition<D extends Delegate> {
	private final Query query;
	private final QueryParameter[] parameters;
	private final Context[] context;

	public QueryDefinition(Query query, QueryParameter[] parameters, Context... context) {
		this.query = query;
		this.parameters = parameters;
		this.context = context;
	}

	protected String getVariableTable(ManagementService service) {
		return service.getTableName(VariableInstanceEntity.class);
	}

	protected String getTaskTable(ManagementService service) {
		return service.getTableName(TaskEntity.class);
	}

	protected String getExecutionTable(ManagementService service) {
		return service.getTableName(ExecutionEntity.class);
	}

	protected String getProcessDefinitionTable(ManagementService service) {
		return service.getTableName(ProcessDefinitionEntity.class);
	}

	protected String getIdentityTable(ManagementService service) {
		return service.getTableName(IdentityLinkEntity.class);
	}

	protected DatabaseType getDatabaseType(ManagementService service) {

		// method = service.getClass().getMethod("getCommandExecutor");
		// CommandExecutor commandExecutor = (CommandExecutor)
		// method.invoke(service);
		// // CommandExecutor commandExecutor = ((ManagementServiceImpl)
		// // service).getCommandExecutor();

		return service.executeCommand(new Command<DatabaseType>() {

			@Override
			public DatabaseType execute(CommandContext commandContext) {
				DbSqlSessionFactory dbSqlSessionFactory = (DbSqlSessionFactory) commandContext.getSessionFactories()
						.get(DbSqlSession.class);

				return DatabaseType.resolve(dbSqlSessionFactory.getDatabaseType());
			}
		});

	}

	protected Query getQuery() {
		return query;
	}

	public QueryParameter[] getParameters() {
		return parameters;
	}

	public String render(ManagementService service) {
		BaseQueryRenderer renderer = queryRenderer(service);
		for (Context context : this.context) {
			renderer.setContext(context);
			getQuery().accept(renderer);
		}

		return renderer.getQuery();
	}

	protected abstract BaseQueryRenderer queryRenderer(ManagementService service);

	public abstract D bind(ManagementService service);
}
