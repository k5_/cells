package eu.k5.cell.activiti.filter;

import org.activiti.engine.runtime.ProcessInstanceQuery;

public interface ProcessFilter {

	ProcessInstanceQuery filter(ProcessInstanceQuery query);
}
