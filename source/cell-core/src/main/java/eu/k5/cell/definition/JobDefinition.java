package eu.k5.cell.definition;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import eu.k5.cell.CellDelegateExecution;
import eu.k5.cell.extension.Reader;
import eu.k5.cell.extension.StateDefinition;
import eu.k5.cell.extension.Writer;

public class JobDefinition {
	private final Writer writer;
	private final Reader reader;
	private final String definitionKey;
	private final Class<?> servantType;
	private final Method servantMethod;
	private final List<StateDefinition<JobStateReader>> jobStates;
	private final List<NestedDataDefinition> nestedData;

	public JobDefinition(String definitionKey, Class<?> servantType, Method servantMethod, Reader reader, Writer writer,
			List<StateDefinition<JobStateReader>> jobStates, List<NestedDataDefinition> nestedData) {
		this.definitionKey = definitionKey;

		this.servantType = servantType;
		this.servantMethod = servantMethod;
		this.reader = reader;
		this.writer = writer;
		this.jobStates = jobStates;
		this.nestedData = nestedData;
	}

	public Method getServantMethod() {
		return servantMethod;
	}

	public String getDefinitionKey() {
		return definitionKey;
	}

	public Object read(CellDelegateExecution execution) {
		Object job = reader.read(execution.getVariables());

		for (StateDefinition<JobStateReader> reader : jobStates) {

			reader.getAssigner().assign(job, reader.getReader().read(execution));
		}
		for (NestedDataDefinition nested : nestedData) {
			nested.getAssigner().assign(job, nested.getDefinition().read(execution.getVariables()));
		}
		return job;
	}

	public Class<?> getServantType() {
		return servantType;
	}

	public Map<String, Object> write(Object job) {
		return writer.write(job);
	}

}
