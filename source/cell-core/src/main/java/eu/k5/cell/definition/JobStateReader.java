package eu.k5.cell.definition;

import eu.k5.cell.CellDelegateExecution;
import eu.k5.cell.annotation.states.JobStateResolver;
import eu.k5.cell.annotation.states.JobStateType;

public class JobStateReader {

	private final JobStateType type;
	private final JobStateResolver reader;
	private final String name;

	public JobStateReader(String name, JobStateType type, JobStateResolver reader) {
		this.name = name;
		this.type = type;
		this.reader = reader;
	}

	public Object read(CellDelegateExecution execution) {
		return reader.read(execution);
	}

	public JobStateType getType() {
		return type;
	}

	public JobStateResolver getReader() {
		return reader;
	}

	public String getName() {
		return name;
	}

}
