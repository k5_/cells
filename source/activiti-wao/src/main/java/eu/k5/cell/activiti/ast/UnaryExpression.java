package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

class UnaryExpression extends Expression {
	private final Expression expression;

	public UnaryExpression(Expression expression) {
		this.expression = expression;
	}

	@Override
	public void accept(QueryVisitor visitor) {
		visitor.visit(this);
	}

	public Expression getExpression() {
		return expression;
	}

	@Override
	public TypeHint getHint(Context context) {
		return expression.getHint(context);
	}
}
