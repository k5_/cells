package eu.k5.cell.activiti.filter;

import java.util.List;

import org.activiti.engine.task.TaskQuery;

import eu.k5.cell.activiti.ProcessState;

public class CandidateFilter implements TaskFilter, AspectFilter {
	private final List<String> candidateGroups;

	public CandidateFilter(List<String> candidateGroups) {
		this.candidateGroups = candidateGroups;
	}

	@Override
	public AspectQuery filter(AspectQuery query) {
		return query.taskCandidateGroupIn(candidateGroups);
	}

	@Override
	public boolean accept(ProcessState state) {
		return true;
	}

	@Override
	public TaskQuery filter(TaskQuery query) {
		return query.taskCandidateGroupIn(candidateGroups);
	}

}
