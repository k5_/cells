package eu.k5.cell.extension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.CheckForNull;
import javax.enterprise.inject.Alternative;

import eu.k5.cell.definition.JobDefinition;

@Alternative
public class Cells {
	private final Map<String, Cell> binderByProcess = new HashMap<>();
	private final Map<Class<?>, Cell> binderByWorfklow = new HashMap<>();

	public Cells() {

	}

	public Cells(List<Cell> cells) {
		for (Cell cell : cells) {
			if (!cell.isComposite()) {
				binderByProcess.put(cell.getWorkflow().getProcessDefinition(), cell);
			}
			binderByWorfklow.put(cell.getType(), cell);
		}
	}

	@CheckForNull
	public Cell get(Class<?> value) {
		return binderByWorfklow.get(value);
	}

	@CheckForNull
	public Cell get(String process) {
		return binderByProcess.get(process);
	}

	public JobDefinition getJob(String key, String job) {
		Cell cell = binderByProcess.get(key);
		if (cell == null) {
			throw new IllegalArgumentException("Cell for process " + key + " does not exist");
		}
		return cell.getJob(job);
	}

}
