package eu.k5.cell.model.aspect;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.In;
import eu.k5.cell.annotation.Out;
import eu.k5.cell.shared.CellTaskReference;

public class AspectTask {
	@CellReference
	private CellTaskReference reference;

	@In
	private String key;

	@In
	private String property;

	@Out
	private String property3;

	@In
	@Out
	private String property2;

	public CellTaskReference getReference() {
		return reference;
	}

	public void setReference(CellTaskReference reference) {
		this.reference = reference;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getProperty3() {
		return property3;
	}

	public void setProperty3(String property3) {
		this.property3 = property3;
	}

	public String getProperty2() {
		return property2;
	}

	public void setProperty2(String property2) {
		this.property2 = property2;
	}

	@Override
	public String toString() {
		return "ATestTask [key=" + key + ", property=" + property + ", property3=" + property3 + ", property2="
				+ property2 + "]";
	}

}
