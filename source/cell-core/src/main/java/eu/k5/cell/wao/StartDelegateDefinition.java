package eu.k5.cell.wao;

import eu.k5.cell.WorkflowManager;

public class StartDelegateDefinition implements DelegateDefinition {
	@Override
	public Delegate bind(final WorkflowManager workflowManager) {

		return new Delegate() {
			@Override
			public Object invoke(Object... args) {
				workflowManager.start(args[0]);
				return null;
			}
		};
	}

}
