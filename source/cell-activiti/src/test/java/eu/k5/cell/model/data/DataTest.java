package eu.k5.cell.model.data;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import eu.k5.cell.activiti.CellManager;
import eu.k5.cell.activiti.TestEngine;
import eu.k5.cell.activiti.filter.BusinessKeyFilter;
import eu.k5.cell.annotation.CellContext;
import eu.k5.cell.extension.CellExtension;
import eu.k5.cell.model.TestProducer;

@RunWith(CdiRunner.class)
@AdditionalClasses({ TestProducer.class, CellExtension.class, DataWorkflow.class })
public class DataTest {
	private static TestEngine refEngine = new TestEngine("data", "data");

	@CellContext(DataWorkflow.class)
	@Inject
	CellManager cell;

	@Produces
	TestEngine engine = refEngine;

	@AfterClass
	public static void destoy() {
		refEngine.destroy();
	}

	@Test
	public void start() {
		DataStart start = new DataStart("key", "property");
		cell.start(start);

		DataData data = cell.readData(DataData.class, new BusinessKeyFilter("key"));

		Assert.assertNotNull(data);
		Assert.assertEquals("property", data.getProperty());

	}
}
