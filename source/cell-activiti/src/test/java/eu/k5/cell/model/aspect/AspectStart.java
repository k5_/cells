package eu.k5.cell.model.aspect;

import eu.k5.cell.annotation.Out;

public class AspectStart {
	@Out
	private final String key;
	@Out
	private final String property;

	public AspectStart(String key, String property) {
		this.key = key;
		this.property = property;
	}

	public String getKey() {
		return key;
	}

	public String getProperty() {
		return property;
	}
}
