package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

public class DoubleLiteral extends Literal<Double> {

	protected DoubleLiteral(Double value) {
		super(value, TypeHint.DOUBLE);
	}

	@Override
	public void accept(ValueVisitor visitor) {
		visitor.visit(this);
	}

}
