package eu.k5.cell.activiti.ast;

enum Operator {
	GT(" > "), GE(" >= "), LE(" <= "), LT(" < "), EQ(" = "), NEQ(" != "), IN(" IN ");
	private final String token;

	private Operator(String token) {
		this.token = token;

	}

	public String getToken() {
		return token;
	}

}
