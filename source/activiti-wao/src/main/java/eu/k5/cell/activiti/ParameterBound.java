package eu.k5.cell.activiti;

public abstract class ParameterBound {
	private final TypeHint type;

	public ParameterBound(TypeHint type) {
		this.type = type;
	}

	public TypeHint getType() {
		return type;
	}
}
