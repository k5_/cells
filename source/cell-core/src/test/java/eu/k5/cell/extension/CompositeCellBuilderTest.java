package eu.k5.cell.extension;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.k5.cell.annotation.workflow.Workflow;
import eu.k5.cell.annotation.workflow.Workflows;
import eu.k5.cell.extension.Validator.Type;

public class CompositeCellBuilderTest {

	private final Validator validator = new Validator();
	private CompositeCellBuilder builder;

	@Before
	public void init() {
		builder = new CompositeCellBuilder().withValidator(validator);
	}

	public interface Binder {

	}

	public interface Activity {

	}

	@Workflow(processKey = "wf",binder = Binder.class,activity = Activity.class)
	public interface Workflow1 {

	}

	@Workflows(workflow = { Workflow1.class })
	public interface Composite {

	}

	@Test
	public void missingSubCell() {
		builder.withType(Composite.class).withCells(new ArrayList<>()).init();
		validator.contains(Type.REFERENCES_UNKOWN_CELL);
		Assert.assertFalse(builder.isInit());
	}

}
