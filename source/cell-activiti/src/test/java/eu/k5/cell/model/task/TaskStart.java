package eu.k5.cell.model.task;

import eu.k5.cell.annotation.Out;
import eu.k5.cell.annotation.workflow.Start;

@Start
public class TaskStart {
	@Out
	private final String key;
	@Out
	private final String property;
	private final String inOut;

	public TaskStart(String key, String property, String inOut) {
		this.key = key;
		this.property = property;
		this.inOut = inOut;
	}

	public String getKey() {
		return key;
	}

	public String getProperty() {
		return property;
	}

	public String getInOut() {
		return inOut;
	}

}
