package eu.k5.cell.model.call;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.In;
import eu.k5.cell.shared.CellTaskReference;

public class CallTask {
	@In
	private String key;

	@In
	private String property;

	@CellReference
	private CellTaskReference reference;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

}
