package eu.k5.cell.annotation.states;

public interface ProcessStateResolver {

	Class<?> getReturnType();

	Object read(Object state);

}
