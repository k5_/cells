package eu.k5.cell.activiti.ast;

import java.util.List;

import eu.k5.cell.activiti.TypeHint;

abstract class NaryExpression extends Expression {
	private final List<Expression> parts;

	public NaryExpression(List<Expression> parts) {
		this.parts = parts;
	}

	public List<Expression> getParts() {
		return parts;
	}

	@Override
	public TypeHint getHint(Context context) {
		for (Expression part : parts) {
			if (!TypeHint.BOOLEAN.equals(part.getHint(context))) {
				return TypeHint.INVALID;
			}
		}
		return TypeHint.BOOLEAN;
	}
}
