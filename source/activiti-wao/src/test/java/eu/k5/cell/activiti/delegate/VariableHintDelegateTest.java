package eu.k5.cell.activiti.delegate;

import org.junit.Test;

public class VariableHintDelegateTest {
	@Test(expected = UnsupportedOperationException.class)
	public void invoke() {
		new VariableHintDelegate("name").invoke();
	}
}
