package eu.k5.cell.activiti.delegate;

import java.util.List;
import java.util.Map;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;

import eu.k5.cell.activiti.InstancesWithVariablesExecution;
import eu.k5.cell.activiti.builder.QueryParameter;

class ListInstancesDelegate extends ParameterMappingDelegate {

	public ListInstancesDelegate(String sql, ManagementService service, QueryParameter[] parameters) {
		super(sql, service, parameters);
	}

	@Override
	public List<ExecutionEntity> invoke(Object... arguments) {
		Map<String, Object> parameters = getParameters(arguments);
		return getService().executeCustomSql(new InstancesWithVariablesExecution(parameters));
	}
}
