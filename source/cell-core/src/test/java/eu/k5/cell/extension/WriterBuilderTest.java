package eu.k5.cell.extension;

import java.lang.reflect.Type;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class WriterBuilderTest {
	WriterBuilder builder;
	Binder binder;

	@Before
	public void init() {
		binder = Mockito.mock(Binder.class);

		builder = new WriterBuilder();
		builder.withBinder(binder);
		builder.withValidator(new Validator());
	}

	public static class Start {
		private String x;

		public Start(String x) {
			this.x = x;
		}

		public void setX(String x) {
			this.x = x;
		}

		public String getX() {
			return x;
		}
	}

	@Test
	public void test() {
		addBinding("x", String.class);
		builder.useAll(true).forEntity(Start.class).init();

		Map<String, Object> variables = builder.build().write(new Start("abc"));
		Assert.assertEquals("abc", variables.get("x"));
	}

	private void addBinding(String name, Type type) {
		Mockito.when(binder.getBinding(name)).thenReturn(new Binding(false, false, name, type));
	}

}
