package eu.k5.cell.model.delegate;

import java.util.List;

import eu.k5.cell.annotation.AccessObject;
import eu.k5.cell.annotation.Aspect;
import eu.k5.cell.annotation.Bind;
import eu.k5.cell.annotation.Param;
import eu.k5.cell.annotation.Query;
import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Task;
import eu.k5.cell.annotation.workflow.Workflow;

@Workflow(processKey = "delegateProcess",binder = DelegateWorkflow.Binder.class,activity = DelegateWorkflow.Activity.class,delegates = { @AccessObject(DelegateWorkflow.Delegate.class) })
public interface DelegateWorkflow {

	public interface Binder {

		@Bind(businessKey = true)
		String getKey();

		@Bind
		String getProperty();

		String getProperty2();

		String getAssign();
	}

	public interface Activity {

		@Start
		DelegateStart start();

		@Task(definition = "DelegateTask")
		DelegateTask delegate();

		@Aspect
		DelegateAspect aspect();
	}

	public interface Delegate {

		void start(DelegateStart start);

		@Query
		List<DelegateTask> listAll();

		@Query("assignee=:assign")
		List<DelegateTask> byAssignee(@Param("assign") String assignee);

		@Query("var.property=:prop")
		List<DelegateTask> byProperty(@Param("prop") String prop);

		@Query("var.property=var.property2")
		List<DelegateTask> propertiesEqual();

		@Query("var.property=:prop")
		List<DelegateAspect> aspectByProperty(@Param("prop") String prop);

	}
}
