package eu.k5.cell.wao;

import eu.k5.cell.WorkflowManager;

public class UnwrapDefinition implements DelegateDefinition {

	private final Class<?> type;

	public UnwrapDefinition(Class<?> type) {
		this.type = type;
	}

	@Override
	public Delegate bind(final WorkflowManager workflowManager) {
		return new Delegate() {

			@Override
			public Object invoke(Object... args) {
				return workflowManager.unwrap(type);
			}
		};
	}

}
