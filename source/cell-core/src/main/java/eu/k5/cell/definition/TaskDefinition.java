package eu.k5.cell.definition;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import eu.k5.cell.CellProcessState;
import eu.k5.cell.CellTask;
import eu.k5.cell.extension.Writer;
import eu.k5.cell.shared.CellTaskReference;

public class TaskDefinition {
	private final String taskDefinitionKey;

	private final PropertyReader referenceReader;

	private final TaskReader reader;
	private final Writer writer;
	private final Class<?> type;

	private final WorkflowDefinition worfklow;

	public TaskDefinition(WorkflowDefinition worfklow, Class<?> type, String taskDefinitionKey, Writer writer,
			PropertyReader referenceReader, TaskReader reader) {
		this.worfklow = worfklow;

		this.type = type;
		this.taskDefinitionKey = taskDefinitionKey;
		this.referenceReader = referenceReader;
		this.writer = writer;
		this.reader = reader;
	}

	public WorkflowDefinition getWorfklow() {
		return worfklow;
	}

	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}

	public TaskReader getReader() {
		return reader;
	}

	public Writer getWriter() {
		return writer;
	}

	public Object read(CellTask task, Map<String, Object> variables) {
		return reader.read(task, variables);
	}

	public Object read(CellTask task) {
		return read(task, task.getProcessVariables());
	}

	public CellTaskReference getTaskReference(Object object) {
		return (CellTaskReference) referenceReader.read(object);
	}

	public List<Object> read(CellProcessState processState) {

		List<Object> tasks = new ArrayList<>();
		for (CellTask task : processState.getTasks()) {
			if (task.getTaskDefinitionKey().equals(taskDefinitionKey)) {
				tasks.add(read(task, processState.getVariables()));
			}
		}
		return tasks;

	}

	public boolean handles(Object task) {
		if (!referenceReader.valid(task)) {
			return false;
		}
		CellTaskReference reference = getTaskReference(task);
		if (reference == null) {
			return false;
		}
		return reference.getTaskDefinition().equals(taskDefinitionKey);
	}

	public Map<String, Object> write(Object task) {
		return writer.write(task);
	}

	public Class<?> getType() {
		return type;
	}

	public boolean matches(eu.k5.cell.annotation.workflow.Task task) {
		if (task.definition().equals(eu.k5.cell.annotation.workflow.Task.DEFAULT)) {
			return true;
		}
		return task.definition().equals(taskDefinitionKey);
	}

}
