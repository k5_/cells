package eu.k5.cell.parser;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Instantiator<T> {
	private static final Object[] zeroParam = new Object[0];
	private final Class<T> type;

	private final Constructor<T> constructor;
	private final int[] constructorIndex;

	private final FieldAssigner[] fields;

	private final PropertyAssigner[] properties;

	public Instantiator(Class<T> type, Constructor<T> constructor, int[] constructorIndex, FieldAssigner[] fields,
			PropertyAssigner[] properties) {
		this.type = type;
		this.constructor = constructor;
		this.constructorIndex = constructorIndex;
		this.fields = fields;
		this.properties = properties;
	}

	public int[] getConstructorIndex() {
		return constructorIndex;
	}

	public PropertyAssigner[] getProperties() {
		return properties;
	}

	public T instantiate(Object[] board) {

		try {
			T instance = newInstance(board);

			for (FieldAssigner assigner : fields) {
				assigner.getField().set(instance, board[assigner.getIndex()]);
			}

			for (PropertyAssigner assigner : properties) {
				assigner.getMethod().invoke(instance, board[assigner.getIndex()]);
			}

			return instance;
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}

	}

	public Class<T> getType() {
		return type;
	}

	private T newInstance(Object[] board)
			throws InstantiationException, IllegalAccessException, InvocationTargetException {
		if (constructorIndex.length == 0) {
			return type.cast(constructor.newInstance(zeroParam));
		}
		Object[] args = new Object[constructorIndex.length];
		for (int index = 0; index < constructorIndex.length; index++) {
			args[index] = board[constructorIndex[index]];
		}
		return type.cast(constructor.newInstance(args));
	}

	public static class FieldAssigner {
		private final int index;
		private final Field field;

		public FieldAssigner(int index, Field field) {
			this.index = index;
			this.field = field;
		}

		public int getIndex() {
			return index;
		}

		public Field getField() {
			return field;
		}

	}

	public static class PropertyAssigner {
		private final int index;
		private final Method method;

		public PropertyAssigner(int index, Method method) {
			super();
			this.index = index;
			this.method = method;
		}

		public int getIndex() {
			return index;
		}

		public Method getMethod() {
			return method;
		}
	}

}
