package eu.k5.cell.activiti;

public class ParameterLiteralBound extends ParameterBound {
	private final Object value;

	public ParameterLiteralBound(TypeHint type, Object value) {
		super(type);
		this.value = value;
	}

	public Object getValue() {
		return value;
	}

}
