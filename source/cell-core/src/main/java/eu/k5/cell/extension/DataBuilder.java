package eu.k5.cell.extension;

import eu.k5.cell.definition.DataDefinition;

public class DataBuilder extends Builder<DataDefinition, DataBuilder> {

	private final ReaderBuilder reader = new ReaderBuilder();

	public DataBuilder() {
		super(DataDefinition.class);
		setBuilder(this);
	}

	@Override
	protected void doInit() {
		reader.withBinder(getBinder()).withValidator(getValidator()).forEntity(getType()).useAll(true).init();

		plusViolations(reader.getViolations());
	}

	@Override
	protected DataDefinition doBuild() {
		return new DataDefinition(getType(), reader.build());
	}

}
