package eu.k5.cell.activiti.delegate;

import org.activiti.engine.ManagementService;

import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.ast.Query;
import eu.k5.cell.activiti.builder.QueryParameter;

final class FindInstanceDefinition extends InstanceQueryDefinition<FindInstanceDelegate> {
	FindInstanceDefinition(Query query, QueryParameter[] parameters, Context... context) {
		super(query, parameters, context);
	}

	@Override
	public FindInstanceDelegate bind(ManagementService service) {
		return DelegateFactory.findInstanceDelegate(service, render(service), getParameters());
	}

}