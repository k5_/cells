package eu.k5.cell.activiti.filter;

import org.activiti.engine.runtime.ProcessInstanceQuery;
import org.activiti.engine.task.TaskQuery;

import eu.k5.cell.activiti.ProcessState;

public class NullFilter implements TaskFilter, AspectFilter, ProcessFilter {

	@Override
	public TaskQuery filter(TaskQuery query) {
		return query;
	}

	@Override
	public AspectQuery filter(AspectQuery query) {
		return query;
	}

	@Override
	public ProcessInstanceQuery filter(ProcessInstanceQuery query) {
		return query;
	}

	@Override
	public boolean accept(ProcessState state) {
		return true;
	}

}
