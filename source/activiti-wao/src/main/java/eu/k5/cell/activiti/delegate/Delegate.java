package eu.k5.cell.activiti.delegate;

public interface Delegate {

	Object invoke(Object... args);

}