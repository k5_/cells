package eu.k5.cell.extension;

import java.util.ArrayList;
import java.util.List;

import eu.k5.cell.definition.DataDefinition;
import eu.k5.cell.definition.NestedDataDefinition;
import eu.k5.cell.extension.ReaderBuilder.PropertyBound;

public class NestedDataBuilder {
	private final List<DataDefinition> datas;
	private final List<NestedDataDefinition> nestedDatas = new ArrayList<>();
	private final Validator validator;
	private final Class<?> root;

	public NestedDataBuilder(Class<?> root, Validator validator, List<DataDefinition> datas) {
		this.root = root;
		this.validator = validator;
		this.datas = datas;
	}

	void init(List<PropertyBound> bounds) {
		for (PropertyBound dataBound : bounds) {
			useData(dataBound);
		}

	}

	private DataDefinition getDatas(Class<?> type) {
		for (DataDefinition definiton : datas) {
			if (definiton != null && definiton.getType().equals(type)) {
				return definiton;
			}
		}

		return null;
	}

	private void useData(PropertyBound bound) {
		Class<?> type = bound.getType();

		DataDefinition definition = getDatas(type);

		if (definition == null) {
			validator.add(root, "Missing data type" + type);
		} else {
			bound.setIgnored(true);
		}

		nestedDatas.add(new NestedDataDefinition(bound.getAssigner(), definition));
	}

	public List<NestedDataDefinition> getNestedDatas() {
		return nestedDatas;
	}
}
