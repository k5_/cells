package eu.k5.cell.activiti.delegate;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import eu.k5.cell.activiti.AqlMapper;
import eu.k5.cell.activiti.builder.QueryParameter;

public class ListTasksDelegateTest extends AbstractDelegateTest {

	@Test
	public void invoke_noParameters() {
		new ListTasksDelegate("sqlQuery", getService(), new QueryParameter[0]).invoke();
		Map<String, Object> actualParameters = captureParameters();
		Assert.assertEquals("sqlQuery", actualParameters.get(AqlMapper.SQL));
	}

	@Test
	public void invoke_parameter() {
		new ListTasksDelegate("sqlQuery", getService(), parameter("test", String.class)).invoke("value");
		Map<String, Object> actualParameters = captureParameters();
		Assert.assertEquals("sqlQuery", actualParameters.get(AqlMapper.SQL));
		Assert.assertEquals("value", actualParameters.get("test"));
	}

}
