package eu.k5.cell.model.delegate;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.In;
import eu.k5.cell.shared.CellTaskReference;

public class DelegateTask {
	@CellReference
	private CellTaskReference reference;

	@In
	private String property;

	@In
	private String key;

	public String getProperty() {
		return property;
	}

	public String getKey() {
		return key;
	}

}
