package eu.k5.cell.activiti;

import java.util.List;

import org.activiti.engine.task.Task;
import org.apache.ibatis.exceptions.TooManyResultsException;
import org.junit.Assert;
import org.junit.Test;

import eu.k5.cell.activiti.annotation.Param;
import eu.k5.cell.activiti.annotation.Query;

public class Wao_getTask_Test extends AbstractExample {
	public interface Variables {
		@VariableHint
		String stringVar();

		@VariableHint
		String stringVar2();

		@VariableHint
		Long longVar();

	}

	public interface Wao extends Variables {

		@Query("taskDefinitionKey = :key")
		Task getByDefinition(@Param("key") String key);

		@Query("taskDefinitionKey = :keyUnion union taskDefinitionKey = :key2")
		List<Task> getByDefinitionWithUnion(@Param("keyUnion") String key, @Param("key2") String key2);
	}

	@Test
	public void getByDefinition_found() {
		start(1);
		Wao wao = getWao(Wao.class);

		Assert.assertNotNull(wao.getByDefinition("Task1"));
	}

	@Test
	public void getByDefinitionUnion_found() {
		start(1);
		start(2);
		Wao wao = getWao(Wao.class);

		List<Task> union = wao.getByDefinitionWithUnion("Task1", "Task2");
		Assert.assertEquals(2, union.size());
	}

	@Test
	public void getByDefinition_notFound() {
		start(1);
		Wao wao = getWao(Wao.class);
		Task byDefinition = wao.getByDefinition("Task2");
		Assert.assertNull(byDefinition);
	}

	@Test(expected = TooManyResultsException.class)
	public void getByDefinition_multipleResults() {
		start(1);
		start(1);
		getWao(Wao.class).getByDefinition("Task1");
	}
}
