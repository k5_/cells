package eu.k5.cell.activiti.delegate;

import java.util.List;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;

import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.ast.Query;
import eu.k5.cell.activiti.builder.QueryParameter;

public final class ListInstancesDefinition extends InstanceQueryDefinition<ListInstancesDelegate> {
	ListInstancesDefinition(Query query, QueryParameter[] parameters, Context... context) {
		super(query, parameters, context);
	}

	@Override
	public ListInstancesDelegate bind(ManagementService service) {
		return DelegateFactory.listInstancesDelegate(service, render(service), getParameters());
	}

	public List<ExecutionEntity> invoke(ManagementService service, Object... parameters) {
		return bind(service).invoke(parameters);
	}
}