package eu.k5.cell;

public class InvalidReferenceException extends CellException {
	private static final long serialVersionUID = 1L;

	public InvalidReferenceException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidReferenceException(String message) {
		super(message);
	}

}
