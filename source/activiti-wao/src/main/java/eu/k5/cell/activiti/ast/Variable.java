package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

class Variable extends Value {

	private final String variable;

	public Variable(String variable) {
		this.variable = variable;

	}

	public String getVariable() {
		return variable;
	}

	@Override
	public String toString() {
		return "VariableCondition [variable=" + variable + "]";
	}

	@Override
	public void accept(ValueVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public TypeHint getHint(Context context) {
		return context.getVariable(variable).getHint();
	}

}
