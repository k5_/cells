package eu.k5.cell.parser;

public class InputConstructor extends Input {

	private final SourceReference reference;

	public InputConstructor(Class<?> type, SourceReference reference) {
		super(type);
		this.reference = reference;
	}

	@Override
	public SourceReference getSource() {
		return reference;
	}
}
