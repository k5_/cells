package eu.k5.cell.activiti.delegate;

import java.util.Map;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;

import eu.k5.cell.activiti.InstanceWithVariablesExecution;
import eu.k5.cell.activiti.builder.QueryParameter;

class FindInstanceDelegate extends ParameterMappingDelegate {

	public FindInstanceDelegate(String sql, ManagementService service, QueryParameter[] parameters) {
		super(sql, service, parameters);
	}

	@Override
	public ExecutionEntity invoke(Object... arguments) {
		Map<String, Object> parameters = getParameters(arguments);
		return getService().executeCustomSql(new InstanceWithVariablesExecution(parameters));
	}

}
