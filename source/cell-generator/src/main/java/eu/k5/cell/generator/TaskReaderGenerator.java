package eu.k5.cell.generator;

import org.jboss.jdeparser.JBlock;
import org.jboss.jdeparser.JExprs;
import org.jboss.jdeparser.JParamDeclaration;
import org.jboss.jdeparser.JTypes;
import org.jboss.jdeparser.JVarDeclaration;

import eu.k5.cell.shared.CellTaskReference;

public class TaskReaderGenerator {

	private TaskReader reader;

	public void test() {

	}

	public void generate() {

	}

	public JVarDeclaration genTaskReference(JBlock block, JParamDeclaration task) {
		return block.var(0, CellTaskReference.class, "reference",
				JTypes.$t(CellTaskReference.class)._new().arg(JExprs.$v(task).call("getProcessInstanceId"))
						.arg(JExprs.$v(task).call("getId")).arg(JExprs.$v(task).call("getTaskDefinitionKey"))
						.arg(JExprs.$v(task).call("getToken")));

		// .arg(JExprs.$v(task).call("getTaskLocalVariables").call("get")
		// .arg(JTypes.$t(TaskTokenCreator.class).field("TOKEN")).cast(String.class)));

	}

}
