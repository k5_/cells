package eu.k5.cell.activiti.delegate;

import java.lang.reflect.Type;
import java.util.Map;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.cmd.CustomSqlExecution;
import org.junit.Before;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import eu.k5.cell.activiti.AqlMapper;
import eu.k5.cell.activiti.TypeHint;
import eu.k5.cell.activiti.builder.QueryParameter;

public class AbstractDelegateTest {

	@Mock
	private ManagementService service;
	@Mock
	private AqlMapper mapper;
	@Captor
	private ArgumentCaptor<Map<String, Object>> parameters;

	@Captor
	private ArgumentCaptor<CustomSqlExecution<AqlMapper, ?>> execution;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

	}

	public ManagementService getService() {
		return service;
	}

	protected Map<String, Object> captureParameters() {
		Mockito.verify(service).executeCustomSql(execution.capture());
		execution.getValue().execute(mapper);
		Mockito.verify(mapper).selectTasksWithVariables(parameters.capture());
		return parameters.getValue();
	}

	protected QueryParameter[] parameter(Object... params) {
		QueryParameter[] parameters = new QueryParameter[params.length / 2];
		for (int index = 0; index < params.length; index += 2) {
			String name = params[index].toString();
			Type type = (Type) params[index + 1];
			TypeHint hint = TypeHint.forType(type);
			parameters[index / 2] = new QueryParameter(name, hint, index / 2);
		}
		return parameters;
	}
}
