package eu.k5.cell.model.composite;

import java.util.Collections;
import java.util.List;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import eu.k5.cell.WorkflowManager;
import eu.k5.cell.activiti.TestEngine;
import eu.k5.cell.annotation.CellContext;
import eu.k5.cell.extension.CellExtension;
import eu.k5.cell.model.TestProducer;
import eu.k5.cell.model.composite.CompositeWorkflow1.CompositeTask1Task;

@RunWith(CdiRunner.class)
@AdditionalClasses({ TestProducer.class, CellExtension.class, CompositeWorkflows.class, CompositeWorkflow1.class,
		CompositeWorkflow2.class })
public class CompositeTest {
	private static TestEngine refEngine = new TestEngine("composite", "composite1", "composite2");

	@CellContext(CompositeWorkflows.class)
	@Inject
	WorkflowManager cell;

	@Produces
	TestEngine engine = refEngine;
	CompositeWorkflows wao;

	@Before
	public void init() {
		wao = cell.wao(CompositeWorkflows.class);

		engine.reset();
	}

	@AfterClass
	public static void destoy() {
		refEngine.destroy();
	}

	@Test
	public void start_retrieveAll() {

		cell.start(new Workflow1Start("key", "prop1"));
		cell.start(new Workflow2Start("key", "prop2"));

		List<CompositeTask> list = cell.list(CompositeTask.class, "true");
		Collections.sort(list);

		Assert.assertEquals(2, list.size());

		Assert.assertEquals("prop1", list.get(0).getProperty());
		Assert.assertEquals("prop2", list.get(1).getProperty());
	}

	@Test
	public void start_retrieveFromOnProcess() {

		cell.start(new Workflow1Start("key", "prop1"));
		cell.start(new Workflow2Start("key", "prop2"));

		List<CompositeTask1Task> list = cell.list(CompositeWorkflow1.CompositeTask1Task.class, "true");

		Assert.assertEquals(1, list.size());

		Assert.assertEquals("prop1", list.get(0).getProperty());
	}

	@Test
	public void start_retrieveByProperty() {

		cell.start(new Workflow1Start("key", "prop1"));
		cell.start(new Workflow2Start("key", "prop2"));

		List<CompositeTask> list = cell.list(CompositeTask.class, "var.property = :1", "prop1");
		Assert.assertEquals(1, list.size());

		Assert.assertEquals("prop1", list.get(0).getProperty());
	}

	@Test
	public void start_retrieveWithWao() {
		cell.start(new Workflow1Start("key", "prop1"));
		cell.start(new Workflow2Start("key", "prop2"));
		Assert.assertEquals(2, wao.listTasks().size());
	}

	@Test
	public void start_retrieveWithQueryWao() {
		cell.start(new Workflow1Start("key", "prop1"));
		cell.start(new Workflow2Start("key", "prop2"));
		Assert.assertEquals(1, wao.listTask("prop1").size());
	}
}
