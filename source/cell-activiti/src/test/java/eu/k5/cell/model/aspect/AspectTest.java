package eu.k5.cell.model.aspect;

import java.util.List;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.activiti.engine.runtime.ProcessInstance;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.common.collect.Sets;

import eu.k5.cell.activiti.CellManager;
import eu.k5.cell.activiti.TestEngine;
import eu.k5.cell.activiti.filter.BusinessKeyFilter;
import eu.k5.cell.annotation.CellContext;
import eu.k5.cell.extension.CellExtension;
import eu.k5.cell.model.TestProducer;

@RunWith(CdiRunner.class)
@AdditionalClasses({ TestProducer.class, CellExtension.class, AspectWorkflow.class })
public class AspectTest {

	private static TestEngine refEngine = new TestEngine("aspect", "aspect");

	@CellContext(AspectWorkflow.class)
	@Inject
	CellManager cell;

	@Produces
	TestEngine engine = refEngine;

	@AfterClass
	public static void destoy() {
		refEngine.destroy();
	}

	@Test
	public void start() {
		AspectStart start = new AspectStart("key", "property");
		cell.start(start);
		Assert.assertEquals(Sets.newHashSet("key"), cell.listBusinessKeys(false));

		AspectAspect aspect = cell.findAspect(AspectAspect.class, new BusinessKeyFilter("key"));

		Assert.assertNotNull(aspect);
		Assert.assertNotNull(aspect.getTask());
		Assert.assertNotNull(aspect.getData());
		Assert.assertFalse(aspect.getIsEnded());
		Assert.assertEquals("aspect", aspect.getDerivedTest());
		Assert.assertEquals("aspect", aspect.getDerivedDirectTest());
		Assert.assertNotNull(aspect.getInstanceId());
		Assert.assertEquals(aspect.getInstance(), aspect.getInstanceId());

		List<AspectAspect> aspects = cell.listAspect(AspectAspect.class);
		Assert.assertEquals(1, aspects.size());

		cell.complete(aspect.getTask());
		AspectAspect aspectReload = cell.findAspect(AspectAspect.class, new BusinessKeyFilter("key"));
		Assert.assertNotNull(aspectReload);
		Assert.assertNull(aspectReload.getTask());

		ProcessInstance instance = engine.find("key");
		Assert.assertNotNull(instance);
		Assert.assertEquals("property", instance.getProcessVariables().get("property"));

	}
}
