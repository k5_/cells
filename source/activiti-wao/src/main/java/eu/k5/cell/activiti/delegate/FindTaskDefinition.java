package eu.k5.cell.activiti.delegate;

import org.activiti.engine.ManagementService;

import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.ast.Query;
import eu.k5.cell.activiti.builder.QueryParameter;

public final class FindTaskDefinition extends TaskQueryDefinition<FindTaskDelegate> {
	FindTaskDefinition(Query query, QueryParameter[] parameters, Context... context) {
		super(query, parameters, context);
	}

	@Override
	public FindTaskDelegate bind(ManagementService service) {
		return DelegateFactory.findTaskDelegate(service, render(service), getParameters());
	}
}