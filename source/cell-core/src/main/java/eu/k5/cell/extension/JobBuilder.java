package eu.k5.cell.extension;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import eu.k5.cell.annotation.In;
import eu.k5.cell.annotation.Out;
import eu.k5.cell.annotation.states.JobState;
import eu.k5.cell.annotation.workflow.Data;
import eu.k5.cell.definition.DataDefinition;
import eu.k5.cell.definition.JobDefinition;
import eu.k5.cell.definition.JobStateReader;
import eu.k5.cell.extension.ReaderBuilder.PropertyBound;

public class JobBuilder extends Builder<JobDefinition, JobBuilder> {
	private final ReaderBuilder reader = new ReaderBuilder();
	private final WriterBuilder writer = new WriterBuilder();
	private final List<StateDefinition<JobStateReader>> jobStates = new ArrayList<>();

	private NestedDataBuilder nestedData;
	private Class<?> servantType;
	private String definition;
	private Method method;

	public JobBuilder() {
		super(JobDefinition.class);
		setBuilder(this);
	}

	@Override
	protected void doInit() {
		reader.forEntity(getType()).withBinder(getBinder()).withValidator(getValidator()).load();
		initJobStates();
		for (PropertyBound bound : reader.selectReaders(Out.class)) {
			if (!bound.isAnnotationPresent(In.class)) {
				bound.setIgnored(true);
			}
		}

		nestedData.init(reader.selectReaders(Data.class));

		reader.init();
		writer.forEntity(getType()).withBinder(getBinder()).withValidator(getValidator()).init();

		plusViolations(reader.getViolations());
		plusViolations(writer.getViolations());
	}

	private void initJobStates() {
		StateBuilder stateBuilder = new StateBuilder(getValidator(), getType());
		for (PropertyBound bound : reader.selectReaders(JobState.class)) {
			JobStateReader state = stateBuilder.readJobStates(bound.getAnnotation(JobState.class), bound.getName(),
					bound.getType());
			if (state != null) {
				jobStates.add(new StateDefinition<JobStateReader>(bound.getAssigner(), state));
				bound.setIgnored(true);
			}
		}
	}

	public JobBuilder withServantType(Class<?> type) {
		servantType = type;
		return this;
	}

	public JobBuilder withDefinition(String definition) {

		this.definition = definition;
		return this;
	}

	public JobBuilder withDatas(List<DataDefinition> datas) {
		nestedData = new NestedDataBuilder(getType(), getValidator(), datas);
		return this;
	}

	public JobBuilder withServantMethod(Method method) {
		this.method = method;
		return this;
	}

	@Override
	protected JobDefinition doBuild() {
		return new JobDefinition(definition, servantType, method, reader.build(), writer.build(), jobStates,
				nestedData.getNestedDatas());
	}
}
