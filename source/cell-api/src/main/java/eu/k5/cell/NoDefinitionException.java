package eu.k5.cell;

public class NoDefinitionException extends CellException {
	private static final long serialVersionUID = 1606194037751653816L;

	public NoDefinitionException(String message) {
		super(message);
	}

}
