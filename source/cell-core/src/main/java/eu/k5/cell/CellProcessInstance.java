package eu.k5.cell;

import java.util.Map;

public interface CellProcessInstance {

	Map<String, Object> getProcessVariables();

}
