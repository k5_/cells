package eu.k5.cell.model.composite;

import java.util.List;

import eu.k5.cell.annotation.AccessObject;
import eu.k5.cell.annotation.Param;
import eu.k5.cell.annotation.Query;
import eu.k5.cell.annotation.workflow.Task;
import eu.k5.cell.annotation.workflow.Workflows;

@Workflows(workflow = { CompositeWorkflow1.class, CompositeWorkflow2.class },delegates = { @AccessObject(CompositeWorkflows.class) })
public interface CompositeWorkflows {

	interface Binder {
		String getKey();

		String getProperty();
	}

	interface Activity {

		@Task(definition = "TestTask")
		CompositeTask task();
	}

	@Query("true")
	List<CompositeTask> listTasks();

	@Query("var.property = :prop")
	List<CompositeTask> listTask(@Param("prop") String prop);
}
