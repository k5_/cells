package eu.k5.cell.activiti.ast;

interface QueryVisitor {

	void visit(AndExpression andExpression);

	void visit(OrExpression orExpression);

	void visit(Query query);

	void visit(UnaryExpression unary);

	void visit(Condition condition);

	void visit(BooleanExpression booleanExpression);

	void visit(Limit limit);

	void visit(Section section);

}
