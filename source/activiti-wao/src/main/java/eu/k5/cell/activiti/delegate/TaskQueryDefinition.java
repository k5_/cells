package eu.k5.cell.activiti.delegate;

import org.activiti.engine.ManagementService;

import eu.k5.cell.activiti.ast.BaseQueryRenderer;
import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.ast.Query;
import eu.k5.cell.activiti.ast.TaskQueryRenderer;
import eu.k5.cell.activiti.builder.QueryDefinition;
import eu.k5.cell.activiti.builder.QueryParameter;

public abstract class TaskQueryDefinition<D extends Delegate> extends QueryDefinition<D> {

	public TaskQueryDefinition(Query query, QueryParameter[] parameters, Context... context) {
		super(query, parameters, context);
	}

	@Override
	protected BaseQueryRenderer queryRenderer(ManagementService service) {
		return new TaskQueryRenderer(getDatabaseType(service), getTaskTable(service), getVariableTable(service),
				getIdentityTable(service), getProcessDefinitionTable(service));
	}

	public static ListTaskDefinition list(Query query, QueryParameter[] parameters, Context... context) {
		return new ListTaskDefinition(query, parameters, context);
	}

	public static FindTaskDefinition find(Query query, QueryParameter[] parameters, Context... context) {
		return new FindTaskDefinition(query, parameters, context);
	}

}
