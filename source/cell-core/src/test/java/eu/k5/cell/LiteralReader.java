package eu.k5.cell;

import eu.k5.cell.definition.PropertyReader;
import eu.k5.cell.extension.Binding;

public class LiteralReader implements PropertyReader {
	private final Object literal;

	public LiteralReader(Object literal) {
		this.literal = literal;
	}

	@Override
	public Object read(Object object) {
		return literal;
	}

	@Override
	public boolean valid(Object object) {
		return true;
	}

	@Override
	public Binding getBinding() {
		return null;
	}

}
