package eu.k5.cell.definition;

import java.util.Map;

import eu.k5.cell.CellProcessInstance;
import eu.k5.cell.extension.Reader;

public class DataDefinition {
	private final Class<?> type;
	private final Reader reader;

	public DataDefinition(Class<?> type, Reader reader) {
		this.type = type;
		this.reader = reader;
	}

	public Object read(Map<String, Object> variables) {
		return reader.read(variables);
	}

	public Object read(CellProcessInstance instance) {
		return read(instance.getProcessVariables());
	}

	public Class<?> getType() {
		return type;
	}
}
