package eu.k5.cell.extension;

import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;

import eu.k5.cell.annotation.Bind;
import eu.k5.cell.annotation.states.JobState;
import eu.k5.cell.annotation.states.ProcessState;
import eu.k5.cell.annotation.states.TaskState;
import eu.k5.cell.definition.JobStateReader;
import eu.k5.cell.definition.ProcessStateReader;
import eu.k5.cell.definition.TaskStateReader;

public class BinderBuilder {

	private Validator validator;
	private Class<?> binder;

	private final List<Bound> bounds = new ArrayList<>();

	private final List<ProcessStateReader> processStates = new ArrayList<>();
	private final List<JobStateReader> jobStates = new ArrayList<>();
	private final List<TaskStateReader> taskStates = new ArrayList<>();

	public BinderBuilder addClass(Class<?> binder) {
		this.binder = binder;
		return this;
	}

	static class Bound {
		private final String name;
		private final Type type;
		private final Bind bind;
		private final Method method;
		private boolean ignored;

		public Bound(String name, Type type, Bind bind, Method method) {
			this.name = name;
			this.type = type;
			this.bind = bind;

			this.method = method;
		}

		public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
			return method.isAnnotationPresent(annotationClass);
		}

		public <A extends Annotation> A getAnnotation(Class<A> annotation) {
			return method.getAnnotation(annotation);
		}

		public String getName() {
			return name;
		}

		public Type getType() {
			return type;
		}

		public boolean isIgnored() {
			return ignored;
		}

		public void setIgnored(boolean ignored) {
			this.ignored = ignored;
		}

		public boolean isFinal() {
			if (bind == null) {
				return false;
			}
			return bind.isFinal() || bind.businessKey();
		}

		public boolean isBusinessKey() {
			if (bind == null) {
				return false;
			}
			return bind.businessKey();
		}

		public Binding getBinding() {
			return new Binding(isBusinessKey(), isFinal(), name, type);
		}
	}

	private <A extends Annotation> List<Bound> select(Class<A> annotation) {
		List<Bound> result = new ArrayList<>();
		for (Bound bound : bounds) {
			if (bound.isAnnotationPresent(annotation)) {
				result.add(bound);
			}
		}
		return result;
	}

	public BinderBuilder init() {
		initBinder(binder);
		initProcessStates();
		initJobStates();
		initTaskStates();
		return this;
	}

	private void initBinder(Class<?> binder) {
		PropertyDescriptor[] descriptors = PropertyUtils.getPropertyDescriptors(binder);

		for (PropertyDescriptor desc : descriptors) {
			Bind readBinding = desc.getReadMethod().getAnnotation(Bind.class);

			if (readBinding != null) {
				String name;
				if (!readBinding.name().equals(Bind.DEFAULT)) {
					name = readBinding.name();
				} else {
					name = desc.getName();
				}

				bounds.add(new Bound(name, desc.getPropertyType(), readBinding, desc.getReadMethod()));

				// bindings.put(name, new Binding(readBinding.businessKey(),
				// isFinal, name, desc.getPropertyType()));
			} else {
				bounds.add(new Bound(desc.getName(), desc.getPropertyType(), null, desc.getReadMethod()));
				// bindings.put(desc.getName(), new Binding(false, false,
				// desc.getName(), desc.getPropertyType()));
			}
		}
	}

	private void initProcessStates() {
		StateBuilder stateBuilder = new StateBuilder(validator, binder);
		for (Bound bound : select(ProcessState.class)) {
			ProcessStateReader processState = stateBuilder.readProcessStates(bound.getAnnotation(ProcessState.class),
					bound.getName(), bound.getType());
			if (processState != null) {
				processStates.add(processState);
				bound.setIgnored(true);
			}
		}
	}

	private void initJobStates() {
		StateBuilder stateBuilder = new StateBuilder(validator, binder);
		for (Bound bound : select(JobState.class)) {
			JobStateReader jobState = stateBuilder.readJobStates(bound.getAnnotation(JobState.class), bound.getName(),
					bound.getType());
			if (jobState != null) {
				jobStates.add(jobState);
				bound.setIgnored(true);
			}
		}
	}

	private void initTaskStates() {
		StateBuilder stateBuilder = new StateBuilder(validator, binder);
		for (Bound bound : select(TaskState.class)) {
			TaskStateReader taskState = stateBuilder.readTaskStates(bound.getAnnotation(TaskState.class),
					bound.getName(), bound.getType());
			if (taskState != null) {
				taskStates.add(taskState);
				bound.setIgnored(true);
			}
		}
	}

	public BinderBuilder withValidator(Validator validator) {
		this.validator = validator;
		return this;
	}

	public Binder build() {

		List<Binding> bindings = new ArrayList<>();
		for (Bound bound : bounds) {
			if (!bound.isIgnored()) {
				bindings.add(bound.getBinding());
			}
		}

		return new Binder(bindings, processStates, jobStates, taskStates);
	}
}
