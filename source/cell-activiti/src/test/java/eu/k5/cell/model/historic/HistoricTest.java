package eu.k5.cell.model.historic;

import java.util.List;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.common.collect.Sets;

import eu.k5.cell.activiti.CellManager;
import eu.k5.cell.activiti.TestEngine;
import eu.k5.cell.activiti.filter.BusinessKeyFilter;
import eu.k5.cell.activiti.filter.IncludeHistoric;
import eu.k5.cell.annotation.CellContext;
import eu.k5.cell.extension.CellExtension;
import eu.k5.cell.model.TestProducer;

@RunWith(CdiRunner.class)
@AdditionalClasses({ TestProducer.class, CellExtension.class, HistoricWorkflow.class })
public class HistoricTest {

	private static TestEngine refEngine = new TestEngine("historic", "historic");

	@CellContext(HistoricWorkflow.class)
	@Inject
	CellManager cell;

	@Produces
	TestEngine engine = refEngine;

	@AfterClass
	public static void destoy() {
		refEngine.destroy();
	}

	@Test
	public void start() {

		cell.start(new HistoricStart("key", "property"));
		cell.start(new HistoricStart("key2", "property2"));

		Assert.assertEquals(Sets.newHashSet("key", "key2"), cell.listBusinessKeys(false));
		{
			HistoricAspect aspect = cell.findAspect(HistoricAspect.class, new BusinessKeyFilter("key"));
			Assert.assertNotNull(aspect);

			cell.complete(aspect.getTask());
			Assert.assertNull(cell.findAspect(HistoricAspect.class, new BusinessKeyFilter("key")));
		}
		{
			HistoricAspect aspect = cell.findAspect(HistoricAspect.class, new BusinessKeyFilter("key"),
					new IncludeHistoric());
			Assert.assertNotNull(aspect);
		}
		List<HistoricAspect> aspects = cell.listAspect(HistoricAspect.class, new IncludeHistoric());
		Assert.assertEquals(2, aspects.size());

		Assert.assertEquals(Sets.newHashSet("key2"), cell.listBusinessKeys(false));
		Assert.assertEquals(Sets.newHashSet("key", "key2"), cell.listBusinessKeys(true));

	}
}
