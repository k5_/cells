package eu.k5.cell.wao;

import java.lang.reflect.Method;
import java.util.Map;

public class Wao {
	private final Class<?> type;
	private final Map<Method, Delegate> delegates;

	public Wao(Class<?> type, Map<Method, Delegate> delegates) {

		this.type = type;
		this.delegates = delegates;
	}

	public Class<?> getType() {
		return type;
	}

	public Delegate getDelegate(Method method) {
		return delegates.get(method);
	}
}
