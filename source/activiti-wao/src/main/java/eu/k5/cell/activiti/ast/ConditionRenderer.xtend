package eu.k5.cell.activiti.ast

import eu.k5.cell.activiti.TypeHint
import eu.k5.cell.activiti.ParameterNameBound
import eu.k5.cell.activiti.ParameterLiteralBound
import eu.k5.cell.activiti.ParameterBound

class ConditionRenderer {
	val BaseQueryRenderer query;
	val Context context;

	new(BaseQueryRenderer query, Context context) {
		this.query = query;
		this.context = context;
	}

	def dispatch void render(Operator op, Value left, Value right) {
		addOperatorPrefix(op)
		addLeft(left, right)
		addOperatorInfix(op)
		addRight(left, right)
		addOperatorSuffix(op)
	}

	def dispatch void render(Operator op, Value left, Property right) {
		render(op, right, left)
	}

	def dispatch void addLeft(Value left, Value right) {
		throw new IllegalArgumentException(
			"missing dispatch for addLeft:" + left.class.name + " Right: " + right.class.name)
	}

	def dispatch void addRight(Value left, Value right) {
		throw new IllegalArgumentException(
			"missing dispatch for addLeft:" + left.class.name + " Right: " + right.class.name)
	}

	def dispatch void addLeft(NamedParameter left, Value context) {
		addNamedParameter(left)
	}

	def dispatch void addRight(Value context, NamedParameter right) {
		addNamedParameter(right)
	}

	def dispatch void addLeft(Literal<?> left, Value context) {
		addLiteral(left)
	}

	def dispatch void addRight(Value context, Literal<?> right) {
		addLiteral(right)
	}

	def dispatch void addLeft(Variable left, Value context) {
		val table = query.addVariable(false)
		renderer().where.append(table).append(".NAME_ = '").append(left.getVariable()).append("' AND ");
		renderer().where.append(table).append(left.getHint(this.context).column);
	}

	def dispatch void addRight(Value context, Variable right) {
		val table = query.addVariable(false)
		renderer().where.append(table).append(right.getHint(this.context).column).append(" AND ");
		renderer().where.append(table).append(".NAME_ = '").append(right.getVariable()).append("' ");
	}

	def dispatch void addLeft(Property left, Value context) {
		addProperty(left)
	}

	def dispatch void addRight(Value context, BasicProperty right) {
		addProperty(right)
	}

	def void addNamedParameter(NamedParameter namedParameter) {
		addParameterBound(this.context.getParameterBound(namedParameter.name));
	}

	def dispatch addParameterBound(ParameterNameBound bound) {
		renderer().where.append("#{").append(bound.getName()).append("}");
	}

	def dispatch addParameterBound(ParameterLiteralBound bound) {
		switch (bound.type) {
			case STRING: {
				addLiteral(new StringLiteral(bound.value.toString))
			}
			default: {
				throw new IllegalArgumentException("Unable to map non string parameter")
			}
		}
	}

	def void addOperatorInfix(Operator op) {
		renderer().where.append(op.token);
		if (Operator.IN.equals(op)) {
			renderer().where.append('(')
		}
	}

	def void addOperatorPrefix(Operator op) {
	}

	def void addOperatorSuffix(Operator op) {
		if (Operator.IN.equals(op)) {
			renderer().where.append(')')
		}
	}

	def dispatch void addLiteral(StringLiteral literal) {
		renderer().where.append('\'').append(literal.value).append('\'')
	}

	def dispatch void addLiteral(LongLiteral literal) {
		renderer().where.append(literal.value);
	}

	def dispatch void addProperty(CandidateProperty property) {
		renderer().includeIdentity()
		renderer().where.append(query.getResourcePrefix()).append(".ASSIGNEE_ is null")
		renderer().where.append(" AND ").append(query.getIdentityPrefix()).append(".TYPE_ = 'candidate'")
		renderer().where.append(" AND ").append(query.getIdentityPrefix()).append(".GROUP_ID_")
	}

	def dispatch void addProperty(BasicProperty property) {
		switch (property.getType()) {
			case ASSIGNEE:
				renderer().where.append(query.getResourcePrefix()).append(".ASSIGNEE_")
			case TASK_DEFINITION_KEY:
				renderer().where.append(query.getResourcePrefix()).append(".TASK_DEF_KEY_")
			case PROCESS_DEFINITION_KEY: {
				renderer().includeProcessDefinition();
				renderer().where.append(query.getProcessDefinitionPrefix()).append(".KEY_")
			}
			case PROCESS_INSTANCE_ID:
				renderer().where.append(query.getResourcePrefix()).append(".PROC_INST_ID_")
			default:
				throw new IllegalArgumentException("Invalid property type: ")
		}
	}

	def String getColumn(TypeHint type) {
		switch (type) {
			case STRING:
				return ".TEXT_"
			case LONG:
				return ".LONG_"
			default:
				throw new IllegalArgumentException("Invalid variable type")
		}
	}

	def BaseQueryRenderer.RenderContext renderer() {
		return query.currentContext;
	}

}
