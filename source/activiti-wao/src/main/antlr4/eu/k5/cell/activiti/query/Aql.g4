grammar Aql;
options 
{
	language=Java;
}

tokens
{
	PARAM,
	NEGATE
}


section
	:  expression (UNION expression)*;

expression
	: 	orExpression (limit)?;
	

	
		
orExpression
	:	andExpression (OR andExpression )* 
	;

OR 	: 	'||' | O R;
	
andExpression
	:	unaryExpression (AND unaryExpression)* 
	;

AND 	: 	'&&' | A N D;

conditional: value operator value;

limit: LIMIT value ',' value;

LIMIT : L I M I T;

operator:
	|LT
	|LTEQ
	|GT
	|GTEQ
	|EQUALS
	|NOTEQUALS 
	|IN
	;

LT	:	'<';
LTEQ	:	'<=';
GT	:	'>';
GTEQ	:	'>=';

EQUALS	
	:	'=' | '==';
NOTEQUALS 
	:	'!=' | '<>';
IN : I N;

unaryExpression
	:	primaryExpression
    	|	NOT primaryExpression
       	;
  
NOT	:	'!' | 'not';

primaryExpression
	:	'(' orExpression ')'
	| BOOLEAN
	| conditional
	;

value	:  literal	

	| 	property	|	 namedParameter
	;


literal 	: 	INTEGER
	|	FLOAT
	|	BOOLEAN
	|	STRING;


property:
	| variable
	| ASSIGNEE 
	| CANDIDATE
	| TASK_DEFINITION_KEY
	| PROCESS_DEFINITION_KEY
	| PROCESS_INSTANCE_ID;
	
	
namedParameter : NAMED_PARAMETER;


variable : VARIABLE_PREFIX IDENTIFIER;

UNION: 'union';
NAMED_PARAMTER_PREFIX : ':';
VARIABLE_PREFIX : 'var.';
ASSIGNEE : 'assignee';
TASK_DEFINITION_KEY : 'taskDefinitionKey';
PROCESS_DEFINITION_KEY : 'processDefinitionKey';
PROCESS_INSTANCE_ID : 'processInstanceId';
CANDIDATE: 'candidate';

STRING
    	:  	'\'' ( EscapeSequence | (options {greedy=false;} : ~('\u0000'..'\u001f' | '\\' | '\'' ) ) )* '\''
    	;


BOOLEAN
	:	'true'
	|	'false'
	;
	
// Must be declared after the BOOLEAN token or it will hide it


NAMED_PARAMETER
	:	':' ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')+
	;
	

IDENTIFIER
	:	('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*
	;
	
INTEGER	
	:	('0'..'9')+
	;

FLOAT
	:	('0'..'9')* '.' ('0'..'9')+
	;

fragment EscapeSequence 
	:	'\\'
  	(	
  		'n' 
	|	'r' 
	|	't'
	|	'\'' 
	|	'\\'
	|	UnicodeEscape
	)
  ;

fragment UnicodeEscape
    	:    	'u' HexDigit HexDigit HexDigit HexDigit 
    	;

fragment HexDigit 
	: 	('0'..'9'|'a'..'f'|'A'..'F') ;

/* Ignore white spaces */	
WS	
	:  (' '|'\r'|'\t'|'\u000C'|'\n') -> channel(HIDDEN)
	;

	
fragment A:('a'|'A');
fragment B:('b'|'B');
fragment C:('c'|'C');
fragment D:('d'|'D');
fragment E:('e'|'E');
fragment F:('f'|'F');
fragment G:('g'|'G');
fragment H:('h'|'H');
fragment I:('i'|'I');
fragment J:('j'|'J');
fragment K:('k'|'K');
fragment L:('l'|'L');
fragment M:('m'|'M');
fragment N:('n'|'N');
fragment O:('o'|'O');
fragment P:('p'|'P');
fragment Q:('q'|'Q');
fragment R:('r'|'R');
fragment S:('s'|'S');
fragment T:('t'|'T');
fragment U:('u'|'U');
fragment V:('v'|'V');
fragment W:('w'|'W');
fragment X:('x'|'X');
fragment Y:('y'|'Y');
fragment Z:('z'|'Z');