package eu.k5.cell.definition;

public class NestedDataDefinition {
	private final PropertyAssigner assigner;
	private final DataDefinition definition;

	public NestedDataDefinition(PropertyAssigner assigner, DataDefinition definition) {

		this.assigner = assigner;
		this.definition = definition;
	}

	public PropertyAssigner getAssigner() {
		return assigner;
	}

	public DataDefinition getDefinition() {
		return definition;
	}
}
