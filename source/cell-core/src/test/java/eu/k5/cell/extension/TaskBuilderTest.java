package eu.k5.cell.extension;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.definition.DataDefinition;
import eu.k5.cell.shared.CellTaskReference;

public class TaskBuilderTest {

	private Validator validator;

	@Before
	public void init() {
		validator = new Validator();

	}

	public static class Task {

		@CellReference
		private CellTaskReference reference;
	}

	@Test
	public void test() {
		TaskBuilder builder = new TaskBuilder().withDatas(new ArrayList<DataDefinition>()).forEntity(Task.class)
				.withValidator(validator).withBinder(binder());
		builder.init();
		Assert.assertTrue(validator.isEmpty());
	}

	static interface Bind {
		String getProperty();
	}

	private Binder binder() {
		BinderBuilder bb = new BinderBuilder();
		bb.withValidator(validator).addClass(Bind.class).init();
		return bb.build();
	}
}
