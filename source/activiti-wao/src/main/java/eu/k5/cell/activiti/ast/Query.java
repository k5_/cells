package eu.k5.cell.activiti.ast;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Query {

	private final List<String> namedParameters;

	private final List<Section> sections;

	public Query(List<Section> sections, List<String> namedParameters) {

		this.sections = sections;
		this.namedParameters = namedParameters;
	}

	public void accept(QueryVisitor visitor) {
		visitor.visit(this);
	}

	public List<Section> getSections() {
		return sections;
	}

	public List<String> getNamedParameters() {
		return namedParameters;
	}

	public Set<String> getVariables() {
		Set<String> variables = new HashSet<>();
		for (Section section : sections) {
			variables.addAll(section.getVariables());
		}
		return variables;
	}
}
