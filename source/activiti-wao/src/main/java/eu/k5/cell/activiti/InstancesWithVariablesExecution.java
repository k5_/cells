package eu.k5.cell.activiti;

import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.cmd.AbstractCustomSqlExecution;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;

public final class InstancesWithVariablesExecution extends AbstractCustomSqlExecution<AqlMapper, List<ExecutionEntity>> {
	private final Map<String, Object> parameters;

	public InstancesWithVariablesExecution(Map<String, Object> parameters) {
		super(AqlMapper.class);
		this.parameters = parameters;
	}

	@Override
	public List<ExecutionEntity> execute(AqlMapper mapper) {
		return mapper.selectInstancesWithVariables(parameters);
	}
}