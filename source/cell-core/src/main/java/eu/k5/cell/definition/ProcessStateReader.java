package eu.k5.cell.definition;

import eu.k5.cell.CellProcessState;
import eu.k5.cell.annotation.states.ProcessStateResolver;
import eu.k5.cell.annotation.states.ProcessStateType;

public class ProcessStateReader {

	private final String name;
	private final ProcessStateType type;
	private final ProcessStateResolver reader;

	public ProcessStateReader(String name, ProcessStateType type, ProcessStateResolver derivedReader) {
		this.name = name;
		this.type = type;
		reader = derivedReader;
	}

	public Object read(CellProcessState instance) {
		return reader.read(instance);
	}

	public String getName() {
		return name;
	}

	public ProcessStateType getType() {
		return type;
	}

	public ProcessStateResolver getReader() {
		return reader;
	}

}