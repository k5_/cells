package eu.k5.cell.annotation.states;

import java.util.Date;
import java.util.Map;

public enum TaskStateType {
	ASSIGNEE(String.class), DERIVED(Object.class), CREATE_TIME(Date.class), PROCESS_INSTANCE_ID(String.class), PROCESS_DEFINITION_ID(
			String.class), VARIABLES(Map.class), LOCAL_VARIABLES(Map.class);

	private final Class<?> type;

	private TaskStateType(Class<?> type) {
		this.type = type;
	}

	public Class<?> getType() {
		return type;
	}
}
