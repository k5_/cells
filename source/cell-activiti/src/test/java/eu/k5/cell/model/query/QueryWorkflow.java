package eu.k5.cell.model.query;

import java.util.List;

import eu.k5.cell.annotation.Param;
import eu.k5.cell.annotation.Query;
import eu.k5.cell.annotation.workflow.Workflow;
import eu.k5.cell.model.task.TaskWorkflow;

@Workflow(processKey = "taskProcess",binder = TaskWorkflow.Binder.class,activity = TaskWorkflow.Activity.class)
public interface QueryWorkflow {

	public interface Delegate {

		@Query("WHERE assignee=:assignee")
		List<QueryTask> listByAssignee(@Param("assignee") String assignee);

	}
}
