package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.ParameterBound;
import eu.k5.cell.activiti.ParameterLiteralBound;
import eu.k5.cell.activiti.ParameterNameBound;
import eu.k5.cell.activiti.TypeHint;
import eu.k5.cell.activiti.ast.BaseQueryRenderer;
import eu.k5.cell.activiti.ast.BasicProperty;
import eu.k5.cell.activiti.ast.CandidateProperty;
import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.ast.Literal;
import eu.k5.cell.activiti.ast.LongLiteral;
import eu.k5.cell.activiti.ast.NamedParameter;
import eu.k5.cell.activiti.ast.Operator;
import eu.k5.cell.activiti.ast.Property;
import eu.k5.cell.activiti.ast.StringLiteral;
import eu.k5.cell.activiti.ast.Value;
import eu.k5.cell.activiti.ast.Variable;
import java.util.Arrays;

@SuppressWarnings("all")
public class ConditionRenderer {
  private final BaseQueryRenderer query;
  
  private final Context context;
  
  public ConditionRenderer(final BaseQueryRenderer query, final Context context) {
    this.query = query;
    this.context = context;
  }
  
  protected void _render(final Operator op, final Value left, final Value right) {
    this.addOperatorPrefix(op);
    this.addLeft(left, right);
    this.addOperatorInfix(op);
    this.addRight(left, right);
    this.addOperatorSuffix(op);
  }
  
  protected void _render(final Operator op, final Value left, final Property right) {
    this.render(op, right, left);
  }
  
  protected void _addLeft(final Value left, final Value right) {
    Class<? extends Value> _class = left.getClass();
    String _name = _class.getName();
    String _plus = ("missing dispatch for addLeft:" + _name);
    String _plus_1 = (_plus + " Right: ");
    Class<? extends Value> _class_1 = right.getClass();
    String _name_1 = _class_1.getName();
    String _plus_2 = (_plus_1 + _name_1);
    throw new IllegalArgumentException(_plus_2);
  }
  
  protected void _addRight(final Value left, final Value right) {
    Class<? extends Value> _class = left.getClass();
    String _name = _class.getName();
    String _plus = ("missing dispatch for addLeft:" + _name);
    String _plus_1 = (_plus + " Right: ");
    Class<? extends Value> _class_1 = right.getClass();
    String _name_1 = _class_1.getName();
    String _plus_2 = (_plus_1 + _name_1);
    throw new IllegalArgumentException(_plus_2);
  }
  
  protected void _addLeft(final NamedParameter left, final Value context) {
    this.addNamedParameter(left);
  }
  
  protected void _addRight(final Value context, final NamedParameter right) {
    this.addNamedParameter(right);
  }
  
  protected void _addLeft(final Literal<?> left, final Value context) {
    this.addLiteral(left);
  }
  
  protected void _addRight(final Value context, final Literal<?> right) {
    this.addLiteral(right);
  }
  
  protected void _addLeft(final Variable left, final Value context) {
    final String table = this.query.addVariable(false);
    BaseQueryRenderer.RenderContext _renderer = this.renderer();
    StringBuilder _where = _renderer.getWhere();
    StringBuilder _append = _where.append(table);
    StringBuilder _append_1 = _append.append(".NAME_ = \'");
    String _variable = left.getVariable();
    StringBuilder _append_2 = _append_1.append(_variable);
    _append_2.append("\' AND ");
    BaseQueryRenderer.RenderContext _renderer_1 = this.renderer();
    StringBuilder _where_1 = _renderer_1.getWhere();
    StringBuilder _append_3 = _where_1.append(table);
    TypeHint _hint = left.getHint(this.context);
    String _column = this.getColumn(_hint);
    _append_3.append(_column);
  }
  
  protected void _addRight(final Value context, final Variable right) {
    final String table = this.query.addVariable(false);
    BaseQueryRenderer.RenderContext _renderer = this.renderer();
    StringBuilder _where = _renderer.getWhere();
    StringBuilder _append = _where.append(table);
    TypeHint _hint = right.getHint(this.context);
    String _column = this.getColumn(_hint);
    StringBuilder _append_1 = _append.append(_column);
    _append_1.append(" AND ");
    BaseQueryRenderer.RenderContext _renderer_1 = this.renderer();
    StringBuilder _where_1 = _renderer_1.getWhere();
    StringBuilder _append_2 = _where_1.append(table);
    StringBuilder _append_3 = _append_2.append(".NAME_ = \'");
    String _variable = right.getVariable();
    StringBuilder _append_4 = _append_3.append(_variable);
    _append_4.append("\' ");
  }
  
  protected void _addLeft(final Property left, final Value context) {
    this.addProperty(left);
  }
  
  protected void _addRight(final Value context, final BasicProperty right) {
    this.addProperty(right);
  }
  
  public void addNamedParameter(final NamedParameter namedParameter) {
    String _name = namedParameter.getName();
    ParameterBound _parameterBound = this.context.getParameterBound(_name);
    this.addParameterBound(_parameterBound);
  }
  
  protected StringBuilder _addParameterBound(final ParameterNameBound bound) {
    BaseQueryRenderer.RenderContext _renderer = this.renderer();
    StringBuilder _where = _renderer.getWhere();
    StringBuilder _append = _where.append("#{");
    String _name = bound.getName();
    StringBuilder _append_1 = _append.append(_name);
    return _append_1.append("}");
  }
  
  protected StringBuilder _addParameterBound(final ParameterLiteralBound bound) {
    TypeHint _type = bound.getType();
    if (_type != null) {
      switch (_type) {
        case STRING:
          Object _value = bound.getValue();
          String _string = _value.toString();
          StringLiteral _stringLiteral = new StringLiteral(_string);
          this.addLiteral(_stringLiteral);
          break;
        default:
          throw new IllegalArgumentException("Unable to map non string parameter");
      }
    } else {
      throw new IllegalArgumentException("Unable to map non string parameter");
    }
    return null;
  }
  
  public void addOperatorInfix(final Operator op) {
    BaseQueryRenderer.RenderContext _renderer = this.renderer();
    StringBuilder _where = _renderer.getWhere();
    String _token = op.getToken();
    _where.append(_token);
    boolean _equals = Operator.IN.equals(op);
    if (_equals) {
      BaseQueryRenderer.RenderContext _renderer_1 = this.renderer();
      StringBuilder _where_1 = _renderer_1.getWhere();
      _where_1.append("(");
    }
  }
  
  public void addOperatorPrefix(final Operator op) {
  }
  
  public void addOperatorSuffix(final Operator op) {
    boolean _equals = Operator.IN.equals(op);
    if (_equals) {
      BaseQueryRenderer.RenderContext _renderer = this.renderer();
      StringBuilder _where = _renderer.getWhere();
      _where.append(")");
    }
  }
  
  protected void _addLiteral(final StringLiteral literal) {
    BaseQueryRenderer.RenderContext _renderer = this.renderer();
    StringBuilder _where = _renderer.getWhere();
    StringBuilder _append = _where.append("\'");
    String _value = literal.getValue();
    StringBuilder _append_1 = _append.append(_value);
    _append_1.append("\'");
  }
  
  protected void _addLiteral(final LongLiteral literal) {
    BaseQueryRenderer.RenderContext _renderer = this.renderer();
    StringBuilder _where = _renderer.getWhere();
    Long _value = literal.getValue();
    _where.append(_value);
  }
  
  protected void _addProperty(final CandidateProperty property) {
    BaseQueryRenderer.RenderContext _renderer = this.renderer();
    _renderer.includeIdentity();
    BaseQueryRenderer.RenderContext _renderer_1 = this.renderer();
    StringBuilder _where = _renderer_1.getWhere();
    String _resourcePrefix = this.query.getResourcePrefix();
    StringBuilder _append = _where.append(_resourcePrefix);
    _append.append(".ASSIGNEE_ is null");
    BaseQueryRenderer.RenderContext _renderer_2 = this.renderer();
    StringBuilder _where_1 = _renderer_2.getWhere();
    StringBuilder _append_1 = _where_1.append(" AND ");
    String _identityPrefix = this.query.getIdentityPrefix();
    StringBuilder _append_2 = _append_1.append(_identityPrefix);
    _append_2.append(".TYPE_ = \'candidate\'");
    BaseQueryRenderer.RenderContext _renderer_3 = this.renderer();
    StringBuilder _where_2 = _renderer_3.getWhere();
    StringBuilder _append_3 = _where_2.append(" AND ");
    String _identityPrefix_1 = this.query.getIdentityPrefix();
    StringBuilder _append_4 = _append_3.append(_identityPrefix_1);
    _append_4.append(".GROUP_ID_");
  }
  
  protected void _addProperty(final BasicProperty property) {
    BasicProperty.PropertyType _type = property.getType();
    if (_type != null) {
      switch (_type) {
        case ASSIGNEE:
          BaseQueryRenderer.RenderContext _renderer = this.renderer();
          StringBuilder _where = _renderer.getWhere();
          String _resourcePrefix = this.query.getResourcePrefix();
          StringBuilder _append = _where.append(_resourcePrefix);
          _append.append(".ASSIGNEE_");
          break;
        case TASK_DEFINITION_KEY:
          BaseQueryRenderer.RenderContext _renderer_1 = this.renderer();
          StringBuilder _where_1 = _renderer_1.getWhere();
          String _resourcePrefix_1 = this.query.getResourcePrefix();
          StringBuilder _append_1 = _where_1.append(_resourcePrefix_1);
          _append_1.append(".TASK_DEF_KEY_");
          break;
        case PROCESS_DEFINITION_KEY:
          BaseQueryRenderer.RenderContext _renderer_2 = this.renderer();
          _renderer_2.includeProcessDefinition();
          BaseQueryRenderer.RenderContext _renderer_3 = this.renderer();
          StringBuilder _where_2 = _renderer_3.getWhere();
          String _processDefinitionPrefix = this.query.getProcessDefinitionPrefix();
          StringBuilder _append_2 = _where_2.append(_processDefinitionPrefix);
          _append_2.append(".KEY_");
          break;
        case PROCESS_INSTANCE_ID:
          BaseQueryRenderer.RenderContext _renderer_4 = this.renderer();
          StringBuilder _where_3 = _renderer_4.getWhere();
          String _resourcePrefix_2 = this.query.getResourcePrefix();
          StringBuilder _append_3 = _where_3.append(_resourcePrefix_2);
          _append_3.append(".PROC_INST_ID_");
          break;
        default:
          throw new IllegalArgumentException("Invalid property type: ");
      }
    } else {
      throw new IllegalArgumentException("Invalid property type: ");
    }
  }
  
  public String getColumn(final TypeHint type) {
    if (type != null) {
      switch (type) {
        case STRING:
          return ".TEXT_";
        case LONG:
          return ".LONG_";
        default:
          throw new IllegalArgumentException("Invalid variable type");
      }
    } else {
      throw new IllegalArgumentException("Invalid variable type");
    }
  }
  
  public BaseQueryRenderer.RenderContext renderer() {
    return this.query.currentContext();
  }
  
  public void render(final Operator op, final Value left, final Value right) {
    if (right instanceof Property) {
      _render(op, left, (Property)right);
      return;
    } else if (right != null) {
      _render(op, left, right);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(op, left, right).toString());
    }
  }
  
  public void addLeft(final Value left, final Value context) {
    if (left instanceof Literal) {
      _addLeft((Literal<?>)left, context);
      return;
    } else if (left instanceof NamedParameter) {
      _addLeft((NamedParameter)left, context);
      return;
    } else if (left instanceof Property) {
      _addLeft((Property)left, context);
      return;
    } else if (left instanceof Variable) {
      _addLeft((Variable)left, context);
      return;
    } else if (left != null) {
      _addLeft(left, context);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(left, context).toString());
    }
  }
  
  public void addRight(final Value context, final Value right) {
    if (right instanceof BasicProperty) {
      _addRight(context, (BasicProperty)right);
      return;
    } else if (right instanceof Literal) {
      _addRight(context, (Literal<?>)right);
      return;
    } else if (right instanceof NamedParameter) {
      _addRight(context, (NamedParameter)right);
      return;
    } else if (right instanceof Variable) {
      _addRight(context, (Variable)right);
      return;
    } else if (right != null) {
      _addRight(context, right);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(context, right).toString());
    }
  }
  
  public StringBuilder addParameterBound(final ParameterBound bound) {
    if (bound instanceof ParameterLiteralBound) {
      return _addParameterBound((ParameterLiteralBound)bound);
    } else if (bound instanceof ParameterNameBound) {
      return _addParameterBound((ParameterNameBound)bound);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(bound).toString());
    }
  }
  
  public void addLiteral(final Literal<?> literal) {
    if (literal instanceof LongLiteral) {
      _addLiteral((LongLiteral)literal);
      return;
    } else if (literal instanceof StringLiteral) {
      _addLiteral((StringLiteral)literal);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(literal).toString());
    }
  }
  
  public void addProperty(final Property property) {
    if (property instanceof BasicProperty) {
      _addProperty((BasicProperty)property);
      return;
    } else if (property instanceof CandidateProperty) {
      _addProperty((CandidateProperty)property);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(property).toString());
    }
  }
}
