package eu.k5.cell.wao;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import eu.k5.cell.WorkflowManager;

public class WaoDefinition {
	private final Class<?> type;
	private final Map<Method, DelegateDefinition> delegates;

	public WaoDefinition(Class<?> type, Map<Method, DelegateDefinition> delegates) {
		this.type = type;
		this.delegates = delegates;
	}

	public Wao bind(WorkflowManager cellManager) {
		Builder<Method, Delegate> builder = ImmutableMap.builder();
		for (Entry<Method, DelegateDefinition> definition : delegates.entrySet()) {
			builder.put(definition.getKey(), definition.getValue().bind(cellManager));
		}

		return new Wao(type, builder.build());
	}

}
