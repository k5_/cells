package eu.k5.cell.activiti.filter;

import org.activiti.engine.task.TaskQuery;

import eu.k5.cell.activiti.TaskTokenCreator;
import eu.k5.cell.shared.CellTaskReference;

public class CellTaskReferenceFilter implements TaskFilter {
	private final CellTaskReference reference;

	public CellTaskReferenceFilter(CellTaskReference reference) {
		this.reference = reference;
	}

	@Override
	public TaskQuery filter(TaskQuery query) {
		return query.taskId(reference.getTaskId()).processInstanceId(reference.getProcessInstance())
				.taskDefinitionKey(reference.getTaskDefinition())
				.taskVariableValueEquals(TaskTokenCreator.TOKEN, reference.getToken());
	}
}
