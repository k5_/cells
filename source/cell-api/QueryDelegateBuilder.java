package eu.k5.cell.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.k5.cell.activiti.TypeHint;
import eu.k5.cell.activiti.query.AqlQueries;
import eu.k5.cell.activiti.query.TaskQuery;
import eu.k5.cell.annotation.Param;
import eu.k5.cell.annotation.Query;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.pao.ParameterMapper;
import eu.k5.cell.pao.TaskListQueryDelegateDefinition;

public class QueryDelegateBuilder extends Builder<TaskListQueryDelegateDefinition, QueryDelegateBuilder> {
	private static final Logger LOGGER = LoggerFactory.getLogger(QueryDelegateBuilder.class);
	private final AqlQueries cql = new AqlQueries();
	private Binder binder;
	private Method method;
	private Class<?> delegate;
	private Class<?> returnType;

	private List<TaskDefinition> tasks;

	private Validator validator;

	private TaskQuery query;

	private final Map<String, QueryParameter> parameters = new HashMap<>();

	static class QueryParameter {
		private final String name;
		private final Type type;
		private final int index;

		public QueryParameter(String name, Type type, int index) {
			this.name = name;
			this.type = type;
			this.index = index;
		}

		public String getName() {
			return name;
		}

		public Type getType() {
			return type;
		}

		public int getIndex() {
			return index;
		}

		public TypeHint getHint() {
			return TypeHint.forType(type);
		}

	}

	public QueryDelegateBuilder() {
		super(TaskListQueryDelegateDefinition.class);
	}

	public QueryDelegateBuilder withMethod(Method method) {
		this.method = method;
		return this;
	}

	@Override
	public QueryDelegateBuilder withBinder(Binder binder) {
		this.binder = binder;
		return this;
	}

	public QueryDelegateBuilder withTasks(List<TaskDefinition> tasks) {
		this.tasks = tasks;
		return this;
	}

	public QueryDelegateBuilder forMethod(Method method) {
		this.method = method;
		return this;
	}

	@Override
	public void doInit() {
		initParameters();
		parseQuery();
		initType();

	}

	@Override
	protected TaskListQueryDelegateDefinition doBuild() {
		Map<String, TaskDefinition> defs = new HashMap<String, TaskDefinition>();

		for (TaskDefinition task : tasks) {
			defs.put(task.getTaskDefinitionKey(), task);
		}

		return new TaskListQueryDelegateDefinition(query, defs, getParameterMapper());
	}

	void initType() {
		Class<?> type = method.getReturnType();
		if (type.isAssignableFrom(List.class)) {
			Type returnType = method.getGenericReturnType();
			if (returnType instanceof ParameterizedType) {
				Type typeArgument = ((ParameterizedType) returnType).getActualTypeArguments()[0];

				if (typeArgument instanceof ParameterizedType) {
					validator.add(delegate, "");

				} else if (typeArgument instanceof Class<?>) {
					returnType = typeArgument;
				}

			} else {
				validator.add(delegate, "Return type of query method {} is an unparamerized List", method);
			}

		} else {
			returnType = type;
		}
	}

	void initParameters() {
		Annotation[][] annotations = method.getParameterAnnotations();

		Class<?>[] types = method.getParameterTypes();
		String[] parameters = new String[types.length];
		for (int i = 0; i < types.length; i++) {
			Param param = getParameter(annotations[i]);
			parameters[i] = param.value();
			this.parameters.put(param.value(), new QueryParameter(param.value(), types[i], i));
		}
	}

	ParameterMapper getParameterMapper() {
		String[] names = new String[parameters.size()];
		for (QueryParameter param : parameters.values()) {
			names[param.getIndex()] = param.getName();
		}
		return new ParameterMapper(names);
	}

	Param getParameter(Annotation[] annotations) {
		for (Annotation a : annotations) {
			if (a instanceof Param) {
				return (Param) a;
			}

		}
		return null;
	}

	void parseQuery() {
		Query anno = method.getAnnotation(Query.class);

		Map<String, TypeHint> parameterHints = new HashMap<>();
		for (Entry<String, QueryParameter> queryParameter : parameters.entrySet()) {
			parameterHints.put(queryParameter.getKey(), queryParameter.getValue().getHint());
		}

		query = cql.parseTaskQuery(anno.value(), parameterHints, binder.getTypeHints());

		//
		// cql.parseTaskQuery(service, query, parameters, variables)
		// query = cql.parse(anno.value());
	}
}