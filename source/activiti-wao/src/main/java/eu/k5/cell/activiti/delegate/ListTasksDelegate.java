package eu.k5.cell.activiti.delegate;

import java.util.List;
import java.util.Map;

import org.activiti.engine.ManagementService;
import org.activiti.engine.impl.persistence.entity.TaskEntity;

import eu.k5.cell.activiti.TasksWithVariablesExecution;
import eu.k5.cell.activiti.builder.QueryParameter;

class ListTasksDelegate extends ParameterMappingDelegate {

	public ListTasksDelegate(String sql, ManagementService service, QueryParameter[] parameters) {
		super(sql, service, parameters);
	}

	@Override
	public List<TaskEntity> invoke(Object... arguments) {
		Map<String, Object> parameters = getParameters(arguments);
		return getService().executeCustomSql(new TasksWithVariablesExecution(parameters));
	}
}
