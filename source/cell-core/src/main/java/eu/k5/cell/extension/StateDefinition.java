package eu.k5.cell.extension;

import eu.k5.cell.definition.PropertyAssigner;

public class StateDefinition<R> {

	private final PropertyAssigner assigner;
	private final R reader;

	public StateDefinition(PropertyAssigner assigner, R reader) {
		this.assigner = assigner;
		this.reader = reader;
	}

	public PropertyAssigner getAssigner() {
		return assigner;
	}

	public R getReader() {
		return reader;
	}

}
