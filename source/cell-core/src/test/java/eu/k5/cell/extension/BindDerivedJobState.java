package eu.k5.cell.extension;

import eu.k5.cell.annotation.states.JobStateResolver;

public class BindDerivedJobState implements JobStateResolver {

	@Override
	public Class<?> getReturnType() {
		return String.class;
	}

	@Override
	public Object read(Object state) {
		return "derived";
	}

}
