package eu.k5.cell.parser;

public interface SourceVisitor<R, P> {

	R visit(VariableSource variable, P param);

	R visit(TaskReferenceSource task, P param);

}
