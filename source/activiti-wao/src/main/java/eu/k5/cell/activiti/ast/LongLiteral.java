package eu.k5.cell.activiti.ast;

import eu.k5.cell.activiti.TypeHint;

public class LongLiteral extends Literal<Long> {
	public LongLiteral(Long literal) {
		super(literal, TypeHint.LONG);
	}

	@Override
	public void accept(ValueVisitor visitor) {
		visitor.visit(this);
	}

}
