package eu.k5.cell;

import java.util.List;

public interface WorkflowManager {

	void start(Object start);

	void complete(Object task);

	<T> List<T> list(Class<T> type, String query, Object... parameters);

	<T> T find(Class<T> type, String query, Object... parameters);

	<U> U unwrap(Class<U> type);

	<D> D wao(Class<D> type);
}
