package eu.k5.cell.activiti;

import java.util.List;

import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.runtime.ProcessInstanceQuery;
import org.activiti.engine.task.TaskQuery;

import eu.k5.cell.activiti.filter.AspectQuery;

class AspectQueryImpl implements AspectQuery {

	private boolean includeHistoric = false;
	private final ProcessInstanceQuery instances;
	private final HistoricProcessInstanceQuery historicInstances;
	private final TaskQuery tasks;

	public AspectQueryImpl(ProcessInstanceQuery instances, HistoricProcessInstanceQuery historicInstances,
			TaskQuery tasks) {
		this.instances = instances;
		this.historicInstances = historicInstances;
		this.tasks = tasks;
	}

	@Override
	public AspectQuery processInstanceBusinessKey(String businessKey) {
		instances.processInstanceBusinessKey(businessKey);
		historicInstances.processInstanceBusinessKey(businessKey);
		tasks.processInstanceBusinessKey(businessKey);
		return this;
	}

	public ProcessInstanceQuery getInstances() {
		return instances;
	}

	public HistoricProcessInstanceQuery getHistoricInstances() {
		return historicInstances;
	}

	public TaskQuery getTasks() {
		return tasks;
	}

	@Override
	public AspectQuery includeHistoric(boolean state) {
		includeHistoric = state;
		return this;
	}

	@Override
	public AspectQuery processVariableValueLessThanOrEqual(String variableName, Object variableValue) {
		instances.variableValueLessThanOrEqual(variableName, variableValue);
		tasks.processVariableValueLessThanOrEqual(variableName, variableValue);
		historicInstances.variableValueLessThanOrEqual(variableName, variableValue);
		return this;
	}

	@Override
	public AspectQuery processVariableValueGreaterThanOrEqual(String variableName, Object variableValue) {
		instances.variableValueGreaterThanOrEqual(variableName, variableValue);
		tasks.processVariableValueGreaterThanOrEqual(variableName, variableValue);
		historicInstances.variableValueGreaterThanOrEqual(variableName, variableValue);
		return this;
	}

	@Override
	public AspectQuery processVariableValueGreaterThan(String variableName, Object variableValue) {
		instances.variableValueGreaterThan(variableName, variableValue);
		tasks.processVariableValueGreaterThan(variableName, variableValue);
		historicInstances.variableValueGreaterThan(variableName, variableValue);
		return this;
	}

	@Override
	public AspectQuery processVariableValueLessThan(String variableName, Object variableValue) {
		instances.variableValueLessThan(variableName, variableValue);
		tasks.processVariableValueLessThan(variableName, variableValue);
		historicInstances.variableValueLessThan(variableName, variableValue);
		return this;
	}

	@Override
	public AspectQuery processVariableValueEquals(String variableName, Object variableValue) {
		instances.variableValueEquals(variableName, variableValue);
		tasks.processVariableValueEquals(variableName, variableValue);
		historicInstances.variableValueEquals(variableName, variableValue);
		return this;
	}

	@Override
	public AspectQuery superProcessInstanceId(String superProcessInstanceId) {
		instances.superProcessInstanceId(superProcessInstanceId);
		return this;
	}

	@Override
	public AspectQueryImpl subProcessInstanceId(String superProcessInstanceId) {
		instances.subProcessInstanceId(superProcessInstanceId);
		return this;
	}

	@Override
	public AspectQuery taskAssignee(String assignee) {
		tasks.taskAssignee(assignee);
		return this;
	}

	public boolean isIncludeHistoric() {
		return includeHistoric;
	}

	@Override
	public AspectQuery taskCandidateGroupIn(List<String> candidateGroups) {
		tasks.taskCandidateGroupIn(candidateGroups);
		return this;
	}
}
