package eu.k5.cell.activiti.delegate;

import java.lang.reflect.Method;
import java.util.Map;

import eu.k5.cell.activiti.VariableBound;
import eu.k5.cell.activiti.builder.Violations;

public abstract class AbstractDelegateBuilder<B, T> {

	protected Method method;
	protected String queryString;
	protected Map<String, VariableBound> variableHints;

	private B builder;
	private final Violations violations = new Violations();

	protected boolean isList;

	protected final void initBuilder(B builder) {
		this.builder = builder;
	}

	public abstract T build();

	public B withMethod(Method method) {
		this.method = method;
		return builder;
	}

	public Violations getViolations() {
		return violations;
	}

	public B asList(boolean isList) {
		this.isList = isList;
		return builder;
	}

	public B withQueryString(String queryString) {
		this.queryString = queryString;
		return builder;
	}

	public B withVariableBounds(Map<String, VariableBound> typeHints) {
		variableHints = typeHints;
		return builder;
	}

}