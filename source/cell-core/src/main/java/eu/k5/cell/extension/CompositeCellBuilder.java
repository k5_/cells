package eu.k5.cell.extension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import eu.k5.cell.annotation.AccessObject;
import eu.k5.cell.annotation.workflow.Workflows;
import eu.k5.cell.definition.AspectDefinition;
import eu.k5.cell.definition.DataDefinition;
import eu.k5.cell.definition.StartDefinition;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.definition.WorkflowDefinition;
import eu.k5.cell.extension.Validator.Type;
import eu.k5.cell.wao.WaoDefinition;

public class CompositeCellBuilder {
	private Class<?> type;
	private Validator validator = new Validator();
	private boolean init = false;
	private int violations = 0;
	private List<CellBuilder> availableCells;

	private final List<Cell> cells = new ArrayList<>();

	private final List<StartDefinition> starts = new ArrayList<>();
	private final List<TaskDefinition> tasks = new ArrayList<>();
	private final List<AspectDefinition> aspects = new ArrayList<>();
	private final List<DataDefinition> datas = new ArrayList<>();
	private final Map<Class<?>, WaoDefinition> waos = new HashMap<Class<?>, WaoDefinition>();
	private WorkflowDefinition workflow;

	public CompositeCellBuilder withType(Class<?> type) {
		this.type = type;
		return this;
	}

	public CompositeCellBuilder withValidator(Validator validator) {
		this.validator = validator;
		return this;
	}

	public CompositeCellBuilder init() {
		if (isInit() || violations > 0) {
			return this;
		}
		if (!type.isAnnotationPresent(Workflows.class)) {
			validator.add(type, "Workflows annotation is missing from type {}", type);

			return this;
		}

		Workflows workflows = type.getAnnotation(Workflows.class);
		for (Class<?> sub : workflows.workflow()) {
			initSubCells(sub);
		}
		initWorkflowDefinition();
		initStarts();
		initTasks();

		for (AccessObject ao : workflows.delegates()) {
			initAccessObject(ao);
		}

		if (violations == 0) {
			init = true;
		}
		return this;
	}

	private void initWorkflowDefinition() {
		workflow = new WorkflowDefinition(type, null, null);
	}

	private void initSubCells(Class<?> subType) {
		List<CellBuilder> subCell = availableCells.stream().filter(b -> b.getType().equals(subType))
				.collect(Collectors.toList());
		if (subCell.isEmpty() || subCell.size() > 1) {
			violation(Type.REFERENCES_UNKOWN_CELL, type, subType);
			return;
		}
		if (!subCell.get(0).isInit()) {
			violation(Type.REFERENCES_INVALID_CELL, type, subType);
			return;
		}
		cells.add(subCell.get(0).build());
	}

	public boolean isInit() {
		return init;
	}

	private void enforceInit() {
		init();
		if (!isInit()) {
			new IllegalStateException("Composite Cell builder for cell " + type + " can not be initialized");
		}
	}

	private void initStarts() {
		Set<Class<?>> found = new HashSet<>();
		boolean invalid = false;
		for (Cell cell : cells) {

			for (Entry<Class<?>, StartDefinition> entry : cell.getStarts().entrySet()) {

				if (found.contains(entry.getKey())) {
					violation(Type.MULTIPLE_START_MAPPINGS, entry.getKey());
					invalid = true;
				}
				starts.add(entry.getValue());
			}
		}
		if (invalid) {
			starts.clear();
		}
	}

	private void initTasks() {

		for (Cell cell : cells) {
			for (TaskDefinition task : cell.getTasks().values()) {
				tasks.add(task);
			}
		}

	}

	private void initAccessObject(AccessObject ao) {
		Class<?> type = ao.value();

		DelegateBuilder builder = new DelegateBuilder();
		builder.withValidator(validator).forEntity(type).withTasks(tasks).withAspects(aspects);

		builder.init();

		if (builder.isInit()) {
			waos.put(type, builder.build());
		} else {
			waos.put(type, null);
		}

	}

	public Cell build() {
		enforceInit();

		return new Cell(workflow, starts, new ArrayList<>(), tasks, aspects, new ArrayList<>(), waos);
	}

	public CompositeCellBuilder withCells(List<CellBuilder> builders) {
		availableCells = builders;
		return this;
	}

	void violation(Type type, Object... arguments) {
		validator.add(this.type, type, arguments);
		violations++;
	}

	public Validator getValidator() {
		return validator;
	}
}
