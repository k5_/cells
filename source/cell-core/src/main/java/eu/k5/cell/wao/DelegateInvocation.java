package eu.k5.cell.wao;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class DelegateInvocation implements InvocationHandler {
	private final Wao delegates;

	public DelegateInvocation(Wao delegates) {
		this.delegates = delegates;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		return delegates.getDelegate(method).invoke(args);
	}

}
