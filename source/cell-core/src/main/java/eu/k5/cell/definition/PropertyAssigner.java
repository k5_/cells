package eu.k5.cell.definition;

import java.lang.annotation.Annotation;

public interface PropertyAssigner {

	void assign(Object instance, Object value);

	boolean isAnnotationPresent(Class<? extends Annotation> annotations);

	<A extends Annotation> A getAnnotation(Class<A> annotation);

	Class<?> getType();

}
