package eu.k5.cell.model.delegate;

import java.util.List;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import eu.k5.cell.WorkflowManager;
import eu.k5.cell.activiti.TestEngine;
import eu.k5.cell.annotation.CellContext;
import eu.k5.cell.extension.CellExtension;
import eu.k5.cell.model.TestProducer;
import eu.k5.cell.model.delegate.DelegateWorkflow.Delegate;

@RunWith(CdiRunner.class)
@AdditionalClasses({ TestProducer.class, CellExtension.class, DelegateWorkflow.class })
public class DelegateTest {
	private static TestEngine refEngine = new TestEngine("delegate", "delegate");

	@CellContext(DelegateWorkflow.class)
	@Inject
	WorkflowManager cell;

	@Produces
	TestEngine engine = refEngine;

	@Before
	public void init() {
		engine.reset();
	}

	@AfterClass
	public static void destoy() {
		refEngine.destroy();
	}

	@Test
	public void readAspect() {
		Delegate delegate = cell.wao(DelegateWorkflow.Delegate.class);

		delegate.start(new DelegateStart("key", "property", "assign", "property2"));
		delegate.start(new DelegateStart("keyx", "propertyx", "assign2", "property2x"));

		List<DelegateAspect> aspects = delegate.aspectByProperty("property");
		Assert.assertEquals(1, aspects.size());

		DelegateAspect aspect = aspects.get(0);
		Assert.assertEquals("property", aspect.getProperty());
		Assert.assertNotNull(aspect.getTask());
	}

	@Test
	public void byProperty() {
		Delegate delegate = cell.wao(DelegateWorkflow.Delegate.class);
		delegate.start(new DelegateStart("key", "property", "assign", "prop"));
		delegate.start(new DelegateStart("key2", "property2", "assign", "prop2"));

		List<DelegateTask> tasks = delegate.byProperty("property");
		Assert.assertEquals(1, tasks.size());
		Assert.assertEquals("key", tasks.get(0).getKey());
	}

	@Test
	public void propertiesEqual() {
		Delegate delegate = cell.wao(DelegateWorkflow.Delegate.class);
		delegate.start(new DelegateStart("key", "p", "assign", "p"));
		delegate.start(new DelegateStart("key2", "property2", "assign", "prop2"));
		delegate.start(new DelegateStart("key3", "p3", "assign", "p3"));

		List<DelegateTask> tasks = delegate.propertiesEqual();
		Assert.assertEquals(2, tasks.size());

	}

	@Test
	public void start() {

		//
		Delegate delegate = cell.wao(DelegateWorkflow.Delegate.class);
		delegate.start(new DelegateStart("key", "property", "assign", "property2"));
		delegate.start(new DelegateStart("keyx", "propertyx", "assign2", "property2x"));

		List<DelegateTask> task = delegate.listAll();
		System.out.println(task);

		List<DelegateTask> byAssignee = delegate.byAssignee("assign");
		System.out.println(byAssignee);
		Assert.assertEquals("property", byAssignee.get(0).getProperty());

		Assert.assertEquals("propertyx", delegate.byAssignee("assign2").get(0).getProperty());
		Assert.assertEquals("propertyx", delegate.byProperty("propertyx").get(0).getProperty());

		//
		// String select =
		// "VAR.ID_ as VAR_ID_, VAR.NAME_ as VAR_NAME_, VAR.TYPE_ as VAR_TYPE_, VAR.REV_ as VAR_REV_,"
		// +
		// "VAR.PROC_INST_ID_ as VAR_PROC_INST_ID_, VAR.EXECUTION_ID_ as VAR_EXECUTION_ID_, VAR.TASK_ID_ as VAR_TASK_ID_,"
		// +
		// "VAR.BYTEARRAY_ID_ as VAR_BYTEARRAY_ID_, VAR.DOUBLE_ as VAR_DOUBLE_, "
		// +
		// "VAR.TEXT_ as VAR_TEXT_, VAR.TEXT2_ as VAR_TEXT2_, VAR.LONG_ as VAR_LONG_";
		//
		// HashMap<String, Object> p = new HashMap<String, Object>();
		// p.put("assigne", "assign");
		// p.put("prop", new String[] { "", "propertyx", "property2" });
		// // p.put("prop2", "property2");
		//
		// p.put("assignee2", "assign");
		//
		// Assignee assignee = new Assignee();
		// NamedParameter parameter = new NamedParameter("assignee");
		// NamedParameter param2 = new NamedParameter("prop");
		// NamedParameter param3 = new NamedParameter("prop2");
		//
		// Variable variable = new Variable("property");
		//
		// Variable variable2 = new Variable("property2");
		//
		// CellParser parser = new CellParser();
		// TaskQuery query =
		// parser.parse("assignee == :assigne AND ( var.property == :prop OR var.property2 == :prop )");
		//
		// TaskQueryBuilder builder = new TaskQueryBuilder();
		// query.accept(builder);
		// System.out.println(builder.getQuery());
		//
		// cell.findTask(DelegateTask.class, builder.getQuery(), p);

		// left outer join ${prefix}ACT_RU_VARIABLE VAR ON RES.ID_ =
		// VAR.TASK_ID_ or RES.PROC_INST_ID_ = VAR.EXECUTION_ID_

		// <id property="id" column="VAR_ID_"/>
		// <result property="name" column="VAR_NAME_" javaType="String"
		// jdbcType="VARCHAR" />
		// <result property="type" column="VAR_TYPE_"
		// javaType="org.activiti.engine.impl.variable.VariableType"
		// jdbcType="VARCHAR" />
		// <result property="revision" column="VAR_REV_" jdbcType="INTEGER" />
		// <result property="processInstanceId" column="VAR_PROC_INST_ID_"
		// jdbcType="VARCHAR" />
		// <result property="executionId" column="VAR_EXECUTION_ID_"
		// jdbcType="VARCHAR" />
		// <result property="taskId" column="VAR_TASK_ID_" jdbcType="VARCHAR" />
		// <result property="byteArrayRef" column="VAR_BYTEARRAY_ID_"
		// typeHandler="ByteArrayRefTypeHandler"/>
		// <result property="doubleValue" column="VAR_DOUBLE_" jdbcType="DOUBLE"
		// />
		// <result property="textValue" column="VAR_TEXT_" jdbcType="VARCHAR" />
		// <result property="textValue2" column="VAR_TEXT2_" jdbcType="VARCHAR"
		// />
		// <result property="longValue" column="VAR_LONG_" jdbcType="BIGINT" />

		List<DelegateTask> list = task;
	}

}