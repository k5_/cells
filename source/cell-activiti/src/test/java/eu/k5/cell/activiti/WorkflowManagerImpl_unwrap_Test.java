package eu.k5.cell.activiti;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.TaskQuery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import eu.k5.cell.CellException;
import eu.k5.cell.extension.Cell;

public class WorkflowManagerImpl_unwrap_Test {
	@InjectMocks
	private WorkflowManagerImpl manager;

	@Mock
	private ManagementService managementService;

	@Mock
	private RuntimeService runtimeService;

	@Mock
	private TaskService taskService;

	@Mock
	private HistoryService historyService;

	@Mock
	private TaskQuery taskQuery;

	@Mock
	private Cell cell;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void unwrapManagementService() {
		Assert.assertEquals(managementService, manager.unwrap(ManagementService.class));
	}

	@Test
	public void unwrapRuntimeService() {
		Assert.assertEquals(runtimeService, manager.unwrap(RuntimeService.class));
	}

	@Test
	public void unwrapTaskService() {
		Assert.assertEquals(taskService, manager.unwrap(TaskService.class));
	}

	@Test
	public void unwrapHistoryService() {
		Assert.assertEquals(historyService, manager.unwrap(HistoryService.class));
	}

	@Test
	public void unwrapCell() {
		Assert.assertEquals(cell, manager.unwrap(Cell.class));
	}

	@Test(expected = CellException.class)
	public void unwrap_invalid() {
		manager.unwrap(Object.class);
	}
}
