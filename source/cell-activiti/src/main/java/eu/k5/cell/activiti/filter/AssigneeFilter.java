package eu.k5.cell.activiti.filter;

import org.activiti.engine.task.TaskQuery;

import eu.k5.cell.activiti.ProcessState;

public class AssigneeFilter implements AspectFilter, TaskFilter {

	private final String assignee;

	public AssigneeFilter(String assignee) {
		this.assignee = assignee;
	}

	@Override
	public AspectQuery filter(AspectQuery query) {
		query.taskAssignee(assignee);
		return query;
	}

	@Override
	public boolean accept(ProcessState state) {
		return true;
	}

	public String getAssignee() {
		return assignee;
	}

	@Override
	public TaskQuery filter(TaskQuery query) {
		return query.taskAssignee(assignee);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (assignee == null ? 0 : assignee.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AssigneeFilter other = (AssigneeFilter) obj;
		if (assignee == null) {
			if (other.assignee != null) {
				return false;
			}
		} else if (!assignee.equals(other.assignee)) {
			return false;
		}
		return true;
	}

}
