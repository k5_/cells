package eu.k5.cell.model.data;

import eu.k5.cell.annotation.Out;

public class DataStart {
	@Out
	private final String key;
	@Out
	private final String property;

	public DataStart(String key, String property) {
		this.key = key;
		this.property = property;
	}

	public String getKey() {
		return key;
	}

	public String getProperty() {
		return property;
	}
}
