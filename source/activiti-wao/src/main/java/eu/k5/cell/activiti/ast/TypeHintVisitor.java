package eu.k5.cell.activiti.ast;

import java.util.Map;

import eu.k5.cell.activiti.ParameterBound;
import eu.k5.cell.activiti.VariableBound;

public class TypeHintVisitor implements QueryVisitor, ValueVisitor {

	private final StringBuilder builder = new StringBuilder();

	private final Context context;

	public TypeHintVisitor(Map<String, VariableBound> variables, Map<String, ParameterBound> parameters) {
		context = new Context(variables, parameters);
	}

	@Override
	public void visit(BasicProperty property) {
		builder.append("[").append(property.getHint(context)).append("]");
		builder.append(property.getType());
	}

	@Override
	public void visit(NamedParameter namedParameter) {
		builder.append("[").append(namedParameter.getHint(context)).append("]");
		builder.append("Parameter:").append(namedParameter.getName());
	}

	@Override
	public void visit(Variable variable) {
		builder.append("[").append(variable.getHint(context)).append("]");
		builder.append("Variable:").append(variable.getVariable());

	}

	@Override
	public void visit(AndExpression andExpression) {
		builder.append("[").append(andExpression.getHint(context)).append("]");
		boolean first = true;
		for (Expression part : andExpression.getParts()) {
			if (first) {
				first = false;
			} else {
				builder.append(" AND ");
			}
			part.accept(this);
		}
	}

	@Override
	public void visit(OrExpression orExpression) {
		builder.append("[").append(orExpression.getHint(context)).append("]");
		boolean first = true;
		for (Expression part : orExpression.getParts()) {
			if (first) {
				first = false;
			} else {
				builder.append(" OR ");
			}
			part.accept(this);
		}
	}

	@Override
	public void visit(Query query) {
		for (Section section : query.getSections()) {
			section.accept(this);
		}
	}

	@Override
	public void visit(Section section) {
		section.getWhereExpression().accept(this);
	}

	@Override
	public void visit(UnaryExpression unary) {
		builder.append("[").append(unary.getHint(context)).append("](");
		unary.getExpression().accept(this);
		builder.append(")");
	}

	@Override
	public void visit(Condition condition) {
		builder.append("[").append(condition.getHint(context)).append("]");
		condition.getLeft().accept(this);
		builder.append(" ").append(condition.getOperator()).append(" ");
		condition.getRight().accept(this);
	}

	@Override
	public void visit(BooleanLiteral literal) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(StringLiteral stringLiteral) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(LongLiteral longLiteral) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(DoubleLiteral doubleLiteral) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(DateLiteral dateLiteral) {
		// TODO Auto-generated method stub

	}

	public String getTypeHintTree() {
		return builder.toString();
	}

	@Override
	public void visit(BooleanExpression booleanExpression) {

		builder.append("[").append(booleanExpression.getHint(context)).append("]").append(booleanExpression.getValue());
	}

	@Override
	public void visit(Limit limit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(CandidateProperty candidateProperty) {
		// TODO Auto-generated method stub

	}

}
