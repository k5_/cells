package eu.k5.cell.activiti.builder;

import java.lang.reflect.Method;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.k5.cell.activiti.annotation.Param;
import eu.k5.cell.activiti.builder.Violations.Type;

public class ParameterBuilderTest {
	ParameterBuilder builder;
	Violations violations;

	@Before
	public void init() {
		violations = new Violations();
		builder = new ParameterBuilder();
		builder.withViolations(violations);
	}

	interface Methods {
		void emptyParameterList();

		void oneMapped(@Param("name") String param);

		void oneUnmapped(String param);

		void doubleMapped(@Param("name") String param1, @Param("name") String param2);

		void invalidName(@Param(":") String param);

		void invalidType(@Param("obj") Object object);
	}

	@Test
	public void emptyParameterList() {
		builder.forMethod(getMethod("emptyParameterList"));
		Assert.assertArrayEquals(new String[] {}, builder.getNames());
		Assert.assertTrue(violations.isEmpty());
	}

	@Test
	public void oneMapped() {
		builder.forMethod(getMethod("oneMapped"));
		Assert.assertArrayEquals(new String[] { "name" }, builder.getNames());
		Assert.assertTrue(violations.isEmpty());
	}

	@Test
	public void oneUnmapped() {
		builder.forMethod(getMethod("oneUnmapped"));
		Assert.assertFalse(violations.isEmpty());
		Assert.assertTrue(violations.contains(Type.INVALID_PARAMETER));
	}

	@Test
	public void doubleMapped() {
		builder.forMethod(getMethod("doubleMapped"));

		Assert.assertFalse(violations.isEmpty());
		Assert.assertTrue(violations.contains(Type.INVALID_PARAMETER));
	}

	@Test
	public void invalidName() {
		builder.forMethod(getMethod("invalidName"));

		Assert.assertFalse(violations.isEmpty());
		Assert.assertTrue(violations.contains(Type.INVALID_PARAMETER));
	}

	@Test
	public void invalidType() {
		builder.forMethod(getMethod("invalidType"));

		Assert.assertFalse(violations.isEmpty());
		Assert.assertTrue(violations.contains(Type.INVALID_PARAMETER));
	}

	private Method getMethod(String name) {
		for (Method method : Methods.class.getMethods()) {
			if (method.getName().equals(name)) {
				return method;
			}
		}
		return null;
	}
}
