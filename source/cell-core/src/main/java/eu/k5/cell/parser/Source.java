package eu.k5.cell.parser;

public interface Source {

	Object read(Context context);

	<R, P> R accept(SourceVisitor<R, P> visitor, P param);
}
