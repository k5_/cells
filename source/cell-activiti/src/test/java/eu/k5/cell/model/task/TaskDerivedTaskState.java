package eu.k5.cell.model.task;

import org.activiti.engine.task.Task;
import org.junit.Assert;

import eu.k5.cell.annotation.states.TaskStateResolver;

public class TaskDerivedTaskState implements TaskStateResolver {

	@Override
	public Class<?> getReturnType() {
		return String.class;
	}

	@Override
	public Object read(Object state) {
		Assert.assertTrue(state instanceof Task);
		return "derived";
	}
}
