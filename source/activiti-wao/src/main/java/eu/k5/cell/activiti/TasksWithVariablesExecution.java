package eu.k5.cell.activiti;

import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.cmd.AbstractCustomSqlExecution;
import org.activiti.engine.impl.persistence.entity.TaskEntity;

public final class TasksWithVariablesExecution extends AbstractCustomSqlExecution<AqlMapper, List<TaskEntity>> {
	private final Map<String, Object> parameters;

	public TasksWithVariablesExecution(Map<String, Object> parameters) {
		super(AqlMapper.class);
		this.parameters = parameters;
	}

	@Override
	public List<TaskEntity> execute(AqlMapper mapper) {
		return mapper.selectTasksWithVariables(parameters);
	}
}