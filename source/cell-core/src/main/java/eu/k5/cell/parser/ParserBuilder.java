package eu.k5.cell.parser;

public abstract class ParserBuilder<T, B> {
	private final Class<T> type;

	private final BoardBuilder board;

	private boolean init = false;

	private B builder;

	public ParserBuilder(Class<T> type) {
		this.type = type;
		this.board = new BoardBuilder();
	}

	protected final void setBuilder(B builder) {
		this.builder = builder;
	}

	protected boolean isInit() {
		return init;
	}

	public BoardBuilder getBoard() {
		return board;
	}

	public Class<T> getType() {
		return type;
	}

	protected void setInit(boolean init) {
		this.init = init;
	}

	public abstract Parser<T> build();
}
