package eu.k5.cell.definition;

import java.util.Map;

public class AssignerBound {

	private final PropertyAssigner assigner;

	private final Initializer initializer;

	public AssignerBound(Initializer initializer, PropertyAssigner assigner) {
		this.initializer = initializer;
		this.assigner = assigner;
	}

	public void assign(Object instance, Map<String, Object> variables) {
		assigner.assign(instance, initializer.init(variables));
	}

	public PropertyAssigner getAssigner() {
		return assigner;
	}

	public Initializer getInitializer() {
		return initializer;
	}

}
