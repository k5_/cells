package eu.k5.cell.model.data;

import eu.k5.cell.annotation.In;

public class DataConstructor {
	private final String key;

	private final String property;

	public DataConstructor(@In("key") String key, @In("property") String property) {
		this.key = key;
		this.property = property;
	}
	
	
}
