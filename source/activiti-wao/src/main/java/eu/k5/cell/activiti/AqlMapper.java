package eu.k5.cell.activiti;

import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

public interface AqlMapper {
	public static final String SQL = "rawSql";

	@Select("${" + SQL + "}")
	@ResultMap("taskWithVariables")
	List<TaskEntity> selectTasksWithVariables(Map<String, Object> parameter);

	@Select("${" + SQL + "}")
	@ResultMap("taskWithVariables")
	TaskEntity selectTaskWithVariables(Map<String, Object> parameter);

	@Select("${" + SQL + "}")
	@ResultMap("instanceWithVariables")
	List<ExecutionEntity> selectInstancesWithVariables(Map<String, Object> parameter);

	@Select("${" + SQL + "}")
	@ResultMap("instanceWithVariables")
	ExecutionEntity selectInstanceWithVariables(Map<String, Object> parameter);

}
