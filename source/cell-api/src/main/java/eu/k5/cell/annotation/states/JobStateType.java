package eu.k5.cell.annotation.states;

import java.util.Map;

public enum JobStateType {
	INSTANCE_ID(String.class), DERIVED(Object.class), PROCESS_DEFINITION_ID(String.class), VARIABLES(Map.class);

	private final Class<?> type;

	private JobStateType(Class<?> type) {
		this.type = type;
	}

	public Class<?> getType() {
		return type;
	}
}
