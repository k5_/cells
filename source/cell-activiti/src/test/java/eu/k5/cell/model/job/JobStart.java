package eu.k5.cell.model.job;

public class JobStart {
	private final String key;
	private final String property;
	private final String inOut;

	public JobStart(String key, String property, String inOut) {
		this.key = key;
		this.property = property;
		this.inOut = inOut;
	}

	public String getKey() {
		return key;
	}

	public String getProperty() {
		return property;
	}

	public String getInOut() {
		return inOut;
	}
}
