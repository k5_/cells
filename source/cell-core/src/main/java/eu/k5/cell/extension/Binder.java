package eu.k5.cell.extension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.collect.ImmutableMap;

import eu.k5.cell.activiti.VariableBound;
import eu.k5.cell.definition.JobStateReader;
import eu.k5.cell.definition.ProcessStateReader;
import eu.k5.cell.definition.TaskStateReader;

public class Binder {

	private final Map<String, Binding> bindings;

	private final Map<String, ProcessStateReader> processStates;

	private final Map<String, JobStateReader> jobStates;

	private final Map<String, TaskStateReader> taskStates;

	private Map<String, VariableBound> variables;

	public Binder(List<Binding> bindings, List<ProcessStateReader> processStates, List<JobStateReader> jobStates,
			List<TaskStateReader> taskStates) {
		{
			ImmutableMap.Builder<String, Binding> bindingBuilder = ImmutableMap.<String, Binding> builder();
			ImmutableMap.Builder<String, VariableBound> variableBuilder = ImmutableMap
					.<String, VariableBound> builder();

			for (Binding binding : bindings) {
				bindingBuilder.put(binding.getBind(), binding);
				variableBuilder.put(binding.getBind(), new VariableBound(binding.getBind(), binding.getHint()));
			}
			this.bindings = bindingBuilder.build();
			variables = variableBuilder.build();
		}
		{
			ImmutableMap.Builder<String, ProcessStateReader> processStateBuilder = ImmutableMap
					.<String, ProcessStateReader> builder();
			for (ProcessStateReader processState : processStates) {
				processStateBuilder.put(processState.getName(), processState);
			}
			this.processStates = processStateBuilder.build();
		}
		{
			ImmutableMap.Builder<String, JobStateReader> jobStateBuilder = ImmutableMap
					.<String, JobStateReader> builder();
			for (JobStateReader jobState : jobStates) {
				jobStateBuilder.put(jobState.getName(), jobState);
			}
			this.jobStates = jobStateBuilder.build();
		}
		{
			ImmutableMap.Builder<String, TaskStateReader> taskStateBuilder = ImmutableMap
					.<String, TaskStateReader> builder();
			for (TaskStateReader taskState : taskStates) {
				taskStateBuilder.put(taskState.getName(), taskState);
			}
			this.taskStates = taskStateBuilder.build();
		}
		{

		}

	}

	public Binding getBinding(String name) {
		return bindings.get(name);
	}

	public ProcessStateReader getProcessState(String name) {
		return processStates.get(name);
	}

	public TaskStateReader getTaskState(String name) {
		return taskStates.get(name);
	}

	public JobStateReader getJobState(String name) {
		return jobStates.get(name);
	}

	public Map<String, VariableBound> getVariables() {
		return variables;
	}

	public Map<String, VariableBound> getVariables(Set<String> names) {
		Map<String, VariableBound> bounds = new HashMap<String, VariableBound>();
		for (Entry<String, VariableBound> entry : variables.entrySet()) {
			if (names.contains(entry.getKey())) {
				bounds.put(entry.getKey(), entry.getValue());
			}
		}
		return bounds;
	}
}
