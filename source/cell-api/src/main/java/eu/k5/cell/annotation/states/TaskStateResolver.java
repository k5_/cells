package eu.k5.cell.annotation.states;

public interface TaskStateResolver {

	Class<?> getReturnType();

	Object read(Object state);

}
