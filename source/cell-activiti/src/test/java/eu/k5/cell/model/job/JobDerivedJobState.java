package eu.k5.cell.model.job;

import org.activiti.engine.delegate.DelegateExecution;
import org.junit.Assert;

import eu.k5.cell.annotation.states.JobStateResolver;

public class JobDerivedJobState implements JobStateResolver {

	@Override
	public Class<?> getReturnType() {
		return String.class;
	}

	@Override
	public Object read(Object state) {
		Assert.assertTrue(state instanceof DelegateExecution);
		return "derived";
	}

}
