package eu.k5.cell.definition;

import java.util.ArrayList;
import java.util.List;

import eu.k5.cell.CellProcessState;
import eu.k5.cell.extension.Reader;
import eu.k5.cell.extension.StateDefinition;

public class AspectDefinition {
	private final Class<?> type;
	private final Reader reader;
	private final List<NestedTaskDefinition> tasks;
	private final List<NestedDataDefinition> datas;
	private final List<StateDefinition<ProcessStateReader>> processStates;
	private final WorkflowDefinition worfklow;

	public AspectDefinition(WorkflowDefinition worfklow, Class<?> type, Reader reader, List<NestedTaskDefinition> tasks,
			List<NestedDataDefinition> datas, List<StateDefinition<ProcessStateReader>> processStates) {
		this.worfklow = worfklow;
		this.type = type;
		this.reader = reader;
		this.tasks = tasks;
		this.datas = datas;
		this.processStates = processStates;
	}

	public WorkflowDefinition getWorfklow() {
		return worfklow;
	}

	public Object read(CellProcessState state) {

		Object aspect = reader.read(state.getVariables());

		for (NestedTaskDefinition task : tasks) {

			List<Object> list = new ArrayList<>();
			for (TaskDefinition definition : task.getDefinition()) {
				list.addAll(definition.read(state));

			}
			if (task.isList()) {
				task.getAssigner().assign(aspect, list);
			} else {
				if (list.size() == 1) {
					task.getAssigner().assign(aspect, list.get(0));
				} else if (list.isEmpty()) {
					task.getAssigner().assign(aspect, null);
				} else if (list.size() > 1) {
					throw new IllegalStateException("multiple results");
				}
			}
		}
		for (NestedDataDefinition data : datas) {
			data.getAssigner().assign(aspect, data.getDefinition().read(state.getVariables()));
		}

		for (StateDefinition<ProcessStateReader> reader : processStates) {
			reader.getAssigner().assign(aspect, reader.getReader().read(state));
		}
		return aspect;

	}

	public Class<?> getType() {
		return type;
	}
}
