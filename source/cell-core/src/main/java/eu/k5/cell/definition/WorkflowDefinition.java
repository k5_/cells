package eu.k5.cell.definition;

import eu.k5.cell.extension.Binder;

public class WorkflowDefinition {
	private final Class<?> type;
	private boolean isComposite;
	private final String processDefinition;
	private final Binder binder;

	public WorkflowDefinition(Class<?> type, String processDefinition, Binder binder) {
		this.type = type;
		this.processDefinition = processDefinition;
		this.binder = binder;
	}

	public Class<?> getType() {
		return type;
	}

	public String getProcessDefinition() {
		return processDefinition;
	}

	public Binder getBinder() {
		return binder;
	}
}
