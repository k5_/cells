package eu.k5.cell.parser;

import java.lang.reflect.Type;

public class Bound {
	private final int index;
	private final Type type;

	public Bound(int index, Type type) {
		this.index = index;
		this.type = type;
	}

	public int getIndex() {
		return index;
	}

	public Type getType() {
		return type;
	}

}
