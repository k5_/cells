package eu.k5.cell.extension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.k5.cell.definition.PropertyReader;

public class Writer {

	private final List<PropertyReader> readers;

	public Writer(List<PropertyReader> readers) {
		this.readers = readers;
	}

	public Map<String, Object> write(Object object) {

		Map<String, Object> variables = new HashMap<>();
		for (PropertyReader b : readers) {
			variables.put(b.getBinding().getBind(), b.read(object));
		}
		return variables;
	}
}
