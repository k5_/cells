package eu.k5.cell.definition;

import java.util.Map;

public interface Initializer {

	Object init(Map<String, Object> variables);
}
