package eu.k5.cell.model.task;

import java.util.List;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.activiti.engine.runtime.ProcessInstance;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import eu.k5.cell.activiti.CellManager;
import eu.k5.cell.activiti.TestEngine;
import eu.k5.cell.activiti.filter.BusinessKeyFilter;
import eu.k5.cell.annotation.CellContext;
import eu.k5.cell.extension.CellExtension;
import eu.k5.cell.model.TestProducer;

@RunWith(CdiRunner.class)
@AdditionalClasses({ TestProducer.class, CellExtension.class, TaskWorkflow.class })
public class TaskTest {

	private static TestEngine refEngine = new TestEngine("task", "task");

	@CellContext(TaskWorkflow.class)
	@Inject
	CellManager cell;

	@Produces
	TestEngine engine = refEngine;

	@AfterClass
	public static void destoy() {
		refEngine.destroy();
	}

	@Test
	public void start() {

		cell.start(new TaskStart("key", "property", "in"));
		cell.start(new TaskStart("key2", "property", "in"));

		TaskTask task = cell.findTask(TaskTask.class, new BusinessKeyFilter("key"));

		Assert.assertNotNull(task);
		Assert.assertEquals("key", task.getKey());
		Assert.assertEquals("property", task.getProperty());
		Assert.assertEquals("assign", task.getAssignee());
		Assert.assertEquals("in", task.getInOut());
		Assert.assertEquals("derived", task.getDerived());
		Assert.assertEquals("property", task.getData().getProperty());

		List<TaskTask> tasks = cell.listTask(TaskTask.class);
		Assert.assertEquals(2, tasks.size());

		task.setProperty3("prop3");
		task.setProperty("willNotUpdate");
		task.setInOut("out");
		cell.complete(task);

		TaskTask taskReload = cell.findTask(TaskTask.class, new BusinessKeyFilter("key"));
		Assert.assertNull(taskReload);

		ProcessInstance instance = engine.find("key");
		Assert.assertNotNull(instance);
		Assert.assertEquals("prop3", instance.getProcessVariables().get("property3"));
		Assert.assertEquals("property", instance.getProcessVariables().get("property"));
		Assert.assertEquals("defaultValue", instance.getProcessVariables().get("defaultOut"));
		Assert.assertEquals("out", instance.getProcessVariables().get("inOut"));
	}

}
