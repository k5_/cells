package eu.k5.cell.parser;

import java.lang.reflect.Type;

public interface SourceReference {
	Type getType();

	Source getSource();
}
