package eu.k5.cell.parser;

import java.util.Map;

import eu.k5.cell.shared.CellTaskReference;

public class VariableContext implements Context {

	private final Map<String, Object> variables;

	public VariableContext(Map<String, Object> variables) {
		this.variables = variables;
	}

	@Override
	public Object getVariable(String name) {
		return variables.get(name);
	}

	@Override
	public CellTaskReference getTaskReference() {
		return null;
	}

}
