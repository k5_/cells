package eu.k5.cell.definition;

import java.util.List;

public class NestedTaskDefinition {
	private final PropertyAssigner assigner;
	private final List<TaskDefinition> definition;
	private final boolean list;

	public NestedTaskDefinition(PropertyAssigner assigner, List<TaskDefinition> definition, boolean list) {

		this.assigner = assigner;
		this.definition = definition;
		this.list = list;
	}

	public PropertyAssigner getAssigner() {
		return assigner;
	}

	public List<TaskDefinition> getDefinition() {
		return definition;
	}

	public boolean isList() {
		return list;
	}

}
