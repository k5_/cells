package eu.k5.cell.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.k5.cell.annotation.AccessObject;
import eu.k5.cell.annotation.Aspect;
import eu.k5.cell.annotation.workflow.Data;
import eu.k5.cell.annotation.workflow.Job;
import eu.k5.cell.annotation.workflow.Servant;
import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Task;
import eu.k5.cell.annotation.workflow.Workflow;
import eu.k5.cell.definition.AspectDefinition;
import eu.k5.cell.definition.DataDefinition;
import eu.k5.cell.definition.JobDefinition;
import eu.k5.cell.definition.StartDefinition;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.definition.WorkflowDefinition;
import eu.k5.cell.wao.WaoDefinition;

public class CellBuilder {
	private Class<?> type;
	private String processDefinition;

	private final Validator validator = new Validator();

	// private final Map<String, Binding> bindings = new HashMap<>();

	private Binder binder;
	private final List<DataDefinition> datas = new ArrayList<>();
	private final List<StartDefinition> starts = new ArrayList<>();
	private final List<TaskDefinition> tasks = new ArrayList<>();
	private final List<AspectDefinition> aspects = new ArrayList<>();
	private final List<JobDefinition> jobs = new ArrayList<>();
	private final List<Type> required = new ArrayList<>();
	private final Map<Class<?>, WaoDefinition> delegates = new HashMap<>();

	private boolean init = false;
	private int violations = 0;

	private WorkflowDefinition workflow;

	public CellBuilder with(Class<?> type) {

		this.type = type;
		return this;
	}

	public Class<?> getType() {
		return type;
	}

	public CellBuilder init() {
		if (isInit() || violations > 0) {
			return this;
		}
		if (!type.isAnnotationPresent(Workflow.class)) {
			validator.add(type, "Workflow type is missing workflow annotation");
			return this;
		}
		Workflow workflow = type.getAnnotation(Workflow.class);

		processDefinition = workflow.processKey();

		binder = new BinderBuilder().withValidator(validator).addClass(workflow.binder()).init().build();

		initWorkflowDefinition();

		initActivities(workflow.activity());

		for (Servant servant : workflow.servants()) {
			initServant(servant);
		}

		for (AccessObject ao : workflow.delegates()) {
			initAccessObject(ao);
		}

		if (violations == 0) {
			init = true;
		}
		return this;
	}

	private void initActivities(Class<?> activity) {
		Method[] methods = activity.getMethods();

		// Resolve in correct order
		for (Method method : getWithAnnotation(Data.class, methods)) {
			initData(method);
		}

		for (Method method : getWithAnnotation(Start.class, methods)) {
			initStart(method);
		}

		for (Method method : getWithAnnotation(Task.class, methods)) {
			initTask(method);
		}

		for (Method method : getWithAnnotation(Aspect.class, methods)) {
			initAspects(method);
		}
	}

	private void initWorkflowDefinition() {
		workflow = new WorkflowDefinition(type, processDefinition, binder);
	}

	private void initData(Method data) {
		DataBuilder reader = new DataBuilder();
		reader.useAll(true).withBinder(binder).withValidator(validator).forEntity(data.getReturnType()).init();
		violations += reader.getViolations();
		if (reader.isInit()) {
			datas.add(reader.build());
		} else {
			datas.add(null);
		}
	}

	private void initStart(Method start) {
		StartBuilder writer = new StartBuilder();
		writer.useAll(true).withBinder(binder).withValidator(validator).forWorkflow(workflow)
				.forEntity(start.getReturnType()).init();
		violations += writer.getViolations();
		if (writer.isInit()) {
			starts.add(writer.build());
		} else {
			starts.add(null);
		}

	}

	private void initTask(Method task) {

		Task t = task.getAnnotation(Task.class);

		TaskBuilder builder = new TaskBuilder();
		builder.withBinder(binder).withValidator(validator).forWorkflow(workflow).forEntity(task.getReturnType())
				.withDefinitionKey(t.definition()).withDatas(datas).init();
		violations += builder.getViolations();
		if (builder.isInit()) {
			tasks.add(builder.build());
		} else {
			tasks.add(null);
		}

	}

	private void initServant(Servant servant) {
		required.add(servant.value());
		Class<?> type = servant.value();
		Method[] methods = type.getMethods();

		for (Method m : methods) {
			Class<?>[] parameterTypes = m.getParameterTypes();
			if (parameterTypes.length != 1) {
				validator.add(type, "Invalid parameter count for servant method");
				break;
			}

			Job job = m.getAnnotation(Job.class);
			if (job == null) {
				validator.add(type, "Missing job annotation on servant method");
				break;
			}

			Class<?> jobType = parameterTypes[0];

			JobBuilder builder = new JobBuilder();
			builder.withDefinition(job.definition()).withBinder(binder).withValidator(validator).forEntity(jobType)
					.withServantType(type).withServantMethod(m).withDatas(datas).init();

			violations += builder.getViolations();
			if (builder.isInit()) {
				jobs.add(builder.build());
			} else {
				jobs.add(null);
			}

		}
	}

	private void initAccessObject(AccessObject ao) {
		Class<?> type = ao.value();

		DelegateBuilder builder = new DelegateBuilder();
		builder.withBinder(binder).withValidator(validator).forEntity(type).withTasks(tasks).withAspects(aspects);

		builder.init();

		if (builder.isInit()) {
			delegates.put(type, builder.build());
		} else {
			aspects.add(null);
		}

	}

	private void initAspects(Method method) {
		AspectBuilder builder = new AspectBuilder();
		builder.withBinder(binder).withTasks(tasks).withDatas(datas).withValidator(validator)
				.forEntity(method.getReturnType()).init();
		violations += builder.getViolations();
		if (builder.isInit()) {
			aspects.add(builder.build());
		} else {
			aspects.add(null);
		}

	}

	private List<Method> getWithAnnotation(Class<? extends Annotation> annotation, Method[] methods) {
		List<Method> result = new ArrayList<>();
		for (Method m : methods) {
			if (m.isAnnotationPresent(annotation)) {
				result.add(m);
			}
		}
		return result;
	}

	public Cell build() {

		Cell cell = new Cell(workflow, starts, datas, tasks, aspects, jobs, delegates);
		return cell;

	}

	public Validator getValidator() {
		return validator;
	}

	public boolean isInit() {
		return init;
	}

	public Collection<? extends Type> getRequiredBeans() {
		return required;
	}
}
