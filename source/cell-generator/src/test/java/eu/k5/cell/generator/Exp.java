package eu.k5.cell.generator;

import static org.jboss.jdeparser.JExprs.$v;
import static org.jboss.jdeparser.JMod.FINAL;
import static org.jboss.jdeparser.JMod.PUBLIC;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.jboss.jdeparser.FormatPreferences;
import org.jboss.jdeparser.JBlock;
import org.jboss.jdeparser.JClassDef;
import org.jboss.jdeparser.JDeparser;
import org.jboss.jdeparser.JExprs;
import org.jboss.jdeparser.JFiler;
import org.jboss.jdeparser.JMethodDef;
import org.jboss.jdeparser.JParamDeclaration;
import org.jboss.jdeparser.JSourceFile;
import org.jboss.jdeparser.JSources;
import org.jboss.jdeparser.JTypes;
import org.jboss.jdeparser.JVarDeclaration;

import eu.k5.cell.definition.AssignerBound;
import eu.k5.cell.definition.Initializer;
import eu.k5.cell.definition.PropertyAssigner;
import eu.k5.cell.definition.PropertyAssignerField;
import eu.k5.cell.definition.TaskReader;
import eu.k5.cell.extension.Reader;
import eu.k5.cell.extension.ReaderBuilder.BindingInitializer;
import eu.k5.cell.extension.TaskBuilder;
import eu.k5.cell.shared.CellTaskReference;

public class Exp {

	private final Map<Initializer, JVarDeclaration> initialized = new IdentityHashMap<>();

	public static void main(String[] args) throws IOException {
		TaskBuilder builder = new TaskBuilder();
		new Exp().gen();
	}

	public void gen() throws IOException {
		{
			JSources sources = JDeparser.createSources(getFiler(), new FormatPreferences(new Properties()));

			JSourceFile source = sources.createSourceFile("eu.k5.cell", "TestDefinition");

			JClassDef reader = source._class(PUBLIC | FINAL, "TestDefinition");

			reader._implements(TaskReader.class);

			JMethodDef read = reader.method(PUBLIC, Object.class, "read");

			JParamDeclaration task = read.param(JTypes.$t(org.activiti.engine.task.Task.class), "task");
			JParamDeclaration param = read.param(JTypes.$t(Map.class).typeArg(String.class, Object.class), "variables");

			JVarDeclaration reference = genTaskReference(read.body(), task);

			JVarDeclaration result = read.body().var(0, Object.class, "result");
			read.body().add($v(result.name()).call("setValue")
					.arg($v(param.name()).call("get").arg(JExprs.str("test")).cast(Integer.class)));

			sources.writeSources();
		}
		{
			final ByteArrayInputStream inputStream = openFile("eu.k5.cell", "TestDefinition.java");
			final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		}
	}

	public void gen(JBlock block, JParamDeclaration variables, Reader reader) {
		List<AssignerBound> assigners = reader.getAssigners();
		for (AssignerBound bound : assigners) {
			Initializer initializer = bound.getInitializer();
			if (initializer instanceof BindingInitializer) {
				genInitializer(block, variables, (BindingInitializer) initializer, bound.getAssigner().getType());
			}
		}
	}

	public JVarDeclaration genTaskReference(JBlock block, JParamDeclaration task) {
		return block.var(0, CellTaskReference.class, "reference",
				JTypes.$t(CellTaskReference.class)._new().arg(JExprs.$v(task).call("getProcessInstanceId"))
						.arg(JExprs.$v(task).call("getId")).arg(JExprs.$v(task).call("getTaskDefinitionKey"))
						.arg(JExprs.$v(task).call("getToken")));

	}

	private void genInitializer(JBlock block, JParamDeclaration variables, BindingInitializer init, Class<?> type) {
		JVarDeclaration var = block.var(0, type, "var1");
		block.add($v(var).assign($v(variables).call("get").arg(JExprs.str(init.getBinding()).cast(type))));
		initialized.put(init, var);
	}

	private void genAssigner(JBlock block, JVarDeclaration result, Initializer initializer,
			PropertyAssignerField field) {
		JVarDeclaration var = initialized.get(initializer);
		block.add($v(result.name()).field(field.getFieldName()).assign($v(var.name())));
	}

	private void assigner(PropertyAssigner assigner) {

	}

	private final JFiler filer = new JFiler() {
		@Override
		public OutputStream openStream(final String packageName, final String fileName) throws IOException {
			final Key key = new Key(packageName, fileName + ".java");
			if (!sourceFiles.containsKey(key)) {
				final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				if (sourceFiles.putIfAbsent(key, stream) == null) {
					return stream;
				}
			}
			throw new IOException("Already exists");
		}
	};

	public JFiler getFiler() {
		return filer;
	}

	private final ConcurrentMap<Key, ByteArrayOutputStream> sourceFiles = new ConcurrentHashMap<>();

	static final class Key {
		private final String packageName;
		private final String fileName;

		Key(final String packageName, final String fileName) {
			this.packageName = packageName;
			this.fileName = fileName;
		}

		@Override
		public boolean equals(final Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			final Key key = (Key) o;
			return fileName.equals(key.fileName) && packageName.equals(key.packageName);
		}

		@Override
		public int hashCode() {
			int result = packageName.hashCode();
			result = 31 * result + fileName.hashCode();
			return result;
		}
	}

	public ByteArrayInputStream openFile(String packageName, String fileName) throws FileNotFoundException {
		final ByteArrayOutputStream out = sourceFiles.get(new Key(packageName, fileName));
		if (out == null) {
			throw new FileNotFoundException("No file found for package " + packageName + " file " + fileName);
		}
		return new ByteArrayInputStream(out.toByteArray());
	}

}
