package eu.k5.cell.activiti;

public class VariableBound {
	private final String bound;
	private final TypeHint hint;

	public VariableBound(String bound, TypeHint hint) {
		this.bound = bound;
		this.hint = hint;
	}

	public String getBound() {
		return bound;
	}

	public TypeHint getHint() {
		return hint;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (bound == null ? 0 : bound.hashCode());
		result = prime * result + (hint == null ? 0 : hint.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VariableBound other = (VariableBound) obj;
		if (bound == null) {
			if (other.bound != null) {
				return false;
			}
		} else if (!bound.equals(other.bound)) {
			return false;
		}
		if (hint != other.hint) {
			return false;
		}
		return true;
	}

}
