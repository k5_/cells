package eu.k5.cell.parser;

import eu.k5.cell.shared.CellTaskReference;

public interface Context {

	Object getVariable(String name);

	CellTaskReference getTaskReference();

}