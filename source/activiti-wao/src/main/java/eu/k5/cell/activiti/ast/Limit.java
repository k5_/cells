package eu.k5.cell.activiti.ast;

public class Limit {
	private final Value offset;
	private final Value length;

	public Limit(Value offset, Value length) {
		this.offset = offset;
		this.length = length;
	}

	public Value getOffset() {
		return offset;
	}

	public Value getLength() {
		return length;
	}

	public void accept(QueryVisitor visitor) {
		visitor.visit(this);
	}
}
