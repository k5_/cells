package eu.k5.cell;

public class CellException extends RuntimeException {
	private static final long serialVersionUID = 401244198129610312L;

	public CellException(String message, Throwable cause) {
		super(message, cause);
	}

	public CellException(String message) {
		super(message);
	}

	public CellException(Throwable cause) {
		super(cause);
	}

}
