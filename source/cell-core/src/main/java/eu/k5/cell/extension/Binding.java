package eu.k5.cell.extension;

import java.lang.reflect.Type;

import eu.k5.cell.activiti.TypeHint;

public class Binding {

	private final String bind;

	private final Type type;

	private final boolean isFinal;

	private final boolean businessKey;

	public Binding(boolean businessKey, boolean isFinal, String bind, Type type) {
		this.businessKey = businessKey;
		this.isFinal = isFinal;
		this.bind = bind;
		this.type = type;
	}

	public String getBind() {
		return bind;
	}

	public Type getType() {
		return type;
	}

	public boolean isBusinessKey() {
		return businessKey;
	}

	public boolean isFinal() {
		return isFinal;
	}

	@Override
	public String toString() {
		return "Binding [bind=" + bind + ", type=" + type + "]";
	}

	public TypeHint getHint() {
		return TypeHint.forType(type);
	}

}
