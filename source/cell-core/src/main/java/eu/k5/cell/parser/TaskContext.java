package eu.k5.cell.parser;

import java.util.Map;

import eu.k5.cell.CellTask;
import eu.k5.cell.shared.CellTaskReference;

public class TaskContext implements Context {

	private final Map<String, Object> variables;

	private final Map<String, Object> taskVariables;

	private final CellTask task;

	public TaskContext(Map<String, Object> variables, Map<String, Object> taskVariables, CellTask task) {
		this.variables = variables;
		this.taskVariables = taskVariables;
		this.task = task;
	}

	@Override
	public Object getVariable(String name) {
		return variables.get(name);
	}

	public Object getTaskVariable(String name) {
		return taskVariables.get(name);
	}

	@Override
	public CellTaskReference getTaskReference() {
		return new CellTaskReference(task.getProcessInstanceId(), task.getId(), task.getTaskDefinitionKey(),
				(String) taskVariables.get("token"));
	}

}
