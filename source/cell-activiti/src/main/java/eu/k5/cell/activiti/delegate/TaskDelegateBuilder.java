package eu.k5.cell.activiti.delegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.k5.cell.activiti.ParameterBound;
import eu.k5.cell.activiti.ParameterLiteralBound;
import eu.k5.cell.activiti.TypeHint;
import eu.k5.cell.activiti.VariableBound;
import eu.k5.cell.activiti.ast.Context;
import eu.k5.cell.activiti.builder.QueryDelegateBuilder;
import eu.k5.cell.definition.TaskDefinition;
import eu.k5.cell.extension.ParameterBuilder;

public class TaskDelegateBuilder extends AbstractDelegateBuilder<TaskDelegateBuilder, TaskDelegateDefinition> {

	private List<TaskDefinition> tasks;
	private ListTaskDefinition query;

	public TaskDelegateBuilder() {
		initBuilder(this);
	}

	public TaskDelegateBuilder withTasks(List<TaskDefinition> tasks) {
		this.tasks = new ArrayList<>(tasks);
		return this;
	}

	public void init() {

		eu.k5.cell.activiti.builder.ParameterBuilder parameters = new ParameterBuilder().withViolations(
				getViolations().sub()).forMethod(method);

		QueryDelegateBuilder<ListTaskDefinition> builder = QueryDelegateBuilder.listTasks();
		builder.withViolations(getViolations().sub()).withParameters(parameters.create()).withQueryString(queryString);

		builder.withContext(createContext(parameters.getBounds(), builder.getRequiredVariables()));

		query = builder.build();
	}

	private List<Context> createContext(Map<String, ParameterBound> parameters, Set<String> variables) {
		List<Context> context = new ArrayList<>();
		for (TaskDefinition definition : tasks) {
			Map<String, ParameterBound> local = new HashMap<>(parameters);
			local.put("taskDefinitionKey",
					new ParameterLiteralBound(TypeHint.STRING, definition.getTaskDefinitionKey()));
			local.put("processDefinitionKey", new ParameterLiteralBound(TypeHint.STRING, definition.getWorfklow()
					.getProcessDefinition()));

			Map<String, VariableBound> bounds = definition.getWorfklow().getBinder().getVariables(variables);
			context.add(new Context(bounds, local));

		}
		return context;
	}

	@Override
	public TaskDelegateDefinition build() {
		init();
		return new TaskDelegateDefinition(query, tasks, isList);
	}
}
