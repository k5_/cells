package eu.k5.cell.annotation.states;

public interface JobStateResolver {

	Class<?> getReturnType();

	Object read(Object state);

}
