package eu.k5.cell.activiti.filter;

import java.util.Arrays;
import java.util.List;

import org.activiti.engine.runtime.ProcessInstanceQuery;
import org.activiti.engine.task.TaskQuery;

import eu.k5.cell.activiti.ProcessState;

public class ChainFilter {
	private static class ChainTaskFilter implements TaskFilter {

		private final List<TaskFilter> filters;

		public ChainTaskFilter(List<TaskFilter> filters) {
			this.filters = filters;
		}

		@Override
		public TaskQuery filter(TaskQuery query) {
			for (TaskFilter filter : filters) {
				filter.filter(query);
			}
			return query;
		}

	}

	private static class ChainAspectFilter implements AspectFilter {

		private final List<AspectFilter> filters;

		public ChainAspectFilter(List<AspectFilter> filters) {
			this.filters = filters;
		}

		@Override
		public AspectQuery filter(AspectQuery query) {
			for (AspectFilter filter : filters) {
				filter.filter(query);
			}
			return query;
		}

		@Override
		public boolean accept(ProcessState state) {
			for (AspectFilter filter : filters) {
				if (!filter.accept(state)) {
					return false;
				}
			}
			return true;
		}

	}

	private static class ChainProcessFilter implements ProcessFilter {

		private final List<ProcessFilter> filters;

		public ChainProcessFilter(List<ProcessFilter> filters) {
			this.filters = filters;
		}

		@Override
		public ProcessInstanceQuery filter(ProcessInstanceQuery query) {
			for (ProcessFilter filter : filters) {
				filter.filter(query);
			}
			return query;
		}

	}

	private static final NullFilter NULL = new NullFilter();

	public static TaskFilter chain(TaskFilter... taskFilters) {
		return chainTasks(Arrays.asList(taskFilters));
	}

	public static TaskFilter chainTasks(List<TaskFilter> taskFilters) {
		if (taskFilters.size() == 1) {
			return taskFilters.get(0);
		} else if (taskFilters.size() == 0) {
			return NULL;
		} else {
			return new ChainTaskFilter(taskFilters);
		}
	}

	public static AspectFilter chain(AspectFilter... aspectFilters) {

		return chainAspects(Arrays.asList(aspectFilters));
	}

	public static AspectFilter chainAspects(List<AspectFilter> aspectFilters) {
		if (aspectFilters.size() == 1) {
			return aspectFilters.get(0);
		} else if (aspectFilters.isEmpty()) {
			return NULL;
		} else {
			return new ChainAspectFilter(aspectFilters);
		}
	}

	public static ProcessFilter chain(ProcessFilter... filters) {
		return chainProcess(Arrays.asList(filters));

	}

	public static ProcessFilter chainProcess(List<ProcessFilter> filters) {
		if (filters.size() == 1) {
			return filters.get(0);
		} else if (filters.isEmpty()) {
			return NULL;
		} else {
			return new ChainProcessFilter(filters);
		}

	}

}
