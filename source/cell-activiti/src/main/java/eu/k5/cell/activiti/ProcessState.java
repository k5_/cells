package eu.k5.cell.activiti;

import java.util.Collection;
import java.util.Map;

import org.activiti.engine.runtime.ProcessInstance;

import eu.k5.cell.CellProcessState;
import eu.k5.cell.CellTask;

public class ProcessState implements CellProcessState {

	private final ProcessInstance processInstance;
	private final Map<String, Object> variables;
	private final Collection<CellTask> tasks;

	public ProcessState(ProcessInstance processInstance, Map<String, Object> variables, Collection<CellTask> tasks) {
		this.processInstance = processInstance;
		this.variables = variables;
		this.tasks = tasks;
	}

	@Override
	public Map<String, Object> getVariables() {
		return variables;
	}

	@Override
	public Collection<CellTask> getTasks() {
		return tasks;
	}

	public ProcessInstance getProcessInstance() {
		return processInstance;
	}

}
