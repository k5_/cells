package eu.k5.cell.parser;

import java.lang.reflect.Type;

public class VariableSource implements Source {
	private final String name;
	private final Type type;

	public VariableSource(Type type, String name) {
		this.type = type;
		this.name = name;
	}

	@Override
	public Object read(Context context) {
		return context.getVariable(name);
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	@Override
	public <R, P> R accept(SourceVisitor<R, P> visitor, P param) {
		return visitor.visit(this, param);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (name == null ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VariableSource other = (VariableSource) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

}
