package eu.k5.cell.extension;

import java.lang.annotation.Annotation;

import eu.k5.cell.annotation.Param;

public class ParameterBuilder extends eu.k5.cell.activiti.builder.ParameterBuilder {

	@Override
	protected String getParameterAnnotation(Annotation[] annotations) {
		for (Annotation a : annotations) {
			if (a instanceof Param) {
				return ((Param) a).value();
			}
		}
		return null;
	}
}
