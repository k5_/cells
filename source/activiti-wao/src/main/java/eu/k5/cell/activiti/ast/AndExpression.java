package eu.k5.cell.activiti.ast;

import java.util.List;

class AndExpression extends NaryExpression{


	public AndExpression(List<Expression> parts) {
		super(parts);
	}

	@Override
	public void accept(QueryVisitor visitor) {
		visitor.visit(this);
	}



}
