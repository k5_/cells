package eu.k5.cell.extension;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.helpers.MessageFormatter;

class Validator {
	enum Type {

		REFERENCES_INVALID_CELL("Composite Cell {} references invalid Cell {}"), REFERENCES_UNKOWN_CELL(
				"Composite Cell {} references unkown Cell {}"), MULTIPLE_START_MAPPINGS("");
		private final String message;

		Type(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}

	static class Violation {
		private final Class<?> location;
		private final Type type;
		private final String message;

		public Violation(Class<?> location, Type type, String message) {
			this.location = location;
			this.type = type;
			this.message = message;
		}

		public Class<?> getLocation() {
			return location;
		}

		public String getMessage() {
			return message;
		}

		public Type getType() {
			return type;
		}

		@Override
		public String toString() {
			return "Violation [location=" + location + ", message=" + message + "]";
		}

	}

	private final List<Violation> violations = new ArrayList<>();

	public void add(Class<?> location, String message, Object... arguments) {
		violations.add(new Violation(location, null, MessageFormatter.format(message, arguments).getMessage()));
	}

	public void add(Class<?> location, Type type, Object... arguments) {
		violations
				.add(new Violation(location, type, MessageFormatter.format(type.getMessage(), arguments).getMessage()));

	}

	public boolean isEmpty() {
		return violations.isEmpty();
	}

	public List<Violation> getViolations() {
		return violations;
	}

	public boolean contains(Type type) {
		for (Violation violation : violations) {
			if (type.equals(violation.getType())) {
				return true;
			}
		}
		return false;
	}

	public void raise() {
		throw new IllegalArgumentException(violations.toString());
	}
}
