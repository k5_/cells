package eu.k5.cell.model.data;

import eu.k5.cell.annotation.In;

public class DataData {
	static final String IGNORE = "ignore";

	@In
	private String key;

	private String property;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

}
