package eu.k5.cell.model.task;

import eu.k5.cell.annotation.CellReference;
import eu.k5.cell.annotation.Unbound;
import eu.k5.cell.shared.CellTaskReference;

public class TaskUnbound {

	@CellReference
	private CellTaskReference reference;

	@Unbound
	private String unbound;
}
