package eu.k5.cell.model.data;

import eu.k5.cell.annotation.Bind;
import eu.k5.cell.annotation.workflow.Data;
import eu.k5.cell.annotation.workflow.Start;
import eu.k5.cell.annotation.workflow.Workflow;

@Workflow(processKey = "dataProcess", binder = DataWorkflow.Binder.class, activity = DataWorkflow.Activity.class)
public interface DataWorkflow {

	interface Binder {

		void setKey(String key);

		@Bind(businessKey = true)
		String getKey();

		void setProperty(String key);

		@Bind
		String getProperty();

	}

	interface Activity {

		@Start
		DataStart getStart();

		@Data
		DataData getData();

		@Data
		DataConstructor getDataConstructor();
	
	}

}
