package eu.k5.cell;

import java.util.Map;

public interface CellDelegateExecution {

	Map<String, Object> getVariables();

}
