package eu.k5.cell.entity;

import eu.k5.cell.parser.Context;

public interface ReadableEntity<T> {

	Class<T> getType();

	T read(Context context);
}
