package eu.k5.cell.model.delegate;

public class DelegateStart {

	private final String property;
	private final String key;
	private final String assign;
	private final String property2;

	public DelegateStart(String key, String property, String assign, String property2) {

		this.key = key;
		this.property = property;
		this.assign = assign;
		this.property2 = property2;
	}

	public String getProperty() {
		return property;
	}

	public String getKey() {
		return key;
	}

	public String getProperty2() {
		return property2;
	}

	public String getAssign() {
		return assign;
	}
}
