package eu.k5.cell.activiti;

import java.util.Arrays;
import java.util.List;

import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Assert;
import org.junit.Test;

import eu.k5.cell.activiti.annotation.Param;
import eu.k5.cell.activiti.annotation.Query;

public class Wao_listInstances_Test extends AbstractExample {

	public interface Variables {
		@VariableHint
		String stringVar();

		@VariableHint
		String stringVar2();

		@VariableHint
		Long longVar();

	}

	public interface ListAll extends Variables {

		@Query("")
		List<ProcessInstance> list();

	}

	@Test
	public void listAll_oneavailable() {
		start(1);
		Assert.assertEquals(1, getWao(ListAll.class).list().size());
	}

	@Test
	public void listAll_twoavailable() {
		start(1);
		start(2);
		Assert.assertEquals(2, getWao(ListAll.class).list().size());
	}

	public interface ListByProcessDefinition extends Variables {

		@Query("processDefinitionKey = :key")
		List<ProcessInstance> listByProcessDefinitionKey(@Param("key") String key);

		@Query("(processDefinitionKey = :key1 AND var.stringVar='1') OR (processDefinitionKey = :key2 AND var.stringVar='2')")
		List<ProcessInstance> listByProcessDefinitionOr(@Param("key1") String key1, @Param("key2") String key2);

		@Query("processDefinitionKey IN :definitions")
		List<ProcessInstance> listInProcesDefinition(@Param("definitions") List<String> definitions);
	}

	@Test
	public void listByDefinition_oneMatch() {
		start(1);
		start(2);
		Assert.assertEquals(1, getWao(ListByProcessDefinition.class).listByProcessDefinitionKey("Process1").size());
	}

	@Test
	public void listByDefinition_bothMatch() {

		start(1, "stringVar", "1");
		start(2, "stringVar", "2");
		Assert.assertEquals(2, getWao(ListByProcessDefinition.class).listByProcessDefinitionOr("Process1", "Process2")
				.size());

	}

	@Test
	public void listByDefinitionOr_oneMatch() {

		start(1, "stringVar", "1");
		start(2, "stringVar", "1");
		Assert.assertEquals(1, getWao(ListByProcessDefinition.class).listByProcessDefinitionOr("Process1", "Process2")
				.size());
	}

	@Test
	public void listByDefinition_noMatch() {

		start(1, "stringVar", "2");
		start(2, "stringVar", "1");
		Assert.assertEquals(0, getWao(ListByProcessDefinition.class).listByProcessDefinitionOr("Process1", "Process2")
				.size());
	}

	@Test
	public void listInDefinition_bothMatch() {
		start(1);
		start(2);

		Assert.assertEquals(2,
				getWao(ListByProcessDefinition.class).listInProcesDefinition(Arrays.asList("Process1", "Process2"))
						.size());
	}

	@Test
	public void listInDefinition_oneMatch() {
		start(1);
		start(2);

		Assert.assertEquals(1, getWao(ListByProcessDefinition.class).listInProcesDefinition(Arrays.asList("Process1"))
				.size());
	}

	@Test
	public void listInDefinition_encoding() {
		start(1);
		start(2);

		Assert.assertEquals(0, getWao(ListByProcessDefinition.class).listInProcesDefinition(Arrays.asList("Pr'ocess1"))
				.size());
	}

}
