package eu.k5.cell.activiti.filter;

import org.activiti.engine.runtime.ProcessInstanceQuery;

import eu.k5.cell.activiti.ProcessState;

public class SuperProcessInstanceFilter implements AspectFilter, ProcessFilter {
	private final String processInstanceId;

	public SuperProcessInstanceFilter(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	@Override
	public ProcessInstanceQuery filter(ProcessInstanceQuery query) {
		return query.superProcessInstanceId(processInstanceId);
	}

	@Override
	public AspectQuery filter(AspectQuery query) {
		return query.superProcessInstanceId(processInstanceId);
	}

	@Override
	public boolean accept(ProcessState state) {
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (processInstanceId == null ? 0 : processInstanceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SuperProcessInstanceFilter other = (SuperProcessInstanceFilter) obj;
		if (processInstanceId == null) {
			if (other.processInstanceId != null) {
				return false;
			}
		} else if (!processInstanceId.equals(other.processInstanceId)) {
			return false;
		}
		return true;
	}

}
