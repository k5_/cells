package eu.k5.cell.activiti;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.ManagementService;
import org.junit.After;
import org.junit.Before;

public abstract class AbstractExample {
	private Awi aql;

	private TestEngine engine;

	private ManagementService service;

	@Before
	public final void baseInit() {
		engine = TestEngine.mssql("test", "Test");
		engine.reset();

		service = engine.getEngine().getManagementService();
		aql = new Awi(service);

	}

	public TestEngine getEngine() {
		return engine;
	}

	public ManagementService getService() {
		return service;
	}

	public Awi getAql() {
		return aql;
	}

	public <W> W getWao(Class<W> type) {
		return Awi.bind(type, service);
	}

	@After
	public void destroy() {
		engine.destroy();
	}

	protected AbstractExample start(int process, Object... parameters) {
		Map<String, Object> variables = new HashMap<String, Object>();
		for (int index = 0; index < parameters.length; index += 2) {
			variables.put(parameters[index].toString(), parameters[index + 1]);
		}
		engine.getEngine().getRuntimeService().startProcessInstanceByKey("Process" + process, variables);
		return this;
	}
}
