package eu.k5.cell.extension;

import eu.k5.cell.annotation.states.TaskStateResolver;

public class BindDerivedTaskState implements TaskStateResolver {

	@Override
	public Class<?> getReturnType() {
		return String.class;
	}

	@Override
	public Object read(Object state) {
		return "derived";
	}

}
