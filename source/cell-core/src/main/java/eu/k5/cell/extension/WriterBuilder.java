package eu.k5.cell.extension;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import eu.k5.cell.annotation.Out;
import eu.k5.cell.definition.PropertyReader;
import eu.k5.cell.definition.PropertyReaderField;

public class WriterBuilder extends Builder<Writer, WriterBuilder> {

	private final List<PropertyReader> readers = new ArrayList<>();
	private boolean allowFinals;

	public WriterBuilder() {
		super(Writer.class);
		setBuilder(this);
	}

	@Override
	protected void doInit() {
		Class<?> current = getType();

		while (current != null) {
			Field[] fields = current.getDeclaredFields();
			for (Field field : fields) {
				if (Modifier.isStatic(field.getModifiers())) {
					continue;
				}

				if (isUseAll() || field.isAnnotationPresent(Out.class)) {

					Out out = field.getAnnotation(Out.class);
					String bind = field.getName();
					if (out != null && !out.value().equals(Out.DEFAULT)) {
						bind = out.value();
					}

					Binding binding = getBinder().getBinding(bind);
					if (binding == null) {
						addViolation("Missing binding '" + bind + "' for field: " + current.toString() + "::"
								+ field.getName());
					} else if (!binding.getType().equals(field.getType())) {
						addViolation("Mismatched types for field:" + field.getName() + " " + field.getType() + " "
								+ binding.getType());
					} else {
						if (binding.isFinal() && !allowFinals) {
							addViolation("Disallowed update on final field: " + binding.getBind());
						} else {
							field.setAccessible(true);
							readers.add(new PropertyReaderField(binding, field));
						}
					}
				}
			}
			current = current.getSuperclass();
		}
	}

	public PropertyReader getBusinessKeyReader() {
		for (PropertyReader reader : readers) {
			if (reader.getBinding().isBusinessKey()) {
				return reader;
			}
		}
		return null;
	}

	public WriterBuilder allowFinals(boolean state) {
		allowFinals = state;
		return this;
	}

	@Override
	public Writer doBuild() {
		return new Writer(readers);
	}
}
