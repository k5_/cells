package eu.k5.cell.parser;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import eu.k5.cell.parser.Instantiator.FieldAssigner;
import eu.k5.cell.parser.Instantiator.PropertyAssigner;

public class InstantiatorBuilder<T> {
	private Class<T> type;
	private int[] constructorIndex;

	private List<FieldAssigner> fields = new ArrayList<>();
	private List<PropertyAssigner> properties = new ArrayList<>();
	private Constructor<T> constructor;



	public InstantiatorBuilder(Class<T> type) {
		this.type = type;
	}

	public InstantiatorBuilder<T> withConstructorIndex(int[] constructorIndex, Constructor<T> constructor) {
		this.constructorIndex = constructorIndex;
		this.constructor = constructor;
		return this;
	}

	public InstantiatorBuilder<T> addField(int index, Field field) {
		fields.add(new FieldAssigner(index, field));
		return this;
	}

	public InstantiatorBuilder<T> addProperty(int index, Method property) {
		properties.add(new PropertyAssigner(index, property));
		return this;
	}

	public static <T> InstantiatorBuilder<T> create(Class<T> type) {
		return new InstantiatorBuilder<>(type);
	}

	public Instantiator<T> build() {

		return new Instantiator<T>(type, constructor, constructorIndex, fields.toArray(new FieldAssigner[0]),
				properties.toArray(new PropertyAssigner[0]));
	}
}
