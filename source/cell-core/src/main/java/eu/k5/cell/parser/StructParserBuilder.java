package eu.k5.cell.parser;

import java.io.BufferedWriter;

public class StructParserBuilder<T> extends ParserBuilder<T, StructParserBuilder<T>> {

	private StructInstantiatorBuilder<T> struct;

	public StructParserBuilder(Class<T> type) {
		super(type);
		setBuilder(this);
	}

	protected void doInit() {

		struct = new StructInstantiatorBuilder<>(getType()).withBoard(getBoard());

		struct.doInit();
		BufferedWriter w;
		getBoard().getUnsatisfied();

		setInit(true);

	}

	@Override
	public StructParser<T> build() {
		if (!isInit()) {
			doInit();
		}

		Instantiator<T> instantiator = struct.create();

		return new StructParser<>(getBoard().build(), instantiator);
	}

}
