package eu.k5.cell.parser;

import java.util.Collections;

import eu.k5.cell.annotation.In;

public class StructParserBuilderExp {

	public static class Struct {

		private final String param;

		public Struct(@In("param") String param) {
			this.param = param;

		}

		public String getParam() {
			return param;
		}
	}

	public static void main(String[] args) {

		StructParserBuilder<Struct> builder = new StructParserBuilder<>(Struct.class);

		StructParser<Struct> struct = builder.build();

		Struct read = struct.parse(new VariableContext(Collections.singletonMap("param", "test")));

		System.out.println(read.getParam());
	}

}
