package eu.k5.cell;

import java.util.Map;

public interface CellTask {

	String getProcessInstanceId();

	String getId();

	String getTaskDefinitionKey();

	String getToken();

	Map<String, Object> getProcessVariables();

}
