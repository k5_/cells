package eu.k5.cell.entity;

import java.util.Map;

public interface WriteableEntity<T> {

	Map<String, Object> write(T entity);

}
