package eu.k5.cell.activiti;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.activiti.engine.task.Task;

import eu.k5.cell.CellTask;

public class ActivitiCellTask implements CellTask {

	private final Task task;

	public ActivitiCellTask(Task task) {
		this.task = task;
	}

	@Override
	public String getProcessInstanceId() {
		return task.getProcessInstanceId();
	}

	@Override
	public String getId() {
		return task.getId();
	}

	@Override
	public String getTaskDefinitionKey() {
		return task.getTaskDefinitionKey();
	}

	@Override
	public String getToken() {
		return (String) task.getTaskLocalVariables().get(TaskTokenCreator.TOKEN);
	}

	@Override
	public Map<String, Object> getProcessVariables() {
		return task.getProcessVariables();
	}

	public static Collection<CellTask> fromCollections(Collection<Task> tasks) {
		List<CellTask> result = new ArrayList<>();
		for (Task task : tasks) {
			result.add(new ActivitiCellTask(task));
		}
		return result;
	}

}
