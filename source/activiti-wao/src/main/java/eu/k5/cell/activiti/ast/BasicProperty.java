package eu.k5.cell.activiti.ast;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import eu.k5.cell.activiti.TypeHint;

public class BasicProperty extends Property {
	enum PropertyType {
		ASSIGNEE(TypeHint.STRING), TASK_DEFINITION_KEY(TypeHint.STRING), PROCESS_DEFINITION_KEY(TypeHint.STRING), PROCESS_INSTANCE_ID(
				TypeHint.STRING);

		private final TypeHint hint;

		private PropertyType(TypeHint hint) {
			this.hint = hint;
		}

		public TypeHint getHint() {
			return hint;
		}
	}

	private final PropertyType type;

	public static void main(String[] args) {
		ZonedDateTime zdt = ZonedDateTime.of(2010, 1, 1, 0, 0, 0, 0, ZoneId.of("UTC")).plusNanos(100l * 1000);
		System.out.println(zdt.toString());
	}

	public BasicProperty(PropertyType type) {
		this.type = type;
	}

	public PropertyType getType() {
		return type;
	}

	@Override
	public void accept(ValueVisitor visitor) {
		visitor.visit(this);

	}

	@Override
	public TypeHint getHint(Context context) {
		return type.getHint();
	}
}
